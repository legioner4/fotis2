@echo off

pushd %~dp0

if not exist venv (
  ECHO "INIT..."
  CALL python -m venv venv || GOTO :exit
)
CALL venv\Scripts\activate || GOTO :exit

pushd frontend
CALL yarn run dev:build
popd

CALL python project\manage.py collectstatic --noinput || GOTO :exit

:exit
popd
if "%errorlevel%" NEQ "0" exit /b %errorlevel%
