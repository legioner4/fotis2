module.exports = {
  printWidth: 120,
  tabWidth: 2,
  semi: false,
  singleQuote: true,
  trailingComma: 'all',
  bracketSpacing: false,
  jsxBracketSameLine: true,
  htmlWhitespaceSensitivity: 'ignore',
  endOfLine: 'auto',
  vueIndentScriptAndStyle: true,
}
