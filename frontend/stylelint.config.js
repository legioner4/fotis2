module.exports = {
  plugins: ['stylelint-scss', 'stylelint-prettier'],
  extends: ['stylelint-config-standard', 'stylelint-config-prettier'],
  rules: {
    'prettier/prettier': true,
    'no-empty-source': null,
    // чтобы stylelint не ругался на vue компоненты без тега style
    'no-descending-specificity': null,
    // порядок правил должен идти от простого к сложному
    'declaration-colon-newline-after': null,
    // не смотрим на многострочные свойства с новой строки
    'block-no-empty': true,
    // запрещаем пустые правила
    'rule-empty-line-before': null,
    // пустая строка перед селектором
    indentation: null,
    // не учитываем неправильный indent
    'selector-pseudo-element-colon-notation': 'double',
    // селекторы псевдоэлементов должны выглядеть как ::
    'font-family-no-missing-generic-family-keyword': true,
    // всегда должен быть указан базовый шрифт в font-family
  },
}
