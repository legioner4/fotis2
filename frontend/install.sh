#!/bin/sh
set -e

mkdir static -p
yarn install --no-progress --non-interactive
yarn cache clean
