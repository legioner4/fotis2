const path = require('path')
const webpack = require('webpack')
const {VueLoaderPlugin} = require('vue-loader')
const {CleanWebpackPlugin} = require('clean-webpack-plugin')
const TerserPlugin = require('terser-webpack-plugin')
const ESLintPlugin = require('eslint-webpack-plugin')
const StylelintPlugin = require('stylelint-webpack-plugin')

module.exports = (env, argv) => {
  const isProduction = argv.mode === 'production'
  const enableCache = !isProduction
  const config = {
    mode: argv.mode,
    entry: {
      'entries/public': path.resolve(__dirname, 'src/entries/public'),
      'entries/private': path.resolve(__dirname, 'src/entries/private'),
    },
    output: {
      path: path.resolve(__dirname, 'static'),
      publicPath: '/static/',
      filename: 'frontend/[name].js',
      chunkFilename: 'frontend/[id].[chunkhash:8].js',
    },
    optimization: {
      splitChunks: {
        cacheGroups: {
          common: {
            name: 'common',
            chunks(chunk) {
              return ['entries/public', 'entries/private'].indexOf(chunk.name) >= 0
            },
            minChunks: 2,
            filename: 'frontend/[name].js',
          },
        },
      },
    },
    module: {
      rules: [
        {
          test: /\.vue$/,
          loader: 'vue-loader',
          options: {
            compilerOptions: {whitespace: 'condense'},
            hotReload: !isProduction,
            productionMode: isProduction,
          },
        },
        {
          test: /\.s?css$/,
          use: [
            'vue-style-loader',
            ...(enableCache ? ['cache-loader'] : []),
            {loader: 'css-loader', options: {importLoaders: 1}},
            'postcss-loader',
          ],
        },
        {
          test: /\.pug$/,
          loader: 'pug-plain-loader',
        },
        {
          test: /\.js$/,
          loader: 'babel-loader',
          include: [path.resolve(__dirname, 'src'), path.resolve(__dirname, 'node_modules', 'vue-full-calendar')],
          options: {
            cacheDirectory: enableCache,
          },
        },
        {
          test: /\.(png|jpg|jpeg|gif|svg|woff|woff2|ttf|eot)$/,
          loader: 'file-loader',
          options: {
            name: 'frontend/files/[name].[hash:8].[ext]',
          },
        },
      ],
    },
    resolve: {
      modules: [path.resolve(__dirname, 'src'), path.resolve(__dirname, 'node_modules')],
      alias: {
        vue$: 'vue/dist/vue.esm.js',
      },
      extensions: ['.js', '.vue', '.css'],
    },
    performance: {
      hints: false,
    },

    plugins: [
      new webpack.DefinePlugin({
        'process.env': {
          WEBPACK_PUBLIC_PATH: "'/static/'",
        },
      }),
      new webpack.ContextReplacementPlugin(/moment[/\\]locale$/, /ru/),
      new webpack.ProvidePlugin({
        _: 'common/lodash',
      }),
      new VueLoaderPlugin(),
    ],
    stats: {
      children: false,
      assets: false,
      colors: true,
      timings: true,
      version: false,
      hash: false,
      chunks: true,
      chunkModules: false,
      modules: false,
    },
  }

  if (isProduction) {
    config.optimization.minimize = true
    config.optimization.minimizer = [
      new TerserPlugin({
        cache: false,
        parallel: true,
        sourceMap: false, // set to true if you want JS source maps
        extractComments: true,
      }),
    ]
    config.plugins = [
      ...(config.plugins || []),
      new webpack.LoaderOptionsPlugin({
        minimize: true,
      }),
    ]
  } else {
    let targetPort = '8000'

    process.argv.forEach(val => {
      if (val.split('=')[0] === '--targetPort') {
        targetPort = val.split('=')[1]
      }
    })

    config.devtool = '#eval-source-map'

    config.module.rules = [...(config.module.rules || [])]

    config.plugins = [
      new CleanWebpackPlugin({
        cleanAfterEveryBuildPatterns: ['!frontend/files/**/*'],
      }),
      ...(config.plugins || []),
      new ESLintPlugin({
        files: ['**/*.{vue,js}'],
        lintDirtyModulesOnly: true,
        fix: true,
      }),
      new StylelintPlugin({
        files: ['**/*.{vue,htm,html,css,sss,less,scss,sass}'],
        lintDirtyModulesOnly: true,
        fix: true,
      }),
    ]
    config.devServer = {
      host: '0.0.0.0',
      port: 3000,
      inline: true,
      hot: true,
      overlay: {
        warnings: false,
        errors: true,
      },
      proxy: [
        {
          context: () => true,
          target: `http://127.0.0.1:${targetPort}`,
        },
      ],
      stats: config.stats,
    }
    config.watchOptions = {
      aggregateTimeout: 600,
      ignored: /node_modules/,
    }
  }

  if (env && env.analize) {
    const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin

    config.plugins = [new BundleAnalyzerPlugin(), ...(config.plugins || [])]
  }
  return config
}
