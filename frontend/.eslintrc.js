module.exports = {
  root: true,
  env: {
    browser: true,
    es6: true,
  },
  extends: ['standard', 'plugin:vue/recommended', 'plugin:prettier/recommended', 'prettier/standard', 'prettier/vue'],
  globals: {
    _: true,
  },
  plugins: ['babel', 'prettier', 'standard', 'vue'],
  parserOptions: {
    parser: 'babel-eslint',
    sourceType: 'module',
  },
  rules: {
    'object-curly-spacing': 'off',
    // нужны пробелы между скобками и полями объекта

    'space-before-function-paren': 'off',
    // нужен пробел между названием функции и скобкой

    indent: 'off',
    // отступы нужных размеров

    'comma-dangle': 'off',
    // лишняя запятая

    curly: 'off',
    // обязательные скобки после if

    'space-before-blocks': 'off',
    // пробел перед блоками {}

    camelcase: 'off',
    // наименования должны быть в camelCase

    'no-unused-vars': 'warn',
    // неиспользованные переменные

    eqeqeq: 'warn',
    // всегда использовать === вместо ==

    'handle-callback-err': 'warn',
    // обрабатывать ошибку, если она приходит

    'new-cap': 'warn',
    // конструктор должен быть с большой буквы

    'prefer-promise-reject-errors': 'warn',
    // promise должен возвращать только Error объект

    'no-throw-literal': 'warn',
    // throw можно только Error объект

    'no-new': 'warn',
    // результат new должен быть куда-то записан

    'no-unused-expressions': 'off',
    // неиспользуемые выражения

    'no-undef': 'warn',
    // работа с еще необъявленной переменной

    'one-var': 'warn',
    // объявление по одной переменной

    'spaced-comment': 'warn',
    // пробел после комментария

    'no-self-compare': 'warn',
    //сравнение с самим собой

    'no-return-await': 'warn',
    // неправильная работа с async|await

    'no-return-assign': 'warn',
    // присваивание в return

    'no-unneeded-ternary': 'warn',
    // бессмысленные тернарные операторы

    'no-useless-return': 'warn',
    // бессмысленные return в конце блока

    'import/no-named-default': 'warn',
    // ненужный импорт default

    'no-debugger': 'warn',
    // breakpoints

    'babel/camelcase': 'off',
    'babel/no-unused-expressions': 'warn',

    'import/no-duplicates': 'off',
    // выключаем самую долгую проверку eslint

    'vue/require-default-prop': 'off',
    'vue/require-prop-types': 'off',
    'vue/this-in-template': 'off',
    'vue/no-v-html': 'off',

    'prettier/prettier': 'warn',
  },
}
