const config = {
  presets: [
    [
      '@babel/preset-env',
      {
        modules: false,
        targets: {
          browsers: ['IE 11'],
        },
        useBuiltIns: 'usage',
        corejs: 3,
      },
    ],
  ],
  plugins: ['@babel/plugin-syntax-dynamic-import', '@babel/plugin-proposal-optional-chaining'],
  ignore: [
    filename => {
      if (!/\/node_modules\//.test(filename)) {
        return false
      } else if (/\/node_modules\/vue-full-calendar\//.test(filename)) {
        return false
      }
    },
  ],
}

module.exports = config
