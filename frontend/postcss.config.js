module.exports = {
  plugins: [
    require('postcss-nested'),
    require('postcss-css-variables'),
    require('postcss-preset-env')({stage: false}),
    require('postcss-csso'),
  ],
}
