@echo off

CALL yarn upgrade-interactive || GOTO :exit
FOR /f "delims=" %%i IN ('yarn global bin') DO SET BIN=%%i
CALL %BIN%\syncyarnlock -ks || GOTO :exit
CALL yarn install || GOTO :exit

:exit
if "%errorlevel%" NEQ "0" exit /b %errorlevel%

