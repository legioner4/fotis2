import DashboardPage from './DashboardPage'

export default function(props) {
  return [
    {
      path: '/',
      name: 'dashboard',
      props: props,
      component: DashboardPage,
    },
    {
      path: '*',
      redirect: '/',
    },
  ]
}
