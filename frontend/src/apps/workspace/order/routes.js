import TransitionRouterView from 'components/TransitionRouterView'
import OrderList from './List'
import OrderPage from './Page'
import OrderView from './OrderView'
import OrderPhotoList from './photo/List'

export default function({apiPath}) {
  return [
    {
      path: '/',
      name: 'order-list',
      component: OrderList,
      props: {apiPath},
    },
    {
      path: '/:id',
      meta: {breadcrumb: 'Заказы', link: {name: 'order-list'}},
      props: route => {
        return {apiPath: `${apiPath}${route.params.id}/`, id: route.params.id}
      },
      component: OrderPage,
      redirect: r => ({name: 'order-view', params: r.params}),
      children: [
        {
          path: 'overview',
          component: TransitionRouterView,
          children: [
            {
              path: '',
              name: 'order-view',
              props: route => ({apiPath: `${apiPath}${route.params.id}/`, ...route.params}),
              component: OrderView,
            },
          ],
        },
        {
          path: 'photo-list',
          name: 'photo-list',
          props: route => {
            return {
              apiPath: `${apiPath}${route.params.id}/photo/`,
              downloadAllPhotoApiPath: `${apiPath}${route.params.id}/download/`,
              id: route.params.id,
            }
          },
          component: OrderPhotoList,
        },
      ],
    },
    {
      path: '/:id/*',
      redirect: r => ({name: 'order-view', params: r.params}),
    },
  ]
}
