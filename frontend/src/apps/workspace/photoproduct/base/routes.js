import CategoryTabs from './CategoryTabs'
import CategoryList from './CategoryList'

export default function() {
  return [
    {
      path: '/',
      component: CategoryTabs,
      redirect: r => ({name: 'photobook', params: r.params}),
      children: [
        {
          path: 'photobook',
          name: 'photobook',
          props: route => {
            return {categoryGroup: 'photobook'}
          },
          component: CategoryList,
        },
        {
          path: 'souvenir',
          name: 'souvenir',
          props: route => {
            return {categoryGroup: 'souvenir'}
          },
          component: CategoryList,
        },
        {
          path: 'interior',
          name: 'interior',
          props: route => {
            return {categoryGroup: 'interior'}
          },
          component: CategoryList,
        },
        {
          path: 'calendar',
          name: 'calendar',
          props: route => {
            return {categoryGroup: 'calendar'}
          },
          component: CategoryList,
        },
        {
          path: 'polygraphy',
          name: 'polygraphy',
          props: route => {
            return {categoryGroup: 'polygraphy'}
          },
          component: CategoryList,
        },
      ],
    },

    {
      path: '*',
      redirect: r => ({name: 'photobook', params: r.params}),
    },
  ]
}
