import TransitionRouterView from 'components/TransitionRouterView'
import CategoryPage from "./CategoryPage"
import CategoryView from "./CategoryView"
import CategoryEdit from "./CategoryEdit"
import FormatList from "./format/List"
import ProductList from "./product/List"

export default function({apiPath}) {
  return [
    {
      path: '/',
      component: CategoryPage,
      props: {apiPath},
      redirect: r => ({name: 'category-view', params: r.params}),
      children: [
         {
          path: 'overview',
          component: TransitionRouterView,
          children: [
            {
              path: '',
              name: 'category-view',
              props: route => ({apiPath: apiPath, ...route.params}),
              component: CategoryView,
            },
            {
              path: 'edit',
              name: 'category-edit',
              props: route => ({apiPath: apiPath, ...route.params}),
              component: CategoryEdit,
            },
          ],
        },
        {
          path: 'formats',
          name: 'formats',
          props: route => {
            return {apiPath: `${apiPath}formats/`}
          },
          component: FormatList,
        },
        {
          path: 'products',
          name: 'products',
          props: route => {
            return {apiPath: `${apiPath}products/`}
          },
          component: ProductList,
        },
      ],
    },

    {
      path: '*',
      redirect: r => ({name: 'category-view', params: r.params}),
    },
  ]
}
