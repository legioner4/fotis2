import List from './List'
import ManageDeleted from './ManageDeleted'

export default function({apiPath, title}) {
  return [
    {
      path: '',
      name: 'models-list',
      props: {apiPath},
      meta: {title},
      component: List,
    },
    {
      path: '/:id/deleted/',
      name: 'deleted-list',
      props: route => {
        return {
          apiPath: `${apiPath}${route.params.id}/deleted/`,
        }
      },
      meta: {breadcrumb: title, link: {name: 'models-list'}},
      component: ManageDeleted,
    },
    {
      path: '*',
      redirect: '/',
    },
  ]
}
