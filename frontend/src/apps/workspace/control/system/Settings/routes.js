import TransitionRouterView from 'components/TransitionRouterView'
import BaseEdit from './base/Edit'
import BaseView from './base/View'
import Page from './Page'

export default function({apiPath}) {
  return [
    {
      path: '/',
      name: 'page',
      props: {apiPath},
      component: Page,
      children: [
        {
          path: 'base',
          props: true,
          component: TransitionRouterView,
          children: [
            {
              path: '',
              name: 'base-view',
              props: {apiPath},
              component: BaseView,
            },
            {
              path: 'edit',
              name: 'base-edit',
              props: {apiPath},
              component: BaseEdit,
            },
          ],
        },
        {
          path: '*',
          redirect: 'base/',
        },
      ],
    },
  ]
}
