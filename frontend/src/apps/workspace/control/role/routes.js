import RoleList from './List'
import RolePage from './Page'

export default function({apiPath, title}) {
  return [
    {
      path: '/',
      component: RoleList,
      name: 'role-list',
      props: {apiPath, title},
    },
    {
      path: '/:id',
      name: 'role-page',
      props: route => {
        return {apiPath: `${apiPath}${route.params.id}/`, id: route.params.id}
      },
      meta: {breadcrumb: title, link: {name: 'role-list'}},
      component: RolePage,
    },
    {
      path: '*',
      redirect: '/',
    },
  ]
}
