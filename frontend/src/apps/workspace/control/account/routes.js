import TransitionRouterView from 'components/TransitionRouterView'
import AccountList from './AccountList'
import AccountPage from './AccountPage'
import AccountView from './AccountView'
import AccountEdit from './AccountEdit'

export default function({apiPath}) {
  return [
    {
      path: '/',
      name: 'account-list',
      component: AccountList,
      props: {apiPath},
    },
    {
      path: '/:id',
      meta: {breadcrumb: 'Реестр пользователей', link: {name: 'account-list'}},
      props: route => {
        return {apiPath: `${apiPath}${route.params.id}/`, id: route.params.id}
      },
      component: AccountPage,
      redirect: r => ({name: 'account-view', params: r.params}),
      children: [
        {
          path: 'overview',
          component: TransitionRouterView,
          children: [
            {
              path: '',
              name: 'account-view',
              props: route => ({apiPath: `${apiPath}${route.params.id}/`, ...route.params}),
              component: AccountView,
            },
            {
              path: 'edit',
              name: 'account-edit',
              props: route => ({apiPath: `${apiPath}${route.params.id}/`, ...route.params}),
              component: AccountEdit,
            },
          ],
        },
      ],
    },
    {
      path: '/:id/*',
      redirect: r => ({name: 'account-view', params: r.params}),
    },
  ]
}
