import TransitionRouterView from 'components/TransitionRouterView'
import OrderList from './List'

export default function({apiPath}) {
  return [
    {
      path: '/',
      name: 'order-list',
      component: OrderList,
      props: {apiPath},
    },
  ]
}
