import TransitionRouterView from 'components/TransitionRouterView'
import OrganizationList from './OrgList'
import OrganizationPage from './OrgPage'
import OrganizationView from './OrgView'
import OrganizationEdit from './OrgEdit'
import SubdivisionsList from './subdivisions/List'
import AccountsList from './accounts/List'
import PaymentsPage from './payments/Page'
import ConfigPage from './config/Page'
import FtpUsersList from './ftp/List'
import MobileConfigPage from './mobile/Page'

export default function({apiPath, smtpApiPath, mobileApiPath}) {
  return [
    {
      path: '/',
      name: 'organization-list',
      component: OrganizationList,
      props: {apiPath},
    },
    {
      path: '/:id',
      meta: {breadcrumb: 'Реестр организаций', link: {name: 'organization-list'}},
      props: route => {
        return {apiPath: `${apiPath}${route.params.id}/`, id: route.params.id}
      },
      component: OrganizationPage,
      redirect: r => ({name: 'organization-view', params: r.params}),
      children: [
        {
          path: 'overview',
          component: TransitionRouterView,
          children: [
            {
              path: '',
              name: 'organization-view',
              props: route => ({apiPath: `${apiPath}${route.params.id}/`, ...route.params}),
              component: OrganizationView,
            },
            {
              path: 'edit',
              name: 'organization-edit',
              props: route => ({apiPath: `${apiPath}${route.params.id}/`, ...route.params}),
              component: OrganizationEdit,
            },
          ],
        },
        {
          path: 'subdivisions',
          name: 'subdivisions',
          props: route => {
            return {apiPath: `${apiPath}${route.params.id}/subdivisions/`, id: route.params.id}
          },
          component: SubdivisionsList,
        },
        {
          path: 'accounts',
          name: 'accounts',
          props: route => {
            return {apiPath: `${apiPath}${route.params.id}/accounts/`, id: route.params.id}
          },
          component: AccountsList,
        },
        {
          path: 'payments',
          name: 'payments',
          props: route => {
            return {apiPath: `${apiPath}${route.params.id}/`, id: route.params.id}
          },
          component: PaymentsPage,
        },
        {
          path: 'ftp-users',
          name: 'ftp-users',
          props: route => {
            return {apiPath: `${apiPath}${route.params.id}/ftp-users/`, id: route.params.id}
          },
          component: FtpUsersList,
        },
        {
          path: 'config',
          name: 'config',
          props: route => {
            return {
              apiPath: `${apiPath}${route.params.id}/`,
              paymentTypeApiPath: `${apiPath}${route.params.id}/payment-type/`,
              smtpApiPath: `${smtpApiPath}${route.params.id}/`,
              id: route.params.id,
            }
          },
          component: ConfigPage,
        },
        {
          path: 'mobile-config',
          name: 'mobile-config',
          props: route => {
            return {
              apiPath: `${apiPath}${route.params.id}/`,
              mobileApiPath: `${mobileApiPath}${route.params.id}/`,
            }
          },
          component: MobileConfigPage,
        },
      ],
    },
    {
      path: '/:id/*',
      redirect: r => ({name: 'organization-view', params: r.params}),
    },
  ]
}
