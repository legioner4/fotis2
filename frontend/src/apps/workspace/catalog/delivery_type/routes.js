import TransitionRouterView from 'components/TransitionRouterView'
import DeliveryTypeList from './DeliveryTypeList'
import DeliveryTypePage from './DeliveryTypePage'
import DeliveryTypeView from './DeliveryTypeView'
import DeliveryTypeEdit from './DeliveryTypeEdit'
import DeliveryRegionList from './delivery_region/List'
import PickupList from './pickup/List'
import TransportCompanyPointList from './transport_company/List'

export default function({apiPath, organizationId}) {
  return [
    {
      path: '/',
      name: 'delivery-type-list',
      component: DeliveryTypeList,
      props: {apiPath},
    },
    {
      path: '/:id',
      meta: {breadcrumb: 'Типы доставки', link: {name: 'delivery-type-list'}},
      props: route => {
        return {apiPath: `${apiPath}${route.params.id}/`, id: route.params.id}
      },
      component: DeliveryTypePage,
      redirect: r => ({name: 'delivery-view', params: r.params}),
      children: [
        {
          path: 'overview',
          component: TransitionRouterView,
          children: [
            {
              path: '',
              name: 'delivery-view',
              props: route => ({apiPath: `${apiPath}${route.params.id}/`, ...route.params}),
              component: DeliveryTypeView,
            },
            {
              path: 'edit',
              name: 'delivery-edit',
              props: route => ({apiPath: `${apiPath}${route.params.id}/`, ...route.params}),
              component: DeliveryTypeEdit,
            },
          ],
        },
        {
          path: 'delivery-region',
          name: 'delivery-region',
          props: route => {
            return {apiPath: `${apiPath}${route.params.id}/delivery-region/`, id: route.params.id}
          },
          component: DeliveryRegionList,
        },
        {
          path: 'pickup',
          name: 'pickup',
          props: route => {
            return {
              apiPath: `/api/workspace/organization/organizations/${organizationId}/subdivisions/`,
              id: route.params.id,
            }
          },
          component: PickupList,
        },
        {
          path: 'transport-company-point',
          name: 'transport-company-point',
          props: route => {
            return {apiPath: `${apiPath}${route.params.id}/transport-company-point/`, id: route.params.id}
          },
          component: TransportCompanyPointList,
        },
      ],
    },
    {
      path: '/:id/*',
      redirect: r => ({name: 'delivery-view', params: r.params}),
    },
  ]
}
