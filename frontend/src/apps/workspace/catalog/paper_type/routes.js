import PaperTypeList from './List'

export default function ({apiPath}) {
  return [
    {
      path: '/',
      name: 'paper-type-list',
      component: PaperTypeList,
      props: {apiPath},
    },
    {
      path: '*',
      redirect: r => ({name: 'paper-type-list', params: r.params}),
    },
  ]
}
