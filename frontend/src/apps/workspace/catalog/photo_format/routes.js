import TransitionRouterView from 'components/TransitionRouterView'
import PhotoFormatList from './PhotoFormatList'
import PhotoFormatPage from './PhotoFormatPage'
import PhotoFormatView from './PhotoFormatView'
import PhotoFormatEdit from './PhotoFormatEdit'
import FormatPriceList from './format_price/List'

export default function({apiPath}) {
  return [
    {
      path: '/',
      name: 'photo-format-list',
      component: PhotoFormatList,
      props: {apiPath},
    },
    {
      path: '/:id',
      meta: {breadcrumb: 'Фото форматы', link: {name: 'photo-format-list'}},
      props: route => {
        return {apiPath: `${apiPath}${route.params.id}/`, id: route.params.id}
      },
      component: PhotoFormatPage,
      redirect: r => ({name: 'format-view', params: r.params}),
      children: [
        {
          path: 'overview',
          component: TransitionRouterView,
          children: [
            {
              path: '',
              name: 'format-view',
              props: route => ({apiPath: `${apiPath}${route.params.id}/`, ...route.params}),
              component: PhotoFormatView,
            },
            {
              path: 'edit',
              name: 'format-edit',
              props: route => ({apiPath: `${apiPath}${route.params.id}/`, ...route.params}),
              component: PhotoFormatEdit,
            },
          ],
        },
        {
          path: 'format-price',
          name: 'format-price',
          props: route => {
            return {apiPath: `${apiPath}${route.params.id}/format-price/`, id: route.params.id}
          },
          component: FormatPriceList,
        },
      ],
    },
    {
      path: '/:id/*',
      redirect: r => ({name: 'format-view', params: r.params}),
    },
  ]
}
