import TransitionRouterView from 'components/TransitionRouterView'
import FormatPricePage from './FormatPricePage'
import FormatPriceView from './FormatPriceView'
import FormatPriceEdit from './FormatPriceEdit'
import DiscountList from './discount/List'

export default function({apiPath}) {
  return [
    {
      path: '/',
      name: 'format_price-list',
      props: {apiPath},
      component: FormatPricePage,
      redirect: r => ({name: 'price-view', params: r.params}),
      children: [
        {
          path: 'overview',
          component: TransitionRouterView,
          children: [
            {
              path: '',
              name: 'price-view',
              props: route => ({apiPath: `${apiPath}`, ...route.params}),
              component: FormatPriceView,
            },
            {
              path: 'edit',
              name: 'price-edit',
              props: route => ({apiPath: `${apiPath}`, ...route.params}),
              component: FormatPriceEdit,
            },
          ],
        },
        {
          path: 'discount',
          name: 'discount',
          props: route => {
            return {apiPath: `${apiPath}discount/`}
          },
          component: DiscountList,
        },
      ],
    },
    {
      path: '/*',
      redirect: r => ({name: 'price-view', params: r.params}),
    },
  ]
}
