import PromoCodeList from './List'
import PromoCodePage from './Page'
import PromoCodeEdit from './Edit'
import ProductGroupEdit from './GroupEdit'

export default function({apiPath}) {
  return [
    {
      path: '/',
      name: 'promo-code-list',
      component: PromoCodeList,
      props: {apiPath},
    },
    {
      path: '/:id',
      meta: {breadcrumb: 'Промокоды', link: {name: 'promo-code-list'}},
      props: route => {
        return {apiPath: `${apiPath}${route.params.id}/`, id: route.params.id}
      },
      component: PromoCodePage,
      redirect: r => ({name: 'promo-code', params: r.params}),
      children: [
        {
          path: 'promo-code',
          name: 'promo-code',
          props: route => {
            return {apiPath: `${apiPath}${route.params.id}/`, id: route.params.id}
          },
          component: PromoCodeEdit,
        },
        {
          path: 'product-group',
          name: 'product-group',
          props: route => {
            return {apiPath: `${apiPath}${route.params.id}/`, id: route.params.id}
          },
          component: ProductGroupEdit,
        },
      ],
    },
  ]
}
