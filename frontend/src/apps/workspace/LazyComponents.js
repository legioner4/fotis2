export default {
  'apps/workspace/dashboard/routes': () =>
    import(/* webpackChunkName: 'workspace' */ 'apps/workspace/dashboard/routes'),

  'apps/workspace/control/system/Settings/routes': () =>
    import(/* webpackChunkName: 'system' */ 'apps/workspace/control/system/Settings/routes'),
  'apps/workspace/control/role/routes': () =>
    import(/* webpackChunkName: 'role' */ 'apps/workspace/control/role/routes'),
  'apps/workspace/control/account/routes': () =>
    import(/* webpackChunkName: 'account' */ 'apps/workspace/control/account/routes'),
  'apps/workspace/control/organization/routes': () =>
    import(/* webpackChunkName: 'organizationpage' */ 'apps/workspace/control/organization/routes'),
  'apps/workspace/control/order/routes': () =>
    import(/* webpackChunkName: 'order' */ 'apps/workspace/control/order/routes'),
  'apps/workspace/control/not_compl_order/routes': () =>
    import(/* webpackChunkName: 'order' */ 'apps/workspace/control/not_compl_order/routes'),

  'apps/workspace/organization/routes': () =>
    import(/* webpackChunkName: 'organizationpage' */ 'apps/workspace/organization/routes'),

  'apps/workspace/catalog/delivery_type/routes': () =>
    import(/* webpackChunkName: 'catalog' */ 'apps/workspace/catalog/delivery_type/routes'),
  'apps/workspace/catalog/paper_type/routes': () =>
    import(/* webpackChunkName: 'catalog' */ 'apps/workspace/catalog/paper_type/routes'),
  'apps/workspace/catalog/photo_format/routes': () =>
    import(/* webpackChunkName: 'catalog' */ 'apps/workspace/catalog/photo_format/routes'),
  'apps/workspace/catalog/photo_format/format_price/routes': () =>
    import(/* webpackChunkName: 'catalog' */ 'apps/workspace/catalog/photo_format/format_price/routes'),

  'apps/workspace/order/routes': () => import(/* webpackChunkName: 'order' */ 'apps/workspace/order/routes'),

  'apps/workspace/marketing/promo_code/routes': () =>
    import(/* webpackChunkName: 'promo_code' */ 'apps/workspace/marketing/promo_code/routes'),

  'apps/workspace/photoproduct/base/routes': () =>
    import(/* webpackChunkName: 'base_photoproduct' */ 'apps/workspace/photoproduct/base/routes'),
  'apps/workspace/photoproduct/base/detail/routes': () =>
    import(/* webpackChunkName: 'base_photoproduct' */ 'apps/workspace/photoproduct/base/detail/routes'),

  'apps/workspace/photoproduct/organization/routes': () =>
    import(/* webpackChunkName: 'organization_photoproduct' */ 'apps/workspace/photoproduct/organization/routes'),
  'apps/workspace/photoproduct/organization/detail/routes': () =>
    import(
      /* webpackChunkName: 'organization_photoproduct' */ 'apps/workspace/photoproduct/organization/detail/routes'
    ),
}
