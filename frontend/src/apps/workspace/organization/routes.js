import TransitionRouterView from 'components/TransitionRouterView'
import OrganizationPage from './OrganizationPage'
import OrganizationView from './OrganizationView'
import OrganizationEdit from './OrganizationEdit'
import SubdivisionsList from './subdivisions/List'
import AccountsList from './accounts/List'
import PaymentsPage from './payments/Page'
import ConfigPage from './config/Page'
import FtpUsersList from './ftp/List'
import MobileConfigPage from './mobile/Page'

export default function({apiPath, accountApiPath, smtpApiPath}) {
  return [
    {
      path: '/',
      props: {apiPath},
      component: OrganizationPage,
      redirect: r => ({name: 'organization-view', params: r.params}),
      children: [
        {
          path: 'overview',
          component: TransitionRouterView,
          children: [
            {
              path: '',
              name: 'organization-view',
              props: route => ({apiPath: apiPath, ...route.params}),
              component: OrganizationView,
            },
            {
              path: 'edit',
              name: 'organization-edit',
              props: route => ({apiPath: apiPath, ...route.params}),
              component: OrganizationEdit,
            },
          ],
        },
        {
          path: 'subdivisions',
          name: 'subdivisions',
          props: route => {
            return {apiPath: `${apiPath}subdivisions/`}
          },
          component: SubdivisionsList,
        },
        {
          path: 'accounts',
          name: 'accounts',
          props: route => {
            return {apiPath: `${accountApiPath}`}
          },
          component: AccountsList,
        },
        {
          path: 'payments',
          name: 'payments',
          props: route => {
            return {apiPath: apiPath}
          },
          component: PaymentsPage,
        },
        {
          path: 'ftp-users',
          name: 'ftp-users',
          props: route => {
            return {apiPath: `${apiPath}ftp-users/`}
          },
          component: FtpUsersList,
        },
        {
          path: 'config',
          name: 'config',
          props: route => {
            return {
              apiPath: apiPath,
              paymentTypeApiPath: `${apiPath}payment-type/`,
              smtpApiPath: smtpApiPath,
            }
          },
          component: ConfigPage,
        },
        {
          path: 'mobile-config',
          name: 'mobile-config',
          props: route => {
            return {
              apiPath: apiPath,
            }
          },
          component: MobileConfigPage,
        },
      ],
    },

    {
      path: '*',
      redirect: r => ({name: 'organization-view', params: r.params}),
    },
  ]
}
