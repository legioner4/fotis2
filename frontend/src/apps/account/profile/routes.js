import AccountPage from './AccountPage'
import AccountView from './AccountView'
import AccountEdit from './AccountEdit'
import AccountChangePasswd from './AccountChangePasswd'

export default function({apiPath, id}) {
  return [
    {
      path: '/',
      props: {apiPath, id},
      component: AccountPage,
      children: [
        {
          path: '',
          name: 'account-view',
          props: {apiPath, id},
          component: AccountView,
        },
        {
          path: 'edit',
          name: 'account-edit',
          props: {apiPath, id},
          component: AccountEdit,
        },
        {
          path: 'change-passwd',
          name: 'change_passwd',
          props: route => {
            return {apiPath: `${apiPath}change_passwd/`}
          },
          component: AccountChangePasswd,
        },
      ],
    },
    {
      path: '*',
      redirect: {name: 'account-view'},
    },
  ]
}
