import SberPaymentPage from './SberPaymentPage'

export default function({order_number, state}) {
  return [
    {
      path: '',
      name: 'index',
      component: SberPaymentPage,
      props: {
        orderNumber: order_number,
        state: state,
      },
    },
  ]
}
