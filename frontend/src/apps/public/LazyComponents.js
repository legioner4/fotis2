export default {
  'apps/public/auth/routes': () => import(/* webpackChunkName: 'auth' */ 'apps/public/auth/routes'),
  'apps/public/payment/routes': () => import(/* webpackChunkName: 'payment' */ 'apps/public/payment/routes'),
}
