import AuthIndex from './AuthIndex'
import Login from './Login'
import PasswordReset from './PasswordReset'
import PasswordResetConfirm from './PasswordResetConfirm'

export default function() {
  return [
    {
      path: '/',
      name: 'index',
      component: AuthIndex,
      children: [
        {
          path: '',
          component: Login,
          name: 'login',
          meta: {title: 'Вход'},
        },
        {
          path: 'password/reset/',
          component: PasswordReset,
          name: 'reset',
          meta: {title: 'Восстановление пароля'},
        },
        {
          path: 'password/reset/confirm/:uid/:token/',
          component: PasswordResetConfirm,
          name: 'reset_confirm',
          props: true,
          meta: {title: 'Восстановление пароля'},
        },
      ],
    },
    {
      path: '*',
      redirect: r => ({name: 'login', params: r.params}),
    },
  ]
}
