export default {
  addProjectData({commit}, data) {
    commit('ADD_PROJECT_DATA', data)
  },
}
