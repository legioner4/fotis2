import axios from 'common/axios'

const CODES = []

const loadHelpTips = _.throttle(commit => {}, 1000, {leading: false})

const getHelpTip = ({commit}, code) => {
  CODES.push(code)
  loadHelpTips(commit)
}

const saveHelpTip = ({state, commit}, data) => {
  const url = `/api/workspace/help/tips/${data.code}/`
  if (data.content || data.link) {
    // сохраняем изменения в общей справке
    axios.put(url, data).then(response => {
      commit('HELP_TIP_UPDATE', {code: response.data.code, data: response.data})

      if (response.data.link) {
        _.each(state.helpTips, (i, code) => {
          if (i.link === response.data.link || code === response.data.link) {
            commit('HELP_TIP_UPDATE', {code, data: {content: response.data.content}})
          }
        })
      }
    })
  }
}

const deleteHelpTip = ({commit}, code) => {
  const url = `/api/workspace/help/tips/${code}/`
  commit('HELP_TIP_DELETE', {code})
  axios.delete(url)
}

export default {
  getHelpTip,
  saveHelpTip,
  deleteHelpTip,
}
