const getHelpTipByCode = state => id => state.helpTips[id] || null

export default {
  getHelpTipByCode,
}
