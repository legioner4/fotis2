import Vue from 'common/vue'

const HELP_TIP_UPDATE = (state, {code, data}) => {
  Vue.set(state.helpTips, code, {...state.helpTips[code], ...data})
}

const HELP_TIP_DELETE = (state, {code}) => {
  Vue.delete(state.helpTips, code)
}

export default {
  HELP_TIP_UPDATE,
  HELP_TIP_DELETE,
}
