import Vue from 'vue'
import Vuex from 'vuex'
import project from './modules/project'
import help from './modules/help'


Vue.use(Vuex)

export default new Vuex.Store({
  strict: process.env.NODE_ENV !== 'production',
  modules: {
    project,
    help
  },
})
