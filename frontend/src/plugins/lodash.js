import * as lodash from 'common/lodash'

export default {
  install(Vue, options) {
    Vue.prototype.$_ = lodash
  },
}
