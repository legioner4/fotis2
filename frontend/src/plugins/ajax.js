import axios from 'common/axios'

export default {
  install(Vue, options) {
    Vue.prototype.$ajax = axios

    Vue.prototype.$delay = ms => new Promise(resolve => setTimeout(resolve, ms))
  },
}
