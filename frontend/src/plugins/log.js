export default {
  install(Vue, options) {
    Vue.prototype.$logError = (error, context) => {
      if (window.RAVEN_CONFIG_PUBLIC_DSN) {
        const Raven = require('raven-js')
        Raven.captureException(error, {
          extra: {...context, error},
        })
      }
      window.errorHandler?.addErrorInfo(error.message, error.stack)
      window.console?.error(error)
    }
  },
}
