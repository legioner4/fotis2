import AppNotify from 'components/AppNotify'

export default {
  install(Vue, options) {
    Vue.prototype.$notify = function(message, options = {}) {
      const parent = this.$root
      const ComponentClass = Vue.extend(AppNotify)
      if (this.$_.isArray(message)) {
        message = message.join('<br>')
      }
      const propsData = {...options, message}
      const instance = new ComponentClass({propsData, parent})
      instance.$mount()
      const container = parent.$root.$refs['modal-container']
      container.appendChild(instance.$el)
      instance.$on('close', () => {
        setTimeout(() => {
          instance.$destroy()
        }, 1000)
      })
    }
    Vue.prototype.$notifyError = function(message, error, options = {}) {
      if (error?.message === 'canceled') {
        return
      }
      let errorMessage = error
      if (error?.detail) {
        errorMessage = error.detail
      }

      if (error?.response) {
        const response = error.response
        errorMessage = response.data?.detail || response.data?.__all__ || response.data
        if (this.$_.isArray(errorMessage)) {
          errorMessage = errorMessage.join('<br>')
        }
        if (!errorMessage || errorMessage.length > 2000) errorMessage = response.status + ' ' + response.statusText
      }
      if (this.$_.isObject(errorMessage)) {
        errorMessage = JSON.stringify(errorMessage)
      }
      errorMessage = errorMessage ? ':<br>' + errorMessage : ''
      Vue.prototype.$notify.call(this, message + errorMessage, {level: 'error', timeout: 10000, ...options})
    }
  },
}
