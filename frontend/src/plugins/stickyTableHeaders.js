import jQuery from 'jquery'
import {throttle} from 'common/lodash'

export default {
  install(Vue, options) {
    Vue.directive('sticky-table-headers', {
      inserted(el, binding, vnode) {
        window.jQuery = jQuery
        require('sticky-table-headers')

        const table = jQuery(el)
        const vScrollContainer = table.parents('.v-table__overflow').first()
        const header = table.find('thead')

        const setHeaderClip = () => {
          // чтобы не вылезало за пределны таблицы делаем заголовку clip
          const left = vScrollContainer[0].scrollLeft
          const width = vScrollContainer.width()
          header[0].style.clip = `rect(0, ${width + left + 1}px, 1000px, ${left + 1}px)`
        }

        setHeaderClip()
        jQuery(window).on(
          `resize.sticky-table-headers-${vnode.context._uid}`,
          throttle(e => {
            jQuery(window).trigger('resize.stickyTableHeaders')
            setHeaderClip()
          }, 300),
        )

        table.stickyTableHeaders({cacheHeaderHeight: true})
        vScrollContainer.on(
          `scroll.sticky-table-headers-${vnode.context._uid}`,
          throttle(e => {
            jQuery(window).trigger('resize.stickyTableHeaders')
            setHeaderClip()
          }, 30),
        )
        vnode.context.$watch(
          () => vnode.context.$composer?.columnsConfig,
          () => {
            table.stickyTableHeaders('destroy')
            table.stickyTableHeaders()
          },
        )
      },
      unbind(el, binding, vnode) {
        jQuery(window).off(`resize.sticky-table-headers-${vnode.context._uid}`)
        const table = jQuery(el)
        const vScrollContainer = table.parents('.v-table__overflow').first()
        table.stickyTableHeaders('destroy')
        vScrollContainer.off(`scroll.sticky-table-headers-${vnode.context._uid}`)
      },
    })
  },
}
