import AsyncModalConfirmation from 'components/modal/AsyncModalConfirmation'
import AsyncModalForm from 'components/modal/AsyncModalForm'
import DeleteConfirm from 'components/modal/DeleteConfirm'

const _close = function(vm) {
  vm.$destroy()
  vm.$el.parentNode.removeChild(vm.$el)
}

function _open(parent, Vue, component, propsData) {
  if (parent.__hasOpenModal) {
    console.error('Preventing the opening of multiple modals')
    return Promise.reject(new Error('Нельзя открыть несколько модальных окон на одном уровне'))
  }
  parent.__hasOpenModal = true
  const modalContainer = parent.$root.$refs['modal-container']
  const div = document.createElement('div')
  modalContainer.appendChild(div)

  if (propsData.component) {
    component = propsData.component
  }
  const Ctor = Vue.extend(component)
  const vm = new Ctor({propsData, parent})
  vm.$mount(div)
  vm.open()

  return new Promise((resolve, reject) => {
    vm.$on('submitted', result => resolve(result))
    vm.$on('hidden', () => {
      _close(vm)
      parent.__hasOpenModal = false
      // reject сработает только если не был вызван resolve
      reject(new Error('canceled'))
    })
  })
}

export default {
  install(Vue, options) {
    Vue.prototype.$modal = function(component, propsData) {
      return _open(this, Vue, component, propsData)
    }
    Vue.prototype.$confirm = function(propsData) {
      return _open(this, Vue, AsyncModalConfirmation, propsData)
    }
    Vue.prototype.$modalForm = function(propsData) {
      return _open(this, Vue, AsyncModalForm, propsData)
    }
    Vue.prototype.$deleteConfirm = function(propsData) {
      return _open(this, Vue, DeleteConfirm, propsData)
    }
  },
}
