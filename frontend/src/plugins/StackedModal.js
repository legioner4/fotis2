const stackAsideModals = []
const MAX_VISIBLE_ASIDE = 5

function stackModals() {
  const count = stackAsideModals.length
  stackAsideModals.forEach(function(el, i) {
    const n = Math.max(MAX_VISIBLE_ASIDE - count + i + 1, 1)
    el.parentNode.classList.remove('stacked-modal-1')
    el.parentNode.classList.remove('stacked-modal-2')
    el.parentNode.classList.remove('stacked-modal-3')
    el.parentNode.classList.remove('stacked-modal-4')
    el.parentNode.classList.remove('stacked-modal-5')
    el.parentNode.classList.add('stacked-modal-' + n)
    if (n === MAX_VISIBLE_ASIDE && count > 1) {
      el.parentNode.classList.add('stacked-modal-has-next')
    }
  })
  document.body.classList.remove('modal-open')
}

function onShow(e) {
  const currentModal = e.target
  stackAsideModals.push(currentModal)
  stackModals()
}

function onHide(e) {
  if (!stackAsideModals.length) {
    return
  }
  const top = stackAsideModals.pop()
  if (e.target !== top) {
    e.preventDefault()
    stackAsideModals.push(top)
    document.body.classList.remove('modal-open')
  } else {
    top.parentNode.classList.remove('stacked-modal-1')
    top.parentNode.classList.remove('stacked-modal-2')
    top.parentNode.classList.remove('stacked-modal-3')
    top.parentNode.classList.remove('stacked-modal-4')
    top.parentNode.classList.remove('stacked-modal-5')
    top.parentNode.classList.remove('stacked-modal-has-next')
    if (stackAsideModals.length) {
      stackModals()
      document.body.classList.add('modal-open')
    }
  }
}

function mount(root) {
  root.$on('bv::modal::show', onShow)
  root.$on('bv::modal::hide', onHide)
}

function umount(root) {
  root.$off('bv::modal::show', onShow)
  root.$off('bv::modal::hide', onHide)
}

const StackedModal = {
  install(Vue, options) {
    Vue.mixin({
      created: function() {
        if (this.$root !== this) {
          return
        }
        mount(this.$root)
      },
      beforeDestroy: function() {
        if (this.$root !== this) {
          return
        }
        umount(this.$root)
      },
    })
  },
}

export default StackedModal
