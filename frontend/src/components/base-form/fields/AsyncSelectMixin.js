export default {
  data() {
    return {
      loading: false,
      items: [],
      dependOn: this.field.dependOn,
      asyncField: this.field.asyncField,
      asyncUrl: this.field.asyncUrl,
      extraFilters: this.field.extraFilters || {},
    }
  },
  computed: {
    isDepends() {
      return this.dependOn && this.model && Object.prototype.hasOwnProperty.call(this.model, this.dependOn)
    },
    dependOnValue() {
      if (this.isDepends) {
        const value = this.model[this.dependOn]
        if (_.isObject(value) && value.pk) {
          return value.pk
        }
        return value
      }
    },
  },

  watch: {
    dependOnValue(val) {
      if (this.isDepends) {
        this.loadItems()
        if (_.isArray(this.model[this.field.name])) {
          this.$set(this.model, this.field.name, [])
        } else {
          this.$set(this.model, this.field.name, null)
        }
      }
    },
  },
  mounted() {
    if (this.field.asyncUrl) {
      this.loadItems()
    } else {
      this.$watch(
        () => this.field.choices,
        () => {
          this.items = _.map(this.field.choices, choice => {
            return {value: choice[0], text: choice[1]}
          })
        },
        {immediate: true},
      )
    }
  },
  methods: {
    getUrl() {
      let url = this.asyncUrl
      if (_.isFunction(this.asyncUrl)) {
        url = this.asyncUrl(this.model)
      }
      return url
    },
    getParams() {
      const params = {}
      let filter = this.extraFilters
      if (_.isFunction(this.extraFilters)) {
        filter = this.extraFilters(this.model)
      }
      if (this.dependOn && this.dependOnValue) {
        filter[this.dependOn] = this.dependOnValue
      }
      if (filter) params.filter = JSON.stringify(filter)
      params.fields = this.asyncField
      return params
    },

    getItems(data) {
      return _.map(data, i => {
        return {value: i.pk, text: i[this.asyncField]}
      })
    },

    async loadItems() {
      if (this.dependOn && !this.dependOnValue) {
        this.items = []
        return
      }
      this.loading = true
      const url = this.getUrl()
      if (!url) {
        this.items = []
        return
      }
      const params = this.getParams()

      try {
        const resp = await this.$ajax(url, {params})
        this.items = this.getItems(resp.data.data || resp.data)
      } catch (err) {}
      this.loading = false
    },
  },
}
