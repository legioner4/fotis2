export default {
  inject: {
    $validator: '$validator',
    $form: '$form',
  },
  props: {
    field: {
      type: Object,
      required: true,
    },
    model: {
      type: Object,
      required: true,
    },
    size: {
      type: String,
      required: false,
      default: '',
    },
    prefix: {
      type: String,
      required: false,
      default: '',
    },
  },
  computed: {
    name() {
      return this.prefix + this.field.name
    },
    id() {
      return this.name + '_id'
    },
  },
}
