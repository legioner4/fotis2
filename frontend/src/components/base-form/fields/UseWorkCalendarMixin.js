export default {
  computed: {
    useWorkCalendar() {
      const fieldPropValue = this.field.useWorkCalendar
      if (_.isString(fieldPropValue)) {
        return this.model[fieldPropValue]
      } else {
        return fieldPropValue
      }
    },
  },
}
