import noun from 'plural-ru'
import GroupDeleteConfirm from './GroupDeleteConfirm'

const GroupDelete = async function ({action, count}) {
  const result = await this.$confirm({
    helpId: 'GroupDeleteConfirm',
    helpTitle: 'Подтверждение группового удаления',
    title: `Применить действие "${action.title}"`,
    confirmationText: `Вы действительно хотите удалить ${count} ${noun(count, 'элемент', 'элемента', 'элементов')}?`,
    btnText: 'Удалить',
    component: GroupDeleteConfirm,
    selected: this.selected,
    apiPath: `${this.apiPath}selected-${action.name}/`,
    afterDelete: this.load
  })
  if (result)
    return result
  throw Error()
}
export {GroupDelete}
