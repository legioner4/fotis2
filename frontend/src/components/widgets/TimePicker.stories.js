import TimePicker from './TimePicker'

export default {
  title: 'Time Picker',
  component: TimePicker,
}

export const TimePickerStory = () => ({
  components: {TimePicker},
  template: `<TimePicker label="Введите время"/>`,
})
