import FiasAddress from './FiasAddress'

export default {
  title: 'Fias Address',
  component: FiasAddress,
}

export const FiasAddressSimple = () => ({
  components: {FiasAddress},
  template: `
    <div>
      <v-text-field v-model="apiRoot" label="ФИАС АПИ УРЛ"/>
      <v-text-field v-model="apiKey" label="ФИАС АПИ Токен"/>
      <FiasAddress ref="input" v-model="address" label="Введите адрес" :api-root="apiRoot" :api-key="apiKey"/>
      <div v-if="address">
        <div> Код региона <strong>{{ address.regioncode }}</strong></div>
        <div> Индекс <strong>{{ address.postalcode }}</strong></div>
        <div> Регион <strong>{{ address.region }}</strong></div>
        <div> Район <strong>{{ address.area }}</strong></div>
        <div> Городское/сельское поселений <strong>{{ address.settlement }}</strong></div>
        <div> Город <strong>{{ address.city }}</strong></div>
        <div> Населенный пункт <strong>{{ address.locality }}</strong></div>
        <div> Планировочная структура <strong>{{ address.plan_struct }}</strong></div>
        <div> Улица <strong>{{ address.street }}</strong></div>
        <div> Дом <strong>{{ address.house }}</strong></div>
        <div> Помещение <strong>{{ address.room }}</strong></div>
        <div> Дополнение <strong>{{ address.rest }}</strong></div>
        <div> ОКАТО <strong>{{ address.okato }}</strong></div>
        <div> ОКТМО <strong>{{ address.oktmo }}</strong></div>
        <div> ИФНС <strong>{{ address.ifns }}</strong></div>
        <div> КОД ФИАС <strong>{{ address.guid }}</strong></div>
        <div> Код кладр <strong>{{ address.kladr }}</strong></div>
        <div> Кадастровый номер <strong>{{ address.cadnum }}</strong></div>
      </div>
    </div>`,
  data() {
    return {
      apiRoot: 'http://127.0.0.1:9000/',
      apiKey: 'TEST_API_KEY',
      address: {text: 'Чувашия Чебоксары Максима Горького 18'},
    }
  },
  watch: {
    address: val => {
      console.log(val)
    },
  },
})
