const RenderSlot = (slot) => {
  return {
    functional: true,
    render: function (h, c) {
      return slot(c.data.attrs)
    }
  }
}

export default RenderSlot
