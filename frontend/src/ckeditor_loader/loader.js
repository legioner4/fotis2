window.CKEDITOR_BASEPATH = `${process.env.WEBPACK_PUBLIC_PATH}node_modules/ckeditor/`

// Load your custom config.js file for CKEditor.
require(`!file-loader?context=${__dirname}&outputPath=node_modules/ckeditor/&name=[path][name].[ext]!./config.js`)

// Load your custom contents.css file in case you use iframe editor.
require(`!file-loader?context=${__dirname}&outputPath=node_modules/ckeditor/&name=[path][name].[ext]!./contents.css`)

// Load your custom styles.js file for CKEditor.
require(`!file-loader?context=${__dirname}&outputPath=node_modules/ckeditor/&name=[path][name].[ext]!./styles.js`)

// Load files from plugins, excluding lang files.
// Limit to active plugins with
// Object.keys(CKEDITOR.plugins.registered).sort().toString().replace(/,/g, '|')

require.context(
  '!file-loader?name=[path][name].[ext]!ckeditor/plugins/',
  true,
  /^\.\/((panelbutton|colorbutton|a11yhelp|about|basicstyles|blockquote|button|clipboard|contextmenu|dialog|dialogui|elementspath|enterkey|entities|fakeobjects|filebrowser|filetools|floatingspace|floatpanel|format|horizontalrule|htmlwriter|image|indent|indentlist|lineutils|link|list|listblock|magicline|maximize|menu|menubutton|notification|notificationaggregator|panel|pastefromword|pastetext|popup|removeformat|resize|richcombo|scayt|showborders|sourcearea|specialchar|stylescombo|tab|table|tableselection|tabletools|toolbar|undo|uploadimage|uploadwidget|widget|widgetselection|wsc|wysiwygarea)(\/(?!lang\/)[^/]+)*)?[^/]*$/,
)

// Load lang files from plugins.
// Limit to active plugins with
// Object.keys(CKEDITOR.plugins.registered).sort().toString().replace(/,/g, '|')
require.context(
  '!file-loader?name=[path][name].[ext]!ckeditor/plugins/',
  true,
  /^\.\/(panelbutton|colorbutton|a11yhelp|about|basicstyles|blockquote|button|clipboard|contextmenu|dialog|dialogui|elementspath|enterkey|entities|fakeobjects|filebrowser|filetools|floatingspace|floatpanel|format|horizontalrule|htmlwriter|image|indent|indentlist|lineutils|link|list|listblock|magicline|maximize|menu|menubutton|notification|notificationaggregator|panel|pastefromword|pastetext|popup|removeformat|resize|richcombo|scayt|showborders|sourcearea|specialchar|stylescombo|tab|table|tableselection|tabletools|toolbar|undo|uploadimage|uploadwidget|widget|widgetselection|wsc|wysiwygarea)\/(.*\/)*lang\/(en|ru)\.js$/,
)

// Load CKEditor lang files.
require.context('!file-loader?name=[path][name].[ext]!ckeditor/lang', true, /(en|ru)\.js/)

// Load skin.
require.context('!file-loader?name=[path][name].[ext]!ckeditor/skins/moono-lisa', true, /.*/)
