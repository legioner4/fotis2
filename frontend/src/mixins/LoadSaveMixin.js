import LoadMixin from './LoadMixin'
import SaveMixin from './SaveMixin'

export default {
  mixins: [LoadMixin, SaveMixin],
  computed: {
    inAction() {
      return this.isLoading || this.isSaving
    },
  },
}
