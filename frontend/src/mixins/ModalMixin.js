import HelpComponentMixin from 'mixins/help/HelpComponentMixin'

export default {
  mixins: [HelpComponentMixin],
  provide() {
    return {
      $modal: this,
    }
  },
  props: {
    helpIsLeafOnly: {
      type: Boolean,
      default: true,
    },
  },
  data() {
    return {
      show: false,
    }
  },
  methods: {
    open() {
      this.show = true
    },
    closeModal() {
      this.show = false
    },
    onHidden() {
      this.$emit('hidden')
    },
  },
}
