export default {
  props: {
    // Идентификатор компонента в справочной системе

    helpTipDisable: {
      type: Boolean,
      default: false,
    },
    helpId: {
      type: String,
      default() {
        return this.$options.propsData.id || null
      },
    },
    // Заголовок компонента в справочной системе
    helpTitle: {
      type: String,
      default() {
        return this.$options.propsData.title || null
      },
    },
    helpIsLeafOnly: Boolean,
  },
}
