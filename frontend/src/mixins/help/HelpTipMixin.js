import Vue from 'common/vue'
import HelpTip from 'components/help/HelpTip'
import {getZIndex} from 'vuetify/es5/util/helpers'

export default {
  inject: {
    $form: {default: null},
  },

  methods: {
    getHelpTipCode() {
      if (!this.$form || this.$form.helpTipDisable) {
        return
      }
      let formId = this.$form.id || this.$form.$parent.$options.name
      let fieldId = this.id || this.name || this.field.name
      if (!(formId && fieldId)) {
        return
      }
      formId = _.camelCase(formId)
      fieldId = _.camelCase(fieldId)

      return [formId, fieldId].join('-')
    },
  },
  mounted() {
    this._helpTip = null
    if (!this.$el?.querySelector) {
      return
    }
    const code = this.getHelpTipCode()
    if (!code) {
      return
    }
    const label = this.$el.querySelector('label')
    if (!label) {
      return
    }
    const title = label.innerText
    label.innerText = ''
    label.style.zIndex = 'auto'
    const span = document.createElement('span')
    label.appendChild(span)
    const Ctor = Vue.extend(HelpTip)
    this._helpTip = new Ctor({propsData: {title, code}, parent: this})
    this._helpTip.$mount(span)

    setTimeout(() => {
      label.style.zIndex = +getZIndex(this.$el.parentNode) + 1
    }, 1000)
  },
  destroy() {
    if (this._helpTip) this._helpTip.$destroy()
  },
}
