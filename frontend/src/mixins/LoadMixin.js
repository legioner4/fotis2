export default {
  props: {
    loadingAlertDefault: {
      type: String,
      required: false,
      default: 'Во время загрузки произошла ошибка. Повторите попытку позже.',
    },
    loadingAlert503: {
      type: String,
      required: false,
      default: 'Сервис временно недоступен. Ведутся технические работы. Повторите попытку позже.',
    },
    loadingAlert403: {
      type: String,
      required: false,
      default: 'У вас нет прав для выполнения этой операции',
    },
    loadingAlert404: {
      type: String,
      required: false,
      default: 'Ресурс не найден',
    },
  },
  data: function() {
    return {
      isLoading: false,
      isLoaded: false,
      loadingAlert: null,
      loadingAlertType: 'error',
    }
  },
  methods: {
    load() {
      this.loaded()
    },
    beforeLoad() {
      this.isLoading = true
    },
    loaded(data) {
      this.isLoaded = true
      this.loadingAlert = null
      this.isLoading = false
      this.$emit('loaded', this, data)
    },

    loadingDecorator(promise) {
      this.beforeLoad()
      if (_.isFunction(promise)) {
        promise = promise()
      }
      promise
        .then(() => {
          this.loaded()
        })
        .catch(error => {
          this.errorOnLoad(error)
        })
      return promise
    },

    errorOnLoad(error) {
      if (error) {
        this.$logError(error)
      }
      const response = error.response
      if (!response) {
        this.loadingAlert = 'Отсутствует соединение с сервером. Повторите попытку позже.'
      } else if (response.status === 400 && response.data) {
        this.loadingAlert = response.data
      } else if (response.status === 403) {
        this.loadingAlert = this.loadingAlert403
        this.loadingAlertType = 'warning'
      } else if (response.status === 404) {
        this.loadingAlert = this.loadingAlert404
        this.loadingAlertType = 'warning'
      } else if ([502, 503, 504].indexOf(response.status) >= 0) {
        this.loadingAlert = this.loadingAlert503
      } else {
        this.loadingAlert = this.loadingAlertDefault
      }
      if (process.env.NODE_ENV !== 'production' && response) {
        this.loadingAlert += ` (${response.status} ${response.statusText})`
      }
      this.isLoading = false
      this.$emit('error', this, error)
    },
  },
  computed: {
    inAction() {
      return this.isLoading
    },
  },

  created() {
    this.load()
  },
}
