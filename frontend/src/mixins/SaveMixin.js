export default {
  data: function() {
    return {
      isSaving: false,
      isSaved: false,
    }
  },
  methods: {
    beforeSave() {
      if (this.isSaving) {
        throw new Error('Double saving!')
      }
      this.isSaving = true
    },

    saved() {
      this.isSaving = false
      this.isSaved = true
      this.$emit('saved', this)
    },

    errorOnSave(error) {
      if (error) {
        this.$logError(error)
      }
      this.isSaving = false
      this.$emit('error', this)
    },
  },
  computed: {
    inAction() {
      return this.isSaving
    },
  },
}
