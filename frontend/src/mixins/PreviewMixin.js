import AsyncModalPreview from 'components/modal/AsyncModalPreview'

export default {
  data() {
    return {
      previewComponent: null,
      previewPath: null,
      previewPagePath: null,
    }
  },

  methods: {
    onPreview() {
      if (this.previewComponent && this.previewPath) {
        const promise = this.$modal(AsyncModalPreview, {
          helpId: 'previewModal',
          helpTitle: 'Модальное окно предпросмотра',
          previewComponent: this.previewComponent,
          previewPath: this.previewPath,
          previewPagePath: this.previewPagePath,
        })
        promise.then(
          () => null,
          () => null,
        )
      }
    },
  },
}
