import 'style/style.css'
import Vue from 'common/vue'
import App from 'common/app/App'
import router from 'common/app/router'
import store from 'store'

import AppLoader from 'components/AppLoader'
import BaseForm from 'components/base-form/BaseForm'
import LoaderOverlay from 'components/LoaderOverlay'
import LoadingAlert from 'components/LoadingAlert'
import PublicAuth from 'common/layout/PublicAuth'

Vue.addComponent(AppLoader)
Vue.addComponent(BaseForm)
Vue.addComponent(LoaderOverlay)
Vue.addComponent(LoadingAlert)
Vue.addComponent(PublicAuth)

export default new Vue({
  el: '#app',
  router,
  store,
  components: {App},
})
