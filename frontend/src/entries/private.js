import 'style/style.css'
import Vue from 'common/vue'
import App from 'common/app/App'
import router from 'common/app/router'
import store from 'store'

import AppCard from 'components/AppCard'
import AppLoader from 'components/AppLoader'
import AppModal from 'components/AppModal'
import AppPagination from 'components/AppPagination'
import AppPre from 'components/AppPre'
import BaseForm from 'components/base-form/BaseForm'
import BaseList from 'components/base-list/BaseList'
import PhotoList from 'components/base-list/PhotoList'
import BaseReadForm from 'components/base-form/BaseReadForm'
import BaseTable from 'components/base-table/BaseTable'
import BooleanIcon from 'components/widgets/BooleanIcon'
import DateTimeFormat from 'components/widgets/DateTimeFormat'
import HelpTip from 'components/help/HelpTip'
import LoaderOverlay from 'components/LoaderOverlay'
import LoadingAlert from 'components/LoadingAlert'
import TranslationFieldsForm from 'components/translation-fields/TranslationFieldsForm'
import TranslationFieldsReadForm from 'components/translation-fields/TranslationFieldsReadForm'
import TranslationFieldsTable from 'components/translation-fields/TranslationFieldsTable'

import AppBreadcrumb from 'common/layout/AppBreadcrumb'
import AppHeader from 'common/layout/AppHeader'
import AppLeftSidebar from 'common/layout/AppLeftSidebar'
import AppRightSidebar from 'common/layout/AppRightSidebar'
import AppFooter from 'common/layout/AppFooter'
import PublicAuth from 'common/layout/PublicAuth'

Vue.addComponent(AppCard)
Vue.addComponent(AppLoader)
Vue.addComponent(AppModal)
Vue.addComponent(AppPagination)
Vue.addComponent(AppPre)
Vue.addComponent(BaseForm)
Vue.addComponent(BaseList)
Vue.addComponent(PhotoList)
Vue.addComponent(BaseReadForm)
Vue.addComponent(BaseTable)
Vue.addComponent(BooleanIcon)
Vue.addComponent(DateTimeFormat)
Vue.addComponent(HelpTip)
Vue.addComponent(LoaderOverlay)
Vue.addComponent(LoadingAlert)
Vue.addComponent(TranslationFieldsForm)
Vue.addComponent(TranslationFieldsReadForm)
Vue.addComponent(TranslationFieldsTable)

Vue.addComponent(AppBreadcrumb)
Vue.addComponent(AppHeader)
Vue.addComponent(AppLeftSidebar)
Vue.addComponent(AppRightSidebar)
Vue.addComponent(AppFooter)
Vue.addComponent(PublicAuth)

export default new Vue({
  el: '#app',
  router,
  store,
  components: {App},
})
