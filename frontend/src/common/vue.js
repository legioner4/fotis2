import VeeValidateLocaleRu from 'common/vee-validate/locale/ru'

import ajax from 'plugins/ajax'
import lodash from 'plugins/lodash'
import log from 'plugins/log'
import modal from 'plugins/modal'
import notify from 'plugins/notify'
import stickyTableHeaders from 'plugins/stickyTableHeaders'
import VeeValidate, {Validator} from 'vee-validate'
import Vue from 'vue'
import VueRouter from 'vue-router'
import uploader from 'vue-simple-uploader'
import Vuebar from 'vuebar'

import Draggable from 'vuedraggable'
import Vuetify from 'vuetify'
import './polyfills'
import customRules from './vee-validate/validators/custom_rules'

import passwordConfirm from './vee-validate/validators/password_confirm'

if (window.RAVEN_CONFIG_PUBLIC_DSN) {
  const Raven = require('raven-js')
  const RavenVue = require('raven-js/plugins/vue')
  Raven.config(window.RAVEN_CONFIG_PUBLIC_DSN)
    .setRelease(window.BUILD_NUMBER)
    .addPlugin(RavenVue, Vue)
    .install()
}
Vue.use(Vuebar)
Vue.use(VueRouter)
Vue.use(uploader)
Vue.use(Vuetify, {
  theme: {
    accent: '#5abeeb',
    light_accent: '#c8e9fc',
    success: '#59ad79',
    primary: '#2196F3',
    warning: '#FF6E40',
  },
  iconfont: 'md',
})

Vue.use(VeeValidate, {
  errorBagName: '$veeErrors', // change if property conflicts.
  fieldsBagName: '$veeFields',
  inject: false,
  locale: 'ru',
  dictionary: {
    ru: VeeValidateLocaleRu,
  },
})
Validator.extend('password_confirm', passwordConfirm)
Validator.extend('custom_rules', customRules)
Vue.use(ajax)
Vue.use(notify)
Vue.use(log)
Vue.use(modal)
Vue.use(lodash)
Vue.use(stickyTableHeaders)

Vue.component('v-draggable', Draggable)

Vue.addComponent = function(component) {
  if (!component.name) {
    throw new Error('name is required')
  }
  Vue.component(component.name, component)
}

const isProduction = process.env.NODE_ENV === 'production'
Vue.config.productionTip = !isProduction
Vue.config.devtools = !isProduction
Vue.config.performance = !isProduction

export default Vue
