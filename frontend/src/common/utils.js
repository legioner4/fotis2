function flatten(arr) {
  return arr.reduce(function (flat, toFlatten) {
    return flat.concat(Array.isArray(toFlatten) ? flatten(toFlatten) : toFlatten)
  }, [])
}

function getElementHeight(el) {
  if (!el) {
    return
  }
  const prev = el.style.height
  el.style.height = 'auto'
  const height = el.scrollHeight || el.offsetHeight
  el.style.height = prev
  return height
}

export {flatten, getElementHeight}
