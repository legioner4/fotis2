import accountComponents from 'apps/account/LazyComponents'
import publicComponents from 'apps/public/LazyComponents'
import workspaceComponents from 'apps/workspace/LazyComponents'

export default {
  BaseTable: () => import('components/base-table/BaseTable'),
  BaseList: () => import('components/base-list/BaseList'),
  BaseReadForm: () => import('components/base-form/BaseReadForm'),
  TransitionRouterView: () => import('components/TransitionRouterView'),
  TransitionAppcardRouterView: () => import('components/TransitionAppcardRouterView'),
  RichTextEditor: () => import('components/widgets/RichTextEditor'),

  ...accountComponents,
  ...publicComponents,
  ...workspaceComponents
}
