import VueRouter from 'vue-router'

const scrollBehavior = function(to, from, savedPosition) {
  if (to.path === from.path) {
    return
  }
  if (savedPosition) {
    return savedPosition
  }

  const position = {}
  if (to.hash) {
    position.selector = to.hash
  }
  if (to.meta.scrollTo) {
    position.selector = to.meta.scrollTo
  }
  if (position.selector) {
    if (document.querySelector(position.selector)) {
      return position
    }
    return false
  }
  if (to.matched.some(m => m.meta.scrollToTop)) {
    return {x: 0, y: 0}
  }
}

const router = new VueRouter({scrollBehavior})

router.afterEach(function(to, from) {
  if (to.path === from.path) {
    return
  }
  const nearestWithTitle = to.matched
    .slice()
    .reverse()
    .find(r => !!r.meta?.title)
  if (nearestWithTitle) {
    router.app.$emit('title-replace', nearestWithTitle.meta.title)
  }
})

export default router
