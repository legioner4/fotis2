export default {
  getMessage(field, args) {
    return 'Пароли должны совпадать'
  },
  validate(value, [password]) {
    if (_.isFunction(password)) {
      password = password()
    }
    return password === value
  },
}
