// реализация rules vuetify
export default {
  getMessage(field, rules, data) {
    return data && data.message
  },
  async validate(value, rules) {
    for (const rule of rules) {
      const message = await rule(value)
      if (message) {
        return {
          valid: false,
          data: {message},
        }
      }
    }
    return true
  },
}
