function concurrentConfirm(text, forceSaveFn, refreshFn, root) {
  const promise = root.$confirm({
    title: 'Ошибка при сохранении',
    btnText: 'Сохранить принудительно',
    cancelBtnText: 'Загрузить изменённые данные',
    confirmationText: text,
  })

  promise
    .then(result => {
      if (result) {
        forceSaveFn()
      } else {
        refreshFn()
      }
    })
    .catch(() => {})
  return promise
}

export {concurrentConfirm}
