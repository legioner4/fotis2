from rest_framework import routers

from .views import AccountView, RoleView

app_name = 'account_control'

router = routers.DefaultRouter()

router.register(r'accounts', AccountView, 'account')
router.register(r'roles', RoleView, 'role')

urlpatterns = router.urls
