import logging

import django_filters
from django.db import transaction
from rest_framework.decorators import action
from rest_framework.permissions import IsAdminUser
from rest_framework.response import Response

from account.api.control import serializers
from account.logic.getters.account import get_user_control_permissions
from account.models import UserAccount
from account.models import UserRole
from base.restapi.viewsets import BaseModelViewSet
from django_group_permissions.checkers import clear_permission_cache

logger = logging.getLogger(__name__)


class RoleMixin:
    serializer_class = serializers.RoleSerializer

    @action(detail=True, methods=['post'])
    def save(self, request, *args, **kwargs):
        instance = self.get_object()
        with transaction.atomic():
            new_permissions = get_user_control_permissions(request.user).filter(pk__in=request.data['permissions'])
            old_permissions = set(instance.permissions.values_list('pk', flat=True))
            if new_permissions != old_permissions:
                instance.permissions.set(new_permissions)
                instance.save()

            clear_permission_cache(request.user)
            self.check_permissions(request)
        return self.retrieve(request, *args, **kwargs)


class RoleView(RoleMixin, BaseModelViewSet):
    """
    Роли
    """
    permission_classes = IsAdminUser

    def get_queryset(self):
        return UserRole.objects


class AccountFilter(django_filters.FilterSet):
    first_name = django_filters.CharFilter(method='filter_first_name')
    last_name = django_filters.CharFilter(method='filter_last_name')
    patronymic = django_filters.CharFilter(method='filter_patronymic')
    email = django_filters.CharFilter(method='filter_email')

    class Meta:
        model = UserAccount
        fields = ('first_name', 'last_name', 'patronymic', 'email')

    def filter_first_name(self, queryset, name, value):
        if value:
            return queryset.filter(first_name__icontains=value)
        return queryset

    def filter_last_name(self, queryset, name, value):
        if value:
            return queryset.filter(last_name__icontains=value)
        return queryset

    def filter_patronymic(self, queryset, name, value):
        if value:
            return queryset.filter(patronymic__icontains=value)
        return queryset

    def filter_email(self, queryset, name, value):
        if value:
            return queryset.filter(email__icontains=value)
        return queryset


class AccountView(BaseModelViewSet):
    permission_classes = IsAdminUser
    queryset = UserAccount.objects
    serializer_class = serializers.AccountSerializer
    ordering_fields = (
        "first_name",
        "last_name",
        "patronymic",
        "username",
        "email",
    )
    filter_class = AccountFilter

    @action(methods=['put', 'get'], detail=True)
    def change_passwd(self, request, *args, **kwargs):
        if request.method.lower() == 'put':
            user = self.get_object()
            context = {'request': request}
            serializer = serializers.ChangePasswdSerializer(instance=user, data=request.data, context=context)
            serializer.is_valid(raise_exception=True)
            serializer.save()
            return Response(serializer.data)
        meta = self.get_meta(serializer=serializers.ChangePasswdSerializer())
        return Response({'meta': meta})
