# -*- coding: utf-8 -*-
import logging
import uuid

from django.conf import settings
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.models import Permission
from django.contrib.auth.password_validation import validate_password
from rest_framework import serializers

from account.logic.getters.account import get_user_control_permissions
from account.models import UserAccount, UserRole
from base.permissions import PERMISSION_CLASS_MAP
from base.restapi.serializer import BaseSerializer
from base.users import get_current_user
from django_group_permissions.permissions import PermissionsManager

logger = logging.getLogger(__name__)


class NotifySerializer(serializers.ModelSerializer):
    theme = serializers.CharField(
        label='Тема', required=True, max_length=settings.DATABASE_STRING_LENGTH, write_only=True
    )
    message = serializers.CharField(label='Сообщение', required=True, write_only=True)
    link = serializers.URLField(label='Ссылка', required=False, write_only=True, allow_blank=True)
    is_important = serializers.BooleanField(label='Важно', default=False)

    class Meta:
        model = UserAccount
        fields = ['theme', 'message', 'link', 'is_important']


class AccountSerializer(BaseSerializer):
    password = serializers.CharField(label="Пароль", max_length=20, required=True, write_only=True)
    password_confirm = serializers.CharField(label="Пароль", max_length=20, required=True, write_only=True)
    organization_permission = serializers.BooleanField(label="Администратор организации", read_only=True)

    class Meta:
        model = UserAccount
        fields = "__all__"
        extra_kwargs = {
            'email': {'style': {'class': 'no-wrap'}},
            'email_is_confirmed': {'label': 'Подтвержден', 'read_only': True},
            'is_active': {'label': 'Активный'},
            'groups': {'label': 'Роли'},
        }

    def validate(self, data):
        if data.get('password') != data.get('password_confirm'):
            raise serializers.ValidationError(
                {'password': "Пароли не совпадают.", 'password_confirm': "Пароли не совпадают."}
            )
        return data

    def validate_password(self, value):
        validate_password(value)
        return value

    def validate_email(self, value):
        users = UserAccount.objects.filter(email__iexact=value.lower())
        if self.instance and self.instance.pk:
            users = users.exclude(pk=self.instance.pk)
        if users.exists():
            raise serializers.ValidationError("Пользователь с таким адресом электронной почты уже существует")
        return value.lower()

    def create(self, validated_data):
        password = validated_data.pop('password')
        validated_data.pop('password_confirm')
        instance = super().create(validated_data)
        instance.set_password(password)
        instance.email_is_confirmed = True
        instance.save()
        return instance


class PermissionSerializer(serializers.ModelSerializer):
    group = serializers.SerializerMethodField(label='Группа', read_only=True)
    parent_permission_ids = serializers.SerializerMethodField(label='Базовые права', read_only=True)

    class Meta:
        model = Permission
        fields = ['id', 'name', 'codename', 'group', 'parent_permission_ids']

    def __init__(self, *args, role_id=None, **kwargs):
        super().__init__(*args, **kwargs)

    def get_group(self, obj):
        if obj.codename in PERMISSION_CLASS_MAP:
            return PERMISSION_CLASS_MAP[obj.codename].group
        return ''

    def get_parent_permission_ids(self, obj):
        if obj.codename in PERMISSION_CLASS_MAP:
            parent_codes = PERMISSION_CLASS_MAP[obj.codename].depends_codes()
            if parent_codes:
                return Permission.objects.filter(codename__in=parent_codes).distinct().values_list('id', flat=True)
        return []


class RoleSerializer(serializers.ModelSerializer):
    permissions = serializers.PrimaryKeyRelatedField(label='Разрешения', many=True, read_only=True)
    grouped_permissions = serializers.SerializerMethodField(label='Полномочия', read_only=True)
    full_title = serializers.CharField(label='Наименование', read_only=True)

    class Meta:
        model = UserRole
        fields = [
            'pk',
            'title',
            'full_title',
            'permissions',
            'grouped_permissions',
            'hidden',
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.current_user = get_current_user()

    def get_grouped_permissions(self, obj):
        permissions = PermissionSerializer(get_user_control_permissions(self.current_user), many=True).data
        permission_groups = PermissionsManager.get_groups()

        perm_codes_map = {p['codename']: p for p in permissions}
        grouped_permissions = []
        for group, perm_codes in sorted(permission_groups):
            grouped_permissions.append(
                [group, [perm_codes_map[perm_code] for perm_code in perm_codes if perm_code in perm_codes_map]]
            )
        return grouped_permissions

    def validate(self, data):
        title = data.get('title', None)
        if UserRole.objects.filter(title=title).exists():
            raise serializers.ValidationError("Роль с таким Наименованием уже существует")
        return data

    def create(self, validated_data):
        validated_data['name'] = '{0}_{1}'.format(validated_data.get('title'), uuid.uuid4())
        role = super().create(validated_data)
        return role


class ChangePasswdSerializer(serializers.ModelSerializer):
    new_password = serializers.CharField(label="Новый пароль", max_length=20, write_only=True, required=True)
    new_password_confirm = serializers.CharField(
        label="Подтверждение нового пароля", max_length=20, write_only=True, required=True
    )

    class Meta:
        model = UserAccount
        fields = ['new_password', 'new_password_confirm']

    def validate_new_password(self, value):
        validate_password(value)
        return value

    def validate(self, data):
        if data['new_password'] != data['new_password_confirm']:
            raise serializers.ValidationError(
                {'password': "Пароли не совпадают.", 'password_confirm': "Пароли не совпадают."}
            )
        return data

    def update(self, instance, validated_data):
        password = validated_data.get('new_password')
        instance.set_password(password)
        instance.save()

        request = self._context['request']
        if request.user == instance:
            update_session_auth_hash(request, instance)
        return instance
