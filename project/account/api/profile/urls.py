from rest_framework import routers

from .views import *

app_name = 'account_profile'

router = routers.DefaultRouter()

router.register(r'login', LoginView, 'login')
router.register(r'profile', ProfileView, 'profile')
router.register(r'password_reset', PasswordResetView, 'password_reset')
router.register(r'password_reset_confirm', PasswordResetConfirmView, 'password_reset_confirm')

urlpatterns = router.urls
