import logging

from django.contrib.auth import authenticate, update_session_auth_hash
from django.contrib.auth.password_validation import validate_password
from rest_framework import serializers

from account.models import UserAccount
from base.restapi.serializer import BaseSerializer

logger = logging.getLogger(__name__)


class AccountSerializer(BaseSerializer):
    organization__name = serializers.CharField(label='Организация', read_only=True, source='organization')

    class Meta:
        model = UserAccount
        fields = "__all__"


class LoginSerializer(serializers.Serializer):
    email = serializers.CharField(label="Адрес электронной почты", required=True)
    password = serializers.CharField(label="Пароль", max_length=20, write_only=True, required=True)

    class Meta:
        fields = ['email', 'password']

    def validate(self, data):
        user_cache = authenticate(email=data['email'], password=data['password'])
        if user_cache is None:
            user_cache = authenticate(username=data['email'], password=data['password'])
        if user_cache is None:
            raise serializers.ValidationError("Пожалуйста, введите правильную электронную почту и пароль")
        elif not user_cache.is_active:
            raise serializers.ValidationError("Эта учетная запись неактивна")
        data['user'] = user_cache
        return data

    def update(self, instance, validated_data):
        pass

    def create(self, validated_data):
        pass


class PasswordResetSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(label="Адрес электронной почты", required=True)

    class Meta:
        model = UserAccount
        fields = ['email']

    def validate(self, data):
        user = UserAccount.objects.filter(email=data['email'].lower()).first()
        data['user'] = user
        return data


class PasswordResetConfirmSerializer(serializers.ModelSerializer):
    password = serializers.CharField(label="Пароль", max_length=20, write_only=True, required=True)
    password_confirm = serializers.CharField(
        label="Подтверждение пароля", max_length=20, write_only=True, required=True
    )

    class Meta:
        model = UserAccount
        fields = ['password', 'password_confirm']

    def validate(self, data):
        if data['password'] != data['password_confirm']:
            raise serializers.ValidationError(
                {'password': "Пароли не совпадают.", 'password_confirm': "Пароли не совпадают."}
            )
        return data

    def validate_password(self, value):
        validate_password(value)
        return value

    def update(self, instance, validated_data):
        password = validated_data.get('password')
        instance.set_password(password)
        instance.save()
        return instance


class ChangePasswdSerializer(serializers.ModelSerializer):
    old_password = serializers.CharField(label="Старый пароль", max_length=20, write_only=True, required=True)
    new_password = serializers.CharField(label="Новый пароль", max_length=20, write_only=True, required=True)
    new_password_confirm = serializers.CharField(
        label="Подтверждение нового пароля", max_length=20, write_only=True, required=True
    )

    class Meta:
        model = UserAccount
        fields = ['old_password', 'new_password', 'new_password_confirm']

    def validate_old_password(self, value):
        if not self.instance.check_password(value):
            raise serializers.ValidationError("Ваш старый пароль был введен неправильно. Введите его еще раз.")
        return value

    def validate_new_password(self, value):
        validate_password(value)
        return value

    def validate(self, data):
        if data['new_password'] != data['new_password_confirm']:
            raise serializers.ValidationError(
                {'password': "Пароли не совпадают.", 'password_confirm': "Пароли не совпадают."}
            )
        return data

    def update(self, instance, validated_data):
        password = validated_data.get('new_password')
        instance.set_password(password)
        instance.save()
        request = self._context['request']
        update_session_auth_hash(request, instance)
        return instance
