import logging

from django.contrib.auth import login
from django.contrib.auth.tokens import default_token_generator
from django.http import Http404
from django.utils.encoding import force_text
from django.utils.http import urlsafe_base64_decode
from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from account.api.profile import serializers
from account.logic.actions.email import password_reset_email
from account.models import UserAccount
from base.restapi.viewsets import BaseModelViewSet

logger = logging.getLogger(__name__)


class ProfileView(BaseModelViewSet):
    permission_classes = IsAuthenticated
    queryset = UserAccount.objects
    serializer_class = serializers.AccountSerializer

    def get_object(self, queryset=None):
        return self.request.user

    @action(methods=['put', 'get'], detail=True)
    def change_passwd(self, request, *args, **kwargs):
        if request.method.lower() == 'put':
            user = self.get_object()
            context = {'request': request}
            serializer = serializers.ChangePasswdSerializer(instance=user, data=request.data, context=context)
            serializer.is_valid(raise_exception=True)
            serializer.save()
            return Response(serializer.data)
        meta = self.get_meta(serializer=serializers.ChangePasswdSerializer())
        return Response({'meta': meta})


class LoginView(viewsets.ViewSet):
    authentication_classes = []
    permission_classes = []

    serializer_class = serializers.LoginSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data
        user = validated_data.get('user')
        login(self.request, user)
        return Response(status=status.HTTP_200_OK)


class PasswordResetView(viewsets.ViewSet):
    """
    Восстановление пароля
    """

    authentication_classes = []
    permission_classes = []

    serializer_class = serializers.PasswordResetSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data
        email = validated_data.get('email')
        user = validated_data.get('user')
        if user:
            password_reset_email(self.request, email, user)
        return Response(status=status.HTTP_200_OK)


class PasswordResetConfirmView(viewsets.GenericViewSet):
    """
    Восстановление пароля завершение
    """

    authentication_classes = []
    permission_classes = []

    lookup_field = 'pk'
    lookup_url_kwarg = None

    queryset = UserAccount.objects.all()
    serializer_class = serializers.PasswordResetConfirmSerializer

    @action(
        methods=['get'],
        url_path=r'(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})',
        detail=False,
    )
    def get(self, request, *args, **kwargs):
        self.get_object()
        return Response(status=status.HTTP_200_OK)

    @action(
        methods=['put'],
        url_path=r'(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})',
        detail=False,
    )
    def put(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        if getattr(instance, '_prefetched_objects_cache', None):
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)

    def get_object(self):
        uidb64 = self.kwargs['uidb64']
        token = self.kwargs['token']
        uid = force_text(urlsafe_base64_decode(uidb64))
        try:
            user = UserAccount.objects.get(pk=uid)
            if default_token_generator.check_token(user, token):
                return user
        except Exception:
            pass
        raise Http404
