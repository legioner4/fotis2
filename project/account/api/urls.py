from django.conf.urls import include
from django.urls import path

app_name = 'account'

urlpatterns = [
    path('', include('account.api.profile.urls', namespace='profile')),
    path('control/', include('account.api.control.urls', namespace='control')),
    path('organization/', include('account.api.organization.urls', namespace='organization')),
]
