import logging

import django_filters

from account.api.organization import serializers
from account.models import UserAccount
from base.restapi.viewsets import BaseModelViewSet
from organization.permissions import CanManageOrganizations

logger = logging.getLogger(__name__)


class AccountFilter(django_filters.FilterSet):
    first_name = django_filters.CharFilter(method='filter_first_name')
    last_name = django_filters.CharFilter(method='filter_last_name')
    patronymic = django_filters.CharFilter(method='filter_patronymic')
    email = django_filters.CharFilter(method='filter_email')

    class Meta:
        model = UserAccount
        fields = ('first_name', 'last_name', 'patronymic', 'email')

    def filter_email(self, queryset, name, value):
        if value:
            return queryset.filter(email__icontains=value)
        return queryset

    def filter_patronymic(self, queryset, name, value):
        if value:
            return queryset.filter(patronymic__icontains=value)
        return queryset

    def filter_last_name(self, queryset, name, value):
        if value:
            return queryset.filter(last_name__icontains=value)
        return queryset

    def filter_first_name(self, queryset, name, value):
        if value:
            return queryset.filter(first_name__icontains=value)
        return queryset


class AccountView(BaseModelViewSet):
    permission_classes = CanManageOrganizations
    queryset = UserAccount.objects
    serializer_class = serializers.AccountSerializer
    ordering_fields = (
        "first_name",
        "last_name",
        "patronymic",
        "username",
        "email",
    )
    filter_class = AccountFilter

    def get_queryset(self):
        return UserAccount.objects.filter(organization=self.request.user.organization)

    def perform_create(self, serializer):
        serializer.save(organization=self.request.user.organization)
