# -*- coding: utf-8 -*-
import logging

from django.contrib.auth.password_validation import validate_password
from rest_framework import serializers

from account.logic.getters.role import get_default_order_manager_role
from account.models import UserAccount
from base.restapi.serializer import BaseSerializer, remove_fields

logger = logging.getLogger(__name__)


class AccountSerializer(BaseSerializer):
    password = serializers.CharField(label="Пароль", max_length=20, required=True, write_only=True)
    password_confirm = serializers.CharField(label="Пароль", max_length=20, required=True, write_only=True)
    organization_permission = serializers.BooleanField(label="Администратор организации", read_only=True)
    order_permission = serializers.BooleanField(label="Администратор заказов")

    class Meta:
        model = UserAccount
        fields = "__all__"

    def __list_init__(self, *args, **kwargs):
        super().__list_init__(*args, **kwargs)
        if 'groups' in self.fields:
            remove_fields(self, 'groups')

    def __object_init__(self, *args, **kwargs):
        super().__object_init__(*args, **kwargs)
        if 'groups' in self.fields:
            remove_fields(self, 'groups')

    def validate(self, data):
        if data.get('password') != data.get('password_confirm'):
            raise serializers.ValidationError(
                {'password': "Пароли не совпадают.", 'password_confirm': "Пароли не совпадают."}
            )
        return data

    def validate_password(self, value):
        validate_password(value)
        return value

    def validate_email(self, value):
        users = UserAccount.objects.filter(email__iexact=value.lower())
        if self.instance and self.instance.pk:
            users = users.exclude(pk=self.instance.pk)
        if users.exists():
            raise serializers.ValidationError("Пользователь с таким адресом электронной почты уже существует")
        return value.lower()

    def create(self, validated_data):
        order_permission = validated_data.pop('order_permission')
        password = validated_data.pop('password')
        validated_data.pop('password_confirm')
        instance = super().create(validated_data)
        instance.set_password(password)
        instance.email_is_confirmed = True
        instance.save()
        if order_permission:
            role = get_default_order_manager_role()
            instance.groups.add(role)
            instance.save()
        return instance

    def update(self, instance, validated_data):
        order_permission = validated_data.pop('order_permission')
        instance = super().update(instance, validated_data)
        role = get_default_order_manager_role()
        if order_permission:
            instance.groups.add(role)
        else:
            instance.groups.remove(role)
        instance.save()
        return instance
