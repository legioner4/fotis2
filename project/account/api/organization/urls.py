from rest_framework import routers

from .views import AccountView

app_name = 'organization_control'

router = routers.DefaultRouter()

router.register(r'accounts', AccountView, 'account')

urlpatterns = router.urls
