from logging import getLogger

from django.contrib.auth import get_user_model
from django.contrib.auth.backends import ModelBackend
from django.contrib.auth.models import Permission

logger = getLogger(__name__)

UserModel = get_user_model()

_get_group_permissions = ModelBackend._get_group_permissions


def _new_get_group_permissions(self, user_obj):
    # фильтруем удаленные роли
    return Permission.objects.filter(group__userrole__user=user_obj).distinct()


ModelBackend._get_group_permissions = _new_get_group_permissions


class EmailModelBackend(ModelBackend):
    def authenticate(self, request, email=None, password=None, **kwargs):
        if email:
            try:
                user = UserModel._default_manager.get(email__iexact=email)
            except UserModel.DoesNotExist:
                # Run the default password hasher once to reduce the timing
                # difference between an existing and a non-existing user (#20760).
                UserModel().set_password(password)
            else:
                if user.check_password(password) and self.user_can_authenticate(user):
                    return user
        return None

    def user_can_authenticate(self, user):
        return user.email_is_confirmed and super().user_can_authenticate(user)
