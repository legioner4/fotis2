from django.conf.urls import url

from . import views

app_name = 'account'

urlpatterns = [
    url(r'^validate/username/$', views.validate_username, name='validate_username'),
    url(r'^validate/email/$', views.validate_email, name='validate_email'),
]
