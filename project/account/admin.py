from django.contrib import admin

from account.models import UserAccount, UserRole

admin.site.register(UserAccount)
admin.site.register(UserRole)
