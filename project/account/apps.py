from django.apps import AppConfig


class AccountConfig(AppConfig):
    verbose_name = 'Пользователи'
    name = 'account'
