from django.utils.encoding import force_text
from django.utils.http import urlsafe_base64_decode

from account.logic.actions.tokens import account_activation_token
from account.models import UserAccount


def create_user(email, password, kwargs):
    """создание пользователя"""
    return UserAccount.objects.create_user(email, password, **kwargs)


def email_confirmation(uidb64, token):
    """Подтверждение почтового адреса пользователя"""
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = UserAccount.objects.get(pk=uid)
    except (TypeError, ValueError, OverflowError, UserAccount.DoesNotExist):
        user = None

    if user is not None and account_activation_token.check_token(user, token) and not user.email_is_confirmed:
        user.email_is_confirmed = True
        user.save()
        return True

    return False
