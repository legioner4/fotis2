from django.conf import settings
from django.contrib.auth.tokens import default_token_generator
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode


def password_reset_email(request, to_email, user):
    mail_subject = 'Сброс пароля на {0}'.format(settings.SITE_URL)
    context = {
        'site_url': settings.SITE_URL,
        'user': user,
        'uid': urlsafe_base64_encode(force_bytes(user.pk)),
        'token': default_token_generator.make_token(user),
    }
    message = render_to_string('account/email/password_reset_email.html', context=context, request=request)
    email = EmailMultiAlternatives(mail_subject, to=[to_email])
    email.attach_alternative(message, 'text/html')
    email.send()
