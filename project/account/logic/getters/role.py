from account.models import UserRole

DEFAULT_ORGANIZATION_MANAGER_ROLE_NAME = 'Администратор организации'
DEFAULT_ORDER_MANAGER_ROLE_NAME = 'Администратор заказов'


def get_default_organization_manager_role():
    return UserRole.objects.filter(title=DEFAULT_ORGANIZATION_MANAGER_ROLE_NAME).first()


def get_default_order_manager_role():
    return UserRole.objects.filter(title=DEFAULT_ORDER_MANAGER_ROLE_NAME).first()
