from django.contrib.auth.models import Permission

from base.permissions import PERMISSION_CLASS_MAP


def get_user_control_permissions(user):
    """
    Получить управляемые пользователем права
    """
    codes = [
        perm.codename
        for perm in Permission.objects.all()
        if perm.codename in PERMISSION_CLASS_MAP and PERMISSION_CLASS_MAP[perm.codename].has_control(user)
    ]
    return Permission.objects.filter(codename__in=codes)
