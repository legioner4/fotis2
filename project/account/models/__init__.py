import uuid

from django.contrib.auth.models import AbstractBaseUser, AbstractUser, Group, PermissionsMixin
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.translation import ugettext_lazy as _

from account.models import managers
from base.models import BaseModel
from order.permissions import CanManageOrder
from organization.permissions import CanManageOrganizations


class UserAccount(BaseModel, PermissionsMixin, AbstractBaseUser):
    username_validator = AbstractUser.username_validator
    username = models.CharField(
        verbose_name='Логин',
        max_length=150,
        unique=True,
        null=True,
        blank=True,
        help_text=_('Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.'),
        validators=[username_validator],
        error_messages={'unique': 'Пользователь с таким логином уже существует.'},
    )
    email_error_message = {'unique': 'Пользователь с таким адресом электронной почты уже существует.'}
    email = models.EmailField('Адрес электронной почты', db_index=True, unique=True, error_messages=email_error_message)
    email_is_confirmed = models.BooleanField('Адрес электронной почты подтвержден', default=False)

    first_name = models.CharField(verbose_name='Имя', max_length=256, blank=True)
    last_name = models.CharField(verbose_name='Фамилия', max_length=256, blank=True)
    patronymic = models.CharField(verbose_name='Отчество', max_length=256, blank=True)
    avatar = models.ImageField('Аватар', upload_to='account/avatar/', blank=True, null=True)
    organization = models.ForeignKey('organization.Organization', models.CASCADE, verbose_name='Организация', related_name='users', null=True, blank=True)

    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)

    groups = models.ManyToManyField(
        'UserRole',
        verbose_name=_('groups'),
        blank=True,
        help_text=_(
            'The groups this user belongs to. A user will get all permissions ' 'granted to each of their groups.'
        ),
        related_name="user_set",
        related_query_name="user",
    )

    USERNAME_FIELD = 'email'

    objects = managers.UserAccountManager()

    class Meta:
        verbose_name = 'пользователь'
        verbose_name_plural = 'пользователи'
        ordering = ('created_at',)

    def __str__(self):
        full_name = self.get_full_name()
        return  full_name if full_name else self.email

    def get_full_name(self):
        return f'{self.last_name} {self.first_name} {self.patronymic}'.strip()

    def get_short_name(self):
        name_parts = [self.first_name, self.patronymic]
        short_name_parts = map(lambda part: f'{part[0]}.', filter(lambda part: len(part) >= 1, name_parts))
        return f'{self.last_name} {"".join(short_name_parts)}'.strip()

    @property
    def organization_permission(self):
        return CanManageOrganizations.checker(self)

    @property
    def order_permission(self):
        return CanManageOrder.checker(self)


class UserRole(BaseModel, Group):
    title = models.CharField('Наименование', max_length=256)
    hidden = models.BooleanField('Скрытая роль', default=False)

    objects = managers.RoleManager()

    class Meta:
        verbose_name = 'роль'
        verbose_name_plural = 'роли'
        ordering = ('title',)

    def __str__(self):
        return self.full_title

    @property
    def full_title(self):
        title = self.title or self.name
        return title


@receiver(post_save, sender=Group)
def group_create(sender, instance, created, *args, **kwargs):
    if created:
        role = UserRole.objects.filter(group_ptr_id=instance.id).first()
        if not role:
            role = UserRole(group_ptr_id=instance.id, uuid=uuid.uuid4())
            role.__dict__.update(instance.__dict__)
            role.save()
