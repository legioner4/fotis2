from django.contrib.auth.models import UserManager
from base.manager import BaseModelManager


class UserAccountManager(UserManager, BaseModelManager):
    def create_user(self, email, password=None, commit=True, **kwargs):
        if not email:
            raise ValueError('Пользователи должны иметь адрес электронной почты')

        user = self.model(email=UserManager.normalize_email(email))
        user.__dict__.update(kwargs)
        user.set_password(password)
        user.is_active = True
        user.is_staff = False
        user.is_superuser = False
        if commit:
            user.save(using=self._db)
        return user

    def create_superuser(self, email, password, commit=True):
        user = self.create_user(email, password=password)
        user.is_staff = True
        user.is_active = True
        user.is_superuser = True
        if commit:
            user.save(using=self._db)
        return user


class RoleManager(BaseModelManager):

    def default_roles(self):
        """Роли назначенные по-умолчанию."""
        return self.filter(default_role=True)
