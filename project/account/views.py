import json
import re

from django.conf import settings
from django.contrib.auth import get_user_model
from django.http import JsonResponse
from django.utils.encoding import force_text

User = get_user_model()


def validate_username(request):
    valid = True
    message = ''
    data = json.loads(request.body)
    username = force_text(data.get('username') or '')
    if not username:
        valid = False
        message = 'Обязательное поле.'
    if valid:
        forbidden_username_list = getattr(settings, 'FORBIDDEN_USERNAME_LIST')
        if forbidden_username_list:
            char_only = re.sub(r'\W', '', username.lower())
            if char_only in forbidden_username_list:
                valid = False
                message = 'Пользователь с таким именем уже существует.'
    if valid and User.objects.filter(username=username).exists():
        valid = False
        message = 'Пользователь с таким именем уже существует.'
    return JsonResponse({'valid': valid, 'message': message})


def validate_email(request):
    valid = True
    message = ''
    data = json.loads(request.body)
    email = force_text(data.get('email') or '')
    if not email:
        valid = False
        message = 'Обязательное поле.'
    elif User.objects.filter(email=email).exists():
        valid = False
        message = 'Пользователь с таким адресом электронной почты уже существует.'
    return JsonResponse({'valid': valid, 'message': message})
