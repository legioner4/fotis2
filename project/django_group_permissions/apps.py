from django.apps import AppConfig
from django.contrib.auth import get_user_model
from django.db.models.signals import post_migrate

from .loader import load_groups_and_permissions, create_groups_and_permission


class GroupPermissions(AppConfig):
    name = 'django_group_permissions'
    verbose_name = "Группы и права"

    def ready(self):
        load_groups_and_permissions()
        post_migrate.connect(create_groups_and_permission, sender=get_user_model()._meta.app_config)
