from __future__ import unicode_literals


class CheckerNotRegistered(Exception):
    pass


class GroupDoesNotExist(Exception):
    pass


