from __future__ import unicode_literals

import inspect

from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group, Permission
from django.contrib.contenttypes.models import ContentType
from six import add_metaclass

from .exceptions import GroupDoesNotExist
from .utils import camelToSnake


class GroupManager(object):
    _registered_groups = {}
    _user_ct = None

    def __iter__(cls):
        return iter(cls._registered_groups)

    @classmethod
    def retrieve_group(cls, group_name):
        if group_name in cls._registered_groups:
            return cls._registered_groups[group_name]

    @classmethod
    def get_groups_names(cls):
        return cls._registered_groups.keys()

    @classmethod
    def get_groups(cls):
        return cls._registered_groups.values()

    @classmethod
    def register_group(cls, group_class):
        cls._registered_groups[group_class.get_name()] = group_class

    @classmethod
    def get_user_ct(cls):
        if cls._user_ct is None:
            cls._user_ct = ContentType.objects.get_for_model(get_user_model())
        return cls._user_ct


class GroupClassRegister(type):
    def __new__(cls, name, parents, dct):
        group_class = super(GroupClassRegister, cls).__new__(cls, name, parents, dct)
        if object not in parents:
            GroupManager.register_group(group_class)
        return group_class


@add_metaclass(GroupClassRegister)
class AbstractUserGroup(object):
    group_model = Group

    @classmethod
    def get_name(cls):
        if hasattr(cls, 'group_name'):
            return cls.group_name

        return camelToSnake(cls.__name__)

    @classmethod
    def get_auth_group(cls):
        return cls.group_model.objects.get_or_create(name=cls.get_name())[0]

    @classmethod
    def apply_permissions_preset(cls):
        group = cls.get_auth_group()
        permissions = group.permissions
        default_permissions = set(cls.get_default_permissions())
        permissions_exists = set(permissions.all())
        permissions_to_add = default_permissions - permissions_exists
        permissions_to_del = permissions_exists - default_permissions
        if permissions_to_add:
            permissions.add(*permissions_to_add)
        preset_strictly = getattr(settings, "GROUP_PERMISSIONS_PRESET_STRICTLY", True)
        if permissions_to_del and preset_strictly:
            permissions.remove(*permissions_to_del)

    @classmethod
    def assign_group_to_user(cls, user):
        group = cls.get_user_group()
        user.groups.add(group)
        return group

    @classmethod
    def remove_group_from_user(cls, user):
        group = cls.get_auth_group()
        user.groups.remove(group)

    @classmethod
    def get_default_permissions(cls):
        if hasattr(cls, 'available_permissions'):
            return cls.get_or_create_permissions(cls.available_permissions)

        return []

    @classmethod
    def get_or_create_permissions(cls, permission_names):
        user_ct = GroupManager.get_user_ct()
        permissions = list(Permission.objects.filter(content_type=user_ct, codename__in=permission_names).all())

        if len(permissions) != len(permission_names):
            from .permissions import PermissionsManager

            for permission_name in permission_names:
                permission, created = Permission.objects.get_or_create(
                    content_type=user_ct, codename=permission_name,
                    defaults={'name': PermissionsManager.get_permission_name(permission_name)})
                if created:
                    permissions.append(permission)

        return permissions


def retrieve_group(group_name):
    """Get a Group object from a group name."""
    return GroupManager.retrieve_group(group_name)


def get_user_groups(user):
    """Get a list of a users's groups."""
    if user:
        return user.groups.order_by("name")
    else:
        return []


def _assign_or_remove_group(user, group, method_name):
    group_cls = group
    if not inspect.isclass(group):
        group_cls = retrieve_group(group)

    if not group_cls:
        raise GroupDoesNotExist

    getattr(group_cls, method_name)(user)

    return group_cls


def assign_group(user, group):
    """Assign a group to a user."""
    return _assign_or_remove_group(user, group, "assign_group_to_user")


def remove_group(user, group):
    """Remove a group from a user."""
    return _assign_or_remove_group(user, group, "remove_group_from_user")


def clear_groups(user):
    user.groups.clear()
