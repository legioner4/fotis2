from __future__ import unicode_literals

from django import template

from ..checkers import has_group, has_permission, has_any_permission, has_all_permission

register = template.Library()


@register.filter(name='has_group')
def has_group_template_tag(user, group):
    group_list = group.split(',')
    return has_group(user, group_list)


@register.filter(name='can')
def can_template_tag(user, permission):
    return has_permission(user, permission)


@register.filter(name='can_any')
def can_template_tag(user, permissions):
    permissions = permissions.split(',')
    return has_any_permission(user, *permissions)


@register.filter(name='can_all')
def can_template_tag(user, permissions):
    permissions = permissions.split(',')
    return has_all_permission(user, *permissions)
