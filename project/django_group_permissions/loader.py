from importlib import import_module

from django.apps import apps
from django.conf import settings
from django.contrib.auth import management as auth_management


def load_groups_and_permissions():
    _monkey_patch()
    module = getattr(settings, "GROUP_PERMISSIONS_MODULE", None)

    if module:
        try:
            import_module(module)
        except ImportError:
            pass
    for app_config in apps.get_app_configs():
        try:
            import_module(".group_permissions", app_config.name)
        except ImportError:
            pass
        try:
            import_module(".permissions", app_config.name)
        except ImportError:
            pass


def _monkey_patch():
    try:
        Permission = apps.get_model("auth", "Permission")
    except LookupError:
        return
    Permission.__str__ = lambda self: self.name

    remove_default = getattr(settings, "GROUP_PERMISSIONS_REMOVE_DEFAULT", True)
    if remove_default:
        # манки патч чтобы не было стандартных разрешений
        _origin_get_builtin_permissions = auth_management._get_builtin_permissions
        auth_management._get_builtin_permissions = lambda *args, **kwargs: []


def create_groups_and_permission(using=None, **kwargs):
    if using != "default":
        return
    try:
        Permission = apps.get_model("auth", "Permission")
    except LookupError:
        return

    from .groups import GroupManager
    from .permissions import PermissionsManager

    user_ct = GroupManager.get_user_ct()
    permissions_dict = PermissionsManager.get_permissions_dict()

    unregistered_permissions = Permission.objects.exclude(
        content_type=user_ct, codename__in=permissions_dict
    )

    remove_default = getattr(settings, "GROUP_PERMISSIONS_REMOVE_DEFAULT", True)
    if remove_default:
        and_unused_permissions = unregistered_permissions.filter(
            user__isnull=True, group__isnull=True
        )
        and_unused_permissions.delete()

    remove_stale = getattr(settings, "GROUP_PERMISSIONS_REMOVE_STALE", True)
    if remove_stale:
        unregistered_permissions.delete()

    exists_perms = dict(
        Permission.objects.filter(content_type=user_ct).values_list("codename", "name")
    )

    new_perms = []
    for (codename, name) in permissions_dict.items():
        if codename not in exists_perms:
            new_perms.append(
                Permission(codename=codename, name=name, content_type=user_ct)
            )
        elif name != exists_perms[codename]:
            Permission.objects.filter(codename=codename, content_type=user_ct).update(
                name=name
            )
    Permission.objects.bulk_create(new_perms)

    for group in GroupManager.get_groups():
        group.apply_permissions_preset()
