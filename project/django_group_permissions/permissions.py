from __future__ import unicode_literals

from django.contrib.auth.models import Permission

from .exceptions import CheckerNotRegistered
from .groups import GroupManager


class PermissionsManager(object):
    _checkers = {}
    _permissions = {}
    _groups = {}

    @classmethod
    def register_checker(cls, name, function):
        cls._checkers[name] = function

    @classmethod
    def get_checkers(cls):
        return cls._checkers

    @classmethod
    def retrieve_checker(cls, checker_name):
        if checker_name in cls._checkers:
            return cls._checkers[checker_name]

        raise CheckerNotRegistered('Checker with name %s was not registered' % checker_name)

    @classmethod
    def register_permissions(cls, code, name, group=None):
        cls._permissions[code] = name
        group = '' if group is None else group
        cls._groups.setdefault(group, []).append(code)

    @classmethod
    def get_groups(cls):
        return sorted(cls._groups.items())

    @classmethod
    def get_permissions_dict(cls):
        return cls._permissions

    @classmethod
    def get_permission_name(cls, code):
        if code in cls._permissions:
            return cls._permissions[code]


def register_permission(code, name, group=None):
    PermissionsManager.register_permissions(code, name, group=group)


def register_object_checker(name=None):
    def fuction_decorator(func):
        checker_name = name if name else func.__name__
        PermissionsManager.register_checker(checker_name, func)

    return fuction_decorator


def get_permission(permission_name):
    """Get a Permission object from a permission name."""
    user_ct = GroupManager.get_user_ct()
    permission, _created = Permission.objects.get_or_create(
        content_type=user_ct, codename=permission_name,
        defaults={'name': PermissionsManager.get_permission_name(permission_name)})

    return permission


def grant_permission(user, permission_name):
    permission = get_permission(permission_name)
    user.user_permissions.add(permission)


def revoke_permission(user, permission_name):
    permission = get_permission(permission_name)
    user.user_permissions.remove(permission)
