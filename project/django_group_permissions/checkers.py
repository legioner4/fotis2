from __future__ import unicode_literals

from .groups import GroupManager
from .permissions import PermissionsManager


def has_group(user, groups):
    """Check if a user has any of the given groups."""
    if not isinstance(groups, list):
        groups = [groups]

    normalized_groups = []
    for group in groups:
        if hasattr(group, "get_name"):
            group = group.get_name()
        normalized_groups.append(group)

    return user.groups.filter(name__in=normalized_groups).exists()


def clear_permission_cache(user):
    for attr in ("_perm_cache", "_user_perm_cache", "_group_perm_cache"):
        if hasattr(user, attr):
            delattr(user, attr)


def has_permission(user, permission_name):
    """Check if a user has a given permission."""
    user_ct = GroupManager.get_user_ct()
    permission_name = "%s.%s" % (user_ct.app_label, permission_name)
    return user.has_perm(permission_name)


def has_any_permission(user, *permissions):
    user_ct = GroupManager.get_user_ct()
    return bool(permissions) and any(
        user.has_perm("%s.%s" % (user_ct.app_label, permission))
        for permission in permissions
    )


def has_all_permission(user, *permissions):
    user_ct = GroupManager.get_user_ct()
    return bool(permissions) and all(
        user.has_perm("%s.%s" % (user_ct.app_label, permission))
        for permission in permissions
    )


def has_object_permission(checker_name, user, obj):
    """Check if a user has permission to perform an action on an object."""
    checker = PermissionsManager.retrieve_checker(checker_name)
    return checker(user, obj)
