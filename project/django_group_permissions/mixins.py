from __future__ import unicode_literals

from django.contrib.auth.views import redirect_to_login
from django.contrib.auth.mixins import AccessMixin
from django_group_permissions.checkers import has_object_permission, has_group, has_permission, has_any_permission, has_all_permission


class HasGroupMixin(AccessMixin):
    raise_exception = True
    allowed_groups = []
    redirect_to_login = None

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return redirect_to_login(request.get_full_path(), self.get_login_url(), self.get_redirect_field_name())
        if not has_group(request.user, self.allowed_groups):
            return self.handle_no_permission()
        return super(HasGroupMixin, self).dispatch(request, *args, **kwargs)


class HasPermissionsMixin(AccessMixin):
    raise_exception = True
    required_permission = ''
    required_permissions_any = ''
    required_permissions_all = ''
    redirect_to_login = None

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return redirect_to_login(request.get_full_path(), self.get_login_url(), self.get_redirect_field_name())
        if self.required_permission and not has_permission(request.user, self.required_permission):
            return self.handle_no_permission()
        if self.required_permissions_any and not has_any_permission(request.user, *self.required_permissions_any):
            return self.handle_no_permission()
        if self.required_permissions_all and not has_all_permission(request.user, *self.required_permissions_all):
            return self.handle_no_permission()
        return super(HasPermissionsMixin, self).dispatch(request, *args, **kwargs)


class HasObjectPermissionsMixin(AccessMixin):
    raise_exception = True
    required_checker_name = ''
    redirect_to_login = None

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return redirect_to_login(request.get_full_path(), self.get_login_url(), self.get_redirect_field_name())
        if not has_object_permission(self.required_checker_name, request.user, self.get_permitted_object()):
            return self.handle_no_permission()
        return super(HasObjectPermissionsMixin, self).dispatch(request, *args, **kwargs)

    def get_permitted_object(self):
        return self.get_object()
