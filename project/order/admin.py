from django.contrib import admin

from .models import *

admin.site.register(Order)
admin.site.register(Photo)
admin.site.register(OrderDelivery)
admin.site.register(OrderPayment)
admin.site.register(PhotoFormat)
admin.site.register(PhotoPrice)
