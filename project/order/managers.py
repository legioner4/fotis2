from django.utils import timezone

from base.manager import BaseModelManager, BaseModelQuerySet


class OrderQuerySet(BaseModelQuerySet):
    def get_next_order_number(self, organization):
        last_order = self.model.objects.filter(organization=organization).order_by('created_at').last()
        if not last_order or not last_order.order_number:
            return 1
        return last_order.order_number + 1


OrderManager = BaseModelManager.from_queryset(OrderQuerySet)
