# Generated by Django 2.2.6 on 2020-11-09 21:19

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0023_auto_20201109_2326'),
        ('order', '0035_auto_20201030_0012'),
    ]

    operations = [
        migrations.AddField(
            model_name='orderdelivery',
            name='transport_company_point',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='catalog.TransportCompanyPoint', verbose_name='Пункт выдачи транспортной компании'),
        ),
    ]
