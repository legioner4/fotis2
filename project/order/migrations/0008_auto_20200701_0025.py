# Generated by Django 2.2.6 on 2020-06-30 21:25

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0007_photo_format_price'),
    ]

    operations = [
        migrations.AlterField(
            model_name='orderdelivery',
            name='subdivision',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='organization.Subdivision', verbose_name='Офис'),
        ),
    ]
