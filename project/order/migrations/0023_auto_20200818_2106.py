# Generated by Django 2.2.6 on 2020-08-18 18:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0022_auto_20200818_2042'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='order',
            name='phone',
        ),
        migrations.AddField(
            model_name='order',
            name='email',
            field=models.EmailField(blank=True, max_length=256, null=True, verbose_name='Адрес электронной почты'),
        ),
    ]
