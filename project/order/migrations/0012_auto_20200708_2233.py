# Generated by Django 2.2.6 on 2020-07-08 19:33

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0011_auto_20200708_2230'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='order',
            options={'ordering': ('state', '-created_at'), 'verbose_name': 'заказ', 'verbose_name_plural': 'заказы'},
        ),
    ]
