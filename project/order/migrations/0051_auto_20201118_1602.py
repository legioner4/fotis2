# Generated by Django 2.2.6 on 2020-11-18 13:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0050_auto_20201116_2240'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='photoprice',
            name='discount_percent',
        ),
        migrations.AddField(
            model_name='photoprice',
            name='discount_value',
            field=models.DecimalField(decimal_places=2, default=0, max_digits=6, verbose_name='Скидка'),
        ),
    ]
