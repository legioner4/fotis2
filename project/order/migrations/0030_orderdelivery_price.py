# Generated by Django 2.2.6 on 2020-10-25 06:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0029_auto_20201023_2152'),
    ]

    operations = [
        migrations.AddField(
            model_name='orderdelivery',
            name='price',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=6, null=True, verbose_name='Цена доставки'),
        ),
    ]
