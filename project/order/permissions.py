from base.decorators import register_permission
from base.permissions import Permission
from organization.permissions import CanManageOrganizations, RequireOrganizationMixin
from system.permissions import CanManageSystem

ORDER_GROUP = 'Заказы'


@register_permission
class CanManageOrder(RequireOrganizationMixin, Permission):
    code = 'can_manage_order'
    name = 'Управление'
    group = ORDER_GROUP
    depends_perms = [CanManageOrganizations]
    control_perms = [CanManageSystem]
