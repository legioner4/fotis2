import os
import zipfile
from io import BytesIO

from PIL import Image
from django.core.files.base import ContentFile

from catalog.logic.getters import format_price_calculate
from catalog.models import DeliveryType
from catalog.models import Discount
from order import models
from order.logic.actions.email import *
from organization.logic.actions.ftp import organization_ftp_dir

ORDER_DIR = 'order'


def activate_order(request, order):
    from order.tasks import task_create_order_photos_zip

    order.state = models.Order.NEW_STATE
    order.save()
    calculate_price(order)
    create_order_manager_notify(request, order)
    create_order_customer_notify(request, order)
    save_description_file(order)
    task_create_order_photos_zip.delay(order.pk)


def after_download(order):
    if order.state == order.NEW_STATE:
        order.state = order.LOADED_STATE
        order.save()


def order_create_photo_groups(order):

    photo_groups = {}
    for photo in order.photos.all():
        photo_groups.setdefault(f'{photo.format.format_name}', []).append(photo)

    for key, photos in photo_groups.items():
        qty = 0

        if photos:
            first_photo = photos[0]
            price_one_photo = first_photo.price.format_price.price

            group = models.PhotoGroup.objects.create(
                order=order,
            )

            for photo in photos:
                qty += photo.qty
                group.photos.add(photo)

            discount = Discount.objects.filter(enabled=True, format_price=first_photo.price.format_price).filter(min_count__lte=qty, max_count__gte=qty).first()

            group.qty = qty
            group.price_one_photo = price_one_photo
            group.price_one_discount_photo = discount.discount_value if discount else None
            group.save()


def calculate_price(order):
    sum_total_price = 0
    sum_discount = 0
    sum_price_without_discount = 0
    order_create_photo_groups(order)

    for group in order.groups.all():
        first_photo = group.photos.all().first()
        if first_photo:
            price = format_price_calculate(group.qty, first_photo.price.format_price.pk)

            sum_total_price += price.total_price
            sum_discount += price.discount_sum
            sum_price_without_discount += price.price_without_discount
    order.total_price = sum_total_price
    order.discount_sum = sum_discount
    order.price_without_discount = sum_price_without_discount
    order.save()
    order.delivery.calculate_delivery_price()


def get_photo_upload_path(photo, filename):
    """
    Использовать только если уверены что есть photo.price.format_price
    """
    photo_format = photo.price.format_price.photo_format
    paper_type = photo.price.format_price.paper_type
    border_slug = 'spolyami' if photo.border else 'bezpoley'
    count = photo.qty
    dir_name = f'{photo_format.slug}_{paper_type.slug}_{border_slug}_x{count}'
    org_dir_name = organization_ftp_dir(photo.order.organization)
    order_dir_name = photo.order.get_order_number()
    return os.path.join(org_dir_name, order_dir_name, dir_name, filename)


def get_source_photo_upload_path(photo, filename):
    return os.path.join(ORDER_DIR, "source_%s" % photo.order.pk, filename)


def get_zip_upload_path(order, filename):
    return os.path.join(ORDER_DIR, "zip_%s" % order.pk, filename)


def modify_photo(photo):
    pass
    # photo.save_source_image()
    # p_image = Image.open(photo.image)
    # p_image.resize((photo.format.width, photo.format.height))


def create_order_photos_zip(order):
    in_memory_data = BytesIO()
    in_memory_zip = zipfile.ZipFile(in_memory_data, "w", zipfile.ZIP_DEFLATED)

    org_dir_name = organization_ftp_dir(order.organization)
    order_dir_name = order.get_order_number()
    path = os.path.abspath(os.path.join(settings.MEDIA_ROOT, org_dir_name, order_dir_name))

    for root, _, files in os.walk(path):

        for name in files:
            file = os.path.join(root, name)
            name = os.path.normpath(name)
            absname = os.path.abspath(os.path.join(root, name))
            arcname = absname[len(path) + 1:]
            in_memory_zip.write(file, arcname)

    in_memory_zip.close()
    filename = f"{order.get_order_number()}.zip"

    file_zip = ContentFile(in_memory_data.getvalue(), filename)
    order.zip_file.save(filename, file_zip)


def save_description_file(order):
    org_dir_name = organization_ftp_dir(order.organization)
    order_dir_name = order.get_order_number()

    with open(os.path.join(settings.MEDIA_ROOT, org_dir_name, order_dir_name, 'readme.txt'), 'w') as f:
        f.write(f'Заказ №{order.get_order_number()}\n')
        f.write(f'Дата создания {order.created_at.strftime("%d.%m.%Y %H:%M")} \n')
        f.write(f'Примечание: {order.note if order.note else ""} \n')

        f.write(f'\n')
        f.write(f'Клиент \n')
        f.write(f'ФИО: {order.fio if order.fio else ""} \n')
        f.write(f'Номер телефона: {order.phone if order.phone else ""} \n')
        f.write(f'email: {order.email if order.email else ""} \n')

        f.write(f'\n')
        f.write(f'Состав заказа\n')
        for group in order.photo_group_info:
            f.write(f'{group.get("description", "")}, {group.get("qty", "")} шт. за {group.get("price_sum_str", "")}\n')

        if order.payment:
            f.write(f'\n')
            f.write(f'Оплата: {order.payment.payment_type.name}\n')

        f.write(f'\n')
        f.write(f'Доставка: {order.delivery.delivery_type.name}\n')
        if order.delivery.delivery_type.type == DeliveryType.POST_TYPE:
            f.write(f'Город: {order.delivery.city}\n')
            f.write(f'Почтовый индекс: {order.delivery.post_index}\n')
            f.write(f'Улица: {order.delivery.street}\n')
            f.write(f'Номер дома: {order.delivery.house_number}\n')
            f.write(f'Корпус: {order.delivery.corps}\n')
            f.write(f'Номер квартиры или офиса: {order.delivery.apartment_number}\n')
        elif order.delivery.delivery_type.type == DeliveryType.COURIER_TYPE:
            f.write(f'Регион доставки: {str(order.delivery.delivery_region)}\n')
            f.write(f'Город: {order.delivery.city}\n')
            f.write(f'Улица: {order.delivery.street}\n')
            f.write(f'Номер дома: {order.delivery.house_number}\n')
            f.write(f'Корпус: {order.delivery.corps}\n')
            f.write(f'Номер квартиры или офиса: {order.delivery.apartment_number}\n')
        elif order.delivery.delivery_type.type == DeliveryType.PICKUP_TYPE:
            f.write(f'Фотоцентр: {str(order.delivery.subdivision)}\n')

        price_info = order.price_info
        all_sum = price_info.get('all_sum', 0)
        discount_sum = price_info.get('discount_sum', 0)
        price_without_discount = price_info.get('price_without_discount', 0)
        delivery_price = price_info.get('delivery_price', 0)

        f.write(f'\n')
        f.write(f'Цена \n')
        f.write(f'Итоговая цена {all_sum}\n')
        f.write(f'Сумма скидки {discount_sum}\n')
        f.write(f'Цена без скидки {price_without_discount}\n')
        f.write(f'Цена доставки {delivery_price}\n')


def change_order_state(request, order):
    """Изменение статуса"""
    from order.models import Order

    if order.state == Order.INPRINT_STATE:
        order_print_state_notify(request, order)
    elif order.state == Order.READY_STATE:
        order_ready_state_notify(request, order)

