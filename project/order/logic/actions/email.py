from email.mime.image import MIMEImage

from django.conf import settings
from django.template.loader import render_to_string
from django.urls import reverse

from order.permissions import CanManageOrder
from organization.email_backands import OrganizationEmailMultiAlternatives


def organization_logo_data(organization):
    with open(organization.logo.path, 'rb') as f:
        logo_data = f.read()
    logo = MIMEImage(logo_data)
    logo.add_header('Content-ID', '<logo>')
    return logo


def create_order_manager_notify(request, order):
    organization_user_pks = order.organization.users.all().values_list('pk', flat=True)
    users = CanManageOrder.users().filter(pk__in=organization_user_pks)
    for user in users:
        mail_subject = f'Создание заказа {order.get_order_number()}'
        context = {
            'site_url': settings.SITE_URL,
            'order': order,
            'user': user
        }
        message = render_to_string('order/email/create_order_manager.html', context=context, request=request)
        email = OrganizationEmailMultiAlternatives(subject=mail_subject, organization=order.organization, to=[user.email])
        email.attach_alternative(message, 'text/html')
        email.attach(organization_logo_data(order.organization))
        email.send()


def create_order_customer_notify(request, order):

    mail_subject = f'Квитанция к заказу №{order.get_order_number()}'
    payment_url = request.build_absolute_uri(reverse('presentation:public:online_payment', args=(order.pk,)))
    context = {
        'site_url': settings.SITE_URL,
        'payment_url': payment_url,
        'order': order
    }
    message = render_to_string('order/email/create_order_customer.html', context=context, request=request)

    email = OrganizationEmailMultiAlternatives(subject=mail_subject, organization=order.organization, to=[order.email])
    email.attach_alternative(message, 'text/html')
    email.attach(organization_logo_data(order.organization))
    email.send()


def order_print_state_notify(request, order):
    mail_subject = f'Заказ №{order.get_order_number()} отправлен в печать'
    context = {
        'site_url': settings.SITE_URL,
        'order': order
    }
    message = render_to_string('order/email/order_print_state.html', context=context, request=request)

    email = OrganizationEmailMultiAlternatives(subject=mail_subject, organization=order.organization, to=[order.email])
    email.attach_alternative(message, 'text/html')
    email.attach(organization_logo_data(order.organization))
    email.send()


def order_ready_state_notify(request, order):
    mail_subject = f'Заказ №{order.get_order_number()} выполнен и доставлен'
    context = {
        'site_url': settings.SITE_URL,
        'order': order
    }
    message = render_to_string('order/email/order_ready_state.html', context=context, request=request)

    email = OrganizationEmailMultiAlternatives(subject=mail_subject, organization=order.organization, to=[order.email])
    email.attach_alternative(message, 'text/html')
    email.attach(organization_logo_data(order.organization))
    email.send()

