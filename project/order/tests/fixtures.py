import factory
from factory.django import DjangoModelFactory

from order.models import *


class OrderFactory(DjangoModelFactory):
    name = factory.Faker('name')

    class Meta:
        model = Order
