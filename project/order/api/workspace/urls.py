from django.conf.urls import include
from django.urls import path

app_name = 'order'

urlpatterns = [
    path('', include('order.api.workspace.info.urls', namespace='info')),
    path('control/', include('order.api.workspace.control.urls', namespace='control')),
]
