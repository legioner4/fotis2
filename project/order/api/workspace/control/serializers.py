import logging

from rest_framework import serializers

from order.api.workspace.info.serializers import OrderSerializer as InfoOrderSerializer

logger = logging.getLogger(__name__)


class OrderSerializer(InfoOrderSerializer):
    created_at_start = serializers.DateField(label='Дата создания (с)', read_only=True)
    created_at_end = serializers.DateField(label='Дата создания (по)', read_only=True)

