# coding: utf-8
import codecs
import logging

import django_filters
import unicodecsv as csv
from django.http import HttpResponse
from rest_framework.decorators import action

from base.restapi.permission import READ, DenyAny, UPDATE, DESTROY, CREATE
from order import models
from order import permissions
from order.api.workspace.control import serializers
from order.api.workspace.info.views import OrderView as InfoOrderView

logger = logging.getLogger(__name__)


class OrderFilter(django_filters.FilterSet):
    created_at_start = django_filters.DateFilter(method='filter_created_at_start')
    created_at_end = django_filters.DateFilter(method='filter_created_at_end')

    class Meta:
        model = models.Order
        fields = ('state', 'organization', 'created_at_start', 'created_at_end')

    def filter_created_at_start(self, queryset, name, value):
        if value:
            return queryset.filter(created_at__gte=value)
        return queryset

    def filter_created_at_end(self, queryset, name, value):
        if value:
            return queryset.filter(created_at__lte=value)

        return queryset


class OrderView(InfoOrderView):
    permission_classes = permissions.CanManageSystem
    permission_classes_map = {READ: permissions.CanManageSystem, UPDATE: permissions.CanManageSystem, DESTROY: DenyAny, CREATE: DenyAny}
    queryset = models.Order.objects
    serializer_class = serializers.OrderSerializer

    ordering_fields = (
        "order_number",
        "state",
        "fio",
        "email",
        "phone"
    )
    filter_class = OrderFilter

    def get_queryset(self):
        return models.Order.objects.exclude(state=models.Order.CREATE_STATE)

    def perform_create(self, serializer):
        serializer.save()

    @action(methods=['get'], detail=False, url_path='export-csv', url_name='export-csv')
    def export(self, request, *args, **kwargs):
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="order.csv"'
        response.write(codecs.BOM_UTF8)

        writer = csv.writer(response, delimiter=';', encoding='utf-8')
        orders = self.get_selected_records(request)
        writer.writerow(['№', 'Организация', 'Дата создания', 'ФИО', 'Email', 'Номер телефона', 'Статус', 'Оплата', 'Цена', 'Доставка', 'Примечание'])

        if orders:
            for order in orders:
                number = f'№{order.get_order_number()}'
                organization = f'{order.organization}'
                created_at = order.created_at.strftime("%Y-%m-%d %H:%M")
                fio = order.fio if order.fio else ''
                email = order.email if order.email else ''
                phone = order.phone if order.phone else ''
                state = order.get_state_display()
                payment = f'Тип: {order.payment.get_type_display()}, Статус: {order.payment.get_payment_state_display()}' if order.payment else ''
                price = f'{order.price_info["all_sum_str"]}'
                delivery = f'{order.delivery.get_delivery_type_str()}, {order.delivery.get_delivery_str()}, Цена: {order.delivery.price_info["price_str"]}'
                note = order.note if order.note else ''

                try:
                    writer.writerow([number, organization, created_at, fio, email, phone, state, payment, price, delivery, note])
                except Exception as e:
                    logger.error([number, organization, created_at, fio, email, phone, state, payment, price, delivery, note])
        return response


class NotCompletedOrderView(OrderView):

    def get_queryset(self):
        return models.Order.objects.filter(state=models.Order.CREATE_STATE)
