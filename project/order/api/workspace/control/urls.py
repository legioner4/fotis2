from rest_framework import routers

from . import views

app_name = 'order'

router = routers.DefaultRouter()

router.register(r'order', views.OrderView, 'order')
router.register(r'not-completed-orders', views.NotCompletedOrderView, 'not_completed_order')

urlpatterns = []
urlpatterns += router.urls
