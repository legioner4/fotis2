# coding: utf-8
import codecs
import logging

import django_filters
import unicodecsv as csv
from django.http import HttpResponse
from django.utils.http import urlquote
from rest_framework.decorators import action

from base.restapi.permission import READ, DenyAny, UPDATE, DESTROY, CREATE
from base.restapi.viewsets import BaseModelViewSet
from order import models
from order import permissions
from order.api.workspace.info import serializers
from order.logic.actions import create_order_photos_zip, after_download

logger = logging.getLogger(__name__)


class OrderFilter(django_filters.FilterSet):
    created_at_start = django_filters.DateFilter(method='filter_created_at_start')
    created_at_end = django_filters.DateFilter(method='filter_created_at_end')

    class Meta:
        model = models.Order
        fields = ('created_at_start', 'created_at_end', 'state')

    def filter_created_at_start(self, queryset, name, value):
        if value:
            return queryset.filter(created_at__gte=value)
        return queryset

    def filter_created_at_end(self, queryset, name, value):
        if value:
            return queryset.filter(created_at__lte=value)

        return queryset


class OrderView(BaseModelViewSet):
    permission_classes = permissions.CanManageOrder
    permission_classes_map = {READ: permissions.CanManageOrder, UPDATE: permissions.CanManageOrder, DESTROY: DenyAny, CREATE: DenyAny}
    queryset = models.Order.objects
    serializer_class = serializers.OrderSerializer

    ordering_fields = (
        "order_number",
        "state",
        "fio",
        "email",
        "phone"
    )
    filter_class = OrderFilter

    def get_queryset(self):
        # order = super().get_queryset().get(pk=27)
        # create_order_customer_notify(self.request, order)
        return super().get_queryset().filter(organization=self.request.user.organization).exclude(state=models.Order.CREATE_STATE)

    def perform_create(self, serializer):
        serializer.save(organization=self.request.user.organization)

    @action(detail=True)
    def download(self, *args, **kwargs):
        order = self.get_object()

        if not order.zip_file:
            create_order_photos_zip(order)

        filename = f"{order.get_order_number()}.zip"
        filename = urlquote(filename.encode('utf-8'))
        response = HttpResponse(order.zip_file.read())
        disposition = 'filename*=UTF-8\'\'%s' % filename
        response['Content-Disposition'] = 'attachment; %s' % disposition
        response['Content-Type'] = "application/x-zip-compressed"
        after_download(order)
        return response

    @action(methods=['get'], detail=False, url_path='export-csv', url_name='export-csv')
    def export(self, request, *args, **kwargs):
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="order.csv"'
        response.write(codecs.BOM_UTF8)

        writer = csv.writer(response, delimiter=';', encoding='utf-8')
        orders = self.get_selected_records(request)
        writer.writerow(
            ['№', 'Дата создания', 'ФИО', 'Email', 'Номер телефона', 'Статус', 'Оплата', 'Цена',
             'Доставка', 'Примечание'])

        if orders:
            for order in orders:
                number = f'№{order.get_order_number()}'
                created_at = order.created_at.strftime("%Y-%m-%d %H:%M")
                fio = order.fio if order.fio else ''
                email = order.email if order.email else ''
                phone = order.phone if order.phone else ''
                state = order.get_state_display()
                payment = f'Тип: {order.payment.get_type_display()}, Статус: {order.payment.get_payment_state_display()}' if order.payment else ''
                price = f'{order.price_info["all_sum_str"]}'
                delivery = f'{order.delivery.get_delivery_type_str()}, {order.delivery.get_delivery_str()}, Цена: {order.delivery.price_info["price_str"]}'
                note = order.note if order.note else ''

                try:
                    writer.writerow(
                        [number, created_at, fio, email, phone, state, payment, price, delivery, note])
                except Exception as e:
                    logger.error([number, created_at, fio, email, phone, state, payment, price, delivery, note])
        return response
