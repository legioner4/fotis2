import logging
from rest_framework import serializers
from base.restapi.serializer import BaseSerializer, remove_fields
from catalog.models import DeliveryType
from order import models
from order.logic.actions import change_order_state

logger = logging.getLogger(__name__)


class OrderDeliverySerializer(BaseSerializer):
    price_info = serializers.SerializerMethodField(label='Цена', read_only=True)

    class Meta:
        model = models.OrderDelivery
        fields = "__all__"

    def __init__(self, *args, delivery_type=None, **kwargs):
        super().__init__(*args, **kwargs)
        if self.instance and self.instance.pk:
            not_remove_field_list = ['price_info', 'delivery_name']
            if self.instance.delivery_type_code == DeliveryType.POST_TYPE:
                not_remove_field_list += ['city', 'post_index', 'street', 'house_number', 'corps', 'apartment_number']
            elif self.instance.delivery_type_code == DeliveryType.COURIER_TYPE:
                not_remove_field_list += ['delivery_region_name', 'city', 'street', 'house_number', 'corps', 'apartment_number']
            elif self.instance.delivery_type_code == DeliveryType.PICKUP_TYPE:
                not_remove_field_list += ['subdivision_name', 'subdivision_address']

            remove_field_list = []
            for field_name, field in self.fields.items():
                if field_name not in not_remove_field_list:
                    remove_field_list.append(field_name)

            remove_fields(self, *remove_field_list)

    def get_price_info(self, order_delivery):
        return order_delivery.price_info


class OrderPaymentSerializer(BaseSerializer):
    payment_type_name = serializers.CharField(read_only=True, source='name')
    payment_state_display = serializers.CharField(read_only=True, source='get_payment_state_display')

    class Meta:
        model = models.OrderPayment
        fields = ['payment_type', 'payment_type_name', 'payment_state', 'payment_state_display']


class OrderSerializer(BaseSerializer):
    display_order_number = serializers.CharField(label='Номер заказа', read_only=True, source='get_order_number')
    client_name = serializers.CharField(label='Клиент', read_only=True, source='client')
    client_key = serializers.CharField(label='Клиент', read_only=True)
    state_name = serializers.CharField(label='Статус', read_only=True, source='get_state_display')
    delivery_info = serializers.SerializerMethodField(label='Доставка')
    price_info = serializers.SerializerMethodField(label='Цена', read_only=True)
    photo_group_info = serializers.SerializerMethodField(label='Группировка фото', read_only=True)
    payment_info = serializers.SerializerMethodField(label='Оплата')
    created_at_start = serializers.DateField(label='Дата создания (с)', read_only=True)
    created_at_end = serializers.DateField(label='Дата создания (по)', read_only=True)

    class Meta:
        model = models.Order
        fields = "__all__"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if 'state' in self.fields:
            self.fields['state'].choices = [(state, name) for state, name in models.Order.STATE if state > 0]
            # for state in self.fields['state'].choices:
            # self.fields['state'].choices = [state for state in self.fields['state'].choices]

    def get_delivery_info(self, order):
        if order.delivery:
            delivery_serializer = OrderDeliverySerializer(instance=order.delivery)
            return delivery_serializer.data
        return {}

    def get_payment_info(self, order):
        if order.payment:
            payment_serializer = OrderPaymentSerializer(instance=order.payment)
            return payment_serializer.data
        return {}

    def get_price_info(self, order):
        return order.price_info

    def get_photo_group_info(self, order):
        return order.photo_group_info

    def update(self, instance, validated_data):
        old_state = instance.state
        instance = super().update(instance, validated_data)
        new_state = instance.state
        if new_state != old_state:
            change_order_state(self.request, instance)
        return instance
