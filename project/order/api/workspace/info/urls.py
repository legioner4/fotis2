from rest_framework import routers
from rest_framework_nested.routers import NestedSimpleRouter

from . import views

app_name = 'order'

router = routers.DefaultRouter()

router.register(r'order', views.OrderView, 'order')

urlpatterns = []
urlpatterns += router.urls
