# coding: utf-8
import logging

import django_filters
from django_filters import rest_framework as filters
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet
from django.http import HttpResponse
from base.restapi.permission import DenyAny, AllowAny
from base.restapi.viewsets import BaseModelViewSet
from order import models
from organization.models import Organization
from .export import OrderGenerator
from .serializers import OrderSerializer

logger = logging.getLogger(__name__)


class OrderFilter(django_filters.FilterSet):
    fromdate = django_filters.DateFilter(method='filter_fromdate')

    class Meta:
        model = models.Order
        fields = ('fromdate',)

    def filter_fromdate(self, queryset, name, value):
        if value:
            return queryset.filter(created_at__gte=value)
        return queryset


class ExportOrderView(ModelViewSet):
    permission_classes = [AllowAny]
    queryset = models.Order.objects
    serializer_class = OrderSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = OrderFilter

    @property
    def export_organization(self):
        key = self.request.GET.get('key', None)
        return Organization.objects.filter(export_key=key).first() if key else None

    def get_queryset(self):
        if not self.export_organization:
            queryset = models.Order.objects.none()
        else:
            queryset = super().get_queryset()
            queryset = self.filter_queryset(queryset)
            queryset = queryset.exclude(state=models.Order.CREATE_STATE).filter(organization=self.export_organization)
        return queryset

    @action(detail=False, methods=['GET'])
    def xml(self, request):
        orders = self.get_queryset()
        generator = OrderGenerator(orders)
        xml_document = generator.generate()
        response = HttpResponse(content=xml_document, content_type='text/xml')
        response['Content-Disposition'] = f'attachment; filename="{generator.filename}"'
        return response
