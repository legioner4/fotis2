from typing import Dict, Optional, List

from django.core.files.base import ContentFile
from django.utils.functional import cached_property
# noinspection PyProtectedMember
from lxml import etree
from pydantic import (ValidationError, validator, BaseModel)
from order.models import Photo


class DocumentNode(BaseModel):
    """Узел документа"""
    name: str
    attrib: Dict[str, str] = {}
    childrens: Optional[List] = None
    text: Optional[str] = ''

    @classmethod
    @validator('childrens')
    def is_childrens_correct(cls, value):
        """Проверяем что все элементы childrens унаследованы от DocumentNode"""
        if not all([isinstance(i, cls.__class__) for i in value]):
            msg: str = f'Дочерние узлы должны быть унаследованы от {cls.__class__.__name__}'
            raise ValidationError(errors=[msg], model=cls)
        return value


class BaseServiceDocumentGenerate:

    def generate(self) -> ContentFile:
        def _generate_root(parent: etree.Element, childs_: List[DocumentNode]):
            """Метод для генерации потомка узла"""
            for child_node in childs_:
                child = etree.SubElement(
                    _parent=parent,
                    _tag=child_node.name,
                    attrib=child_node.attrib,
                )
                child.text = child_node.text or None
                if child_node.childrens:
                    # рекурсивно генерируем документ
                    _generate_root(parent=child, childs_=child_node.childrens)

        _generate_root(self.root, self.nodes)
        body: str = etree.tounicode(element_or_tree=self.root, pretty_print=True, )
        stream: bytes = f'{self.header}{body}'.encode(self.encoding)
        return ContentFile(content=stream, name=self.filename)

    @cached_property
    def header(self) -> str:
        return f'<?xml version="{self.version}" encoding="{self.encoding}"?>'

    @cached_property
    def encoding(self) -> str:
        """Кодировка XML-документа"""
        return 'windows-1251'

    @cached_property
    def version(self) -> str:
        return '1.0'

    @cached_property
    def filename(self) -> str:
        """Наименование сгенерированного XML-документа"""
        raise NotImplemented

    # noinspection PyTypeChecker
    @cached_property
    def root(self) -> etree.Element:
        """Наименование корневого узла"""
        raise NotImplemented

    # noinspection PyTypeChecker
    @cached_property
    def nodes(self) -> List[DocumentNode]:
        """Узлы дерева документа"""
        raise NotImplemented


class OrderGenerator(BaseServiceDocumentGenerate):
    orders = None

    def __init__(self, orders):
        self.orders = orders

    @cached_property
    def filename(self) -> str:
        return 'order.xml'

    @cached_property
    def encoding(self) -> str:
        return 'utf-8'

    @cached_property
    def root(self) -> etree.Element:
        document_node: DocumentNode = DocumentNode(
            name='response',
        )
        return etree.Element(_tag=document_node.name, attrib=document_node.attrib)

    def OrderNumber(self, order) -> DocumentNode:
        return DocumentNode(
            name='number',
            text=order.get_order_number()
        )

    def OrderCreatedDate(self, order) -> DocumentNode:
        return DocumentNode(
            name='created_date',
            text=order.created_at.strftime('%d-%m-%y %H:%M:%I')
        )

    def OrderUpdateDate(self, order) -> DocumentNode:
        return DocumentNode(
            name='updated_date',
            text=order.updated_at.strftime('%d-%m-%y %H:%M:%I')
        )

    def OrderComment(self, order) -> DocumentNode:
        return DocumentNode(
            name='comment',
            text=order.note
        )

    def OrderPaid(self, order) -> DocumentNode:
        payment = 1 if order.payment.has_payment() else 2
        return DocumentNode(
            name='paid',
            text=payment
        )

    def OrderCost(self, order) -> DocumentNode:
        return DocumentNode(
            name='cost',
            text=order.price_info['all_sum']
        )

    def OrderMarginCost(self, order) -> DocumentNode:
        return DocumentNode(
            name='margin_cost',
            text='0.00'
        )

    def OrderWrittenOffBonuses(self, order) -> DocumentNode:
        return DocumentNode(
            name='written_off_bonuses',
            text='0.00'
        )

    def OrderClient(self, order) -> DocumentNode:
        return DocumentNode(
            name='client',
            attrib={
                'id': order.client.pk
            },
            childrens=[
                DocumentNode(
                    name='phone',
                    text=order.phone
                ),
                DocumentNode(
                    name='email',
                    text=order.email
                ),
                DocumentNode(
                    name='full_name',
                    text=order.fio
                )
            ]
        )

    def OrderDelivery(self, order) -> DocumentNode:
        return DocumentNode(
            name='delivery',
            attrib={
                'id': order.delivery.pk
            },
            childrens=[
                DocumentNode(
                    name='name',
                    text=order.delivery.get_delivery_type_str()
                ),
                DocumentNode(
                    name='address',
                    text=order.delivery.get_delivery_str()
                ),
            ]
        )

    def OrderPayment(self, order) -> DocumentNode:
        return DocumentNode(
            name='payment',
            attrib={
                'id': order.payment.pk
            },
            childrens=[
                DocumentNode(
                    name='name',
                    text=order.payment.get_type_display()
                ),
            ]
        )

    def OrderStatus(self, order) -> DocumentNode:
        return DocumentNode(
            name='status',
            attrib={
                'id': order.state
            },
            childrens=[
                DocumentNode(
                    name='name',
                    text=order.get_state_display()
                ),
            ]
        )

    def PhotoProperty(self, photo) -> DocumentNode:
        return DocumentNode(
            name='properties',
            childrens=[
                DocumentNode(
                    name='format',
                    attrib={
                        'id': photo.format.pk
                    },
                    childrens=[
                        DocumentNode(
                            name='name',
                            text=photo.format.format_name
                        ),
                    ]
                ),
                DocumentNode(
                    name='paper',
                    attrib={
                        'id': photo.format.pk
                    },
                    childrens=[
                        DocumentNode(
                            name='name',
                            text=photo.format.paper_type_name
                        ),
                    ]
                ),
            ]
        )

    def OrderItems(self, order) -> DocumentNode:
        order_photos = []

        for group_info in order.photo_group_info:
            photo_pk = group_info['photo_pk']
            photo = Photo.objects.get(pk=photo_pk)

            order_group = DocumentNode(
                name='item',
                attrib={
                    'product_group_id': 1
                },
                childrens=[
                    DocumentNode(
                        name='name',
                        text=group_info['description']
                    ),
                    DocumentNode(
                        name='quantity',
                        text=group_info['qty']
                    ),
                    DocumentNode(
                        name='price',
                        text=group_info['price_one_item']
                    ),
                    DocumentNode(
                        name='amount',
                        text=group_info['price_sum']
                    ),
                    self.PhotoProperty(photo),
                ]
            )
            order_photos.append(order_group)

        return DocumentNode(
            name='items',
            childrens=order_photos
        )

    @cached_property
    def OrdersNode(self) -> DocumentNode:
        childrens = []

        for order in self.orders:
            order_node = DocumentNode(
                name='order',
                attrib={
                    'id': order.pk
                },
                childrens=[
                    self.OrderNumber(order),
                    self.OrderCreatedDate(order),
                    self.OrderUpdateDate(order),
                    self.OrderComment(order),
                    self.OrderPaid(order),
                    self.OrderCost(order),
                    self.OrderMarginCost(order),
                    self.OrderWrittenOffBonuses(order),
                    self.OrderClient(order),
                    self.OrderDelivery(order),
                    self.OrderPayment(order),
                    self.OrderStatus(order),
                    self.OrderItems(order),
                ]
            )
            childrens.append(order_node)

        return DocumentNode(
            name='orders',
            childrens=childrens
        )

    @cached_property
    def nodes(self) -> List[DocumentNode]:
        return [self.OrdersNode]
