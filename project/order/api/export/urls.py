from rest_framework import routers

from . import views

app_name = 'order'

router = routers.DefaultRouter()

router.register(r'order', views.ExportOrderView, 'order')

urlpatterns = []
urlpatterns += router.urls
