import logging

from base.restapi.serializer import BaseSerializer
from order import models

logger = logging.getLogger(__name__)


class OrderSerializer(BaseSerializer):

    class Meta:
        model = models.Order
        fields = "__all__"
