import decimal
import os
from threading import Lock

from django.core.files import File
from django.db import models

from base.models import BaseModel
from catalog.logic.getters import get_delivery_price
from catalog.models import DeliveryType
from order.logic.actions import get_photo_upload_path, get_source_photo_upload_path, get_zip_upload_path
from organization.logic.getters import value_convert_organization_currency
from organization.models import PaymentType
from . import managers

unique_number_lock = Lock()


class Order(BaseModel):
    CREATE_STATE = 0
    NEW_STATE = 1
    LOADED_STATE = 5
    INPRINT_STATE = 10
    SUBMITTED_STATE = 15
    READY_STATE = 20
    ISSUED_STATE = 25
    CANCELED_STATE = 30
    STATE = (
        (CREATE_STATE, 'Создание заказа'),
        (NEW_STATE, 'Новый'),
        (LOADED_STATE, 'Загружен'),
        (INPRINT_STATE, 'В печати'),
        (SUBMITTED_STATE, 'Отправлен'),
        (READY_STATE, 'Готов к выдаче'),
        (ISSUED_STATE, 'Выдан'),
        (CANCELED_STATE, 'Отменен'),
    )

    order_number = models.PositiveIntegerField(verbose_name='Номер заказа', null=True, blank=True, editable=False)
    organization = models.ForeignKey('organization.Organization', models.CASCADE, related_name='orders', verbose_name='Организация')
    client = models.ForeignKey('organization.Client', models.CASCADE, related_name='orders', verbose_name='Клиент')
    state = models.IntegerField(verbose_name='Статус', choices=STATE, default=CREATE_STATE)
    delivery = models.ForeignKey('OrderDelivery', models.CASCADE, verbose_name='Доставка', null=True, blank=True)
    payment = models.ForeignKey('OrderPayment', models.CASCADE, verbose_name='Оплата', null=True, blank=True)
    total_price = models.DecimalField(verbose_name='Итоговая цена (без доставки)', max_digits=6, decimal_places=2, default=0)
    discount_sum = models.DecimalField(verbose_name='Сумма скидки', max_digits=6, decimal_places=2, default=0)
    price_without_discount = models.DecimalField(verbose_name='Цена без скидки', max_digits=6, decimal_places=2, default=0)

    fio = models.CharField('ФИО', max_length=256, blank=True, null=True)
    email = models.EmailField('Адрес электронной почты', max_length=256, blank=True, null=True)
    phone = models.CharField('Номер телефона', max_length=256, blank=True, null=True)
    note = models.TextField(verbose_name='Примечание', null=True, blank=True)

    zip_file = models.FileField(verbose_name='Zip файл заказа', upload_to=get_zip_upload_path, null=True, blank=True, max_length=1024)
    photos_deleted = models.BooleanField(verbose_name='Фотографии удалены', default=False)

    objects = managers.OrderManager()

    class Meta:
        verbose_name = 'заказ'
        verbose_name_plural = 'заказы'
        ordering = ('-order_number', '-created_at')

    def __str__(self):
        return f'{self.client} {self.created_at}'

    def save(self, *args, **kwargs):
        if not self.order_number:
            global unique_number_lock
            with unique_number_lock:
                self.order_number = Order.objects.get_next_order_number(self.organization)
        return super().save(*args, **kwargs)

    def get_order_number(self):
        return f'AP{str(self.created_at.year)[2:]}{self.created_at.month}{self.created_at.day}-{self.organization.pk}-{self.order_number}'

    @property
    def creation_state(self):
        return self.state == self.CREATE_STATE

    @property
    def photo_group_info(self):
        info = []
        for group in self.groups.all():
            photo = group.photos.first()

            info.append({
                'photo_pk': photo.pk,
                'description': photo.description(),
                'qty': group.qty,
                'price_one_item_str': value_convert_organization_currency(self.organization, group.price),
                'price_one_item': group.price,
                'price_sum_str': value_convert_organization_currency(self.organization, group.price_sum),
                'price_sum': group.price_sum
            })

        return info

    @property
    def price_info(self):
        all_sum = self.delivery.price + self.total_price
        all_sum_penny = all_sum * 100
        return {
            'total_price_str': value_convert_organization_currency(self.organization, self.total_price),
            'total_price': self.total_price,

            'discount_sum_str': value_convert_organization_currency(self.organization, self.discount_sum),
            'discount_sum': self.discount_sum,

            'price_without_discount_str': value_convert_organization_currency(self.organization, self.price_without_discount),
            'price_without_discount': self.price_without_discount,

            'delivery_price_str': value_convert_organization_currency(self.organization, self.delivery.price),
            'delivery_price': self.delivery.price,

            'all_sum_str': value_convert_organization_currency(self.organization, all_sum),
            'all_sum': all_sum,
            'all_sum_penny': all_sum_penny
        }


class PhotoGroup(BaseModel):
    order = models.ForeignKey(Order, models.CASCADE, related_name='groups', verbose_name='Заказ')
    photos = models.ManyToManyField('order.Photo', related_name='Фотографии', blank=True)
    qty = models.PositiveIntegerField("Кол-во изображений", default=0)

    price_one_photo = models.DecimalField(verbose_name='Цена за 1 фото', max_digits=6, decimal_places=2, default=0)
    price_one_discount_photo = models.DecimalField(verbose_name='Цена за 1 фото (учитывая скидку)', max_digits=6, decimal_places=2, null=True, blank=True)

    class Meta:
        verbose_name = 'группа фото'
        verbose_name_plural = 'группа фото'
        ordering = ('created_at',)

    def __str__(self):
        return f'{self.price}'

    @property
    def price(self):
        return self.price_one_discount_photo if self.price_one_discount_photo else self.price_one_photo

    @property
    def price_sum(self):
        return self.qty * decimal.Decimal(self.price)


class PhotoFormat(BaseModel):
    format = models.ForeignKey('catalog.PhotoFormat', models.SET_NULL, verbose_name='цена', null=True, blank=True)

    paper_type_name = models.CharField(verbose_name='Наименование типа бумаги', max_length=256)
    paper_type_slug = models.CharField(max_length=256, null=True, blank=True)
    format_name = models.CharField(verbose_name='Наименование формата', max_length=256)

    width = models.PositiveIntegerField(verbose_name='Ширина (мм)', null=True, blank=True)
    height = models.PositiveIntegerField(verbose_name='Высота (мм)', null=True, blank=True)

    polaroid_frame_width = models.PositiveIntegerField(verbose_name='Ширина поля (мм)')
    polaroid_frame_bottom_width = models.PositiveIntegerField(verbose_name='Ширина нижнего поля (мм)')

    instagram_frame_width = models.PositiveIntegerField(verbose_name='Ширина поля по периметру (мм)')

    class Meta:
        verbose_name = 'формат фото заказа'
        verbose_name_plural = 'формат фото заказа'
        ordering = ('created_at',)

    def __str__(self):
        return f'{self.format_name}'


class PhotoPrice(BaseModel):
    format_price = models.ForeignKey('catalog.FormatPrice', models.SET_NULL, verbose_name='цена', null=True, blank=True)
    format_price_pk = models.IntegerField(default=0)

    class Meta:
        verbose_name = 'цена фото заказа'
        verbose_name_plural = 'цена фото заказа'
        ordering = ('created_at',)

    def __str__(self):
        return f'{self.format_price_pk}'


class Photo(BaseModel):
    NO_FRAME = 'not'
    INSTAGRAMM_FRAME = 'instagram'
    POLAROID_FRAME = 'polaroid'
    FRAMES = (
        (NO_FRAME, 'Без рамки'),
        (INSTAGRAMM_FRAME, 'Instagram'),
        (POLAROID_FRAME, 'Polaroid'),
    )

    order = models.ForeignKey(Order, models.CASCADE, related_name='photos', verbose_name='Заказ')
    price = models.ForeignKey(PhotoPrice, models.PROTECT, verbose_name='цена', null=True, blank=True)
    format = models.ForeignKey(PhotoFormat, models.PROTECT, verbose_name='формат', null=True, blank=True)

    image = models.ImageField("Изображение", upload_to=get_photo_upload_path, max_length=1024)
    source_image = models.ImageField("Исходное изображение", upload_to=get_source_photo_upload_path, max_length=1024)
    qty = models.PositiveIntegerField("Кол-во изображений", default=0)
    border = models.BooleanField("С бордером", default=False)
    frame = models.CharField("Рамка", choices=FRAMES, default=NO_FRAME, max_length=32)
    crop_x = models.PositiveIntegerField("x для обрезания", null=True, blank=True)
    crop_y = models.PositiveIntegerField("y для обрезания", null=True, blank=True)
    crop_w = models.PositiveIntegerField("width для обрезания", null=True, blank=True)
    crop_h = models.PositiveIntegerField("height для обрезания", null=True, blank=True)

    class Meta:
        verbose_name = 'фотография'
        verbose_name_plural = 'фотографии'
        ordering = ('created_at',)

    def __str__(self):
        return f'{self.image} {self.qty}'

    def save_source_image(self):
        self.source_image = File(self.image, os.path.basename(self.image.path))
        self.save()

    def description(self):
        parts = []
        if self.format:
            parts.append(f'Формат: {self.format.format_name}')
            parts.append(f'Тип бумаги: {self.format.paper_type_name}')
        return ', '.join(parts)

    def price_per_count(self):
        return self.description()


class OrderDelivery(BaseModel):
    delivery_type = models.ForeignKey('catalog.DeliveryType', models.SET_NULL, related_name='order_delivery', verbose_name='Тип доставки', null=True, blank=True)
    organization = models.ForeignKey('organization.Organization', models.CASCADE, verbose_name='Организация', null=True, blank=True)
    delivery_name = models.CharField(verbose_name='Наименование доставки', max_length=512, null=True, blank=True)
    delivery_type_code = models.CharField(verbose_name='Тип доставки', max_length=512, null=True, blank=True, choices=DeliveryType.TYPES)
    delivery_cost = models.DecimalField(verbose_name='Стоимость доставки', max_digits=6, decimal_places=2, default=0)
    delivery_description = models.CharField(verbose_name='Описание', max_length=512, null=True, blank=True)

    city = models.CharField(verbose_name='Город', max_length=512, null=True, blank=True)
    street = models.CharField(verbose_name='Улица', max_length=512, null=True, blank=True)
    house_number = models.CharField(verbose_name='Номер дома', max_length=512, null=True, blank=True)
    corps = models.CharField(verbose_name='Корпус', max_length=512, null=True, blank=True)
    apartment_number = models.CharField(verbose_name='Номер квартиры или офиса', max_length=512, null=True, blank=True)

    post_index = models.CharField(verbose_name='Почтовый индекс', max_length=512, null=True, blank=True)

    delivery_region = models.ForeignKey('catalog.DeliveryRegion', models.SET_NULL, verbose_name='Регион доставки', null=True, blank=True)
    delivery_region_name = models.CharField(verbose_name='Регион доставки', max_length=512, null=True, blank=True)

    subdivision = models.ForeignKey('organization.Subdivision', models.SET_NULL, verbose_name='Фотоцентр', null=True, blank=True)
    subdivision_name = models.CharField(verbose_name='Фотоцентр', max_length=512, null=True, blank=True)
    subdivision_address = models.CharField(verbose_name='Адрес фотоцентра', max_length=512, null=True, blank=True)

    transport_company_point = models.ForeignKey('catalog.TransportCompanyPoint', models.PROTECT, verbose_name='Пункт выдачи транспортной компании', null=True, blank=True)

    price = models.DecimalField(verbose_name='Цена доставки', max_digits=6, decimal_places=2, default=0, null=True, blank=True)

    class Meta:
        verbose_name = 'доставка заказа'
        verbose_name_plural = 'доставки заказов'
        ordering = ('created_at',)

    def __str__(self):
        return f'{self.delivery_name}'

    def calculate_delivery_price(self):
        """Вызывать после activate_order"""
        delivery_region_id = self.delivery_region.pk if self.delivery_region else None
        price = get_delivery_price(self.delivery_type.pk, delivery_region_id)
        self.price = price
        self.save()

    @property
    def price_info(self):
        return {
            'price_str': value_convert_organization_currency(self.organization, self.price),
            'price': self.price
        }

    def get_delivery_type_str(self):
        if self.delivery_type_code == 'post':
            return "Доставка почтой"
        elif self.delivery_type_code == 'courier':
            return "Доставка курьером"
        elif self.delivery_type_code == 'pickup':
            return "Самовывоз из фотоцентра"
        elif self.delivery_type_code == 'transport_company':
            return "Транспортная компания"
        elif self.delivery_type_code == 'random_delivery':
            return self.delivery_name

    def get_delivery_str(self):
        parts = []
        if self.delivery_type_code == 'post':
            if self.post_index:
                parts.append(f'Индекс: {self.post_index}')
            if self.city:
                parts.append(f'г. {self.city}')
            if self.street:
                parts.append(f'ул. {self.street}')
            if self.house_number:
                parts.append(f'{self.house_number}')
            if self.corps:
                parts.append(f'{self.corps}')
            if self.apartment_number:
                parts.append(f'{self.apartment_number}')
        elif self.delivery_type_code == 'courier':
            if self.city:
                parts.append(f'г. {self.city}')
            if self.street:
                parts.append(f'ул. {self.street}')
            if self.house_number:
                parts.append(f'{self.house_number}')
            if self.corps:
                parts.append(f'{self.corps}')
            if self.apartment_number:
                parts.append(f'г. {self.apartment_number}')
        elif self.delivery_type_code == 'pickup':
            if self.subdivision_name:
                parts.append(f'{self.subdivision_name}')
            if self.subdivision.address:
                parts.append(f'{self.subdivision.address}')
            if self.subdivision.phone_list:
                for phone in self.subdivision.phone_list:
                    parts.append(f'{phone}')
        elif self.delivery_type_code == 'transport_company':
            if self.transport_company_point.address:
                parts.append(f'{self.transport_company_point.address}')
        elif self.delivery_type_code == 'random_delivery':
            pass
        return ', '.join(parts)


class OrderPayment(BaseModel):
    payment_type = models.ForeignKey('organization.PaymentType', models.SET_NULL, related_name='order_payment_type', null=True, blank=True, verbose_name='Тип оплаты')

    name = models.CharField(verbose_name='Название', max_length=512, null=True, blank=True)
    type = models.CharField(verbose_name='Тип оплаты', choices=PaymentType.PAYMENT_TYPES, max_length=64, null=True, blank=True)
    description = models.CharField(verbose_name='Описание', max_length=512, null=True, blank=True)

    NOT_PAYMENT = 'not_payment'
    FAIL_PAYMENT = 'fail_payment'
    SBER_PAYMENT = 'sber_payment'
    PAYMENT_STATE = (
        (NOT_PAYMENT, 'Не оплачено'),
        (FAIL_PAYMENT, 'Ошибка при оплате'),
        (SBER_PAYMENT, 'Оплачено через СберБанк'),
    )
    payment_state = models.CharField(verbose_name='Статус оплаты', choices=PAYMENT_STATE, default=NOT_PAYMENT, max_length=64, null=True, blank=True)

    class Meta:
        verbose_name = 'оплата заказа'
        verbose_name_plural = 'оплата заказов'
        ordering = ('created_at',)

    def __str__(self):
        return f'{self.name}'

    def has_payment(self):
        return False if self.payment_state == self.NOT_PAYMENT else True
