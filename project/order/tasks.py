import logging
from datetime import datetime, timedelta

import os
from celery import shared_task
from django.conf import settings

logger = logging.getLogger(__name__)


@shared_task
def delete_order_photo():
    from order import models

    filter_time = datetime.now() - timedelta(days=settings.DELETE_PHOTO_DAY)
    orders = models.Order.objects.filter(photos_deleted=False).filter(created_at__lte=filter_time)
    for order in orders:
        for photo in order.photos.all():
            try:
                os.remove(photo.image.path)
            except FileNotFoundError:
                pass
            try:
                os.remove(photo.source_image.path)
            except FileNotFoundError:
                pass

        if order.zip_file:
            try:
                os.remove(order.zip_file.path)
            except FileNotFoundError:
                pass

        order.photos_deleted = True
        order.save()
        logger.error(f'Удаление фотографий заказа №{order.get_order_number()} дата удаления {datetime.now().strftime("%d.%m.%y %H:%M")} дата создания {order.created_at.strftime("%d.%m.%y %H:%M")}')


@shared_task
def task_create_order_photos_zip(order_pk):
    from order.logic.actions import create_order_photos_zip
    from order import models

    order = models.Order.objects.get(pk=order_pk)
    create_order_photos_zip(order)
