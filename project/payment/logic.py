import logging

import requests
from django.urls import reverse

from base.global_request import get_request
from order.models import OrderPayment, Order
from payment.models import Payment

logger = logging.getLogger(__name__)


NOT_ORDER = 'not_order'
ERROR = 'error'
UNPAID = 'unpaid'
SUCCESS = 'success'
PROGRESS = 'progress'
FAIL = 'fail'


class PaymentException(Exception):
    pass


def get_order_last_payment(order):
    return order.payments.last()


def set_create_error_state(payment, error):
    payment.state = Payment.CREATE_ERROR_STATE
    payment.error = error
    payment.save()


def set_progress_state(payment, url=None):
    payment.state = Payment.PROGRESS_STATE
    if url:
        payment.url = url
    payment.save()

    payment.order.payment.payment_state = OrderPayment.NOT_PAYMENT
    payment.order.payment.save()


def payment_change_success_state(payment):
    payment.state = Payment.SUCCESS_STATE
    payment.save()

    payment.order.payment.payment_state = OrderPayment.SBER_PAYMENT
    payment.order.payment.save()


def payment_change_fail_state(payment):
    payment.state = Payment.FAIL_STATE
    payment.save()

    payment.order.payment.payment_state = OrderPayment.FAIL_PAYMENT
    payment.order.payment.save()


def get_return_and_fail_urls(payment):
    request = get_request()
    return_url = request.build_absolute_uri(reverse('presentation:public:success_payment', args=(payment.pk,)))
    fail_url = request.build_absolute_uri(reverse('presentation:public:fail_payment', args=(payment.pk,)))
    return return_url, fail_url


def can_pay_sber_payment(payment):
    organization = payment.order.organization
    sber_payment = organization.get_sber_payment()
    if organization.has_payment_method_sber() and sber_payment and sber_payment.login and sber_payment.password:
        return True
    return False


def check_payment_order(order):
    if order.state == Order.CREATE_STATE:
        raise PaymentException('Заказ не завершен')

    if order.payments.filter(state__in=[Payment.SUCCESS_STATE]).exclude():
        raise PaymentException('Заказ ранее был оплачен')


def get_sber_base_payment_url(demo):
    if demo:
        return 'https://3dsec.sberbank.ru'
    return 'https://securepayments.sberbank.ru'


def create_sber_payment(payment, os_type=None):

    if not can_pay_sber_payment(payment):
        raise PaymentException('Не настроена оплата через сбербанк')

    organization = payment.order.organization
    sber_payment = organization.get_sber_payment()
    order = payment.order
    amount = order.price_info.get('all_sum_penny')
    return_url, fail_url = get_return_and_fail_urls(payment)
    data = {
        'userName': sber_payment.login,
        'password': sber_payment.password,
        'orderNumber': payment.get_order_number(),
        'amount': int(amount),
        'returnUrl': return_url,
        'failUrl': fail_url,
        'description': order.get_order_number(),
    }

    if sber_payment.app_deeplink and os_type:
        data['jsonParams'] = '{"app2app":"true","app.osType":"' + os_type + '","app.deepLink":"' + sber_payment.app_deeplink + '"}'

    base_url = get_sber_base_payment_url(sber_payment.demo)
    response = requests.post(f'{base_url}/payment/rest/register.do', data=data)
    try:
        response.raise_for_status()
    except Exception as e:
        set_create_error_state(payment, e)
        logger.error(f'Создание оплаты. ошибка при отправке: {e}')
        raise PaymentException(e)

    response_data = response.json()
    logger.error(f'Создание оплаты. отправляемые данные: {data}, пришедшие данные: {response_data}')

    if 'orderId' not in response_data:
        error = response_data['errorMessage']
        set_create_error_state(payment, error)
        logger.error(f'Создание оплаты. ошибка сбербанка: {error}')
        raise PaymentException(error)

    url = response_data.get('formUrl', None)
    set_progress_state(payment, url)
    return url


def check_state_payment(payment):
    if payment.is_success_state:
        return SUCCESS

    organization = payment.order.organization
    sber_payment = organization.get_sber_payment()
    data = {
        'userName': sber_payment.login,
        'password': sber_payment.password,
        'orderNumber': payment.get_order_number(),
    }
    base_url = get_sber_base_payment_url(sber_payment.demo)
    response = requests.post(f'{base_url}/payment/rest/getOrderStatusExtended.do', data=data)
    try:
        response.raise_for_status()
    except Exception as e:
        return ERROR

    response_data = response.json()
    logger.error(f'Проверка статуса оплаты. отправляемые данные: {data}, пришедшие данные: {response_data}')

    if response_data['orderStatus'] == '0':
        error = response_data['errorMessage']
        logger.error(f'Проверка статуса оплаты. ошибка: {error}')
        return ERROR

    order_status = response_data.get('orderStatus', None)

    SBER_SUCCESS_STATES = [1, 2]
    SBER_PROGRESS_STATES = [0]
    if order_status in SBER_SUCCESS_STATES:
        payment_change_success_state(payment)
        return SUCCESS

    elif order_status in SBER_PROGRESS_STATES:
        set_progress_state(payment)
        return PROGRESS

    else:
        payment_change_fail_state(payment)
        return FAIL


def get_or_create_order_payment(order, os_type=None):
    def create_payment(order, os_type):
        check_payment_order(order)
        payment = Payment.objects.create(order=order)
        return payment, create_sber_payment(payment, os_type)

    order_not_payments = order.payments.filter(state__in=[Payment.CREATE_STATE, Payment.PROGRESS_STATE])

    if order_not_payments.exclude():
        payment = order_not_payments.last()
        state = check_state_payment(payment)

        if state == SUCCESS:
            raise PaymentException('Заказ ранее был оплачен')

        if state == PROGRESS:
            url = payment.url

        if state in [FAIL, ERROR]:
            payment, url = create_payment(order, os_type)
    else:
        payment, url = create_payment(order, os_type)

    return payment, url
