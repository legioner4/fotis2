from django.db import models

from base.models import BaseModel


class Payment(BaseModel):
    CREATE_STATE = 'create'
    CREATE_ERROR_STATE = 'create_error'
    PROGRESS_STATE = 'progress'
    SUCCESS_STATE = 'success'
    FAIL_STATE = 'fail'

    STATES = (
        (CREATE_STATE, 'Создание оплаты'),
        (CREATE_ERROR_STATE, 'Ошибка создания оплаты'),
        (PROGRESS_STATE, 'Процесс оплаты'),
        (SUCCESS_STATE, 'Успешная оплата'),
        (FAIL_STATE, 'Неуспешная оплата'),
    )
    order = models.ForeignKey('order.Order', models.CASCADE, related_name='payments', verbose_name='Заказ')
    state = models.CharField(verbose_name='Статус', choices=STATES, default=CREATE_ERROR_STATE, max_length=256)
    error = models.TextField(verbose_name='Ошибка', null=True, blank=True)
    url = models.URLField(verbose_name='URL-адрес платёжной формы', null=True, blank=True)

    class Meta:
        verbose_name = 'Оплата'
        verbose_name_plural = 'Оплата'
        ordering = ('created_at',)

    def __str__(self):
        return f'{self.order.pk} - {self.get_state_display()}'

    @property
    def is_progress_state(self):
        return self.state == self.PROGRESS_STATE

    @property
    def is_success_state(self):
        return self.state == self.SUCCESS_STATE

    def get_order_number(self):
        return f'{self.order.get_order_number()} ({self.pk})'
