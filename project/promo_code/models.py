from django.db import models

from base.models import BaseModel


class PromoCode(BaseModel):
    organization = models.ForeignKey('organization.Organization', models.CASCADE, related_name='promo_codes', verbose_name='Организация')

    code = models.CharField(verbose_name='Код промокода', max_length=256)
    start_date = models.DateField(verbose_name='Дата начала')
    end_date = models.DateField(verbose_name='Дата окончания')

    DISCOUNT_PERCENTAGE = 'discount_percentage'
    DISCOUNT_AMOUNT = 'discount_amount'
    FIXED_PRICE = 'fixed_price'
    NUMBER_FREE_UNITS = 'number_free_units'
    PROMOCODE_TYPES = (
        (DISCOUNT_PERCENTAGE, 'Процент скидки'),
        (DISCOUNT_AMOUNT, 'Сумма скидки'),
        (FIXED_PRICE, 'Фиксированная цена'),
        (NUMBER_FREE_UNITS, 'Количество бесплатных единиц услуги'),
    )
    type = models.CharField(verbose_name='Тип промокода', choices=PROMOCODE_TYPES, max_length=64)
    value = models.PositiveIntegerField(verbose_name='Значение', default=0)

    minimum_amount = models.PositiveIntegerField(verbose_name='Минимальная сумма заказа', default=0)
    number_uses = models.PositiveIntegerField(verbose_name='Количество возможных использований', default=1)
    comment = models.CharField(verbose_name='Комментарий', max_length=512, null=True, blank=True)

    PHOTO_PRINTING = 'photo_printing'
    PHOTOBOOKS = 'photobooks'
    SOUVENIR = 'souvenir'
    INTERIOR = 'interior'
    PRODUCT_GROUPS = (
        (PHOTO_PRINTING, 'Фотопечать'),
        (PHOTOBOOKS, 'Фотокниги'),
        (SOUVENIR, 'Сувенирка'),
        (INTERIOR, 'Интерьерка'),
    )
    product_group = models.CharField(verbose_name='Группа продукции', choices=PRODUCT_GROUPS, null=True, blank=True, max_length=64)
    photo_formats = models.ManyToManyField('catalog.PhotoFormat', verbose_name='Форматы', blank=True)

    class Meta:
        verbose_name = 'промокод'
        verbose_name_plural = 'промокоды'
        ordering = ('code', '-start_date')

    def __str__(self):
        return f'{self.code}'
