from django.apps import AppConfig


class PromoCodeConfig(AppConfig):
    name = 'promo_code'
    verbose_name = 'Промокоды'
