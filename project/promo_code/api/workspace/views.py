import logging

import django_filters

from base.restapi.viewsets import BaseModelViewSet
from organization.permissions import CanManageOrganizations
from promo_code import models
from promo_code.api.workspace import serializers

logger = logging.getLogger(__name__)


class PromoCodeFilter(django_filters.FilterSet):
    start_date = django_filters.DateFilter(method='filter_start_date')
    end_date = django_filters.DateFilter(method='filter_end_date')

    class Meta:
        model = models.PromoCode
        fields = ('start_date', 'end_date')

    def filter_start_date(self, queryset, name, value):
        if value:
            return queryset.filter(start_date__gte=value)
        return queryset

    def filter_end_date(self, queryset, name, value):
        if value:
            return queryset.filter(end_date__lte=value)

        return queryset


class PromoCodeView(BaseModelViewSet):
    permission_classes = CanManageOrganizations
    queryset = models.PromoCode.objects
    serializer_class = serializers.PromoCodeSerializer

    ordering_fields = (
        "code",
        "start_date",
        "end_date",
        "type",
    )
    filter_class = PromoCodeFilter

    def get_queryset(self):
        return super().get_queryset().filter(organization=self.request.user.organization)

    def perform_create(self, serializer):
        serializer.save(organization=self.request.user.organization)
