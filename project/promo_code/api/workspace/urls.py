from rest_framework import routers
from rest_framework_nested.routers import NestedSimpleRouter

from . import views

app_name = 'promo_code'

router = routers.DefaultRouter()

router.register(r'promo-code', views.PromoCodeView, 'promo_code')

urlpatterns = []
urlpatterns += router.urls
