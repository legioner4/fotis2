import logging

from base.restapi.serializer import BaseSerializer
from catalog.models import PhotoFormat
from promo_code import models

logger = logging.getLogger(__name__)


class PromoCodeSerializer(BaseSerializer):

    class Meta:
        model = models.PromoCode
        fields = "__all__"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if 'photo_formats' in self.fields:
            self.fields['photo_formats'].child_relation.queryset = PhotoFormat.objects.filter(organization=self.request.user.organization)
