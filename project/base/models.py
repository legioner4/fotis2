import uuid
from logging import getLogger

from django.db import models

from base.manager import BaseModelManager

logger = getLogger(__name__)


class BaseModel(models.Model):
    uuid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True, db_index=True)

    created_at = models.DateTimeField('Дата создания', auto_now_add=True, editable=False)
    updated_at = models.DateTimeField('Дата изменения', auto_now=True, editable=False)

    objects = BaseModelManager()

    class Meta:
        abstract = True
        base_manager_name = 'objects'
        default_manager_name = 'objects'
        ordering = ['created_at']


class SortableMixin(models.Model):
    ORDER_SPREAD = 1
    order = models.IntegerField('Сортировка', blank=True, db_index=True, default=0)

    class Meta:
        abstract = True
        ordering = ('order', 'pk')

    @classmethod
    def get_next_order(cls, last_order=None):
        if last_order is None:
            last = cls.objects.order_by('-order').first()
            last_order = -cls.ORDER_SPREAD if not last else last.order
        return last_order + cls.ORDER_SPREAD

    def save(self, *args, **kwargs):
        if not self.pk:
            self.order = self.order or self.get_next_order()
        return super(SortableMixin, self).save(*args, **kwargs)
