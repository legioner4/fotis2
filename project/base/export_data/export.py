import logging
import mimetypes
from typing import Iterable

from django.http import HttpResponse
from django.utils.http import urlquote
from openpyxl import Workbook
from openpyxl.cell import MergedCell
from openpyxl.styles import Font, Border, Alignment
from openpyxl.styles.borders import BORDER_THIN, Side
from openpyxl.utils import get_column_letter
from openpyxl.writer.excel import save_virtual_workbook

logger = logging.getLogger(__name__)


def bytes_to_http_response(data, filename, is_attachment=True):
    response = HttpResponse(data)
    mimetype, encoding = mimetypes.guess_type(filename)
    if mimetype is None:
        mimetype = 'application/octet-stream'

    if encoding is not None:
        response['Content-Encoding'] = encoding

    filename = urlquote(filename.encode('utf-8'))
    disposition = 'filename*=UTF-8\'\'%s' % filename
    if is_attachment:
        disposition = 'attachment; %s' % disposition

    response['Content-Length'] = str(len(data))
    response['Content-Type'] = mimetype
    response['Content-Disposition'] = disposition

    return response


def get_column_widths(prev_column_widths, row):
    column_widths = prev_column_widths
    row = map(str, row)
    for i, cell in enumerate(row):
        cell_length = len(cell)
        try:
            column_widths[i] = max(column_widths[i], cell_length)
        except IndexError:
            column_widths += [cell_length]
    return column_widths


def check_is_two_row_header(column_groups):
    return bool(list(filter(lambda elem: len(elem['columns']) > 1, column_groups)))


def apply_styles(ws, is_two_row_header):
    head_font = Font(bold=True)
    head_alignment = Alignment(horizontal="center", vertical="center")
    border_side = Side(border_style=BORDER_THIN, color="FF000000")
    border = Border(top=border_side, bottom=border_side, left=border_side, right=border_side)
    for row_index, row in enumerate(ws[ws.dimensions]):
        for cell in row:
            if row_index == 0 or (row_index == 1 and is_two_row_header):
                cell.font = head_font
                cell.alignment = head_alignment
            cell.border = border


def create_header(ws, column_groups, header_column_labels, is_two_row_header):
    column_label_row = 1
    if is_two_row_header:
        column_label_row = 2
        merge_start_col = 1
        for column_group in column_groups:
            label = column_group['group_label']
            merge_end_col = merge_start_col + len(column_group['columns']) - 1
            if merge_start_col == merge_end_col:
                ws.merge_cells(start_row=1, end_row=2, start_column=merge_start_col, end_column=merge_start_col)
            else:
                ws.merge_cells(start_row=1, end_row=1, start_column=merge_start_col, end_column=merge_end_col)
            ws.cell(1, merge_start_col, value=label)
            merge_start_col = merge_start_col + len(column_group['columns'])

    for i, column_label in enumerate(header_column_labels):
        if not isinstance(ws.cell(column_label_row, i + 1), MergedCell):
            ws.cell(column_label_row, i + 1, value=column_label)


def beatify_value_of_iterable(elem):
    if elem is None:
        return ''
    if isinstance(elem, dict):
        str_value = elem.get('__str__', None)
        if not str_value:
            str_value = elem.get('name', '')
        return str_value

    return str(elem)


def beatify_value(record, column, field_infos, first_row=False, index=None):
    field_name = column.get('column_name')
    if (
        (field_name not in record or not first_row)
        and not column.get('source')
        or (column.get('source') and (column['source'] not in record))
    ):
        return ''
    if column.get('source'):
        return record[column['source']][index].get(field_name) if len(record[column['source']]) > index else ''
    else:
        values = record[field_name]
    if field_name not in field_infos:
        if isinstance(values, bool):
            values = 'Да' if values else 'Нет'
        if isinstance(values, Iterable) and not isinstance(values, (str, bytes)):
            values = '; '.join(map(lambda s: beatify_value_of_iterable(s), values))
        return values
    choices = field_infos[field_name]
    if not isinstance(values, Iterable) or isinstance(values, (str, bytes)):
        values = [values]
    else:
        pass
    values = [choices[value] if value in choices else value for value in values]
    return '; '.join(map(lambda s: '' if s is None else str(s), values))


def create_workbook(serializer_data, fields, column_groups):
    wb = Workbook()
    ws = wb.active

    header_columns = [column for column_group in column_groups for column in column_group['columns']]
    header_columns_by_name = {
        column.get('column_name'): column for column_group in column_groups for column in column_group['columns']
    }
    header_column_labels = list(map(lambda elem: elem['column_label'], header_columns))
    is_two_row_header = check_is_two_row_header(column_groups)
    create_header(ws, column_groups, header_column_labels, is_two_row_header)
    column_widths = get_column_widths([], header_column_labels)

    field_infos = {field['name']: dict(field['choices']) for field in fields if 'choices' in field}
    if serializer_data:
        not_existed_fields = [
            column.get('column_name')
            for column in list(header_columns_by_name.values())
            if (not column.get('source'))
               and column.get('column_name') not in serializer_data[0]
               or (column.get('source') and not column['source'] in serializer_data[0])
        ]
        if not_existed_fields:
            logger.warning(
                'Для формирования таблицы, запрошены следующие несуществующие поля: {0}'.format(
                    ', '.join(not_existed_fields)
                )
            )
    for record in serializer_data:
        rowspan = 1
        for field in record:
            if isinstance(record.get(field), list) and rowspan < len(record.get(field)):
                rowspan = len(record.get(field))

        for i in range(rowspan):
            columns_data = [
                beatify_value(record, column, field_infos, first_row=(i == 0), index=i)
                for column in list(header_columns_by_name.values())
            ]
            ws.append(columns_data)
            column_widths = get_column_widths(column_widths, columns_data)

    for i, column_width in enumerate(column_widths):
        # магические числа для правильной ширины
        ws.column_dimensions[get_column_letter(i + 1)].width = column_width + 5

    apply_styles(ws, is_two_row_header)

    return save_virtual_workbook(wb)
