import ast
import functools
import logging
import operator as op
import re
from decimal import Decimal

from base.datasource import DATASOURCES
from base.datasource.exceptions import FormulaCalculateNotDataError, FormulaSyntaxError, FormulaCalculateError

logger = logging.getLogger(__name__)


class FormulaVisit(ast.NodeVisitor):
    def visit_Name(self, node):
        if node.id not in self.functions:
            self.variables.append(node.id)
        self.generic_visit(node)

    def visit_UnaryOp(self, node):
        self.operators.append(type(node.op))
        self.generic_visit(node)

    def visit_BinOp(self, node):
        self.operators.append(type(node.op))
        self.generic_visit(node)

    def visit_Attribute(self, node):
        self.callable_attributes.append(node.attr)
        self.generic_visit(node)

    def visit_Index(self, node):
        if isinstance(node.value, ast.Num):
            self.used_indexes.append(node.value.n)
        elif isinstance(node.value, ast.Name):
            self.used_indexes.append(node.value.id)
        self.generic_visit(node)

    def visit_Call(self, node):
        self.functions.append(node.func.id)
        self.generic_visit(node)


class FormulaCalculate(object):
    def eval(self, node):
        method = 'eval_' + node.__class__.__name__
        visitor = getattr(self, method, self.generic_eval)
        return visitor(node)

    def generic_eval(self, node):
        for _, value in ast.iter_fields(node):
            if isinstance(value, list):
                for item in value:
                    if isinstance(item, ast.AST):
                        self.eval(item)
            elif isinstance(value, ast.AST):
                self.eval(value)

    def eval_Str(self, node):
        return node.s

    def eval_Num(self, node):
        val = node.n
        if isinstance(val, float):
            val = Decimal(val)
        return val

    def eval_Name(self, node):
        if node.id in self.functions_maps.keys():
            return self.functions_maps[node.id]
        else:
            data_source = self.get_datasource(node.id)
            if data_source and data_source.return_func:
                data = self.get_data_source_required_data(data_source)
                return functools.partial(data_source.function, **data)
            return self.computed_data[node.id]

    def eval_UnaryOp(self, node):
        val = self.eval(node.operand)
        return self.operators_maps[type(node.op)](val)

    def eval_BinOp(self, node):
        lhs = self.eval(node.left)
        rhs = self.eval(node.right)
        return self.operators_maps[type(node.op)](lhs, rhs)

    def eval_Attribute(self, node):
        l_value = self.eval(node.value)
        if isinstance(l_value, dict):
            return l_value[node.attr]
        return getattr(l_value, node.attr)

    def eval_Call(self, node):
        return self.eval(node.func)(*(self.eval(a) for a in node.args))


class FormulaCalculator(FormulaVisit, FormulaCalculate):
    operators_maps = {ast.Add: op.add, ast.Sub: op.sub, ast.Mult: op.mul, ast.Div: op.truediv, ast.USub: op.neg}

    functions_maps = {'ДОЛЯ': lambda x: x / 100, 'abs': abs}

    def __init__(self, formula, data_list=None, data_sources=DATASOURCES):
        if data_list is None:
            data_list = []
        self.data_list = data_list
        self.available_data_sources = data_sources
        self._data_type_list = [type(data) for data in self.data_list]
        self.formula = self.formula_normalization(formula)

        self.computed_data = {}
        self.variables = []  # теги
        self.operators = []  # операторы
        self.functions = []  # функции
        self.callable_attributes = []  # вызываемые атрибуты
        self.used_indexes = []  # используемые индексы

        self.use_variables = []  # используемые теги
        self.use_operators = []  # используемые операторы
        self.use_functions = []  # используемые функции

        self.allowed_variables = []  # поддерживаемые теги
        self.allowed_functions = []  # поддерживаемые функции
        self.allowed_operators = self.operators_maps.keys()  # поддерживаемые операторы

        self.get_allowed_variables_and_functions()

        self.not_allowed_variables = []  # не поддерживаемые теги
        self.not_allowed_operators = []  # не поддерживаемые операции
        self.not_allowed_functions = []  # не поддерживаемые теги

    def formula_normalization(self, formula):
        """
        Нормализация формулы
        """
        formula = re.sub(r'\{([^\}]+)\}', r'\1', formula)  # убираем {}
        formula = re.sub(r'\|([^\|]+)\|', r'abs(\1)', formula)  # заменяем || на abs
        formula = formula.replace('%', '')  # убираем %
        return formula

    def check_availability_variable_data(self, variable):
        """
        Проверка доступности данных для переменной или для функции
        """
        data_source = self.get_datasource(variable)
        return [
            data_type.__name__
            for data_type in data_source.required_data.values()
            if data_type not in self._data_type_list
        ]

    def check_availability_data(self):
        """
        Проверка входных данных для тегов. Если переданных данных не хватает вызываем исключение
        """
        not_available_data_list = []
        variables = self.use_variables + self.use_functions
        for variable in variables:
            not_available_data = self.check_availability_variable_data(variable)
            if not_available_data:
                not_available_data_list += not_available_data

        if len(not_available_data_list):
            err = 'Не хватает следующих данных: {0}'.format(','.join(not_available_data_list))
            logger.error(err)
            raise FormulaCalculateNotDataError(err)

    def get_data_source_required_data(self, data_source):
        """
        Получение данных требуемых для data_source
        """
        kwargs = {}
        for arg_name, data_type in data_source.required_data.items():
            for data in self.data_list:
                if type(data) == data_type:
                    kwargs[arg_name] = data
        return kwargs

    def get_datasource(self, name):
        """
        Источник данных по наименованию
        """
        try:
            return self.available_data_sources[name]
        except KeyError:
            return None

    def compute_variable_data(self, variables):
        """
        Вычисление переменных для последующей вставки в формулу при вычислении формулы
        """
        for variable in variables:
            data_source = self.get_datasource(variable)
            data = self.get_data_source_required_data(data_source)
            self.computed_data[variable] = data_source.function(**data)

    def get_allowed_variables_and_functions(self):
        """
        Получение доступных переменных для формул и функций
        """
        for data_source in self.available_data_sources.values():
            if data_source.return_func:
                self.allowed_functions.append(data_source.code)
            else:
                self.allowed_variables.append(data_source.code)

    @property
    def info(self):
        return {
            'variables': self.variables,
            'operators': self.operators,
            'functions': self.functions,
            'callable_attributes': self.callable_attributes,
            'used_indexes': self.used_indexes,
            'use_variables': self.use_variables,
            'use_operators': self.use_operators,
            'use_functions': self.use_functions,
            'not_allowed_variables': self.not_allowed_variables,
            'not_allowed_operators': self.not_allowed_operators,
            'not_allowed_functions': self.not_allowed_functions,
        }

    def check_formula(self):
        """
        Проверка формулы на наличие
        1. неподдерживаемых переменных
        2. неподдерживаемых операторов
        3. неподдерживаемых функций
        """
        for variable in self.variables:
            if variable in self.allowed_variables:
                self.use_variables.append(variable)
            else:
                self.not_allowed_variables.append(variable)
        self.use_variables = list(set(self.use_variables))
        self.not_allowed_variables = list(set(self.not_allowed_variables))

        for operator in self.operators:
            if operator in self.allowed_operators:
                self.use_operators.append(operator)
            else:
                self.not_allowed_operators.append(operator)
        self.use_operators = list(set(self.use_operators))
        self.not_allowed_operators = list(set(self.not_allowed_operators))

        for function in self.functions:
            if function in self.allowed_functions:
                self.use_functions.append(function)
            elif function in self.functions_maps.keys():
                pass
            else:
                self.not_allowed_functions.append(function)
        self.use_functions = list(set(self.use_functions))
        self.not_allowed_functions = list(set(self.not_allowed_functions))

    def is_valid(self):
        """
        Валидация формулы
        """
        try:
            tree = ast.parse(self.formula)
            self.visit(tree)
            self.check_formula()
            return not bool(
                self.not_allowed_variables
                or self.not_allowed_operators
                or self.not_allowed_functions
                or self.used_indexes
                or self.callable_attributes
            )
        except Exception as exp:
            logger.error('Ошибка синтаксиса: {0}'.format(exp))
            return False

    def calculate(self):
        """
        Вычисление формулы
        """
        if self.is_valid():
            self.check_availability_data()
            try:
                self.compute_variable_data(self.use_variables)
                tree = ast.parse(self.formula, mode='eval')
                return self.eval(tree.body)
            except Exception as exp:
                err = 'Ошибка при вычислении формулы: {0}'.format(exp)
                logger.error(err)
                raise FormulaCalculateError('Ошибка при вычислении формулы')
        raise FormulaSyntaxError


def filter_datasources_by_data_types(data_types):
    """
    фильтрация поставщиков данных по типам данных
    """
    return_datasources = {}
    for code, data_source in DATASOURCES.items():
        not_types = [data_type for data_type in data_source.required_data.values() if data_type not in data_types]
        if not not_types:
            return_datasources[code] = data_source
    return return_datasources


def formula_check(formula, data_types):
    """
    Проверка формулы на доступность тегов и доступных операций
    """
    checker = FormulaCalculator(formula, data_sources=filter_datasources_by_data_types(data_types))
    result = checker.is_valid()
    return result, checker.info
