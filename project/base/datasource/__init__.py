import logging

logger = logging.getLogger(__name__)
DATASOURCES = {}


class DataSource(object):
    def __init__(self, code, function, required_data, func_args_count, descr):
        self.code = code
        self.func_args_count = func_args_count
        self.return_func = bool(func_args_count)
        self.required_data = required_data or {}
        self.function = function
        self.descr = descr

    @property
    def format_code(self):
        if not self.return_func:
            return '{%s}' % self.code
        else:
            args = ['Пер_%s' % i for i in range(1, self.func_args_count + 1)]
            return '{%s(%s)}' % (self.code, ', '.join(args))


def register_data_source(code, required_data=None, func_args_count=0, descr=''):
    def decorator(function):
        if code in DATASOURCES:
            logger.error('Данный {0} тег ранее был зарегистрирован'.format(code))
        else:
            DATASOURCES[code] = DataSource(code, function, required_data, func_args_count, descr)

        return function

    return decorator
