from django.core import exceptions
from rest_framework import serializers

from base.datasource.utils import formula_check, filter_datasources_by_data_types


class FormulaField(serializers.CharField):
    """
    Поле для ввода формул
    """

    default_error_messages = {'syntax': 'Ошибка синтаксиса'}
    required_data_types = []  # передаваемые типы данных для фильтрации доступных тегов

    def __init__(self, required_data_types, **kwargs):
        self.required_data_types = required_data_types
        super().__init__(**kwargs)
        self.available_tags = self.get_available_tags()

    def run_validation(self, data=serializers.empty):
        value = super().run_validation(data)
        result, info = formula_check(value, data_types=self.required_data_types)
        if not result:
            raise exceptions.ValidationError(self.error_messages['syntax'])
        return value

    def get_available_tags(self):
        tags = []
        for data_source in filter_datasources_by_data_types(self.required_data_types).values():
            tags.append({'code': data_source.format_code, 'descr': data_source.descr})
        return tags
