class FormulaSyntaxError(Exception):
    pass


class FormulaCalculateNotDataError(Exception):
    pass


class FormulaCalculateError(Exception):
    pass
