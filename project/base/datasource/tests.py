import ast
import unittest

from base.datasource import DATASOURCES, register_data_source
from base.datasource import exceptions
from base.datasource.utils import formula_check, FormulaCalculator


class TestData1(object):
    n = 1


class TestData2(object):
    n = 2


class TestData3(object):
    n = 3


@register_data_source('Ф1', descr='Функция 1')  # {Ф1}
@register_data_source('Ф11', descr='Функция 11', func_args_count=1)  # {Ф11(2)}
@register_data_source('Ф111', descr='Функция 111', required_data={'test_data2': TestData2})  # {Ф111}
@register_data_source(
    'Ф1111', descr='Функция 1111', required_data={'test_data2': TestData2}, func_args_count=2
)  # {Ф111(Ф1, 2)}
def f1(*args, test_data2: TestData2 = None, **kwargs):
    if not args and test_data2 is None:
        return 1
    elif args and len(args) == 1 and test_data2 is None:
        return 11
    elif not args and test_data2 is not None:
        return 111
    elif args and len(args) == 2 and test_data2 is not None:
        return 1111


@register_data_source('Ф2', descr='Функция 2', required_data={'data2': TestData2})
def f2(*args, data2: TestData2, **kwargs):
    return data2.n


class TestRegisterDataSource(unittest.TestCase):
    def test_register(self):
        f2_data_source = DATASOURCES.get('Ф2', None)
        self.assertIsNotNone(f2_data_source)
        self.assertEqual(f2_data_source.code, 'Ф2')
        self.assertEqual(f2_data_source.descr, 'Функция 2')
        self.assertTrue(all([data in f2_data_source.required_data.values() for data in [TestData2]]))


class TestFormulaChecker(unittest.TestCase):
    not_valid_formula_1 = '1+2-{Ф1}/3*{Ф2}?'
    not_valid_formula_2 = '{Ф2.К1}+{Ф3.Л1}'
    not_valid_formula_3 = '{Ф2[2]}+{Ф3[Ф1]}'
    not_valid_formula_4 = '1+2-{Ф1}/3*{Ф2}+4^4+{Ф3}+|2|'
    not_valid_formula_5 = '{Ф1(1)}+{Ф11}+{Ф5(Ф2)}+|2|+ДОЛЯ(10)*{Ф11(3)}'

    def test_formula1(self):
        result, info = formula_check(self.not_valid_formula_1, data_types=[TestData2])
        self.assertFalse(result)

    def test_formula2(self):
        result, info = formula_check(self.not_valid_formula_2, data_types=[TestData2])
        self.assertFalse(result)
        callable_attributes = ['К1', 'Л1']
        self.assertTrue(all([data in callable_attributes for data in info['callable_attributes']]))

    def test_formula3(self):
        result, info = formula_check(self.not_valid_formula_3, data_types=[TestData2])
        self.assertFalse(result)
        used_indexes = [2, 'Ф1']
        self.assertTrue(all([data in used_indexes for data in info['used_indexes']]))

    def test_formula4(self):
        result, info = formula_check(self.not_valid_formula_4, data_types=[TestData1, TestData2])
        use_variables = ['Ф1', 'Ф2']
        use_functions = ['abs']
        use_operators = [ast.Sub, ast.Add, ast.Mult, ast.Div]
        not_allowed_variables = ['Ф3']
        not_allowed_operators = [ast.BitXor]
        self.assertTrue(all([data in use_variables for data in info['use_variables']]))
        self.assertTrue(all([data in not_allowed_variables for data in info['not_allowed_variables']]))
        self.assertTrue(all([data in use_functions for data in info['use_functions']]))
        self.assertTrue(all([data in use_operators for data in info['use_operators']]))
        self.assertTrue(all([data in not_allowed_operators for data in info['not_allowed_operators']]))

    def test_formula5(self):
        result, info = formula_check(self.not_valid_formula_5, data_types=[])
        use_functions = ['abs', 'Ф11', 'ДОЛЯ']
        not_allowed_functions = ['Ф1', 'Ф5']
        self.assertTrue(all([data in use_functions for data in info['use_functions']]))
        self.assertTrue(all([data in not_allowed_functions for data in info['not_allowed_functions']]))


class TestFormulaCalculate(unittest.TestCase):
    not_valid_formula = '1+2-{Ф1}/3*{Ф2}?'
    not_tag_data_formula = '{Ф111} + {Ф2}'
    formula_1 = '2*2/2+1-1'
    formula_2 = '{Ф1} + {Ф2} + |-3| + ДОЛЯ(100)'
    formula_3 = '{Ф1}'
    formula_4 = '{Ф11(Ф1)}'
    formula_5 = '{Ф111}'
    formula_6 = '{Ф1111(Ф1, 3)}'

    def test_not_valid(self):
        with self.assertRaises(exceptions.FormulaSyntaxError):
            FormulaCalculator(self.not_valid_formula).calculate()

    def test_not_tag_data(self):
        with self.assertRaises(exceptions.FormulaCalculateNotDataError):
            data_class_1 = TestData1()
            FormulaCalculator(self.not_tag_data_formula, data_list=[data_class_1]).calculate()

    def test_calculate_formula_1(self):
        result = FormulaCalculator(self.formula_1).calculate()
        self.assertEqual(result, 2)

    def test_calculate_formula_2(self):
        data_class_1 = TestData1()
        data_class_2 = TestData2()
        result = FormulaCalculator(self.formula_2, data_list=[data_class_1, data_class_2]).calculate()
        self.assertEqual(result, 7)

    def test_calculate_formula_3(self):
        data_class_2 = TestData2()
        result = FormulaCalculator(self.formula_3).calculate()
        self.assertEqual(result, 1)
        result = FormulaCalculator(self.formula_4).calculate()
        self.assertEqual(result, 11)
        result = FormulaCalculator(self.formula_5, data_list=[data_class_2]).calculate()
        self.assertEqual(result, 111)
        result = FormulaCalculator(self.formula_6, data_list=[data_class_2]).calculate()
        self.assertEqual(result, 1111)
