from collections import OrderedDict
from logging import getLogger

from django.core.paginator import Paginator as DjangoPaginator, PageNotAnInteger
from django.utils.translation import ugettext_lazy as _
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response

logger = getLogger(__name__)

UNSPEC_PAGE_SIZE = 9999


class Paginator(DjangoPaginator):
    def validate_number(self, number):
        try:
            number = int(number)
        except (TypeError, ValueError):
            raise PageNotAnInteger(_('That page number is not an integer'))
        if number < 1:
            number = 1
        if number > self.num_pages:
            if number == 1 and self.allow_empty_first_page:
                pass
            else:
                number = self.num_pages
        return number


class Pagination(PageNumberPagination):
    page_size_query_param_not_provided = False
    page_size = UNSPEC_PAGE_SIZE
    page_query_param = 'page'
    page_size_query_param = 'page_size'
    template = None
    django_paginator_class = Paginator

    def get_paginated_response(self, data, total_count=0):
        count = self.page.paginator.count
        if self.page_size_query_param_not_provided and count > UNSPEC_PAGE_SIZE:
            logger.warning(f'Превышен предельный размер страницы по-умолчанию – {UNSPEC_PAGE_SIZE} элементов')
        return Response(
            OrderedDict(
                [
                    (
                        'paginator',
                        OrderedDict(
                            [
                                ('count', count),
                                ('total_count', total_count),
                                ('page', self.page.number),
                                ('num_pages', self.page.paginator.num_pages),
                                ('page_size', self.page.paginator.per_page),
                                ('start_index', self.page.start_index()),
                                ('end_index', self.page.end_index()),
                            ]
                        ),
                    ),
                    ('data', data),
                ]
            )
        )

    def paginate_queryset(self, queryset, request, view=None):
        self.page_size_query_param_not_provided = self.page_size_query_param not in request.query_params
        if request.query_params.get(self.page_size_query_param) == '__all__':
            self.page_size = max(queryset.count(), self.page_size)
        return super().paginate_queryset(queryset, request, view=view)
