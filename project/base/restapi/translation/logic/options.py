from modeltranslation.translator import translator, TranslationOptions


class BaseTranslationOptions(TranslationOptions):
    sorted_fields = []
