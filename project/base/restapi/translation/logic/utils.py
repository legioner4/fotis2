from django.conf import settings

from base.users import get_current_user


def get_system_languages(translation_organization=None):
    user = get_current_user()

    if not translation_organization:
        if user and user.organization:
            translation_organization = user.organization

    if translation_organization:
        return [(language.code, language.name) for language in translation_organization.languages.all()]
    return settings.LANGUAGES


def get_field_name_translation_fields(field_name, translation_organization=None, check_organization=True):
    fields = []
    languages = get_system_languages(translation_organization) if check_organization else settings.LANGUAGES

    for l, _ in languages:
        fields.append("{}_{}".format(field_name, l))
    return fields


def get_field_name_translation_fields_and_languages(field_name, translation_organization=None, check_organization=True):
    fields = []
    languages = get_system_languages(translation_organization) if check_organization else settings.LANGUAGES

    for l, _ in languages:
        fields.append(("{}_{}".format(field_name, l), l))
    return fields


def get_language_name_by_code(code):
    return settings.LANGUAGES_DICT[code]


def get_translatable_fields_for_model(model):
    from modeltranslation.translator import NotRegistered, translator
    try:

        field_names = translator.get_options_for_model(model).get_field_names()
        return field_names
    except NotRegistered:
        return None


def get_translatable_sorted_fields_for_model(model):
    from modeltranslation.translator import NotRegistered, translator
    try:

        field_names = translator.get_options_for_model(model).sorted_fields
        return field_names
    except NotRegistered:
        return None
