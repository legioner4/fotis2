from base.restapi.translation.logic.utils import get_field_name_translation_fields, get_translatable_fields_for_model, \
    get_translatable_sorted_fields_for_model
from base.restapi.viewsets import BaseModelViewSet, NestedViewSet


class TranslationFieldBaseModelViewSet(BaseModelViewSet):

    def get_translation_organization(self):
        return None

    def get_translation_fields(self, field_name, translation_organization):
        return get_field_name_translation_fields(field_name, translation_organization)

    def get_meta(self, serializer=None):
        meta = super().get_meta(serializer)
        serializer = serializer if serializer else self.get_serializer()
        trans_fields = get_translatable_fields_for_model(serializer.Meta.model)
        trans_sorted_fields = get_translatable_sorted_fields_for_model(serializer.Meta.model)
        translation_organization = self.get_translation_organization()

        fields = self.request.GET.get('fields')

        all_fields = []
        if trans_fields:
            for field_name in trans_fields:
                if field_name in fields:
                    all_fields += self.get_translation_fields(field_name, translation_organization)

        meta['trans_fields'] = all_fields
        meta['trans_sorted_fields'] = trans_sorted_fields
        return meta


class TranslationFieldNestedViewSet(TranslationFieldBaseModelViewSet, NestedViewSet):
    pass
