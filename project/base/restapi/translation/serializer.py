from base.restapi.serializer import BaseSerializer
from base.restapi.translation.logic.utils import get_field_name_translation_fields, \
    get_field_name_translation_fields_and_languages, get_language_name_by_code, get_translatable_fields_for_model


class TranslationSerializer(BaseSerializer):

    def get_translation_organization(self):
        return None

    def get_translation_fields(self, field_name, translation_organization):
        return get_field_name_translation_fields(field_name, translation_organization)

    def get_translation_fields_and_languages(self, field_name, translation_organization):
        return get_field_name_translation_fields_and_languages(field_name, translation_organization)

    def get_field_names(self, declared_fields, info):
        fields = super().get_field_names(declared_fields, info)
        translation_organization = self.get_translation_organization()
        trans_fields = get_translatable_fields_for_model(self.Meta.model)
        all_fields = []

        for field_name in fields:
            if not trans_fields or field_name not in trans_fields:
                all_fields.append(field_name)
            else:
                all_fields += self.get_translation_fields(field_name, translation_organization)
        return all_fields

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        translation_organization = self.get_translation_organization()
        trans_fields = get_translatable_fields_for_model(self.Meta.model)
        if trans_fields:
            for field_name in trans_fields:
                for translation_field, language in self.get_translation_fields_and_languages(field_name, translation_organization):
                    if translation_field in self.fields:
                        label = self.fields[translation_field].label
                        label = label.replace(language,  get_language_name_by_code(language))
                        self.fields[translation_field].label = label
                        self.fields[translation_field].required = True
