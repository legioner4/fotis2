import json
from datetime import datetime

from django.db.models import Q
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.fields import DateTimeField

operation_to_query = {
    'Contains': '__icontains',
    'StartsWith': '__istartswith',
    'EndsWith': '__iendswith',
    'Equals': '',
    'Less': '__lt',
    'LessEquals': '__lte',
    'Greater': '__gt',
    'GreaterEquals': '__gte',
    'Between': ['__gt', '__lt'],
    'MultipleEquals': '__in',
}

operation_negatives = {'NotContains': 'Contains', 'NotEquals': 'Equals', 'MultipleNotEquals': 'MultipleEquals'}


class FakeQuerySet:
    def __init__(self, queryset):
        self.queryset = queryset
        self.query = None

    def first(self):
        if self.query:
            return self.queryset.filter(self.query).first()
        return self.queryset.first()

    def all(self):
        if self.query:
            return self.queryset.filter(self.query).all()
        return self.queryset.all()

    def filter(self, *args, **kwargs):
        if args:
            for arg in args:
                self.query = self.query & arg if self.query else arg
        if kwargs:
            self.query = self.query & Q(**kwargs) if self.query else Q(**kwargs)
        return self


def is_group(com):
    if len(com) > 1:
        return isinstance(com[1], list)
    return len(com) == 1


def group_get_query(group, filter_class, queryset, serializer_fields):
    if not group or not len(group):
        return None

    if not is_group(group):
        name = group[0]
        operation = group[1]
        operation2 = None
        val = group[2]
        val2 = None
        is_negative = False

        if not name:
            return None

        if operation in operation_negatives:
            is_negative = True
            operation = operation_negatives[operation]
        operation = operation_to_query[operation]

        if isinstance(operation, list):
            operation2 = operation[1]
            operation = operation[0]
            val2 = group[3]

        if operation == '__in':
            val = [val]
        if operation2 == '__in':
            val2 = [val2]

        kwarg = {name + operation: val}
        query = Q(**kwarg)
        if operation2:
            kwarg2 = {name + operation2: val2}
            query = query & Q(**kwarg2)

        if hasattr(filter_class, 'declared_filters'):
            if name in filter_class.declared_filters:
                filter_func = filter_class.declared_filters[name].filter.f.method
                filter_func = getattr(filter_class, filter_func)

                fake = FakeQuerySet(queryset)
                fake = filter_func(None, fake, name, val)
                query = fake.query
            else:
                if name in serializer_fields:
                    source = serializer_fields[name].source
                    if isinstance(serializer_fields[name], DateTimeField):
                        source += '__date'
                        val = datetime.strptime(val, '%Y-%m-%d')
                        if operation2:
                            val2 = datetime.strptime(val2, '%Y-%m-%d')
                    if source:
                        source_name = source.replace('.', '__')
                        query = Q(**{source_name + operation: val})

                        if operation2:
                            kwarg2 = {source_name + operation2: val2}
                            query = query & Q(**kwarg2)
        if is_negative:
            query = ~query
        return query

    query = None
    operation = group[0]

    for iter_group in group[1:]:
        iter_query = group_get_query(iter_group, filter_class, queryset, serializer_fields)
        if iter_query:
            if query:
                if operation == 'And':
                    query = query & iter_query
                elif operation == 'Or':
                    query = query | iter_query
                elif operation == 'NotAnd':
                    query = query & ~iter_query
                elif operation == 'NotOr':
                    query = query | ~iter_query
            else:
                if operation == 'And' or operation == 'Or':
                    query = iter_query
                else:
                    query = ~iter_query
    return query


class FilterBackend(DjangoFilterBackend):
    def filter_queryset(self, request, queryset, view):
        filter_class = self.get_filter_class(view, queryset)
        serializer_fields = view.serializer_class._declared_fields

        if filter_class:
            args = request.query_params.get('filter')
            if args:
                args = json.loads(args)
                if '_group' in args:
                    group = args.pop('_group')
                    group_query = group_get_query(group, filter_class, queryset, serializer_fields)
                    if group_query:
                        queryset = queryset.filter(group_query)
                return filter_class(args, queryset=queryset, request=request).qs

        return queryset
