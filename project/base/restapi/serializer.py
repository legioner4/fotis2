import json

from django.db import models
from rest_framework import serializers
from rest_framework.fields import empty
from rest_framework.serializers import Serializer

from base.restapi.fields import UserField
from base.restapi.permission import CREATE, LIST, RETRIEVE, UPDATE
from chunked_upload_app.api.fields import ChunkUploadField


class WriteOnceMixin:
    """Adds support for write once fields to serializers.

    To use it, specify a list of fields as `write_once_fields` on the
    serializer's Meta:
    ```
    class Meta:
        model = SomeModel
        fields = '__all__'
        write_once_fields = ('collection', )
    ```

    Now the fields in `write_once_fields` can be set during POST (create),
    but cannot be changed afterwards via PUT or PATCH (update).
    Inspired by http://stackoverflow.com/a/37487134/627411.
    """

    def get_extra_kwargs(self):
        extra_kwargs = super().get_extra_kwargs()

        # We're only interested in PATCH/PUT.
        if 'update' in getattr(self.context.get('view'), 'action', ''):
            return self._set_write_once_fields(extra_kwargs)

        return extra_kwargs

    def _set_write_once_fields(self, extra_kwargs):
        """Set all fields in `Meta.write_once_fields` to read_only."""
        write_once_fields = getattr(self.Meta, 'write_once_fields', None)
        if not write_once_fields:
            return extra_kwargs

        if not isinstance(write_once_fields, (list, tuple)):
            raise TypeError(
                'The `write_once_fields` option must be a list or tuple. '
                'Got {}.'.format(type(write_once_fields).__name__)
            )

        for field_name in write_once_fields:
            kwargs = extra_kwargs.get(field_name, {})
            kwargs['read_only'] = True
            extra_kwargs[field_name] = kwargs

        return extra_kwargs


class BaseSerializer(serializers.ModelSerializer):
    created_by = UserField(label='Создал', read_only=True)
    updated_by = UserField(label='Изменил', read_only=True)
    locked_by = UserField(label='Заархивировал', read_only=True)
    deleted_by = UserField(label='Удалил', read_only=True)

    created_by_id = serializers.IntegerField(label='Создал', read_only=True, source='created_by')
    updated_by_id = serializers.IntegerField(label='Изменил', read_only=True, source='updated_by')
    locked_by_id = serializers.IntegerField(label='Заархивировал', read_only=True, source='locked_by')
    deleted_by_id = serializers.IntegerField(label='Удалил', read_only=True, source='deleted_by')

    __str__ = serializers.CharField(label='Наименование', read_only=True)

    related_fields = {}
    prefetch_related_fields = {}

    extra_fields = {'__str__'}

    def __init__(self, *args, editable_fields=None, **kwargs):
        super().__init__(*args, **kwargs)
        self.editable_fields = editable_fields or []
        if editable_fields:
            self._init_editable_fields()

        if not self.request:
            return

        fields = self.request.query_params.get('fields')
        if not fields:
            fields = self.request.data.keys()
        else:
            fields = fields.split(',')
        if not fields:
            return
        # Drop any fields that are not specified in the `fields` argument.
        allowed = set(fields) | self.extra_fields
        existing = set(self.fields.keys())
        for field_name in existing - allowed:
            self.fields.pop(field_name)

        action = self.get_action()
        if action in [CREATE, UPDATE, RETRIEVE]:
            self.__object_init__(self, *args, action=action, **kwargs)
        elif action in [LIST]:
            self.__list_init__(self, *args, action=action, **kwargs)

    def get_related_fields(self):
        related_fields = []
        for field in self.fields:
            fields = self.related_fields.get(field)
            if fields:
                related_fields.extend(fields)
        return related_fields

    def get_prefetch_related_fields(self):
        prefetch_related_fields = []
        for field in self.fields:
            fields = self.prefetch_related_fields.get(field)
            if isinstance(fields, dict) and 'method_name' in fields:
                # если указан  {method_name: 'method_name'}, нужно его вызвать
                fields = getattr(self, fields['method_name'])
                if callable(fields):
                    fields = fields()
            if isinstance(fields, list):
                prefetch_related_fields.extend(fields)
        return prefetch_related_fields

    def __object_init__(self, *args, **kwargs):
        """
        начальные операции с объектом
        """

    def __list_init__(self, *args, **kwargs):
        """
        начальные операции со списком
        """

    def _init_editable_fields(self):
        self.make_fields_all_readonly_disabled(*self.editable_fields)

    @staticmethod
    def setup_queryset(queryset):
        """ Для добавления select_related, prefetch_related и т.п. """
        return queryset

    def to_representation(self, instance):
        result = super().to_representation(instance)
        result['pk'] = instance.pk if hasattr(instance, 'pk') else None
        result.move_to_end('pk', last=False)
        if hasattr(instance, 'change_number'):
            result['change_number'] = instance.change_number
        return result

    def _set_fields_behaviour(self, *fields, disabled=None, read_only=None, required=None, all_except_mode=False):
        """ Групповое управление атрибутами read_only и disabled """
        assert disabled is not None or read_only is not None or required is not None
        assert not required or not disabled and not read_only
        fields = set(fields).intersection(self.fields.keys())
        if all_except_mode:
            fields = self.fields.keys() - fields

        # не меняем свойства служебных полей
        fields = fields - {'change_number', 'pk'}
        for field_name in fields:
            field = self.fields[field_name]
            if read_only is not None:
                field.read_only = read_only
            if disabled is not None:
                field.disabled = disabled
            if required is not None and not getattr(field, 'read_only', None) and not getattr(field, 'disabled', None):
                field.required = required
                if hasattr(field, 'allow_null'):
                    field.allow_null = not required
                if hasattr(field, 'allow_blank'):
                    field.allow_blank = not required

    def make_fields_readonly_disabled(self, *fields):
        self._set_fields_behaviour(*fields, disabled=True, read_only=True)

    def make_fields_all_readonly_disabled(self, *except_fields):
        self._set_fields_behaviour(*except_fields, disabled=True, read_only=True, all_except_mode=True)

    def make_fields_required(self, *fields):
        self._set_fields_behaviour(*fields, required=True)

    def get_action(self):
        view = self.get_context_view()
        if view:
            return view.action
        return None

    def get_context_view(self):
        return self.context.get('view', None)

    @property
    def instance_single(self):
        """ Возвращает объект модели если задан и это не список объектов """
        if isinstance(self.instance, models.Model):
            return self.instance
        return None

    @property
    def request(self):
        return self.context.get('request')

    def to_internal_value(self, data):
        """
        Для работы с полем ChunkUploadField.
        """
        if not isinstance(data, serializers.Mapping):
            message = self.error_messages['invalid'].format(datatype=type(data).__name__)
            raise serializers.ValidationError(
                {serializers.api_settings.NON_FIELD_ERRORS_KEY: [message]}, code='invalid'
            )

        ret = serializers.OrderedDict()
        errors = serializers.OrderedDict()
        fields = self._writable_fields

        for field in fields:
            validate_method = getattr(self, 'validate_' + field.field_name, None)
            primitive_value = field.get_value(data)
            try:
                if isinstance(field, ChunkUploadField):
                    validated_value = field.run_validation(primitive_value, self.instance)
                else:
                    validated_value = field.run_validation(primitive_value)
                if validate_method is not None:
                    validated_value = validate_method(validated_value)
            except serializers.ValidationError as exc:
                errors[field.field_name] = exc.detail
            except serializers.DjangoValidationError as exc:
                errors[field.field_name] = serializers.get_error_detail(exc)
            except serializers.SkipField:
                pass
            else:
                serializers.set_value(ret, field.source_attrs, validated_value)

        if errors:
            raise serializers.ValidationError(errors)

        return ret


def remove_fields(serializer, *fields):
    for field_name in fields:
        if field_name in serializer.fields:
            del serializer.fields[field_name]


class SelectRelatedSerializer(serializers.ModelSerializer):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields.values():
            field.disabled = True
            field.read_only = True

    def to_representation(self, instance):
        result = super().to_representation(instance)
        result['pk'] = instance.pk if hasattr(instance, 'pk') else None
        result.move_to_end('pk', last=False)
        return result

    def to_internal_value(self, data):
        if 'pk' in data:
            return self.Meta.model.objects.filter(pk=data['pk']).first()
        return empty

    def run_validators(self, value):
        """
        Add read_only fields with defaults to value before running validators.
        """
        to_validate = self._read_only_defaults()
        # to_validate.update(value)
        super(Serializer, self).run_validators(to_validate)

    def get_value(self, dictionary):
        data = dictionary.get(self.field_name, empty)
        if data and isinstance(data, str):
            try:
                return json.loads(data)
            except Exception:
                return {}
        return super().get_value(dictionary)


class ExtraRowRelatedSerializer(BaseSerializer):
    """ Сериализатор который разварачивает связанные данные в дополниетельные строки таблицы"""

    pass



