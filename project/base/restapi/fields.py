from collections import Iterable

from django.contrib.auth import get_user_model
from django.core.cache import caches
from rest_framework.fields import CharField, ChoiceField, MultipleChoiceField, DateField


class UserField(CharField):
    cache = caches['default']

    def to_representation(self, value):
        empty = '-'
        if not value:
            return empty

        cache_key = f'user_field_by_pk:{value}'
        user = self.cache.get(cache_key)
        if not user:
            user = get_user_model().objects.filter(pk=value).first()
            user = user and user.get_full_name() or empty
            self.cache.set(cache_key, user)
        return user


class MultipleChoiceListField(MultipleChoiceField):
    """ Для совместного использования с django.contrib.postgres.fields.array.ArrayField """

    def to_internal_value(self, data):
        if isinstance(data, Iterable):
            data = [item for item in data if str(item) in self.choice_strings_to_values]
        value = super().to_internal_value(data)
        return list(value)

    def to_representation(self, value):
        choice = self.choice_strings_to_values
        return {choice[str(item)] for item in value if str(item) in choice}


class InverseMultipleChoiceListField(MultipleChoiceField):
    """ Для совместного использования с django.contrib.postgres.fields.array.ArrayField """

    def to_internal_value(self, data):
        if isinstance(data, Iterable):
            data = [item for item in data if str(item) in self.choice_strings_to_values]
        inverse_data = [item for item in self.choices if item not in data]
        value = super().to_internal_value(inverse_data)
        return list(value)

    def to_representation(self, value):
        choice = self.choice_strings_to_values
        data = [item for item in value if str(item) in choice]
        inverse_data = [item for item in self.choices if item not in data]
        return [choice[str(item)] for item in inverse_data]


class RadioField(ChoiceField):
    pass


class MonthField(DateField):
    FORMAT = '%m.%Y'

    def __init__(self, format=FORMAT, input_formats=None, *args, **kwargs):
        if input_formats is None:
            input_formats = [self.FORMAT]
        super().__init__(format=format, input_formats=input_formats, *args, **kwargs)
