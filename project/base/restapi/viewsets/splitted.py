from .bases import _BaseModelViewSet, _NestedViewSet


class BaseModelViewSet(_BaseModelViewSet):
    """
    Для использования в workspace
    """
    pass


class NestedViewSet(_NestedViewSet, BaseModelViewSet):
    """
    Для использования в workspace
    """
    pass
