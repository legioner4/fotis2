import json
from collections import OrderedDict
from logging import getLogger

from django.conf import settings
from django.db import router
from django.db.models import ProtectedError, Q
from rest_framework import filters, status
from rest_framework.decorators import action
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet
from rest_framework.authentication import TokenAuthentication, SessionAuthentication

from base.deletion import get_deleted_objects, get_restored_objects
from base.restapi.filters import FilterBackend
from base.restapi.pagination import Pagination
from base.restapi.permission import DESTROY, PermissionMethodMixin, RESTORE
from base.restapi.renderers import ReadFieldRenderer

logger = getLogger(__name__)


class _BaseModelViewSet(PermissionMethodMixin, ModelViewSet):
    pagination_class = Pagination
    total_count = 0
    filter_backends = (filters.OrderingFilter, filters.SearchFilter, FilterBackend)
    ordering_fields = ()
    default_ordering_fields = (
        'change_number',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
        'locked_at',
        'locked_by',
        'deleted_at',
        'deleted_by',
    )
    search_fields = ()
    filter_fields = ()
    filter_class = None
    sortable = False
    serializer_class_map = None

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.ordering_fields:
            self.ordering_fields += self.default_ordering_fields

    def get_serializer_kwargs(self):
        return {}

    def get_serializer(self, *args, **kwargs):
        parent_kwargs = self.get_serializer_kwargs()
        parent_kwargs.update(kwargs)
        serializer = super().get_serializer(*args, **parent_kwargs)
        return serializer

    def get_serializer_class(self):
        serializer_class = self.serializer_class
        if self.serializer_class_map and self.action in self.serializer_class_map:
            serializer_class = self.serializer_class_map[self.action]
        assert serializer_class is not None
        return serializer_class

    def get_filter_fields(self):
        if self.filter_fields:
            return self.filter_fields
        elif self.filter_class:
            return [field_name for field_name in self.filter_class.get_fields()]
        return []

    def get_meta(self, serializer=None):
        if serializer is None:
            serializer = self.get_serializer()

        if getattr(serializer, 'many', False) and hasattr(serializer, 'child'):
            serializer = serializer.child
        renderer = ReadFieldRenderer(self.ordering_fields)
        result = OrderedDict([('fields', renderer.render(serializer))])

        filter_fields = self.get_filter_fields()
        if filter_fields:
            result['filter_fields'] = filter_fields

        extra_actions = self.get_extra_actions()
        result['extra_detail_actions'] = [action.__name__ for action in extra_actions if action.detail]
        result['extra_list_actions'] = [action.__name__ for action in extra_actions if not action.detail]
        permissions = self.get_permissions_map(self.request)
        permissions['search'] = bool(self.search_fields)
        permissions['filter'] = bool(filter_fields)
        permissions['sort'] = bool(self.sortable)
        result['permissions'] = permissions
        if settings.DEBUG:
            result['viewset'] = '.'.join((self.__class__.__module__, self.__class__.__name__))
        return result

    def list(self, request, *args, **kwargs):
        meta = request.GET.get('meta')
        if meta:
            if meta == "false":
                return super().list(request, *args, **kwargs)
            self.action = 'create'
            return Response({'meta': self.get_meta()})
        response = super().list(request, *args, **kwargs)
        response.data['meta'] = self.get_meta()
        return response

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        response = Response(serializer.data)
        response.data['meta'] = self.get_meta(serializer=serializer)
        return response

    def filter_queryset(self, queryset):
        serializer_class = self.get_serializer_class()
        if hasattr(serializer_class, 'setup_queryset'):
            queryset = serializer_class.setup_queryset(queryset)
        self.total_count = queryset.count()
        return super().filter_queryset(queryset).distinct()

    def update(self, request, *args, **kwargs):
        kwargs['partial'] = True
        return super().update(request, *args, **kwargs)

    def perform_update(self, serializer):
        ignore_concurrent_changes = self.request.data.get('ignore_concurrent_changes', False)
        serializer.instance._ignore_concurrent_changes = ignore_concurrent_changes
        return super().perform_update(serializer)

    @action(methods=['GET'], detail=True, url_path='delete-confirm', url_name='delete-confirm')
    def delete_confirm(self, request, *args, **kwargs):
        instance = self.get_object()
        using = router.db_for_write(instance.__class__, instance=instance)
        to_delete, model_count, protected, perms_needed = get_deleted_objects([instance], request.user, using)
        return Response(
            {'toDelete': to_delete, 'modelCount': model_count, 'protected': protected, 'permsNeeded': perms_needed}
        )

    delete_confirm.permitted_action = DESTROY

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        using = router.db_for_write(instance.__class__, instance=instance)
        _, _, protected, perms_needed = get_deleted_objects([instance], request.user, using)
        if protected or perms_needed:
            logger.error('Удаление запрещено.\nprotected: %s\nperms_needed: %s', protected, perms_needed, exc_info=1)
            return Response(status=status.HTTP_403_FORBIDDEN)
        try:
            self.perform_destroy(instance)
            return Response(status=status.HTTP_204_NO_CONTENT)
        except ProtectedError as e:
            logger.error('Удаление запрещено.\nProtectedError:%s', e, exc_info=1)
            return Response(status=status.HTTP_403_FORBIDDEN)

    def get_object(self, queryset=None):
        """
        Returns the object the view is displaying.

        You may want to override this if you need to provide non-standard
        queryset lookups.  Eg if objects are referenced using multiple
        keyword arguments in the url conf.
        """
        queryset = self.filter_queryset(queryset or self.get_queryset())

        # Perform the lookup filtering.
        lookup_url_kwarg = self.lookup_url_kwarg or self.lookup_field

        assert lookup_url_kwarg in self.kwargs, (
            'Expected view %s to be called with a URL keyword argument '
            'named "%s". Fix your URL conf, or set the `.lookup_field` '
            'attribute on the view correctly.' % (self.__class__.__name__, lookup_url_kwarg)
        )

        filter_kwargs = {self.lookup_field: self.kwargs[lookup_url_kwarg]}
        obj = get_object_or_404(queryset, **filter_kwargs)

        # May raise a permission denied
        self.check_object_permissions(self.request, obj)

        return obj

    @action(methods=['get', 'post'], detail=True)
    def restore(self, request, *args, **kwargs):
        queryset_ = self.queryset
        try:
            queryset = self.get_queryset().only_deleted()
            instance = self.get_object(queryset)
            if request.method == 'GET':
                using = router.db_for_write(instance.__class__, instance=instance)
                return Response({'children': get_restored_objects([instance], using)})

            if request.method == 'POST':
                cascade = request.data.get('cascade', True)
                instance.restore(restore_children=cascade)
                return Response(status=status.HTTP_200_OK)
        finally:
            self.queryset = queryset_

    @action(methods=['get', 'post'], detail=False, url_path='selected-restore', url_name='selected-restore')
    def selected_restore(self, request, *args, **kwargs):
        queryset_ = self.queryset
        try:
            queryset = self.get_queryset().only_deleted()
            records = self.get_selected_records(request, queryset=queryset)
            if request.method == 'GET':
                return Response({'children': get_restored_objects(records, records.db)})

            if request.method == 'POST':
                cascade = request.data.get('cascade', True)
                records = self.get_selected_records(request, queryset=queryset)
                records.restore(restore_children=cascade)
                return Response(status=status.HTTP_200_OK)
        finally:
            self.queryset = queryset_

    selected_restore.permitted_action = RESTORE

    def get_paginated_response(self, data):
        return self.paginator.get_paginated_response(data, self.total_count)

    def get_selected_records(self, request, queryset=None):
        selected = request.GET.get('selected', '[]')
        selected = json.loads(selected)

        if selected:
            queryset = queryset or self.get_queryset()

            selected_all = selected.get('all')
            checked = selected.get('checked')
            indeterminate = selected.get('indeterminate')

            if selected_all:
                # фильтруем
                records = self.filter_queryset(queryset)
            elif indeterminate:
                # фильтруем
                queryset = self.filter_queryset(queryset)
                records = queryset.exclude(pk__in=checked)
            elif checked:
                # не фильтруем
                records = queryset.filter(pk__in=checked)
            else:
                records = queryset.none()
            return records
        return None

    @action(methods=['get'], detail=False)
    def selected(self, request, *args, **kwargs):
        values = []
        choices = []
        records = self.get_selected_records(request)
        if records:
            values = records.values_list('pk', flat=True)
            choices = ((record.pk, str(record)) for record in records)
        data = {'values': values, 'choices': choices}
        fields = request.GET.get('fields')
        if records and fields:
            serializer = self.get_serializer(records, many=True)
            data['data'] = serializer.data

        return Response(data)

    @action(methods=['get', 'post'], detail=False, url_path='selected-delete')
    def selected_delete(self, request, *args, **kwargs):
        records = self.get_selected_records(request)
        if not records:
            return Response(status=status.HTTP_204_NO_CONTENT)

        if request.method == 'GET':
            to_delete, model_count, protected, perms_needed = get_deleted_objects(records, request.user, records.db)
            return Response(
                {'toDelete': to_delete, 'modelCount': model_count, 'protected': protected, 'permsNeeded': perms_needed}
            )

        if request.method == 'POST':
            deleted = []
            for record in records:
                deleted.append(record.pk)
                self.perform_destroy(record)
            return Response({'deleted': deleted})

    selected_delete.permitted_action = DESTROY

    @action(methods=['post'], detail=False, url_path='selected-delete/restore')
    def selected_delete_restore(self, request, *args, **kwargs):
        queryset_ = self.queryset
        deleted = request.data.get('deleted', [])
        try:
            queryset = self.get_queryset().only_deleted()
            records = queryset.filter(pk__in=deleted)
            records.restore()
        finally:
            self.queryset = queryset_
        return Response(status=status.HTTP_200_OK)

    selected_delete_restore.permitted_action = RESTORE

    @action(methods=['post'], detail=False)
    def sort(self, request, *args, **kwargs):
        page = int(request.query_params.get('page', None))
        page_size = int(request.query_params.get('page_size', None))
        type_ = request.data.get('type', None)
        data = request.data.get('data', None)

        if not (self.sortable and all((page, page_size, type_, data))):
            return Response(status=status.HTTP_400_BAD_REQUEST, data={})

        queryset = self.get_queryset()

        if type_ == 'move':
            mfrom = queryset.get(pk=data['from'])
            mdest = queryset.get(pk=data['dest'])
        else:
            mfrom = queryset.get(pk=data)
            if type_ == 'top':
                mdest = queryset.first()
            elif type_ == 'last':
                mdest = queryset.last()
            elif type_ == 'prev':
                mdest = queryset[(page - 1) * page_size - 1]
            else:
                mdest = queryset[page * page_size]

        if mfrom.order < mdest.order:
            order = mdest.order + 1
            grad = 1
        else:
            order = mdest.order - 1
            grad = -1

        mfrom.order = order
        mfrom.save()

        last = mfrom
        while True:
            last = queryset.filter(order=order).exclude(pk=last.pk)
            if not last.exists():
                break
            last = last.first()
            order += grad
            last.order = order
            last.save()

        return Response(status=status.HTTP_200_OK, data={})


class _NestedViewSet(ModelViewSet):
    parent_lookup_kwargs = {}

    def get_serializer(self, *args, **kwargs):
        parent_kwargs = self.get_serializer_kwargs()
        parent_kwargs.update(kwargs)
        serializer = super().get_serializer(*args, **parent_kwargs)
        for field_name in parent_kwargs.keys():
            if hasattr(serializer, 'fields'):
                parent_attr = field_name.rsplit('_id')[0].rsplit('_pk')[0]
                serializer.fields.pop(parent_attr, None)
        return serializer

    def get_parent_lookup(self):
        return {k: self.kwargs[v] for k, v in self.parent_lookup_kwargs.items()}

    def get_serializer_kwargs(self):
        return {k.split('__')[-1]: self.kwargs[v] for k, v in self.parent_lookup_kwargs.items()}

    def get_serializer_save_kwargs(self):
        return {k: self.kwargs[v] for k, v in self.parent_lookup_kwargs.items() if '__' not in k}

    def perform_create(self, serializer):
        serializer.save(**self.get_serializer_save_kwargs())

    def update(self, request, *args, **kwargs):
        # полностью переопределен метод без super,
        # чтобы добавить признак принудительно сохранения до вызова  perform_update
        partial = True
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)

        # признак принудительно сохранения
        ignore_concurrent_changes = self.request.data.get('ignore_concurrent_changes', False)
        instance._ignore_concurrent_changes = ignore_concurrent_changes

        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}
        return Response(serializer.data)

    def perform_update(self, serializer):
        serializer.save(**self.get_serializer_save_kwargs())

    def get_queryset(self):
        queryset = super().get_queryset()
        return queryset.filter(**self.get_parent_lookup())


serializers_related_models_cache = {}
