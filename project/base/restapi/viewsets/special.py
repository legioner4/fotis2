from collections import OrderedDict

from rest_framework import status
from rest_framework.decorators import action
from rest_framework.mixins import RetrieveModelMixin, UpdateModelMixin
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from base.deletion import get_deleted_objects
from base.restapi.permission import PermissionMethodMixin
from base.restapi.renderers import ReadFieldRenderer

HARD_DELETE = 'hard_delete'


class BaseModelRetrieveUpdateViewSet(PermissionMethodMixin, RetrieveModelMixin, UpdateModelMixin, GenericViewSet):
    def get_meta(self, serializer=None):
        if serializer is None:
            serializer = self.get_serializer()
        renderer = ReadFieldRenderer()
        fields = renderer.render(serializer)
        result = OrderedDict([('fields', fields)])

        extra_actions = self.get_extra_actions()
        result['extra_detail_actions'] = [action.__name__ for action in extra_actions if action.detail]
        result['extra_list_actions'] = [action.__name__ for action in extra_actions if not action.detail]
        permissions = self.get_permissions_map(self.request)
        result['permissions'] = permissions
        return result

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        response = Response(serializer.data)
        response.data['meta'] = self.get_meta(serializer=serializer)
        return response

    def update(self, request, *args, **kwargs):
        kwargs['partial'] = True
        return super().update(request, *args, **kwargs)

    def perform_update(self, serializer):
        ignore_concurrent_changes = self.request.data.get('ignore_concurrent_changes', False)
        serializer.instance._ignore_concurrent_changes = ignore_concurrent_changes
        return super().perform_update(serializer)


class HardDeletableMixin:
    @action(methods=['delete'], detail=True, url_name='hard-delete', url_path='hard-delete')
    def hard_delete(self, request, *args, **kwargs):
        queryset_ = self.queryset
        try:
            queryset = self.get_queryset().only_deleted()
            instance = self.get_object(queryset)
            instance.hard_delete()
        finally:
            self.queryset = queryset_
        return Response(status=status.HTTP_204_NO_CONTENT)

    @action(methods=['get', 'post'], detail=False, url_path='selected-hard_delete', url_name='selected-hard_delete')
    def selected_hard_delete(self, request, *args, **kwargs):
        queryset_ = self.queryset
        try:
            queryset = self.get_queryset().only_deleted()
            records = self.get_selected_records(request, queryset=queryset)
            if request.method == 'GET':
                to_delete, model_count, protected, perms_needed = get_deleted_objects(
                    records, request.user, records.db, check_permissions=False
                )
                return Response(
                    {
                        'toDelete': to_delete,
                        'modelCount': model_count,
                        'protected': protected,
                        'permsNeeded': perms_needed,
                    }
                )

            if request.method == 'POST':
                records = self.get_selected_records(request, queryset=queryset)
                records.hard_delete()
                return Response(status=status.HTTP_200_OK)
        finally:
            self.queryset = queryset_

    selected_hard_delete.permitted_action = HARD_DELETE
