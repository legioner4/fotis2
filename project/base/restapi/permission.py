from django.utils.functional import cached_property
from rest_framework.permissions import AllowAny, BasePermission
from rest_framework.views import APIView

__all__ = ['AllowAny']

LIST = 'list'
RETRIEVE = 'retrieve'
SELECT = 'selected'
CREATE = 'create'
UPDATE = 'update'
DESTROY = 'destroy'
RESTORE = 'restore'
SELECT_APPLY = 'selectedapply'
FILTER = 'filter'


READ = 'read'
WRITE = 'write'


class PermissionMethodMixin(APIView):
    any_permission_check = False
    permission_classes_map = {}

    read_actions = {LIST, RETRIEVE, SELECT, FILTER}
    write_actions = {CREATE, UPDATE, DESTROY, SELECT_APPLY}
    default_actions = read_actions | write_actions

    def get_permissions(self, action=None):
        if action is None:
            action = self.action

        permission_classes = self.permission_classes
        action = self._get_action(action)
        if action in self.permission_classes_map:
            permission_classes = self.permission_classes_map[action]
        elif action in self.read_actions:
            if READ in self.permission_classes_map:
                permission_classes = self.permission_classes_map[READ]
        elif action in self.write_actions:
            if WRITE in self.permission_classes_map:
                permission_classes = self.permission_classes_map[WRITE]
        elif action == RESTORE and DESTROY in self.permission_classes_map:
            permission_classes = self.permission_classes_map[DESTROY]

        if not isinstance(permission_classes, list):
            assert issubclass(permission_classes, BasePermission), 'Permission class must be an instance of'
            permission_classes = [permission_classes]

        return [permission() for permission in permission_classes]

    def has_action_permissions(self, request, action):
        if not (request.user and request.user.is_authenticated):
            return False
        checks = (p.has_permission(request, self) for p in self.get_permissions(action))
        return self._check_all_or_any(checks)

    def check_permissions(self, request):
        if not (request.user and request.user.is_authenticated):
            self.permission_denied(request)
        checks = (p.has_permission(request, self) for p in self.get_permissions())
        if not self._check_all_or_any(checks):
            self.permission_denied(request)

    def check_object_permissions(self, request, obj):
        if not (request.user and request.user.is_authenticated):
            self.permission_denied(request)
        checks = (p.has_object_permission(request, self, obj) for p in self.get_permissions())
        if not self._check_all_or_any(checks):
            self.permission_denied(request)

    def get_permissions_map(self, request):
        result = {}
        extra_actions = set(
            getattr(action, 'permitted_action', None) or action.url_name or action.__name__
            for action in self.get_extra_actions()
        )

        actions = self.default_actions | extra_actions | set(self.permission_classes_map.keys())
        for action in actions:
            action = self._get_action(action)
            result[action] = self.has_action_permissions(request, action)
        return result

    def _check_all_or_any(self, checks):
        return any(checks) if self.any_permission_check else all(checks)

    def _get_action(self, action):
        if action == 'partial_update':
            return UPDATE
        handler = action and getattr(self, action, None)
        return handler and getattr(handler, 'permitted_action', None) or action

    @cached_property
    def permission_action(self):
        return self._get_action(self.action)


class DenyAny(BasePermission):
    """ Инвертированный вариант rest_framework.permissions.AllowAny """

    def has_permission(self, request, view):
        return False
