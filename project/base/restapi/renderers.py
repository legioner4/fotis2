import datetime

from django.core.exceptions import FieldDoesNotExist
from django.db import models
from rest_framework import serializers
from rest_framework.fields import empty
from rest_framework.settings import api_settings
from rest_framework.utils.field_mapping import ClassLookupDict

from base.datasource.rest.fields import FormulaField
from base.restapi.fields import MonthField, RadioField
from base.restapi.serializer import SelectRelatedSerializer, ExtraRowRelatedSerializer
from chunked_upload_app.api.fields import ChunkUploadField, MultiChunkUploadField


class ReadFieldRenderer:
    default_types = ClassLookupDict(
        {
            serializers.Field: 'text',
            serializers.CharField: 'text',
            serializers.EmailField: 'email',
            serializers.URLField: 'url',
            serializers.IntegerField: 'number',
            serializers.FloatField: 'number',
            serializers.DateTimeField: 'datetime',
            serializers.DateField: 'date',
            serializers.TimeField: 'time',
            MonthField: 'month',
            serializers.BooleanField: 'boolean',
            serializers.NullBooleanField: 'boolean',
            serializers.ChoiceField: 'choice',
            RadioField: 'radio',
            serializers.MultipleChoiceField: 'select-multiple',
            serializers.RelatedField: 'select',
            serializers.ManyRelatedField: 'select-multiple',
            SelectRelatedSerializer: 'select-related',
            ExtraRowRelatedSerializer: 'extra-row-related',
            serializers.FileField: 'file',
            ChunkUploadField: 'chunk_upload',
            MultiChunkUploadField: 'multi_chunk_upload',
            serializers.ListField: 'list',
            serializers.HiddenField: 'hidden',
            FormulaField: 'formula',
        }
    )

    def __init__(self, ordering_fields=None):
        self.ordering_fields = self._parse_ordering_fields(ordering_fields)

    @staticmethod
    def _parse_ordering_fields(fields):
        fields = fields or dict()
        if isinstance(fields, dict):
            return fields
        fixed_fields = dict()
        for field in fields:
            # для поддержки source
            fixed_fields[field] = field
            # для поддержки
            key = field.split('__')[0]
            fixed_fields[key] = field
        return fixed_fields

    def get_related_info(self, field):
        if isinstance(field, SelectRelatedSerializer):
            return {'fields': self.render(field)}
        if isinstance(field, serializers.ListSerializer) and isinstance(field.child, ExtraRowRelatedSerializer):
            return {'fields': self.render(field.child)}
        return None

    def get_type(self, field):
        try:
            if isinstance(field, serializers.ListSerializer):
                return self.default_types[field.child]
            return self.default_types[field]
        except KeyError:
            return 'text'

    def get_model_default(self, serializer: serializers.Serializer, field_name: str):
        Meta = getattr(serializer, 'Meta', None)
        model = getattr(Meta, 'model', None)
        if not model:
            return None
        try:
            field = model._meta.get_field(field_name)
        except FieldDoesNotExist:
            return None

        if isinstance(field, models.Field):
            return field.get_default()
        return None

    def render(self, serializer):
        request = None
        if 'request' in serializer.context:
            request = serializer.context['request']

        for name, field in serializer.fields.items():
            ordering = name
            if field.source:
                ordering = '__'.join(field.source.split('.'))

            field_info = {
                'name': name,
                'type': self.get_type(field),
                'meta': field.style,
                'required': getattr(field, 'required', False),
                'ordering': self.ordering_fields.get(ordering, ''),
            }

            model_default = self.get_model_default(serializer, name)
            if model_default is not None:
                field_info['initial'] = model_default

            related_info = self.get_related_info(field)
            if related_info:
                field_info['related'] = related_info

            attrs = [
                'read_only',
                'write_only',
                'disabled',
                'label',
                'help_text',
                'min_length',
                'max_length',
                'min_value',
                'max_value',
                'allow_null',
                'allow_blank',
                'initial',
                'accept',
                'available_tags',
            ]
            for attr in attrs:
                value = getattr(field, attr, None)
                if value is not None and value != '':
                    field_info[attr] = value

            initial = field.get_initial()
            if initial is not empty:
                if isinstance(initial, datetime.datetime):
                    initial = initial.strftime(api_settings.DATETIME_FORMAT)
                if isinstance(initial, datetime.date):
                    initial = initial.strftime(api_settings.DATE_FORMAT)
                field_info['initial'] = initial

            if request:
                relation = field

                if hasattr(field, 'child_relation'):
                    relation = field.child_relation

            if hasattr(field, 'choices'):
                field_info['choices'] = ((k, v) for k, v in field.choices.items())
            yield field_info
