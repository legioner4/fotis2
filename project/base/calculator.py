import ast
import operator as op
import re
from decimal import Decimal

from base.mappings import Dict2Object


def power(a, b):
    if any(abs(n) > 100 for n in [a, b]):
        raise ValueError((a, b))
    return op.pow(a, b)


# supported operators
operators = {ast.Add: op.add, ast.Sub: op.sub, ast.Mult: op.mul, ast.Div: op.truediv, ast.Pow: power, ast.USub: op.neg}


def _eval(node, variables):
    if isinstance(node, str):
        return _eval(ast.parse(node, mode='eval').body, variables)
    if isinstance(node, ast.Name):
        return variables[node.id]
    if isinstance(node, ast.Num):
        val = node.n
        if isinstance(val, float):
            val = Decimal(val)
        return val
    elif isinstance(node, ast.BinOp):
        return operators[type(node.op)](_eval(node.left, variables), _eval(node.right, variables))
    elif isinstance(node, ast.UnaryOp):
        return operators[type(node.op)](_eval(node.operand, variables))
    elif isinstance(node, ast.Attribute):
        return getattr(_eval(node.value, variables), node.attr)
    elif isinstance(node, ast.Call):
        return _eval(node.func, variables)(*(_eval(a, variables) for a in node.args))
    else:
        raise TypeError(f'Незарегистрированная операция {node}')


def calculator(formula, get_var_values_fn):
    formula = re.sub(r'{([^\}]+)\}', r'\1', formula)
    formula = re.sub(r'\|([^|]+)\|', r'abs(\1)', formula)
    formula = formula.replace('%', '')

    st = ast.parse(formula, mode='eval')

    var_names = []

    for node in ast.walk(st):
        if isinstance(node, ast.Name):
            var_names.append(node.id)

    functions = {'ДОЛЯ': lambda x: x / 100, 'abs': abs}
    variables = {}
    variables.update(functions)
    variables.update(Dict2Object(get_var_values_fn(var_names)).__dict__)

    for var_name in var_names:
        if var_name not in variables:
            raise Exception(f'Не задано значение для {{{var_name}}}')

    return _eval(st.body, variables)
