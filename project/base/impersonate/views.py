from logging import getLogger

from django.contrib.auth import get_user_model
from django.shortcuts import get_object_or_404
from rest_framework import status
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView

from system.permissions import CanManageSystem

logger = getLogger(__name__)
User = get_user_model()


class StartImpersonateAPIView(APIView):
    permission_classes = [CanManageSystem]

    def post(self, request, pk, *args, **kwargs):
        """ Подмена пользователя """
        self.action = "impersonate"
        impersonator = request.user
        impersonating = get_object_or_404(User, pk=pk)
        request.session["_impersonating"] = impersonating.pk
        request.session["_impersonator"] = impersonator.pk
        request.session.modified = True
        logger.warning("Успешная подмена пользователя: %s -> %s" % (impersonator, impersonating))
        return Response(status=status.HTTP_200_OK)


class StopImpersonateAPIView(APIView):
    permission_classes = [AllowAny]

    def delete(self, request, *args, **kwargs):
        """ Отмена подмены пользователя """
        self.action = "delete-impersonate"
        impersonating = request.session.pop("_impersonating", None)
        impersonator = request.session.pop("_impersonator", None)
        if impersonating is not None:
            try:
                impersonating = User.objects.get(pk=impersonating)
            except User.DoesNotExist:
                logger.warning("Подменённого пользователя с PK %s не существует" % (impersonating,))
                impersonating = None

        if impersonating is not None:
            request.session.modified = True
            logger.warning("Подмена пользователя завершена %s -> %s" % (impersonating, impersonator))

        return Response(status=status.HTTP_200_OK)
