import datetime
from urllib.parse import unquote_plus

import pytz
from django.conf import settings
from django.contrib.auth import get_user_model
from django.utils import timezone

User = get_user_model()


def ImpersonateMiddleware(get_response):
    def middleware(request):
        request.user.is_impersonate = False
        request.impersonator = None
        if request.user.is_authenticated and "_impersonating" in request.session:
            impersonating = request.session["_impersonating"]
            if isinstance(impersonating, User):
                impersonating = impersonating.pk
            try:
                impersonating = User.objects.get(pk=impersonating)
            except User.DoesNotExist:
                return

            request.impersonator = request.user
            request.impersonating = impersonating
            request.user = impersonating
            request.user.is_impersonate = True

        request.real_user = request.impersonator or request.user

        response = get_response(request)
        return response

    return middleware


def _get_timezone_name(tz_offset):
    timezone_name = ''
    for zone in pytz.common_timezones:
        x = datetime.datetime.now(pytz.timezone(zone)).strftime('%z')
        if x == tz_offset:
            timezone_name = zone

    return timezone_name


def TimezoneMiddleware(get_response):
    def middleware(request):
        try:
            if request and hasattr(request, 'user') and request.user and hasattr(request.user, 'timezone'):
                tz = request.user.timezone
                timezone.activate(tz or settings.TIME_ZONE)
            elif request:
                from pytz import FixedOffset, all_timezones

                tz_offset = request.COOKIES.get(settings.TIMEZONE_OFFSET_COOKIE_NAME, False)
                tz_offset = tz_offset and unquote_plus(tz_offset)
                timezone_name = tz_offset and _get_timezone_name(tz_offset)
                timezone.activate(timezone_name or settings.TIME_ZONE)
        except Exception:
            pass
        return get_response(request)

    return middleware
