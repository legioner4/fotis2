from django.urls import path

from .views import StartImpersonateAPIView, StopImpersonateAPIView

app_name = "impersonate"

urlpatterns = [
    path("<pk>/", StartImpersonateAPIView.as_view(), name="impersonate"),
    path("", StopImpersonateAPIView.as_view(), name="stop-impersonate"),
]
