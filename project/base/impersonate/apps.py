from django.apps import AppConfig


class ImpersonateConfig(AppConfig):
    name = "base.impersonate"
