# -*- coding: utf-8 -*-
from threading import current_thread

import six
from django.contrib.auth import get_user_model
from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpRequest
from django.template import RequestContext
from django.utils.deprecation import MiddlewareMixin


class GlobalRequest(MiddlewareMixin):
    _requests = {}

    @staticmethod
    def get_request():
        return GlobalRequest._requests.get(current_thread(), None)

    @staticmethod
    def set_request(request):
        GlobalRequest._requests[current_thread()] = request

    @staticmethod
    def del_request():
        del GlobalRequest._requests[current_thread()]

    def process_request(self, request):
        GlobalRequest.set_request(request)
        request.current_user = self._get_current_user(request)

    def process_exception(self, request, exception):
        request.current_user = self._get_current_user(request)

    def process_response(self, request, response):
        # Cleanup
        thread = current_thread()
        if thread in GlobalRequest._requests:
            del GlobalRequest._requests[thread]
        return response

    @staticmethod
    def _get_current_user(request):
        if not hasattr(request, 'user') or not request.user:
            from django.contrib.auth.models import AnonymousUser
            user = AnonymousUser()
        else:
            user = request.user

        if hasattr(request, 'session') and 'im_user' in request.session:
            im_user = request.session.get('im_user') if user.is_authenticated() else None
            try:
                user = get_user_model().objects.get(pk=im_user)
            except (ObjectDoesNotExist, ValueError):
                # если нет в базе или в сесии какойто мусор
                request.session.pop('im_user')
        return user


def get_request(context=None):
    if context and isinstance(context, RequestContext):
        return context['request']
    return GlobalRequest.get_request()


class require_request(object):
    def __call__(self, func):
        @six.wraps(func)
        def decorated(*args, **kwargs):
            try:
                self._do_enter()
                ret = func(*args, **kwargs)
            finally:
                self._do_exit()
            return ret

        return decorated

    def _do_enter(self):
        self.old_request = GlobalRequest.get_request()
        self.request = self.old_request or HttpRequest()
        GlobalRequest.set_request(self.request)

    def _do_exit(self):
        if self.old_request:
            GlobalRequest.set_request(self.old_request)
        else:
            GlobalRequest.del_request()

    def __enter__(self):
        self._do_enter()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self._do_exit()
