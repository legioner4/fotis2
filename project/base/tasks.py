import json

from celery import shared_task
from django.core.files.base import ContentFile

from base.export_data.export import create_workbook


@shared_task
def export_data(user_id, serializer_data, fields, column_groups):
    if isinstance(fields, str):
        fields = json.loads(fields)
    ContentFile(create_workbook(serializer_data, fields, column_groups), name='Экспорт.xlsx')
