import logging

from .global_request import get_request

logger = logging.getLogger(__name__)


def get_current_user(request=None, context=None):
    request = request or get_request(context)
    if request:
        if hasattr(request, 'user') and request.user:
            return request.user

    from django.contrib.auth.models import AnonymousUser
    return AnonymousUser()
