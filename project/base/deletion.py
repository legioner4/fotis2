import re
from collections import Counter, defaultdict
from logging import getLogger
from operator import attrgetter

from django.db import transaction
from django.db.models import Q, signals, sql
from django.db.models.deletion import Collector, ProtectedError
from django.utils import timezone
from django.utils.encoding import force_text
from django.utils.text import capfirst

from system.permissions import CanManageSystem

logger = getLogger(__name__)


class RestorableCollector(Collector):
    for_restore = False

    def __init__(self, using, for_restore=False, restore_children=True, restore_parents=True):
        self.for_restore = for_restore
        self.restore_children = restore_children
        self.restore_parents = restore_parents
        super().__init__(using)

    def collect(self, objs, source=None, nullable=False, **kwargs):
        if self.for_restore and not self.restore_children:
            kwargs['collect_related'] = False
        super().collect(objs, source, nullable, **kwargs)
        if self.for_restore and source is None and self.restore_parents:
            edges = self.collect_parents(objs)
            seen = set()
            items = [[None, list(edges[None])]]
            while items:
                src, parents = items.pop(0)
                group_parents = {}
                for parent, attrs in parents:
                    if parent in seen:
                        continue
                    seen.add(parent)
                    items.append([parent, edges.get(parent, [])])
                    group_parents.setdefault(attrs.get('rel_name'), []).append(parent)
                if src:
                    for rel_name, _parents in group_parents.items():
                        self.collect(
                            _parents, source=src, nullable=nullable, reverse_dependency=True, source_attr=rel_name
                        )

    def related_objects(self, related, objs):
        qs = related.related_model._base_manager.using(self.using)
        if self.for_restore:
            if hasattr(qs, 'all_with_deleted'):
                qs = qs.all_with_deleted()

            # для случая восстановления немного иначе
            related_is_restorable = hasattr(related.related_model, 'deleted_at') and hasattr(
                related.related_model, 'deleted_by'
            )
            obj_is_restorable = hasattr(objs[0], 'deleted_at') and hasattr(objs[0], 'deleted_by')
            if related_is_restorable and obj_is_restorable:
                q = Q()
                for obj in objs:
                    q = q | Q(**{related.field.name: obj, 'deleted_at': obj.deleted_at, 'deleted_by': obj.deleted_by})
                return qs.filter(q)
            else:
                return qs.none()

        else:
            return qs.filter(**{"%s__in" % related.field.name: objs})

    def collect_parents(self, objs):
        edges = {None: [(obj, {}) for obj in objs]}
        while objs:
            next_objects = []
            for obj in objs:
                model = obj.__class__
                for related in get_parent_relations(model._meta):
                    rel_obj = getattr(obj, related.name, None)
                    if not rel_obj:
                        continue
                    if not getattr(rel_obj, 'deleted_at'):
                        # интересуют только удаленные
                        continue
                    if rel_obj in objs:
                        continue
                    edges.setdefault(obj, []).append((rel_obj, {'rel_name': related.name}))
                    next_objects.append(rel_obj)
            objs = next_objects
        return edges

    def restore(self, deleted_at=None, deleted_by=None):
        #  фильтруем связанные данные по deleted_at, deleted_by чтобы не восстанавливать лишнего

        restore_instances = set()
        for model, instances in self.data.items():
            self.data[model] = sorted(instances, key=attrgetter("pk"))
            restore_instances.update(instances)

        self.sort()

        restore_counter = Counter()

        def filter_deleted(instance):
            if deleted_at and instance.deleted_at != deleted_at:
                return False
            if deleted_by and instance.deleted_by != deleted_by:
                return False

            return True

        with transaction.atomic(using=self.using, savepoint=False):

            for qs in self.fast_deletes:
                for instance in qs:
                    if hasattr(instance, '_restore') and filter_deleted(instance):
                        restore_counter.update([instance._meta.label])
                        instance._restore()

            for _, instances in self.data.items():
                for instance in instances:
                    if hasattr(instance, '_restore') and filter_deleted(instance):
                        restore_counter.update([instance._meta.label])
                        instance._restore()

        return sum(restore_counter.values()), dict(restore_counter)


class SoftCollector(RestorableCollector):
    def can_fast_delete(self, objs, from_field=None):
        return False

    def delete(self, deleted_at=None, deleted_by=None):
        if deleted_at is None:
            deleted_at = timezone.now()

        # sort instance collections
        for model, instances in self.data.items():
            self.data[model] = sorted(instances, key=attrgetter("pk"))

        self.sort()

        deleted_instances = set()

        deleted_counter = Counter()

        with transaction.atomic(using=self.using, savepoint=False):
            # send pre_delete signals
            for model, instance in self.instances_with_model():
                if not model._meta.auto_created:
                    signals.pre_delete.send(sender=model, instance=instance, using=self.using)

                deleted_instances.add(instance)

            # fast deletes
            for qs in self.fast_deletes:
                for instance in qs:
                    deleted_counter.update([instance._meta.label])
                    instance._delete(deleted_at=deleted_at, deleted_by=deleted_by)

            # update fields
            for model, instances_for_field_values in self.field_updates.items():
                query = sql.UpdateQuery(model)
                for (field, value), instances in instances_for_field_values.items():
                    instances = (instance for instance in instances if instance not in deleted_instances)
                    query.update_batch([instance.pk for instance in instances], {field.name: value}, self.using)

            # reverse instance collections
            for instances in self.data.values():
                instances.reverse()

            for model, instances in self.data.items():
                for instance in instances:
                    if hasattr(instance, '_delete'):
                        deleted_counter.update([instance._meta.label])
                        instance._delete(deleted_at=deleted_at, deleted_by=deleted_by)
                        if not model._meta.auto_created:
                            signals.post_delete.send(sender=model, instance=instance, using=self.using, hard=False)

        # update collected instances
        for _, instances_for_field_values in self.field_updates.items():
            for (field, value), instances in instances_for_field_values.items():
                for instance in instances:
                    if instance not in deleted_instances:
                        setattr(instance, field.attname, value)

        return sum(deleted_counter.values()), dict(deleted_counter)


class HardCollector(Collector):
    def related_objects(self, related, objs):
        qs = related.related_model._base_manager.using(self.using)
        if hasattr(qs, 'all_with_deleted'):
            qs = qs.all_with_deleted()
        return qs.filter(**{"%s__in" % related.field.name: objs})


class TreeNestedObjects(RestorableCollector):
    def __init__(self, *args, user=None, check_permissions=True, **kwargs):
        super().__init__(*args, **kwargs)
        self.user = user
        self.check_permissions = check_permissions
        self.edges = {}  # {from_instance: [(to_instance, type)]}
        self.protected = set()
        self.perms_needed = set()
        self.model_objs = defaultdict(set)

    def add_edge(self, source, target, type_=None):
        if target._meta.auto_created:
            return
        model = type(target)
        can_check_permissions = (
            self.check_permissions
            and type_ is None
            and hasattr(model.objects, 'by_user')
            and self.user
            and not CanManageSystem.checker(self.user)
        )
        if can_check_permissions:
            has_permissions = model.objects.by_user(self.user).filter(pk=target.pk).exists()
            if not has_permissions:
                self.perms_needed.add(target)
                type_ = 'perms_needed'
        self.edges.setdefault(source, []).append((target, {'type': type_}))

    def collect(self, objs, source=None, source_attr=None, **kwargs):
        for obj in objs:
            if source_attr and not source_attr.endswith('+'):
                related_name = source_attr % {'class': source._meta.model_name, 'app_label': source._meta.app_label}
                if hasattr(obj, related_name):
                    self.add_edge(getattr(obj, related_name), obj)
                else:
                    self.add_edge(source, getattr(source, related_name))
            else:
                self.add_edge(None, obj)
            self.model_objs[obj._meta.model].add(obj)
        try:
            return super().collect(objs, source_attr=source_attr, **kwargs)
        except ProtectedError as e:
            field = re.search("(?<=protected foreign key: ')[^']+", e.args[0]).group(0)
            field = field.split('.')[-1]
            for obj in e.protected_objects:
                self.add_edge(getattr(obj, field), obj, type_='is_protected')
            self.protected.update(e.protected_objects)

    def related_objects(self, related, objs):
        qs = super().related_objects(related, objs)
        return qs.select_related(related.field.name)

    def _nested(self, edges, obj, seen, format_callback, **kwargs):
        if obj in seen:
            return []
        seen.add(obj)
        children = []
        for child, kw in edges.get(obj, ()):
            children.extend(self._nested(edges, child, seen, format_callback, **kw))
        if format_callback:
            name = format_callback(obj)
        else:
            name = str(obj)

        ret = {'id': f'{type(obj).__name__}-{obj.pk}', 'name': name}
        ret.update(kwargs)
        if children:
            ret['children'] = children
        return [ret]

    def nested(self, objs, format_callback=None):
        """
        Return the graph as a nested list.
        """
        self.collect(objs)

        seen = set()
        roots = []
        for root, kw in self.edges.get(None, ()):
            roots.extend(self._nested(self.edges, root, seen, format_callback, **kw))
        return roots

    def covered(self, objs, format_callback=None):
        seen = set()
        parents = []
        edges = self.collect_parents(objs)
        for parent, kw in edges[None]:
            parents.extend(self._nested(edges, parent, seen, format_callback, **kw))
        return parents

    def can_fast_delete(self, *args, **kwargs):
        """
        We always want to load the objects into memory so that we can display
        them to the user in confirm page.
        """
        return False


def get_deleted_objects(objs, user, using, check_permissions=True):
    collector = TreeNestedObjects(user=user, using=using, check_permissions=check_permissions)

    def format_callback(obj):
        try:
            text = force_text(obj)
        except Exception:
            text = obj.pk
        return '%s: %s' % (capfirst(obj._meta.verbose_name), text)

    to_delete = collector.nested(objs, format_callback)

    perms_needed = [format_callback(obj) for obj in collector.perms_needed]
    protected = [format_callback(obj) for obj in collector.protected]
    model_count = [
        (str(model._meta.verbose_name_plural), len(objs))
        for model, objs in collector.model_objs.items()
        if not model._meta.auto_created
    ]

    return to_delete, model_count, protected, perms_needed


def get_parent_relations(opts):
    return (
        f
        for f in opts.get_fields(include_hidden=True)
        if not f.auto_created and f.concrete and (f.one_to_one or f.many_to_one)
    )


def get_restored_objects(objs: list, using):
    collector = TreeNestedObjects(using, for_restore=True)

    def format_callback(obj):
        try:
            text = force_text(obj)
        except Exception:
            text = obj.pk
        return '%s: %s' % (capfirst(obj._meta.verbose_name), text)

    return collector.nested(objs, format_callback)
