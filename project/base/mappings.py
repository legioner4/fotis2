class Dict2Object:
    def __init__(self, dict_data):
        for k, v in dict_data.items():
            self.__dict__[k] = Dict2Object(v) if isinstance(v, dict) else v
