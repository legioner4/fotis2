# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import mimetypes

import six
from django import http as django_http
from django.contrib.sessions.backends.base import SessionBase
from django.http import HttpResponse, HttpRequest
from django.utils.encoding import force_text
from django.utils.http import urlquote


@six.python_2_unicode_compatible
class HttpException(Exception):
    status_code = None
    message = None

    def __init__(self, status_code=None, message=None):
        if status_code:
            self.status_code = status_code
        if message:
            self.message = message

    def __str__(self):
        return ':'.join(map(force_text, filter(None, (self.status_code, self.message))))


class Http304(HttpException):
    status_code = 304
    message = 'Not Modified'


class Http400(HttpException):
    status_code = 400
    message = 'Bad Request'


class Http401(HttpException):
    status_code = 401
    message = 'Unauthorized'


class Http403(HttpException):
    status_code = 403
    message = 'Forbidden'


class Http404(HttpException, django_http.Http404):
    status_code = 404
    message = 'Not Found'


class Http405(HttpException):
    status_code = 405
    message = 'Method Not Allowed'


class Http500(HttpException):
    status_code = 500
    message = 'Internal Server Error'


class Http503(HttpException):
    status_code = 503
    message = 'Service Temporarily Unavailable'


HttpNotModifiedException = Http304
HttpBadRequestException = Http400
HttpUnauthorizedException = Http401
HttpForbiddenException = Http403
HttpNotFoundException = Http404
HttpMethodNotAllowedException = Http405
HttpServerErrorException = Http500


def stream_to_http_response(stream, filename, is_attachment=True):
    response = HttpResponse(stream.read())
    mimetype, encoding = mimetypes.guess_type(filename)
    if mimetype is None:
        mimetype = 'application/octet-stream'

    if encoding is not None:
        response['Content-Encoding'] = encoding

    filename = urlquote(filename.encode('utf-8'))
    disposition = 'filename*=UTF-8\'\'%s' % filename
    if is_attachment:
        disposition = 'attachment; %s' % disposition

    response['Content-Length'] = str(stream.tell())
    response['Content-Type'] = mimetype
    response['Content-Disposition'] = disposition

    return response


class HttpRequestFake(HttpRequest):
    def __init__(self):
        super(HttpRequestFake, self).__init__()
        self.session = SessionBase()
        self.caches = None
