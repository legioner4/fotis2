from email.mime.image import MIMEImage
from functools import lru_cache

from django.contrib.staticfiles import finders


@lru_cache()
def logo_data():
    with open(finders.find(f'logo/logo.jpg'), 'rb') as f:
        logo_data = f.read()
    logo = MIMEImage(logo_data)
    logo.add_header('Content-ID', '<logo>')
    return logo
