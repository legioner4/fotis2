import collections

from django.conf import settings
from django.utils import timezone


def get_start_of_today():
    """ Дата время начала текущего дня """
    return timezone.now().replace(hour=0, minute=0, second=0, microsecond=0)


def get_current_year():
    return timezone.now().year


def get_next_year():
    return timezone.now().year + 1


def get_year_choices():
    if settings.PLANNING_PREVIOUS_YEARS:
        PLANNING_SINCE = 2010
    else:
        PLANNING_SINCE = get_next_year()
    return [(year, year) for year in range(get_next_year(), PLANNING_SINCE - 1, -1)]


def split_to_bunches(seq: collections.Sequence, size=100):
    count = len(seq)
    if count > size:
        for i in range(0, count, size):
            yield seq[i : i + size]
    else:
        yield seq
