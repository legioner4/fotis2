from django.core.exceptions import PermissionDenied
from django.http import Http404
from rest_framework import exceptions, status
from rest_framework.response import Response
from rest_framework.views import exception_handler as rest_exception_handler, set_rollback


def exception_handler(exc, context):
    return rest_exception_handler(exc, context)
    response = rest_exception_handler(exc, context)

    if response is not None:
        customized_response = {}
        customized_response['error'] = []

        for key, value in response.data.items():
            error = {'key': key, 'message': value}
            customized_response['error'].append(error)

        response.data = customized_response

        return response
    else:
        if isinstance(exc, Http404):
            exc = exceptions.NotFound()
        elif isinstance(exc, PermissionDenied):
            exc = exceptions.PermissionDenied()

        if isinstance(exc, exceptions.APIException):
            headers = {}
            if getattr(exc, 'auth_header', None):
                headers['WWW-Authenticate'] = exc.auth_header
            if getattr(exc, 'wait', None):
                headers['Retry-After'] = '%d' % exc.wait
            if isinstance(exc.detail, (list, dict)):
                data = exc.detail
            else:
                data = {'error': exc.detail}

            set_rollback()
            return Response(data, status=exc.status_code, headers=headers)

        data = {'error': str(exc)}
        return Response(data, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

