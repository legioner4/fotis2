from logging import getLogger

from django.contrib.auth import get_user_model
from django.db.models import Q
from django.utils.datastructures import OrderedSet
from django_group_permissions.checkers import has_any_permission
from django_group_permissions.permissions import register_permission
from rest_framework.permissions import IsAuthenticated

logger = getLogger(__name__)

PERMISSION_CLASS_MAP = {}


class Permission(IsAuthenticated):
    code = None
    name = None
    group = None
    # полномочия которые дают право на управление данным полномочием
    control_perms = []
    # полномочия которые включая текущее учитываются при работе
    depends_perms = []

    @classmethod
    def codes(cls):
        codes = [cls.code] if cls.code else []
        return cls.depends_codes(codes)

    @classmethod
    def depends_codes(cls, codes=None):
        """ Список кодов полномочий от которых зависит текущее полномочие"""
        codes = OrderedSet(codes or [])
        perms = cls.depends_perms[:]
        while perms:
            perm = perms.pop(0)
            if getattr(perm, 'code', None):
                if perm.code in codes:
                    continue
                codes.add(perm.code)

            if issubclass(perm, Permission):
                perms += perm.depends_perms
        return list(codes)

    @classmethod
    def checker(cls, user):
        codes = cls.codes()
        if codes:
            return has_any_permission(user, *codes)
        return False

    def has_permission(self, request, view):
        if super().has_permission(request, view):
            return self.checker(request.user)
        return False

    @classmethod
    def users(cls):
        codes = cls.codes()
        q = Q(groups__permissions__codename__in=codes) | Q(user_permissions__codename__in=codes)
        return get_user_model().objects.filter(q).distinct()

    @classmethod
    def has_control(cls, user):
        codes = []
        if user.is_superuser:
            return True
        for perm_cls in cls.control_perms:
            codes += perm_cls.codes()
        return has_any_permission(user, *codes)


def register(cls):
    assert cls.code
    assert cls.name
    PERMISSION_CLASS_MAP[cls.code] = cls
    register_permission(cls.code, cls.name, cls.group)
    return cls
