from django.contrib.auth.models import AnonymousUser
from .global_request import get_request


def get_request_user(request=None):
    request = request or get_request()
    if request:
        return request.user
    return AnonymousUser()
