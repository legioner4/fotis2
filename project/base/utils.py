import shutil
from io import BytesIO
from os import path

from django.core.files import File
from django.db.models.fields.files import FieldFile


def get_subclasses(classes, level=0):
    """
        Return the list of all subclasses given class (or list of classes) has.
        Inspired by this question:
        http://stackoverflow.com/questions/3862310/how-can-i-find-all-subclasses-of-a-given-class-in-python
    """
    # for convenience, only one class can can be accepted as argument
    # converting to list if this is the case
    if not isinstance(classes, list):
        classes = [classes]

    if level < len(classes):
        classes += classes[level].__subclasses__()
        return get_subclasses(classes, level + 1)
    else:
        return classes


def receiver_subclasses(signal, sender, dispatch_uid_prefix, **kwargs):
    """
    A decorator for connecting receivers and all receiver's subclasses to signals. Used by passing in the
    signal and keyword arguments to connect::

        @receiver_subclasses(post_save, sender=MyModel)
        def signal_receiver(sender, **kwargs):
            ...
    """

    def _decorator(func):
        all_senders = get_subclasses(sender)
        for snd in all_senders:
            signal.connect(func, sender=snd, dispatch_uid=dispatch_uid_prefix + '_' + snd.__name__, **kwargs)
        return func

    return _decorator


def clone_file(field_file):
    assert isinstance(field_file, FieldFile)
    stream = BytesIO()
    field_file.open()
    with field_file as source:
        shutil.copyfileobj(source, stream)
    __, name = path.split(field_file.name)
    stream.seek(0)
    return File(stream, name=name)
