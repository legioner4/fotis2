from logging import getLogger

from django.db import models

logger = getLogger(__name__)


class BaseModelManager(models.Manager):
    pass


class BaseModelQuerySet(models.QuerySet):
    pass


BaseModelManager = BaseModelManager.from_queryset(BaseModelQuerySet)
