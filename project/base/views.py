from django.contrib.auth.mixins import AccessMixin
from rest_framework.permissions import BasePermission


class HasPermissionMixin(AccessMixin):
    any_permission_check = False
    permission_classes = None
    permission_denied_message = 'У вас нет прав для выполнения этой операции.'

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            self.raise_exception = False
            return self.handle_no_permission()
        if not self.has_permissions(request):
            self.raise_exception = True
            return self.handle_no_permission()

        return super().dispatch(request, *args, **kwargs)

    def get_permissions(self):
        permission_classes = self.permission_classes

        if not isinstance(permission_classes, list):
            assert issubclass(permission_classes, BasePermission)
            permission_classes = [permission_classes]

        return (permission() for permission in permission_classes)

    def has_permissions(self, request):
        if not (request.user and request.user.is_authenticated):
            return False
        checks = (p.has_permission(request, self) for p in self.get_permissions())
        return any(checks) if self.any_permission_check else all(checks)
