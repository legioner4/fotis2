from base.permissions import register as register_permission

__all__ = ['register_permission']
