import logging
import os
import time

from django.conf import settings
from django.core.files.base import ContentFile
from django.core.files.uploadedfile import UploadedFile
from django.db import models

from chunked_upload_app.exceptions import UploadError
from chunked_upload_app.settings import settings as chunked_settings

logger = logging.getLogger(__name__)


class _NoMigrateFileField(models.FileField):
    """
    Позволяет исключить из миграций различные поля
    """

    def deconstruct(self):
        name, path, args, kwargs = super(_NoMigrateFileField, self).deconstruct()
        kwargs.pop('upload_to', None)
        kwargs.pop('storage', None)
        return name, path, args, kwargs


def _filename(filename):
    return time.strftime(os.path.join('{0}/%Y_%m_%d'.format(chunked_settings.UPLOAD_PATH), filename))


def _upload_filename(instance, filename):
    filename = '{0}.part'.format(instance.uuid)
    return _filename(filename)


def _chunk_filename(instance, filename):
    filename = '{0}/{1}.part'.format(instance.upload_file.uuid, instance.number)
    return _filename(filename)


class UploadFile(models.Model):
    CHUNKED_UPLOAD_STATE_UPLOADING = 1
    CHUNKED_UPLOAD_STATE_COMPLETE = 2

    CHUNKED_UPLOAD_CHOICES = (
        (CHUNKED_UPLOAD_STATE_UPLOADING, 'Загрузка'),
        (CHUNKED_UPLOAD_STATE_COMPLETE, 'Завершено'),
    )

    uuid = models.CharField(verbose_name='Идентификатор', max_length=36, unique=True, editable=False)
    file = _NoMigrateFileField(verbose_name='Файл', max_length=255, upload_to=_upload_filename, null=True, blank=True)
    filename = models.CharField(verbose_name='Имя файла', max_length=255)
    total_chunks = models.IntegerField(verbose_name='Кол-во фрагментов', default=0)
    total_size = models.BigIntegerField(verbose_name='Размер фрагментов', default=0)

    created_on = models.DateTimeField(verbose_name='Дата создания', auto_now_add=True)
    status = models.PositiveSmallIntegerField(
        verbose_name='Состояние', choices=CHUNKED_UPLOAD_CHOICES, default=CHUNKED_UPLOAD_STATE_UPLOADING
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, verbose_name='Владелец', on_delete=models.SET_NULL, null=True, blank=True
    )

    class Meta:
        verbose_name = 'Файл'
        verbose_name_plural = 'Файлы'
        db_table = 'chunked_upload_file'

    def __str__(self):
        return self.uuid

    @property
    def has_loaded(self):
        return self.status == self.CHUNKED_UPLOAD_STATE_COMPLETE

    def check_completion(self):
        if self.total_chunks == self.chunks.all().count():
            self.status = self.CHUNKED_UPLOAD_STATE_COMPLETE
            self.file.save(name='', content=ContentFile(''), save=True)

            self.file.close()
            self.file.open(mode='ab')
            for chunk in self.chunks.all().order_by('number'):
                try:
                    self.file.write(chunk.file.read())
                    chunk.file.close()
                except Exception as e:
                    logging.error('Сhunked Upload: {0}'.format(e))
                    raise UploadError('Ошибка во время загрузки')
            self.file.close()
            self.save()
            # при удалении файлов вылезает ошибка [WinError 32] Процесс не может получить доступ к файлу, так как этот файл занят другим процессом
            # удаление реализовано через периодические задачи

    def get_uploaded_file(self):
        self.file.close()
        self.file.open(mode='rb')
        return UploadedFile(file=self.file, name=self.filename, size=self.file.size)


class FileChunk(models.Model):
    upload_file = models.ForeignKey(UploadFile, verbose_name='Файл', related_name='chunks', on_delete=models.CASCADE)

    file = _NoMigrateFileField(verbose_name='Файл', max_length=255, upload_to=_chunk_filename)
    number = models.IntegerField(verbose_name='Номер фрагмента', default=0)
    size = models.BigIntegerField(verbose_name='Размер фрагмента', default=0)

    class Meta:
        verbose_name = 'Фрагмент файла'
        verbose_name_plural = 'Фрагменты файлов'
        db_table = 'chunked_file_chunk'

    def __str__(self):
        return str(self.number)
