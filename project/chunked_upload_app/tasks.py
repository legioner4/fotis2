import logging
import os
import shutil
import time

from celery import shared_task
from django.conf import settings
from django.utils import timezone

from chunked_upload_app.models import UploadFile
from chunked_upload_app.settings import settings as chunked_settings

logger = logging.getLogger(__name__)


def get_old_dirs(path, older_than_days):
    """
    return a list of all subfolders under dirPath older than olderThanDays
    """
    older_than_days *= 86400  # convert days to seconds
    now = time.time()
    for root, dirs, _ in os.walk(path, topdown=False):
        for name in dirs:
            sub_dir_path = os.path.join(root, name)
            if (now - os.path.getmtime(sub_dir_path)) > older_than_days:
                yield sub_dir_path


@shared_task
def remove_old_upload_files():
    old_files = UploadFile.objects.filter(created_on__lte=timezone.now() - chunked_settings.EXPIRATION_DELTA)
    old_files.delete()

    media_root = getattr(settings, 'MEDIA_ROOT', None)
    file_path = os.path.join(media_root, chunked_settings.UPLOAD_PATH)
    for dir_path in get_old_dirs(file_path, 1):
        try:
            shutil.rmtree(dir_path)
        except Exception as e:
            logging.error('Removed chunked old files error: {0}'.format(e))
