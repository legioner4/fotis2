import logging

from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from chunked_upload_app.api import serializers
from chunked_upload_app.exceptions import UploadError
from chunked_upload_app.models import UploadFile

logger = logging.getLogger(__name__)


class ChunkedUploadView(ModelViewSet):
    permission_classes = [IsAuthenticated]
    queryset = UploadFile.objects.all()
    serializer_class = serializers.ChunkedUploadSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        try:
            upload_file = serializer.save()
            upload_file.check_completion()
            return Response(
                {'uuid': upload_file.uuid, 'loaded': upload_file.has_loaded}, status=status.HTTP_201_CREATED
            )
        except UploadError as e:
            return Response({'error': str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
