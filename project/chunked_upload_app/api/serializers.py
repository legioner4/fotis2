import logging
import os

from django.utils.encoding import force_text
from rest_framework import serializers

from base.restapi.serializer import BaseSerializer
from chunked_upload_app import models
from chunked_upload_app.exceptions import UploadError
from chunked_upload_app.settings import settings as chunked_settings, sizeof_fmt

logger = logging.getLogger(__name__)


class ChunkedUploadSerializer(BaseSerializer):
    identifier = serializers.CharField(label='Идентификатор', write_only=True)
    filename = serializers.CharField(label='Наименование файла', write_only=True)
    file = serializers.FileField(label='Файл', write_only=True)
    chunkNumber = serializers.IntegerField(label='Номер фрагмента', write_only=True)
    totalChunks = serializers.IntegerField(label='Кол-во фрагментов', write_only=True)
    currentChunkSize = serializers.IntegerField(label='Размер фрагмента', write_only=True)
    totalSize = serializers.IntegerField(label='Размер файла', write_only=True)
    accept = serializers.CharField(label='Поддерживаемые типы файлов', write_only=True)

    class Meta:
        model = models.UploadFile
        fields = [
            'identifier',
            'filename',
            'file',
            'chunkNumber',
            'totalChunks',
            'currentChunkSize',
            'totalSize',
            'accept',
        ]

    def validate(self, data):
        file = data.get('file')
        accept = data.get('accept')
        total_size = data.get('totalSize')

        if accept:
            file_ext = os.path.splitext(file.name)[1].lower()
            accept = force_text(accept).split(',')
            content_type_group = file.content_type.split('/')[0] + '/*'

            if {file.content_type, content_type_group, file_ext}.isdisjoint(accept):
                raise serializers.ValidationError({'error': "Недопустимый тип файла"})

            if file_ext.lstrip('.') in chunked_settings.HIGH_RISK_EXTENSIONS:
                raise serializers.ValidationError({'error': "Загрузка на сервер файлов данного типа запрещена"})

            if total_size > chunked_settings.MAX_BYTES:
                raise serializers.ValidationError(
                    {'error': f'Размер файла превышает лимит {sizeof_fmt(chunked_settings.MAX_BYTES)}'}
                )
        else:
            raise serializers.ValidationError({'error': "В загрузке файла отказано"})
        return data

    def create(self, validated_data):
        identifier = validated_data.get('identifier')
        filename = validated_data.get('filename')
        file = validated_data.get('file')
        chunk_number = validated_data.get('chunkNumber')
        total_сhunks = validated_data.get('totalChunks')
        current_сhunk_size = validated_data.get('currentChunkSize')
        total_size = validated_data.get('totalSize')

        upload_file, created = models.UploadFile.objects.get_or_create(uuid=identifier)
        if created:
            upload_file.filename = filename
            upload_file.total_chunks = total_сhunks
            upload_file.total_size = total_size
            upload_file.save()

        if not file:
            raise UploadError('Ошибка во время загрузки')

        if file.size == current_сhunk_size:
            # https://github.com/simple-uploader/Uploader
            # You should allow for the same chunk to be uploaded more than once; this isn't standard behaviour,
            # but on an unstable network environment it could happen, and this case is exactly what Uploader.js is designed for.
            upload_file.chunks.filter(number=chunk_number).delete()

            file_chunk = models.FileChunk.objects.create(
                upload_file=upload_file, number=chunk_number, size=current_сhunk_size
            )
            file_chunk.file.save(name='', content=file, save=True)
        return upload_file
