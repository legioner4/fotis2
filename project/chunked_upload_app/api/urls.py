from rest_framework import routers

from . import views

app_name = 'chunked_upload_app'

router = routers.DefaultRouter()
router.register(r'upload', views.ChunkedUploadView, 'upload')

urlpatterns = router.urls
