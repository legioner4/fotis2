import os

from rest_framework import serializers
from rest_framework.fields import FileField, ListField

from chunked_upload_app.models import UploadFile
from chunked_upload_app.settings import settings as chunked_settings, sizeof_fmt


class ChunkUploadField(FileField):
    instance = None
    accept = ()

    def __init__(self, accept=chunked_settings.UPLOAD_EXTENSIONS, *args, **kwargs):
        self.accept = accept
        super().__init__(*args, **kwargs)

    def run_validation(self, data=serializers.empty, instance=None):
        self.instance = instance
        return super().run_validation(data)

    def run_validators(self, file):
        file_ext = os.path.splitext(file.name)[1].lower()
        if file_ext not in self.accept:
            raise serializers.ValidationError('Загрузка на сервер файлов данного типа запрещена')
        if file.size > chunked_settings.MAX_BYTES:
            raise serializers.ValidationError(
                {'error': 'Размер файла превышает лимит (%s байт)' % chunked_settings.MAX_BYTES}
            )

    def to_internal_value(self, uuid):
        if not uuid:
            return None
        if isinstance(uuid, dict):
            initial = None
            if self.source:
                initial = getattr(self.instance, self.source, None)
            elif not initial:
                initial = getattr(self.instance, self.field_name, None)
            return initial
        uploaded_file = UploadFile.objects.get(uuid=uuid)
        return uploaded_file.get_uploaded_file()

    def to_representation(self, file):
        if not file:
            return None

        if not getattr(file, 'url', None):
            return None

        url = file.url
        request = self.context.get('request', None)
        if request is not None:
            url = request.build_absolute_uri(url)

        filename = os.path.basename(file.name)
        return {'name': filename, 'path': url}


class MultiChunkUploadField(ListField):
    instance_field_name = None
    accept = ()

    def __init__(self, instance_field_name=None, accept=chunked_settings.UPLOAD_EXTENSIONS, *args, **kwargs):
        self.instance_field_name = instance_field_name
        self.accept = accept
        super().__init__(*args, **kwargs)

    def to_internal_value(self, uuid_list):
        not_removed_file_pks = []
        add_file_uuid = []
        for uuid in uuid_list:
            if isinstance(uuid, dict):
                not_removed_file_pks.append(uuid['pk'])
            else:
                add_file_uuid.append(uuid)
        uploaded_files = UploadFile.objects.filter(uuid__in=add_file_uuid)
        return [uploaded_file.get_uploaded_file() for uploaded_file in uploaded_files], not_removed_file_pks

    def run_validators(self, value):
        upload_files, _ = value
        for file in upload_files:
            file_ext = os.path.splitext(file.name)[1].lower()
            filename = os.path.basename(file.name)
            if file_ext not in self.accept:
                raise serializers.ValidationError(f'Загрузка на сервер файла {filename} данного типа запрещена')
            if file.size > chunked_settings.MAX_BYTES:
                raise serializers.ValidationError(
                    f'Размер файла {filename} превышает лимит {sizeof_fmt(chunked_settings.MAX_BYTES)}'
                )

    def to_representation(self, many_related_manager):
        if self.instance_field_name:
            queryset = many_related_manager.all()
            files = [
                (instance.pk, getattr(instance, self.instance_field_name))
                for instance in queryset
                if getattr(instance, self.instance_field_name, None)
            ]

            def file_data(pk, file):
                url = file.url
                request = self.context.get('request', None)
                if request is not None:
                    url = request.build_absolute_uri(url)
                filename = os.path.basename(file.name)
                return {'pk': pk, 'name': filename, 'path': url}

            return [file_data(pk, file) for pk, file in files if getattr(file, 'url', None)]
        return None
