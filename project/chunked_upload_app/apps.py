from django.apps import AppConfig


class ChunkedUploadConfig(AppConfig):
    name = 'chunked_upload_app'
    verbose_name = 'Закачка файлов частями'
