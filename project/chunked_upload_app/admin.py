from django.contrib import admin

from . import models

admin.site.register(models.FileChunk)
admin.site.register(models.UploadFile)
