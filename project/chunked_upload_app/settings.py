from datetime import timedelta

from django.conf import settings as django_settings
from django.utils.functional import cached_property


def sizeof_fmt(num):
    for unit in ['байт', 'Кб', 'Мб', 'ГБ']:
        if abs(num) < 1024:
            return f"{num:3} {unit}"
        num //= 1024
    return f"{num} Тб"


class _ChunkedUploadSettings(object):
    @cached_property
    def UPLOAD_PATH(self):
        DEFAULT_UPLOAD_PATH = 'chunked_uploads'
        return getattr(django_settings, 'CHUNKED_UPLOAD_PATH', DEFAULT_UPLOAD_PATH)

    @cached_property
    def EXPIRATION_DELTA(self):
        DEFAULT_EXPIRATION_DELTA = timedelta(days=1)
        return getattr(django_settings, 'CHUNKED_UPLOAD_EXPIRATION_DELTA', DEFAULT_EXPIRATION_DELTA)

    @cached_property
    def HIGH_RISK_EXTENSIONS(self):
        DEFAULT_HIGH_RISK_EXTENSIONS = {
            'action',
            'apk',
            'app',
            'bat',
            'bin',
            'cmd',
            'com',
            'command',
            'cpl',
            'csh',
            'exe',
            'gadget',
            'inf',
            'ins',
            'inx',
            'ipa',
            'isu',
            'job',
            'jse',
            'ksh',
            'lnk',
            'msc',
            'msi',
            'msp',
            'mst',
            'osx',
            'out',
            'paf',
            'pif',
            'prg',
            'ps1',
            'reg',
            'rgs',
            'run',
            'scr',
            'sct',
            'shb',
            'shs',
            'u3p',
            'vb',
            'vbe',
            'vbs',
            'vbscript',
            'workflow',
            'ws',
            'wsf',
            'wsh',
        }
        return getattr(django_settings, 'HIGH_RISK_EXTENSIONS', DEFAULT_HIGH_RISK_EXTENSIONS)

    @cached_property
    def UPLOAD_EXTENSIONS(self):
        DEFAULT_UPLOAD_EXTENSIONS = (
            '.7z',
            '.doc',
            '.docx',
            '.jpeg',
            '.jpg',
            '.odt',
            '.pdf',
            '.png',
            '.pptx',
            '.rar',
            '.rtf',
            '.tar',
            '.vsd',
            '.xlsx',
            '.zip',
        )
        return getattr(django_settings, 'UPLOAD_EXTENSIONS', DEFAULT_UPLOAD_EXTENSIONS)

    @cached_property
    def MAX_BYTES(self):
        DEFAULT_MAX_BYTES = 1 * 1024 * 1024
        return getattr(django_settings, 'UPLOAD_MAX_BYTES', DEFAULT_MAX_BYTES)


settings = _ChunkedUploadSettings()
