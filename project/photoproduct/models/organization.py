from django.db import models
from django.utils.translation import ugettext as _

from base.models import BaseModel, SortableMixin


class OrgProductCategory(BaseModel, SortableMixin):
    LIST_OPTION = 'list'
    PICTURE_OPTION = 'picture'
    DISPLAY_OPTIONS = (
        (LIST_OPTION, _('Список')),
        (PICTURE_OPTION, _('Картинка')),
    )
    organization = models.ForeignKey('organization.Organization', models.CASCADE, related_name='org_categories', verbose_name='Организация')
    base_category = models.ForeignKey('photoproduct.BaseProductCategory', models.CASCADE, related_name='org_categories', verbose_name='Базовая категория')
    name = models.CharField(verbose_name='Название', max_length=512)
    icon = models.ImageField(verbose_name='Иконка', upload_to='photoproduct/org/category/icon/', max_length=1024)

    description = models.CharField(verbose_name='Описание', max_length=512, null=True, blank=True)
    display_option = models.CharField(verbose_name='Вариант показа', max_length=16, choices=DISPLAY_OPTIONS)
    enabled = models.BooleanField(verbose_name='Включен', default=False)

    class Meta:
        verbose_name = 'катагория продукции организации'
        verbose_name_plural = 'катагория продукции организации'
        ordering = ('created_at',)

    def __str__(self):
        return f'{self.name}'


class GalleryPhoto(BaseModel):
    category = models.ForeignKey(OrgProductCategory, models.CASCADE, related_name='photos', verbose_name='Категория')
    photo = models.ImageField(verbose_name='Фото', upload_to='photoproduct/org/category/gallery/', max_length=1024)

    class Meta:
        verbose_name = 'фото галереи'
        verbose_name_plural = 'фото галереи'
        ordering = ('created_at',)

    def __str__(self):
        return f'{self.pk}'


class OrgProduct(BaseModel, SortableMixin):
    category = models.ForeignKey(OrgProductCategory, models.CASCADE, related_name='products', verbose_name='Категория')
    base_product = models.ForeignKey('photoproduct.BaseProduct', models.CASCADE, related_name='org_products', verbose_name='Базовая продукция')

    name = models.CharField(verbose_name='Название', max_length=512)
    price = models.DecimalField(verbose_name='Цена', max_digits=6, decimal_places=2, default=0)
    enabled = models.BooleanField(verbose_name='Включен', default=False)

    class Meta:
        verbose_name = 'продукция'
        verbose_name_plural = 'продукция'
        ordering = ('created_at',)

    def __str__(self):
        return f'{self.name}'


