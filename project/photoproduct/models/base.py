from django.db import models
from django.utils.translation import ugettext as _
from uuslug import slugify

from base.models import BaseModel


class BaseProductCategory(BaseModel):
    PHOTOBOOK_GROUP = 'photobook'
    SOUVENIR_GROUP = 'souvenir'
    INTERIOR_GROUP = 'interior'
    CALENDAR_GROUP = 'calendar'
    POLYGRAPHY_GROUP = 'polygraphy'
    GROUPS = (
        (PHOTOBOOK_GROUP, _('Фотокниги')),
        (SOUVENIR_GROUP, _('Сувенирка')),
        (INTERIOR_GROUP, _('Интерьерка')),
        (CALENDAR_GROUP, _('Календари')),
        (POLYGRAPHY_GROUP, _('Полиграфия')),
    )
    name = models.CharField(verbose_name='Название', max_length=512)
    group = models.CharField(verbose_name='Группа', choices=GROUPS, max_length=64)
    icon = models.ImageField(verbose_name='Иконка', upload_to='photoproduct/base/category/icon/', max_length=1024)

    class Meta:
        verbose_name = 'категория продукции'
        verbose_name_plural = 'категория продукции'
        ordering = ('created_at',)

    def __str__(self):
        return f'{self.name}'


class CategoryFormat(BaseModel):
    category = models.ForeignKey(BaseProductCategory, models.CASCADE, related_name='formats', verbose_name='Категория')
    name = models.CharField(verbose_name='Наименование', max_length=256)
    slug = models.SlugField(null=False)

    description = models.CharField(verbose_name='Описание', max_length=512, null=True, blank=True)

    width = models.PositiveIntegerField(verbose_name='Ширина (мм)', null=True, blank=True)
    height = models.PositiveIntegerField(verbose_name='Высота (мм)', null=True, blank=True)

    class Meta:
        verbose_name = 'формат'
        verbose_name_plural = 'форматы'
        ordering = ('name',)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name, separator="_")
        return super().save(*args, **kwargs)

    def __str__(self):
        return f'{self.name}'


class BaseProduct(BaseModel):
    category = models.ForeignKey(BaseProductCategory, models.CASCADE, related_name='category_products', verbose_name='Категория')
    format = models.ForeignKey(CategoryFormat, models.SET_NULL, related_name='format_products', verbose_name='Формат', null=True, blank=True)

    name = models.CharField(verbose_name='Название', max_length=512)
    icon = models.ImageField(verbose_name='Иконка', upload_to='photoproduct/base/category/icon/', max_length=1024)
    background_image = models.ImageField(verbose_name='Фоновое изображение', upload_to='photoproduct/base/category/background/', max_length=1024)

    class Meta:
        verbose_name = 'продукция'
        verbose_name_plural = 'продукция'
        ordering = ('created_at',)

    def __str__(self):
        return f'{self.name}'
