from django.contrib import admin

from .models import *

admin.site.register(BaseProductCategory)
admin.site.register(CategoryFormat)
admin.site.register(BaseProduct)

admin.site.register(OrgProductCategory)
admin.site.register(GalleryPhoto)
admin.site.register(OrgProduct)
