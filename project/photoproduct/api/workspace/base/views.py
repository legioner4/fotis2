from base.restapi.translation.logic.utils import get_field_name_translation_fields
from base.restapi.translation.views import TranslationFieldBaseModelViewSet, TranslationFieldNestedViewSet
from photoproduct import models
from photoproduct.api.workspace.base import serializers
from system.permissions import CanManageSystem


class PermissionMixin(object):
    permission_classes = CanManageSystem


class AllLanguagesMixin(object):
    def get_translation_fields(self, field_name, translation_organization):
        return get_field_name_translation_fields(field_name, translation_organization, False)


class CategoryView(PermissionMixin, AllLanguagesMixin, TranslationFieldBaseModelViewSet):
    queryset = models.BaseProductCategory.objects
    serializer_class = serializers.BaseProductCategorySerializer
    ordering_fields = (
        "name",
    )

    def get_group(self):
        return self.kwargs.get('group', models.BaseProductCategory.PHOTOBOOK_GROUP)

    def get_queryset(self):
        q = super().get_queryset()
        group = self.get_group()
        return q.filter(group=group)

    def perform_create(self, serializer):
        group = self.get_group()
        serializer.save(group=group)


class CategoryFormatView(PermissionMixin, AllLanguagesMixin, TranslationFieldNestedViewSet):
    parent_lookup_kwargs = {'category_id': 'category_pk'}
    serializer_class = serializers.CategoryFormatSerializer
    queryset = models.CategoryFormat.objects
    ordering_fields = ("name",)


class CategoryProductView(PermissionMixin, AllLanguagesMixin, TranslationFieldNestedViewSet):
    parent_lookup_kwargs = {'category_id': 'category_pk'}
    serializer_class = serializers.BaseProductSerializer
    queryset = models.BaseProduct.objects
    ordering_fields = ("name",)
