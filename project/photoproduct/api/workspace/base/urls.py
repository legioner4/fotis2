from rest_framework import routers
from rest_framework_nested.routers import NestedSimpleRouter

from . import views

app_name = 'base_photoproduct'

router = routers.DefaultRouter()
router.register(r'categories/(?P<group>[\w-]+)', views.CategoryView, 'category')
category_router = NestedSimpleRouter(router, r'categories/(?P<group>[\w-]+)', lookup='category')
category_router.register(r'formats', views.CategoryFormatView, 'format')
category_router.register(r'products', views.CategoryProductView, 'product')


urlpatterns = []
urlpatterns += router.urls
urlpatterns += category_router.urls
