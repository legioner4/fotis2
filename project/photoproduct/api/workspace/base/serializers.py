import logging

from base.restapi.translation.logic.utils import get_field_name_translation_fields, \
    get_field_name_translation_fields_and_languages
from base.restapi.translation.serializer import TranslationSerializer
from chunked_upload_app.api.fields import ChunkUploadField
from photoproduct import models

logger = logging.getLogger(__name__)


class AllLanguagesMixin(TranslationSerializer):
    def get_translation_fields(self, field_name, translation_organization):
        return get_field_name_translation_fields(field_name, translation_organization, False)

    def get_translation_fields_and_languages(self, field_name, translation_organization):
        return get_field_name_translation_fields_and_languages(field_name, translation_organization, False)


class BaseProductCategorySerializer(AllLanguagesMixin):
    icon = ChunkUploadField(label='Иконка', required=True)

    class Meta:
        model = models.BaseProductCategory
        fields = "__all__"


class CategoryFormatSerializer(AllLanguagesMixin):

    class Meta:
        model = models.CategoryFormat
        fields = "__all__"

    def __init__(self, *args, category_id=None, **kwargs):
        self.category_id = category_id
        super().__init__(*args, **kwargs)


class BaseProductSerializer(AllLanguagesMixin):
    icon = ChunkUploadField(label='Иконка', required=True)
    background_image = ChunkUploadField(label='Фоновое изображение', required=True)

    class Meta:
        model = models.BaseProduct
        fields = "__all__"
        extra_kwargs = {"format": {"required": True}}

    def __init__(self, *args, category_id=None, **kwargs):
        self.category_id = category_id
        super().__init__(*args, **kwargs)
        if 'format' in self.fields:
            self.fields['format'].queryset = models.CategoryFormat.objects.filter(category__pk=self.category_id)
