from django.conf.urls import include
from django.urls import path

app_name = 'organization'

urlpatterns = [
    path('base/', include('photoproduct.api.workspace.base.urls', namespace='base')),
    path('organization/', include('photoproduct.api.workspace.organization.urls', namespace='organization')),
]
