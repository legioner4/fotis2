import logging

from base.restapi.translation.serializer import TranslationSerializer
from chunked_upload_app.api.fields import ChunkUploadField, MultiChunkUploadField
from photoproduct import models
from photoproduct.logic.actions import copy_base_category_data, copy_base_product_data

logger = logging.getLogger(__name__)


class OrgProductCategorySerializer(TranslationSerializer):
    icon = ChunkUploadField(label='Иконка', required=True)
    photos = MultiChunkUploadField(label='Фотографии', instance_field_name='photo', required=False)

    class Meta:
        model = models.OrgProductCategory
        fields = "__all__"

    def __init__(self, *args, group=None, **kwargs):
        self.group = group
        super().__init__(*args, **kwargs)
        if 'base_category' in self.fields:
            self.fields['base_category'].queryset = models.BaseProductCategory.objects.filter(group=self.group)

    def create(self, validated_data):
        instance = super().create(validated_data)
        copy_base_category_data(instance)
        return instance

    def update(self, instance, validated_data):
        photos = validated_data.pop('photos')
        instance = super().update(instance, validated_data)
        if photos:
            news, olds = photos
            instance.photos.exclude(pk__in=olds).delete()
            for photo in news:
                models.GalleryPhoto.objects.create(category=instance, photo=photo)
        return instance


class OrgProductSerializer(TranslationSerializer):

    class Meta:
        model = models.OrgProduct
        fields = "__all__"

    def __init__(self, *args, category_id=None, **kwargs):
        self.category_id = category_id
        org_category = models.OrgProductCategory.objects.get(pk=category_id)
        super().__init__(*args, **kwargs)
        if 'base_product' in self.fields:
            self.fields['base_product'].queryset = models.BaseProduct.objects.filter(category=org_category.base_category)

    def create(self, validated_data):
        instance = super().create(validated_data)
        copy_base_product_data(instance)
        return instance
