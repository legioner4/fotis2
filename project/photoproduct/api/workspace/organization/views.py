from base.restapi.translation.views import TranslationFieldBaseModelViewSet, TranslationFieldNestedViewSet
from organization import permissions
from photoproduct import models
from photoproduct.api.workspace.organization import serializers


class PermissionMixin(object):
    permission_classes = permissions.CanManageOrganizations


class CategoryView(PermissionMixin, TranslationFieldBaseModelViewSet):
    queryset = models.OrgProductCategory.objects
    serializer_class = serializers.OrgProductCategorySerializer
    ordering_fields = (
        "name",
    )

    def get_serializer_kwargs(self):
        return {'group': self.get_group()}

    def perform_create(self, serializer):
        serializer.save(organization=self.request.user.organization)

    def get_group(self):
        return self.kwargs.get('group', models.BaseProductCategory.PHOTOBOOK_GROUP)

    def get_queryset(self):
        q = super().get_queryset()
        group = self.get_group()
        return q.filter(organization__pk=self.request.user.organization.pk).filter(base_category__group=group)


class OrgProductView(PermissionMixin, TranslationFieldNestedViewSet):
    parent_lookup_kwargs = {'category_id': 'category_pk'}
    serializer_class = serializers.OrgProductSerializer
    queryset = models.OrgProduct.objects
    ordering_fields = ("name",)
