from modeltranslation.translator import translator

from base.restapi.translation.logic.options import BaseTranslationOptions
from .models import *


class BaseProductCategoryTranslationOptions(BaseTranslationOptions):
    fields = (
        'name',
    )
    sorted_fields = [
       {'field': 'name', 'order': 1}
    ]
    required_languages = ('ru',)
    empty_values = None


translator.register(BaseProductCategory, BaseProductCategoryTranslationOptions)


class CategoryFormatTranslationOptions(BaseTranslationOptions):
    fields = ('name', 'description')
    sorted_fields = [{'field': 'description', 'order': 2}, {'field': 'name', 'order': 1}]
    required_languages = ('ru',)
    empty_values = None


translator.register(CategoryFormat, CategoryFormatTranslationOptions)


class BaseProductTranslationOptions(BaseTranslationOptions):
    fields = ('name',)
    sorted_fields = [{'field': 'name', 'order': 1}]
    required_languages = ('ru',)
    empty_values = None


translator.register(BaseProduct, BaseProductTranslationOptions)


class OrgProductCategoryTranslationOptions(BaseTranslationOptions):
    fields = ('name', 'description')
    sorted_fields = [{'field': 'description', 'order': 2}, {'field': 'name', 'order': 1}]
    required_languages = ('ru',)
    empty_values = None


translator.register(OrgProductCategory, OrgProductCategoryTranslationOptions)


class OrgProductTranslationOptions(BaseTranslationOptions):
    fields = ('name',)
    sorted_fields = [{'field': 'name', 'order': 1}]
    required_languages = ('ru',)
    empty_values = None


translator.register(OrgProduct, OrgProductTranslationOptions)
