# Generated by Django 2.2.6 on 2021-02-22 05:26

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('photoproduct', '0005_orgproductcategory_organization'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='orgproductcategory',
            name='photos',
        ),
        migrations.AddField(
            model_name='galleryphoto',
            name='category',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, related_name='photos', to='photoproduct.OrgProductCategory', verbose_name='Категория'),
            preserve_default=False,
        ),
    ]
