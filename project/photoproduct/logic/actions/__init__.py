from base.restapi.translation.logic.utils import get_field_name_translation_fields
from django.core.files.base import ContentFile


def copy_base_category_data(org_category):
    """Копирование данных"""

    base_category = org_category.base_category

    # name
    fields = get_field_name_translation_fields("name", None, False)
    for translate_field_name in fields:
        value = getattr(base_category, translate_field_name)
        setattr(org_category, translate_field_name, value)

    org_category.name = base_category.name

    icon = ContentFile(base_category.icon.read())
    icon.name = base_category.icon.name
    org_category.icon = icon
    org_category.save()


def copy_base_product_data(org_product):
    """Копирование данных для продукции"""

    base_product = org_product.base_product

    # name
    fields = get_field_name_translation_fields("name", None, False)
    for translate_field_name in fields:
        value = getattr(base_product, translate_field_name)
        setattr(org_product, translate_field_name, value)

    org_product.name = base_product.name
    org_product.save()
    
