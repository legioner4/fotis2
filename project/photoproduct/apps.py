from django.apps import AppConfig


class PhotoproductConfig(AppConfig):
    name = 'photoproduct'
    verbose_name = 'Фотопродукция'
