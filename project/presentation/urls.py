from django.conf.urls import url, include

app_name = 'presentation'

urlpatterns = [
    url(r'^', include('presentation.app.public.urls', namespace='public')),
    url(r'^account/', include('presentation.app.account.urls', namespace='account')),
    url(r'^workspace/', include('presentation.app.workspace.urls', namespace='workspace')),
]
