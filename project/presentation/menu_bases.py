from copy import deepcopy

from rest_framework.permissions import BasePermission


class MenuItemDivider:
    divider = True
    title = ''
    icon = ''
    url = ''
    url_active = None
    children = None
    permission_classes = None

    def filter_by_permission(self, user):
        return self


class MenuItem:
    divider = False
    title = ''
    icon = ''
    url = ''
    url_active = None
    children = None
    permission_classes = None
    any_permission_check = False

    def __init__(self, *args, **kwargs):
        keys = ['title', 'icon', 'url', 'url_active', 'children', 'permission_classes']
        for i, arg in enumerate(args):
            kwargs[keys[i]] = arg

        for key, value in kwargs.items():
            if value is not None:
                setattr(self, key, value)

    def get_permissions(self):
        permission_classes = self.permission_classes

        if not isinstance(permission_classes, list):
            assert issubclass(permission_classes, BasePermission)
            permission_classes = [permission_classes]

        return (permission() for permission in permission_classes)

    def has_permissions(self, request):
        if not (request.user and request.user.is_authenticated):
            return False
        checks = (p.has_permission(request, self) for p in self.get_permissions())
        return any(checks) if self.any_permission_check else all(checks)

    def filter_by_permission(self, request):
        item = deepcopy(self)
        if self.permission_classes and not self.has_permissions(request):
            return None

        if item.children:
            item.children = list(filter(None, (child.filter_by_permission(request) for child in item.children)))
            if not item.children:
                return None
        return item


def menu_schema(request, menu):
    path = request.path

    def menu_has_active(menu_item):
        if menu_item.children:
            for child in menu_item.children:
                if child.url_active and path.startswith(str(child.url_active)):
                    return 'open has-active'
                if child.url and path.startswith(str(child.url)):
                    return 'open has-active'
        else:
            if menu_item.url and path.startswith(str(menu_item.url)):
                return 'active'

        if menu_item.url_active and path.startswith(str(menu_item.url_active)):
            return 'active'

        return ''

    def get_menu_item_schema(menu_item):
        return {
            'name': menu_item.title,
            'url': menu_item.url if menu_item.url else '#',
            'icon': menu_item.icon,
            'active': menu_has_active(menu_item),
            'divider': int(menu_item.divider),
        }

    menu_list = list(filter(None, (item.filter_by_permission(request) for item in menu)))
    result = []
    for menu_item in menu_list:
        item_schema = get_menu_item_schema(menu_item)

        if menu_item.children:
            children_schema = []
            for sub_menu in menu_item.children:
                children_schema.append(get_menu_item_schema(sub_menu))

            item_schema['children'] = children_schema
        result.append(item_schema)

    return result
