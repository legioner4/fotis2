from django.urls import reverse_lazy
from rest_framework.permissions import IsAdminUser, IsAuthenticated

from order.permissions import CanManageOrder
from organization.permissions import CanManageOrganizations
from system.permissions import CanManageSystem
from .menu_bases import MenuItem

PROFILE_MENU = [
    MenuItem(
        'Профиль',
        'user',
        children=[
            MenuItem('Информация', 'address-book', url=reverse_lazy('presentation:account:profile')),
        ],
        permission_classes=IsAuthenticated,
        url_active=reverse_lazy('presentation:account:profile'),
    )
]

WORKSPACE_MENU = [
    MenuItem(
        'Управление',
        'dashboard',
        children=[
            MenuItem(
                'Реестр пользователей',
                'people',
                url=reverse_lazy('presentation:workspace:control:account_list'),
                permission_classes=IsAdminUser,
            ),
            MenuItem(
                'Реестр организаций',
                'domain',
                url=reverse_lazy('presentation:workspace:control:organization_list'),
                permission_classes=CanManageSystem,
            ),
            MenuItem(
                'Реестр заказов',
                'business_center',
                url=reverse_lazy('presentation:workspace:control:order_list'),
                permission_classes=CanManageSystem,
            ),
            MenuItem(
                'Реестр незавершенных заказов',
                'business_center',
                url=reverse_lazy('presentation:workspace:control:not_completed_order'),
                permission_classes=CanManageSystem,
            ),
            MenuItem(
                'Роли и Полномочия',
                'toc',
                url=reverse_lazy('presentation:workspace:control:role_list'),
                permission_classes=IsAdminUser,
            ),
            MenuItem(
                'Описание системы',
                'settings',
                url=reverse_lazy('presentation:workspace:control:settings'),
                permission_classes=IsAdminUser,
            ),
            MenuItem(
                'Фотопродукция',
                'photo_filter',
                url=reverse_lazy('presentation:workspace:photoproduct:base_category'),
                permission_classes=CanManageSystem,
            ),
            MenuItem('Админка', 'bug_report', url="/admin/", permission_classes=IsAdminUser),
        ],
    ),
    MenuItem(
        'Сведения организации',
        'domain',
        url=reverse_lazy('presentation:workspace:organization:info'),
        permission_classes=CanManageOrganizations,
    ),
    MenuItem(
        'Заказы',
        'business_center',
        url=reverse_lazy('presentation:workspace:order:order'),
        permission_classes=CanManageOrder,
    ),
    MenuItem(
        'Маркетинг',
        'offline_bolt',
        children=[
            MenuItem(
                'Промокоды', 'swap_vertical_circle',
                url=reverse_lazy('presentation:workspace:marketing:promo_code'),
                permission_classes=CanManageOrganizations
            ),

        ],
        permission_classes=[],
    ),
    MenuItem(
        'Фотопродукция',
        'photo_filter',
        url=reverse_lazy('presentation:workspace:photoproduct:organization_category'),
        permission_classes=CanManageOrganizations,
    ),
    MenuItem(
        'Справочники',
        'bookmarks',
        children=[
            MenuItem(
                'Типы бумаги', 'amp_stories',
                url=reverse_lazy('presentation:workspace:catalog:paper_type'),
                permission_classes=CanManageSystem
            ),
            MenuItem(
                'Типы доставки', 'how_to_vote',
                url=reverse_lazy('presentation:workspace:catalog:delivery_type'),
                permission_classes=CanManageOrganizations
            ),
            MenuItem(
                'Фото форматы', 'open_with',
                url=reverse_lazy('presentation:workspace:catalog:photo_format'),
                permission_classes=CanManageOrganizations),
        ],
        permission_classes=[],
    ),
]
