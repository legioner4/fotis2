from django.template import Library

from presentation.menu import WORKSPACE_MENU
from presentation.menu_bases import menu_schema

register = Library()


@register.simple_tag(takes_context=True)
def get_private_menu(context):
    user = context.request.user
    if not user.is_authenticated:
        return []
    return menu_schema(context.request, WORKSPACE_MENU)
