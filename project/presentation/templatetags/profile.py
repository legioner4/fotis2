from django.templatetags.static import static
from django.template import Library

from presentation.menu import PROFILE_MENU
from presentation.menu_bases import menu_schema

register = Library()


@register.simple_tag(takes_context=True)
def get_profile_info(context):
    info = {'icon': static('images/avatar_not_found.png'), 'menu': menu_schema(context.request, PROFILE_MENU)}
    return info
