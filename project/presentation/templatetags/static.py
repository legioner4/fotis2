from django.conf import settings
from django.template import Library
from django.templatetags.static import StaticNode as DjangoStaticNode

register = Library()


class StaticNode(DjangoStaticNode):
    @classmethod
    def handle_simple(cls, path):
        result = super().handle_simple(path)
        return result


@register.tag('static')
def do_static(parser, token):
    return StaticNode.handle_token(parser, token)
