from django.conf.urls import url

from . import views

app_name = 'account'

# workspace
urlpatterns = [
    url(r'^profile/$', views.ProfileView.as_view(), name='profile'),
]
