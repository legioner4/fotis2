from django.contrib.auth import get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse
from django.views.generic import TemplateView

from presentation.app import AppRoutesMixin, BreadcrumbMixin

User = get_user_model()


class ProfileView(LoginRequiredMixin, BreadcrumbMixin, AppRoutesMixin, TemplateView):
    template_name = 'private/router_view.html'
    title = 'Профиль'

    routes_source = 'apps/account/profile/routes'

    @property
    def routes_api_path(self):
        return reverse('api:account:profile:profile-detail', args=(self.request.user.pk,))

    def get_routes_config(self):
        config = super().get_routes_config()
        config['id'] = self.request.user.id
        return config

    def get_breadcrumb(self):
        return []
