import json
from logging import getLogger

from django.urls import reverse_lazy

logger = getLogger(__name__)


class BreadcrumbMixin(object):
    title = ''

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['breadcrumb'] = self.get_breadcrumb()
        return context

    def get_breadcrumb(self):
        breadcrumb = [{'name': 'Рабочий стол', 'url': reverse_lazy('presentation:workspace:dashboard:dashboard')}]
        return breadcrumb


class AppRoutesMixin(object):
    routes_source = None
    routes_api_path = None

    def get_routes_config(self):
        config = {}
        if hasattr(self, 'title'):
            config['title'] = self.title
        if self.routes_api_path:
            config['apiPath'] = str(self.routes_api_path)
        return config

    @property
    def routes_meta(self):
        if self.routes_source:
            return {'source': str(self.routes_source), 'config': self.get_routes_config()}

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['routes_meta'] = json.dumps(self.routes_meta)
        return context
