from django.conf.urls import url

from . import views

app_name = 'control'

urlpatterns = [
    url(r'^account/$', views.AccountListView.as_view(), name='account_list'),
    url(r'^organization/$', views.OpranizationListView.as_view(), name='organization_list'),
    url(r'^order/$', views.OrderListView.as_view(), name='order_list'),
    url(r'^not-completed-orders/$', views.NotCompletedOrderView.as_view(), name='not_completed_order'),
    url(r'^role/$', views.RoleListView.as_view(), name='role_list'),
    url(r'^settings/$', views.SystemSettingsView.as_view(), name='settings'),
]
