from django.urls import reverse_lazy, reverse
from django.views.generic import TemplateView
from rest_framework.permissions import IsAdminUser

from base.views import HasPermissionMixin
from presentation.app import AppRoutesMixin, BreadcrumbMixin
from system.permissions import CanManageSystem


class AccountListView(HasPermissionMixin, BreadcrumbMixin, AppRoutesMixin, TemplateView):
    permission_classes = IsAdminUser
    template_name = 'private/router_view.html'
    title = 'Реестр пользователей'
    routes_source = 'apps/workspace/control/account/routes'
    routes_api_path = reverse_lazy('api:account:control:account-list')


class OpranizationListView(HasPermissionMixin, BreadcrumbMixin, AppRoutesMixin, TemplateView):
    permission_classes = CanManageSystem
    template_name = 'private/router_view.html'
    title = 'Реестр организаций'
    routes_source = 'apps/workspace/control/organization/routes'
    routes_api_path = reverse_lazy('api:workspace:organization:control:organization-list')

    def get_routes_config(self):
        config = super().get_routes_config()
        config['smtpApiPath'] = reverse('api:workspace:organization:control:organization_smtp-list')
        config['mobileApiPath'] = reverse('api:workspace:organization:control:organization_mobile-list')
        return config


class RoleListView(HasPermissionMixin, AppRoutesMixin, BreadcrumbMixin, TemplateView):
    permission_classes = IsAdminUser
    template_name = 'private/router_view.html'
    title = 'Роли и Полномочия'

    routes_api_path = reverse_lazy('api:account:control:role-list')
    routes_source = 'apps/workspace/control/role/routes'


class SystemSettingsView(HasPermissionMixin, BreadcrumbMixin, AppRoutesMixin, TemplateView):
    permission_classes = IsAdminUser
    template_name = 'private/router_view.html'
    title = 'Описание системы'

    routes_source = 'apps/workspace/control/system/Settings/routes'
    routes_api_path = reverse_lazy('api:workspace:system:settings')

    def get_routes_config(self):
        config = super().get_routes_config()
        return config


class OrderListView(HasPermissionMixin, BreadcrumbMixin, AppRoutesMixin, TemplateView):
    permission_classes = CanManageSystem
    template_name = 'private/router_view.html'
    title = 'Реестр заказов'
    routes_source = 'apps/workspace/control/order/routes'
    routes_api_path = reverse_lazy('api:workspace:order:control:order-list')


class NotCompletedOrderView(HasPermissionMixin, BreadcrumbMixin, AppRoutesMixin, TemplateView):
    permission_classes = CanManageSystem
    template_name = 'private/router_view.html'
    title = 'Реестр незавершенных заказов'
    routes_source = 'apps/workspace/control/not_compl_order/routes'
    routes_api_path = reverse_lazy('api:workspace:order:control:not_completed_order-list')
