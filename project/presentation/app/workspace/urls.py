from django.conf.urls import include, url

app_name = 'workspace'

urlpatterns = [
    url(r'^dashboard/', include('presentation.app.workspace.dashboard.urls', namespace='dashboard')),
    url(r'^control/', include('presentation.app.workspace.control.urls', namespace='control')),
    url(r'^organization/', include('presentation.app.workspace.organization.urls', namespace='organization')),
    url(r'^catalog/', include('presentation.app.workspace.catalog.urls', namespace='catalog')),
    url(r'^order/', include('presentation.app.workspace.order.urls', namespace='order')),
    url(r'^marketing/', include('presentation.app.workspace.marketing.urls', namespace='marketing')),
    url(r'^photoproduct/', include('presentation.app.workspace.photoproduct.urls', namespace='photoproduct')),
]
