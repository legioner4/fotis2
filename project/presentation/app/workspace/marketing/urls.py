from django.conf.urls import url

from . import views

app_name = 'marketing'

urlpatterns = [
    url(r'^promo-codes/$', views.PromoCodeView.as_view(), name='promo_code'),
]
