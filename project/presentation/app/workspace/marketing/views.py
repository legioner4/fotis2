from django.urls import reverse
from django.views.generic import TemplateView

from base.views import HasPermissionMixin
from organization.permissions import CanManageOrganizations
from presentation.app import AppRoutesMixin, BreadcrumbMixin


class PromoCodeView(HasPermissionMixin, BreadcrumbMixin, AppRoutesMixin, TemplateView):
    permission_classes = CanManageOrganizations
    template_name = 'private/router_view.html'
    title = 'Заказы'
    routes_source = 'apps/workspace/marketing/promo_code/routes'

    @property
    def routes_api_path(self):
        return reverse('api:workspace:promo_code:promo_code-list')
