from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy, reverse
from django.views.generic import TemplateView

from order.models import Order
from order.permissions import CanManageOrder
from organization.permissions import CanManageOrganizations
from presentation.app import AppRoutesMixin, BreadcrumbMixin


class DashboardView(LoginRequiredMixin, BreadcrumbMixin, AppRoutesMixin, TemplateView):
    template_name = 'private/router_view.html'
    title = 'Рабочий стол'
    routes_api_path = ''

    @property
    def routes_source(self):
        return 'apps/workspace/dashboard/routes'

    def get_routes_config(self):
        config = super().get_routes_config()
        config['canManageOrder'] = CanManageOrder().has_permission(self.request, None)
        config['canManageOrganizations'] = CanManageOrganizations().has_permission(self.request, None)
        config['orderApiPath'] = reverse('api:workspace:order:info:order-list')
        organization_orders = Order.objects.filter(organization=self.request.user.organization)
        config['orderData'] = [
            {'state': 'Новый', 'count': organization_orders.filter(state=Order.NEW_STATE).count()},
            {'state': 'Загружен', 'count': organization_orders.filter(state=Order.LOADED_STATE).count()},
            {'state': 'В печати', 'count': organization_orders.filter(state=Order.INPRINT_STATE).count()},
            {'state': 'Отправлен', 'count': organization_orders.filter(state=Order.SUBMITTED_STATE).count()},
            {'state': 'Готов к выдаче', 'count': organization_orders.filter(state=Order.READY_STATE).count()},
            {'state': 'Выдан', 'count': organization_orders.filter(state=Order.ISSUED_STATE).count()},
            {'state': 'Отменен', 'count': organization_orders.filter(state=Order.CANCELED_STATE).count()},
        ]
        return config
