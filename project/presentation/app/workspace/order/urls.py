from django.conf.urls import url

from . import views

app_name = 'order'

urlpatterns = [
    url(r'^orders/$', views.OrderView.as_view(), name='order'),
]
