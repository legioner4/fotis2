from django.urls import reverse
from django.views.generic import TemplateView, DetailView

from base.views import HasPermissionMixin
from order.permissions import CanManageOrder
from presentation.app import AppRoutesMixin, BreadcrumbMixin


class OrderView(HasPermissionMixin, BreadcrumbMixin, AppRoutesMixin, TemplateView):
    permission_classes = CanManageOrder
    template_name = 'private/router_view.html'
    title = 'Заказы'
    routes_source = 'apps/workspace/order/routes'

    @property
    def routes_api_path(self):
        return reverse('api:workspace:order:info:order-list')
