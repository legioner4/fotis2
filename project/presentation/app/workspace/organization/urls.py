from django.conf.urls import url

from . import views

app_name = 'organization'

urlpatterns = [
    url(r'^info/$', views.OrganizationView.as_view(), name='info'),
]
