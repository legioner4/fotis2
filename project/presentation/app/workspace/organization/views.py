from django.urls import reverse
from django.views.generic import TemplateView

from base.views import HasPermissionMixin
from organization.permissions import CanManageOrganizations
from presentation.app import BreadcrumbMixin, AppRoutesMixin


class OrganizationView(HasPermissionMixin, BreadcrumbMixin, AppRoutesMixin, TemplateView):
    permission_classes = CanManageOrganizations
    template_name = 'private/router_view.html'
    title = 'Сведения организации'
    routes_source = 'apps/workspace/organization/routes'

    @property
    def routes_api_path(self):
        return reverse('api:workspace:organization:info:organization-detail', args=[self.request.user.organization.pk,])

    def get_routes_config(self):
        config = super().get_routes_config()
        config['accountApiPath'] = reverse('api:account:organization:account-list')
        config['smtpApiPath'] = reverse('api:workspace:organization:info:organization_smtp-detail', args=[self.request.user.organization.pk,])
        return config
