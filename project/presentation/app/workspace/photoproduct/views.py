from django.urls import reverse
from django.views.generic import TemplateView, DetailView

from base.views import HasPermissionMixin
from organization.permissions import CanManageOrganizations
from photoproduct.models import BaseProductCategory, OrgProductCategory
from presentation.app import BreadcrumbMixin, AppRoutesMixin
from system.permissions import CanManageSystem


class BaseCategoryView(HasPermissionMixin, BreadcrumbMixin, AppRoutesMixin, TemplateView):
    permission_classes = CanManageSystem
    template_name = 'private/router_view.html'
    title = 'Фотопродукция'
    routes_source = 'apps/workspace/photoproduct/base/routes'


class BaseCategoryDetailView(HasPermissionMixin, BreadcrumbMixin, AppRoutesMixin, DetailView):
    model = BaseProductCategory
    permission_classes = CanManageSystem
    template_name = 'private/router_view.html'
    routes_source = 'apps/workspace/photoproduct/base/detail/routes'

    @property
    def title(self):
        return f'Категория: {self.object}'

    def get_routes_config(self):
        return {
            'title': self.title,
            'apiPath': reverse(
                'api:workspace:photoproduct:base:category-detail', args=(self.object.group, self.object.pk,)
            ),
        }

    def get_breadcrumb(self):
        breadcrumb = super().get_breadcrumb()
        url = reverse('presentation:workspace:photoproduct:base_category')
        breadcrumb.append({'name': f'Фотопродукция: {self.object.get_group_display()}', 'url': f'{url}#/{self.object.group}'})
        return breadcrumb


class OrganizationCategoryView(HasPermissionMixin, BreadcrumbMixin, AppRoutesMixin, TemplateView):
    permission_classes = CanManageOrganizations
    template_name = 'private/router_view.html'
    title = 'Фотопродукция'
    routes_source = 'apps/workspace/photoproduct/organization/routes'


class OrganizationCategoryDetailView(HasPermissionMixin, BreadcrumbMixin, AppRoutesMixin, DetailView):
    model = OrgProductCategory
    permission_classes = CanManageOrganizations
    template_name = 'private/router_view.html'
    routes_source = 'apps/workspace/photoproduct/organization/detail/routes'

    @property
    def title(self):
        return f'Категория: {self.object}'

    def get_routes_config(self):
        return {
            'title': self.title,
            'apiPath': reverse(
                'api:workspace:photoproduct:organization:category-detail', args=(self.object.base_category.group, self.object.pk,)
            ),
        }

    def get_breadcrumb(self):
        breadcrumb = super().get_breadcrumb()
        url = reverse('presentation:workspace:photoproduct:organization_category')
        breadcrumb.append({'name': f'Фотопродукция: {self.object.base_category.get_group_display()}', 'url': f'{url}#/{self.object.base_category.group}'})
        return breadcrumb
