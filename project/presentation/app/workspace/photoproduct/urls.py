from django.conf.urls import url

from . import views

app_name = 'base_photoproduct'

urlpatterns = [
    url(r'^base/category/$', views.BaseCategoryView.as_view(), name='base_category'),
    url(r'^base/category/(?P<pk>\d+)/$', views.BaseCategoryDetailView.as_view(), name='base_category_detail'),
    url(r'^organization/category/$', views.OrganizationCategoryView.as_view(), name='organization_category'),
    url(r'^organization/category/(?P<pk>\d+)/$', views.OrganizationCategoryDetailView.as_view(), name='organization_category_detail'),
]
