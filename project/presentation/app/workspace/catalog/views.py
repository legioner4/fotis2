from django.urls import reverse
from django.views.generic import TemplateView, DetailView

from base.views import HasPermissionMixin
from catalog.models import FormatPrice
from organization.permissions import CanManageOrganizations
from presentation.app import AppRoutesMixin, BreadcrumbMixin
from system.permissions import CanManageSystem


class DeliveryTypeView(HasPermissionMixin, BreadcrumbMixin, AppRoutesMixin, TemplateView):
    permission_classes = CanManageOrganizations
    template_name = 'private/router_view.html'
    title = 'Типы доставки'
    routes_source = 'apps/workspace/catalog/delivery_type/routes'

    @property
    def routes_api_path(self):
        return reverse('api:workspace:catalog:delivery_type-list')

    def get_routes_config(self):
        config = super().get_routes_config()
        config['organizationId'] = self.request.user.organization.pk
        return config


class PaperTypeView(HasPermissionMixin, BreadcrumbMixin, AppRoutesMixin, TemplateView):
    permission_classes = CanManageSystem
    template_name = 'private/router_view.html'
    title = 'Типы бумаги'
    routes_source = 'apps/workspace/catalog/paper_type/routes'

    @property
    def routes_api_path(self):
        return reverse('api:workspace:catalog:paper_type-list')


class PhotoFormatView(HasPermissionMixin, BreadcrumbMixin, AppRoutesMixin, TemplateView):
    permission_classes = CanManageOrganizations
    template_name = 'private/router_view.html'
    title = 'Фото форматы'
    routes_source = 'apps/workspace/catalog/photo_format/routes'

    @property
    def routes_api_path(self):
        return reverse('api:workspace:catalog:photo_format-list')


class FormatPriceView(HasPermissionMixin, BreadcrumbMixin, AppRoutesMixin, DetailView):
    permission_classes = CanManageOrganizations
    template_name = 'private/router_view.html'
    routes_source = 'apps/workspace/catalog/photo_format/format_price/routes'

    def get_queryset(self):
        return FormatPrice.objects.filter(photo_format__organization=self.request.user.organization)

    @property
    def title(self):
        return str(self.object)

    def get_routes_config(self):
        return {
            'title': self.title,
            'apiPath': reverse(
                'api:workspace:catalog:format_price-detail', args=(self.object.photo_format.pk, self.object.pk)
            ),
        }

    def get_breadcrumb(self):
        breadcrumb = super().get_breadcrumb()
        url = reverse('presentation:workspace:catalog:photo_format')
        breadcrumb.append({'name': 'Фото форматы', 'url': url})
        breadcrumb.append({'name': str(self.object.photo_format), 'url': f'{url}/#/{self.object.photo_format.pk}/format-price'})
        return breadcrumb
