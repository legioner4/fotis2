from django.conf.urls import url

from . import views

app_name = 'catalog'

urlpatterns = [
    url(r'^delivery-type/$', views.DeliveryTypeView.as_view(), name='delivery_type'),
    url(r'^paper-type/$', views.PaperTypeView.as_view(), name='paper_type'),
    url(r'^photo-format/$', views.PhotoFormatView.as_view(), name='photo_format'),
    url(r'^photo-format/price/(?P<pk>\d+)/$', views.FormatPriceView.as_view(), name='photo_price'),
]
