from django.contrib.auth import get_user_model, views as auth_views
from django.contrib.auth.views import REDIRECT_FIELD_NAME
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy, reverse
from django.views.generic import View, TemplateView, DetailView

from account.logic.actions.account import email_confirmation
from payment.logic import payment_change_success_state, payment_change_fail_state, get_or_create_order_payment
from payment.models import Payment
from order.models import Order
from presentation.app import AppRoutesMixin

User = get_user_model()


class LoginView(AppRoutesMixin, TemplateView):
    template_name = 'public/index.html'
    routes_source = 'apps/public/auth/routes'
    dashboard_url = reverse_lazy('presentation:workspace:dashboard:dashboard')

    def dispatch(self, request, *args, **kwargs):
        redirect_to = self.request.GET.get(REDIRECT_FIELD_NAME, '')
        if request.user.is_authenticated:
            url = redirect_to if redirect_to else self.dashboard_url
            return HttpResponseRedirect(url)
        return super().dispatch(request, *args, **kwargs)


class LogoutView(auth_views.LogoutView):
    next_page = reverse_lazy('presentation:public:login')


class EmailConfirmView(View):

    def dispatch(self, request, *args, **kwargs):
        assert 'uidb64' in kwargs and 'token' in kwargs
        confirm_result = email_confirmation(kwargs['uidb64'], kwargs['token'])
        login_url = reverse('presentation:public:login')
        confirm_page = 'success/confirm/' if confirm_result else 'invalid/confirm/'
        return HttpResponseRedirect(f'{login_url}#/email/{confirm_page}')


class SuccessPaymentView(AppRoutesMixin, TemplateView):
    template_name = 'public/index.html'
    routes_source = 'apps/public/payment/routes'
    title = 'Ваш заказ успешно оплачен'

    def get(self, request, *args, **kwargs):
        payment_pk = self.kwargs.get('pk')
        self.payment = Payment.objects.filter(pk=payment_pk).first()

        if not self.payment or not self.payment.is_progress_state:
            url = reverse('presentation:public:fail_payment', args=(payment_pk,))
            return HttpResponseRedirect(url)

        payment_change_success_state(self.payment)
        return super().get(request, *args, **kwargs)

    def get_routes_config(self):
        config = super().get_routes_config()
        config['order_number'] = self.payment.order.get_order_number()
        config['state'] = 'success'
        return config


class FailPaymentView(AppRoutesMixin, TemplateView):
    template_name = 'public/index.html'
    routes_source = 'apps/public/payment/routes'
    title = 'При попытке оплаты произошла ошибка'

    def get(self, request, *args, **kwargs):
        payment_pk = self.kwargs.get('pk')
        self.payment = Payment.objects.filter(pk=payment_pk).first()

        if self.payment and self.payment.is_progress_state:
            payment_change_fail_state(self.payment)

        return super().get(request, *args, **kwargs)

    def get_routes_config(self):
        config = super().get_routes_config()
        config['order_number'] = self.payment.order.get_order_number() if self.payment else None
        config['state'] = 'fail'
        return config


class OnlinePaymentView(AppRoutesMixin, DetailView):
    template_name = 'public/index.html'
    routes_source = 'apps/public/payment/routes'
    title = 'Заказ ранее был оплачен'
    model = Order

    def get(self, request, *args, **kwargs):
        order = self.get_object()
        try:
            payment, url = get_or_create_order_payment(order)
            return HttpResponseRedirect(url)
        except Exception as e:
            return super().get(request, *args, **kwargs)

    def get_routes_config(self):
        config = super().get_routes_config()
        order = self.get_object()
        config['order_number'] = order.get_order_number()
        config['state'] = 'previously_paid'
        return config
