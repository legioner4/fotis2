from django.conf.urls import url

from . import views

app_name = 'public'

urlpatterns = [
    url(r'^$', views.LoginView.as_view(), name='login'),
    url(r'^logout/$', views.LogoutView.as_view(), name='logout'),
    url(
        r'^email/confirm/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        views.EmailConfirmView.as_view(),
        name='email_confirm',
    ),

    url(r'^payment/(?P<pk>\d+)/success/$', views.SuccessPaymentView.as_view(), name='success_payment'),
    url(r'^payment/(?P<pk>\d+)/fail/$', views.FailPaymentView.as_view(), name='fail_payment'),
    url(r'^online/(?P<pk>\d+)/payment/$', views.OnlinePaymentView.as_view(), name='online_payment'),
]
