from base.decorators import register_permission
from base.permissions import Permission

GROUP_NAME = 'Система'


@register_permission
class CanManageSystem(Permission):
    code = 'site_admin'
    name = 'Управление'
    group = GROUP_NAME
