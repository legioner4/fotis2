from django.contrib import admin

from . import models

admin.site.register(models.SystemSettings)
admin.site.register(models.SystemModels)
