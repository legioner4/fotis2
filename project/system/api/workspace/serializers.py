import logging

from django.conf import settings
from rest_framework import serializers

from base.restapi.serializer import BaseSerializer
from system import models

logger = logging.getLogger(__name__)


class SystemSettingsSerializer(BaseSerializer):
    class Meta:
        model = models.SystemSettings
        fields = '__all__'


class SystemModelSerializer(BaseSerializer):
    total = serializers.SerializerMethodField(label='Всего', read_only=True)
    deleted = serializers.SerializerMethodField(label='Удалено', read_only=True)

    class Meta:
        model = models.SystemModels
        fields = '__all__'

    def get_total(self, obj):
        model_class = obj.model_class()
        if model_class:
            return model_class.objects.all_with_deleted().count()

    def get_deleted(self, obj):
        model_class = obj.model_class()
        if model_class:
            return model_class.objects.only_deleted().count()


class SystemModelDeletedSerializer(BaseSerializer):
    str_field = serializers.SerializerMethodField(label='Данные', read_only=True)

    class Meta:
        model = models.SystemSettings
        fields = ('str_field', 'created_at', 'created_by', 'updated_at', 'updated_by', 'deleted_at', 'deleted_by')

    def get_str_field(self, obj):
        return str(obj)
