import logging

from django.conf import settings
from django.db import models as db_models
from django.shortcuts import get_object_or_404

from base.restapi.permission import CREATE, DESTROY, DenyAny, RESTORE, UPDATE, WRITE
from base.restapi.viewsets import BaseModelRetrieveUpdateViewSet, BaseModelViewSet, HardDeletableMixin
from base.restapi.viewsets.special import HARD_DELETE
from system import models, permissions
from . import serializers

logger = logging.getLogger(__name__)


class SystemSettingsViewSet(BaseModelRetrieveUpdateViewSet):
    permission_classes = permissions.CanManageSystem
    serializer_class = serializers.SystemSettingsSerializer

    def get_object(self):
        obj = models.SystemSettings.objects.get_or_create(codename=settings.SYSTEM_CODENAME)[0]
        self.check_object_permissions(self.request, obj)
        return obj


class ModelsViewSet(BaseModelViewSet):
    permission_classes = permissions.CanManageSystem
    permission_classes_map = {WRITE: DenyAny}
    serializer_class = serializers.SystemModelSerializer
    queryset = models.SystemModels.objects.all()

    filter_fields = ('app_name',)


class DeletedViewSet(HardDeletableMixin, BaseModelViewSet):
    permission_classes = permissions.CanManageSystem
    permission_classes_map = {
        CREATE: DenyAny,
        UPDATE: DenyAny,
        DESTROY: DenyAny,
        HARD_DELETE: permission_classes,
        RESTORE: permission_classes,
    }
    serializer_class = serializers.SystemModelDeletedSerializer

    @property
    def model(self):
        return get_object_or_404(models.SystemModels, pk=self.kwargs['model_pk'])

    @property
    def search_fields(self):
        model_class = self.model.model_class()
        if model_class:
            result = []
            field_classes = (db_models.CharField, db_models.TextField)
            for field in self.model.model_class()._meta.fields:
                if isinstance(field, field_classes):
                    result.append(field.name)
                if isinstance(field, db_models.ForeignKey):
                    for rel_field in field.related_fields:
                        if isinstance(rel_field, field_classes):
                            result.append(f'{field.name}__{rel_field.name}')
            return result
        return None

    def get_queryset(self):
        model_class = self.model.model_class()
        if model_class:
            return self.model.model_class().objects.only_deleted()

    def get_meta(self, serializer=None):
        meta = super().get_meta(serializer=serializer)
        meta['title'] = f'{self.model.app_name}: {self.model.model_name}'
        return meta
