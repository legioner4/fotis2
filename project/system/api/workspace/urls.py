from rest_framework import routers
from rest_framework.routers import DynamicRoute, Route
from rest_framework_nested.routers import NestedSimpleRouter

from . import views

app_name = 'system'

retrieve_router = routers.DefaultRouter()

# просмотр и редактирование без id
retrieve_router.routes = [
    # Detail route.
    Route(
        url=r'^{prefix}{trailing_slash}$',
        mapping={'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'},
        name='{basename}',
        detail=True,
        initkwargs={'suffix': 'Instance'},
    ),
    # Dynamically generated detail routes. Generated using
    # @action(detail=True) decorator on methods of the viewset.
    DynamicRoute(
        url=r'^{prefix}/{url_path}{trailing_slash}$', name='{basename}-{url_name}', detail=True, initkwargs={}
    ),
]

retrieve_router.register(r'settings', views.SystemSettingsViewSet, 'settings')

router = routers.DefaultRouter()
router.register(r'models', views.ModelsViewSet, 'models')

models_router = NestedSimpleRouter(router, r'models', lookup='model')
models_router.register(r'deleted', views.DeletedViewSet, 'deleted')

urlpatterns = retrieve_router.urls + router.urls + models_router.urls
