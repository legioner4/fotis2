import logging
from datetime import timedelta

import django
import django.apps
from celery import shared_task
from django.conf import settings
from django.utils import timezone

logger = logging.getLogger(__name__)

BATCH_SIZE = 500


def split_to_bunches(qs):
    count = len(qs)
    if count > BATCH_SIZE:
        for i in range(0, count, BATCH_SIZE):
            yield qs[i : i + BATCH_SIZE]
    else:
        yield qs


def hard_delete(qs):
    count, deleted = qs.hard_delete()
    for cls, cnt in deleted.items():
        logger.info(f'Deleted "{cls}": {cnt}')
    return count


@shared_task
def hard_delete_preserved():
    from base.models import BaseModel

    models = django.apps.apps.get_models()

    now = timezone.now()
    expired = now - timedelta(days=settings.SOFT_DELETED_PRESERVE_DAYS)
    logger.info(f'expired "{expired}"')
    count = 0

    for model in models[::-1]:
        if not issubclass(model, BaseModel):
            continue

        qs = model.objects.only_deleted().filter(deleted_at__lte=expired).order_by('deleted_at')

        if not qs.exists():
            continue

        for bunch in split_to_bunches(qs.values_list('pk', flat=True)):
            count += hard_delete(qs.filter(pk__in=bunch))

    return count


if __name__ == '__main__':
    django.setup()
    hard_delete_preserved()
