from django.apps import apps
from django.db import models
from base.models import BaseModel


class SystemSettings(BaseModel):
    codename = models.CharField(verbose_name='код системы', max_length=16, unique=True, db_index=True)
    name = models.CharField(verbose_name='Наименование', max_length=255, null=True, blank=True)
    short_name = models.CharField(verbose_name='Краткое наименование', max_length=255, null=True, blank=True)
    description = models.TextField(verbose_name='Описание', null=True, blank=True)

    class Meta:
        verbose_name = 'описание системы'
        verbose_name_plural = 'описание системы'

    def __str__(self):
        return 'описание системы'

    @classmethod
    def get(cls):
        return cls.objects.all().first()


class SystemModels(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)
    app_name = models.CharField(verbose_name='Приложение', max_length=255)
    model_name = models.CharField(verbose_name='Наименование', max_length=255)

    class Meta:
        verbose_name = 'данные'
        verbose_name_plural = 'данные'
        ordering = ['app_name', 'model_name']

    def model_class(self):
        try:
            return apps.get_model(self.app_label, self.model)
        except LookupError:
            return None
