from django.apps import AppConfig, apps
from django.db.models.signals import post_migrate, pre_migrate
from django.utils.text import capfirst


class SystemAppConfig(AppConfig):
    name = 'system'
    verbose_name = 'Описание системы'

    def ready(self):
        pre_migrate.connect(checking_migrations, sender=self)
        post_migrate.connect(syn_system_models, sender=self)


def checking_migrations(app_config, **kwargs):
    from django.conf import settings

    if settings.DEBUG and settings.DATABASES['default']['HOST'] not in settings.DEV_DATABASES_HOSTS:
        raise RuntimeError('NOT APPLY MIGRATIONS ON PROD DATABASE!')


def syn_system_models(app_config, **kwargs):
    from base.models import BaseModel

    try:
        SystemModels = apps.get_model('system', 'SystemModels')
    except LookupError:
        return

    for_del = set(SystemModels.objects.all().values_list('pk', flat=True))

    app_models = apps.get_models()
    for app_model in app_models[::-1]:
        if not issubclass(app_model, BaseModel):
            continue
        meta = app_model._meta
        model = meta.model_name
        obj = SystemModels.objects.filter(model=model).first() or SystemModels(model=model)
        obj.app_label = meta.app_label
        obj.app_name = capfirst(meta.app_config.verbose_name)
        obj.model_name = capfirst(meta.verbose_name)
        obj.save()

        for_del.discard(obj.pk)

    SystemModels.objects.filter(pk__in=for_del)
