from pathlib import Path

import sentry_sdk
from django.urls import reverse_lazy
from sentry_sdk.integrations.django import DjangoIntegration
from django.utils.translation import gettext_lazy as _

sentry_sdk.init(
    dsn="http://e9ac5eaf198e4f1f85d07ab9aa98fa3f@80.78.249.240:9000/2",
    integrations=[DjangoIntegration()],
    traces_sample_rate=1.0,
    send_default_pii=True
)

BASE_DIR = Path(__file__).parents[2]

SECRET_KEY = 'zu8&p8l1p@z0%fkdvin!*9uh=tmq&o@^^y^9n1^68z%mb$3-@g'
DEBUG = False
ALLOWED_HOSTS = ['*']
TIME_ZONE = 'Europe/Moscow'
LANGUAGE_CODE = 'ru'
USE_I18N = True
USE_L10N = True
USE_TZ = True
LOCALE_PATHS = (str(BASE_DIR / 'locale'),)

LANGUAGES = (
    ('ru', 'Русский'),
    ('en', 'Английский'),
    ('kk', 'Казахский'),
    ('tt', 'Татарский'),
    ('lt', 'Литовский'),
    ('lv', 'Латышский'),
    ('ro', 'Румынский'),
    ('et', 'Эстонский'),
)
LANGUAGES_DICT = dict(LANGUAGES)
MODELTRANSLATION_DEFAULT_LANGUAGE = 'ru'
MODELTRANSLATION_FALLBACK_LANGUAGES = ('ru',)

DATE_FORMAT = 'd.m.Y'
DATE_INPUT_FORMATS = ['%d.%m.%Y', '%d.%m.%y']
DATETIME_FORMAT = 'd.m.y H:M'
DATETIME_INPUT_FORMATS = ['%d.%m.%Y %H:%M', '%d.%m.%Y %H:%M:%S']

ROOT_URLCONF = 'fotis.urls'
WSGI_APPLICATION = 'fotis.wsgi.application'

DJANGO_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.postgres',
    'django.contrib.admin',
)

REQUIREMENTS_APPS = (
    'rest_framework',
    'rest_framework.authtoken',
    'django_filters',
    'django_group_permissions',
    'treebeard',
    'django_ftpserver',
    'django_celery_beat',
    'django_cleanup.apps.CleanupConfig',
    'modeltranslation',
)

PROJECT_APPS = (
    'base',
    "base.impersonate",
    'system',
    'account',
    'presentation',
    'organization',
    'catalog',
    'order',
    'payment',
    'promo_code',
    'photoproduct',
    'chunked_upload_app',
    'mobile',
)
INSTALLED_APPS = DJANGO_APPS + REQUIREMENTS_APPS + PROJECT_APPS

AUTHENTICATION_BACKENDS = [
    'django.contrib.auth.backends.ModelBackend',
    'account.auth.backends.EmailModelBackend'
]
AUTH_USER_MODEL = 'account.UserAccount'
FORBIDDEN_USERNAME_LIST = ['admin', 'god', 'sa', 'root', 'superadmin', 'superuser', 'super', 'system']

LOGIN_URL = reverse_lazy('presentation:public:login')
LOGIN_REDIRECT_URL = reverse_lazy('presentation:workspace:dashboard:dashboard')
LOGOUT_REDIRECT_URL = reverse_lazy('presentation:public:logout')

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    "base.impersonate.middleware.ImpersonateMiddleware",
    "base.impersonate.middleware.TimezoneMiddleware",
    'base.global_request.GlobalRequest',
]

STATIC_ROOT = BASE_DIR / 'static'
STATIC_URL = '/static/'
STATICFILES_DIRS = (str(BASE_DIR.parent / 'frontend' / 'static'),)
STATICFILES_FINDERS = [
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
]

MEDIA_ROOT = BASE_DIR / 'media'
MEDIA_URL = "/media/"

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'debug': DEBUG,
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'base.context_processors.app_settings',
                'fotis.context_processors.frontend_project_data',
            ],
        },
    }
]

COOKIE_NAME_PREFIX = '_'

# хосты баз данных доступные во время разработки
DEV_DATABASES_HOSTS = ['localhost', '127.0.0.1']


def add_cookie_name_prefix(cookie_name):
    return COOKIE_NAME_PREFIX + cookie_name


SESSION_COOKIE_NAME = add_cookie_name_prefix('sessionid')
CSRF_COOKIE_NAME = add_cookie_name_prefix('csrftoken')
LOCK_COOKIE_NAME = add_cookie_name_prefix('lock_cookie')
TIMEZONE_OFFSET_COOKIE_NAME = add_cookie_name_prefix('timezone')

# email
EMAIL_USE_TLS = True
EMAIL_HOST = 'mx.fotis.online'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'app@fotis.online'
DEFAULT_FROM_EMAIL = EMAIL_HOST_USER
EMAIL_HOST_PASSWORD = 'yn-8xUc-CLk3^68tS'
EMAIL_PASSWD_KEY: bytes = b'BXVX_wY1ZxkckOIV91Nofzu0ITeVfEb0BmM04gb1mvQ='


#redis
REDIS_HOST = 'fotis.app'
REDIS_PORT = '6379'
REDIS_PASSWORD = ''

CACHES: dict = {
    'default': {
        'KEY_PREFIX': 'default',
        'BACKEND': 'redis_lock.django_cache.RedisCache',
        'LOCATION': f'redis://{REDIS_HOST}:{REDIS_PORT}/0',
        'OPTIONS': {
            'PASSWORD': REDIS_PASSWORD,
            'CLIENT_CLASS': 'django_redis.client.DefaultClient',
        },
    },
}

# Celery settings
# region#############################################################################################
from celery.schedules import crontab
from kombu import Queue

RABBITMQ_HOST = 'fotis.app'
RABBITMQ_VHOST = '/'
RABBITMQ_PORT = '5672'
RABBITMQ_DEFAULT_USER = 'admin'
RABBITMQ_DEFAULT_PASS = '123'

CELERY_BROKER_URL = f'amqp://{RABBITMQ_DEFAULT_USER}:{RABBITMQ_DEFAULT_PASS}@{RABBITMQ_HOST}/{RABBITMQ_VHOST}'
CELERY_ACCEPT_CONTENT = ['json', 'pickle']
CELERY_TASK_SERIALIZER = 'json'

CELERY_ENABLE_UTC: bool = True


CELERY_ALWAYS_EAGER: bool = False
CELERY_WORKER_POOL_RESTARTS: bool = True
CELERY_WORKER_MAX_TASKS_PER_CHILD: int = 50

CELERY_BEAT_MAX_LOOP_INTERVAL: int = 5

CELERY_TASK_DEFAULT_QUEUE: str = 'default'
CELERY_TASK_DEFAULT_EXCHANGE: str = 'default'
CELERY_TASK_DEFAULT_ROUTING_KEY: str = 'default'

CELERY_BEAT_SCHEDULER = 'base.celery.scheduler:DatabaseSchedulerWithCleanup'

CELERY_TASK_QUEUES = (
    Queue('system', routing_key='system.#'),
    Queue('default'),
)

CELERY_BEAT_SCHEDULE = {
    'delete-order-photo': {
        'task': 'order.tasks.delete_order_photo',
        'schedule': crontab(minute='*/15'),
    },
}


# rest framework
REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': ('rest_framework.permissions.IsAuthenticated',),
    'DEFAULT_AUTHENTICATION_CLASSES': ('rest_framework.authentication.SessionAuthentication',),
    # Input and output formats
    'DATE_FORMAT': '%d.%m.%Y',
    'DATE_INPUT_FORMATS': ('%d.%m.%Y',),
    'DATETIME_FORMAT': '%d.%m.%Y %H:%M',
    'DATETIME_INPUT_FORMATS': ('%d.%m.%Y %H:%M',),
    'TIME_FORMAT': '%H:%M',
    'TIME_INPUT_FORMATS': ('%H:%M',),
    'NON_FIELD_ERRORS_KEY': '__all__',
    'EXCEPTION_HANDLER': 'base.rest.exception_handler',
}

# permission config
GROUP_PERMISSIONS_MODULE = 'system.group_permissions'
GROUP_PERMISSIONS_REMOVE_DEFAULT = False
GROUP_PERMISSIONS_PRESET_STRICTLY = False

# ================
# PROJECT SETTINGS
# ================
SYSTEM_CODENAME = 'fotis'
APP_NAME = 'Fotis'
COPYRIGHT = '«Fotis»'
SITE_URL = 'https://fotis.app/'

DATABASE_STRING_LENGTH = 1024
ROOT_URL = None

DELETE_PHOTO_DAY = 30
