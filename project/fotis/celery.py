import os
from celery import Task

from celery import Celery

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "fotis.settings")


class TransactionAwareTask(Task):
    """ Задачи нужно ставить в очередь строго после коммита транзакции, чтобы в них был доступ к новым данным """

    abstract = True

    def apply_async(self, *args, **kwargs):
        from django.db import connection

        if connection.in_atomic_block:
            connection.on_commit(lambda: super(TransactionAwareTask, self).apply_async(*args, **kwargs))
        else:
            return super().apply_async(*args, **kwargs)


app = Celery('fotis', task_cls=TransactionAwareTask)

app.config_from_object('django.conf:settings', namespace='CELERY')

app.autodiscover_tasks()
