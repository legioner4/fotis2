from .celery import app as celery_app
from .patch import apply_patches

__all__ = ['celery_app']

apply_patches()
