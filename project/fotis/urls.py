from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path

workspace_api_urls = [
    path('system/', include('system.api.workspace.urls', namespace='system')),
    path('organization/', include('organization.api.workspace.urls', namespace='organization')),
    path('catalog/', include('catalog.api.workspace.urls', namespace='catalog')),
    path('order/', include('order.api.workspace.urls', namespace='order')),
    path('promo-code/', include('promo_code.api.workspace.urls', namespace='promo_code')),
    path('photoproduct/', include('photoproduct.api.workspace.urls', namespace='photoproduct')),
]

api_urls = [
    path('account/', include('account.api.urls', namespace='account')),
    path('workspace/', include((workspace_api_urls, 'fotis'), namespace='workspace')),
    path('chunked/', include('chunked_upload_app.api.urls', namespace='chunked_upload')),
    path("impersonate/", include("base.impersonate.urls")),
    path('mobile/', include('mobile.api.urls', namespace='mobile')),
    path('export/', include('order.api.export.urls', namespace='export')),
]

urlpatterns = [
    path('', include('presentation.urls', namespace='presentation')),
    path('api/', include((api_urls, 'fotis'), namespace='api')),
    path('account/', include('account.urls', namespace='account')),
    path('admin/', admin.site.urls),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


def _include_if_installed(app, path_prefix=None, namespace=None):
    path_prefix = path_prefix or namespace
    if app in settings.INSTALLED_APPS:
        urlpatterns.append(
            url('^' + path_prefix + '/', include(app + '.urls', namespace=namespace)))


_include_if_installed('mozilla_django_oidc', path_prefix='oidc')
