import json

from django.urls import reverse

from system.permissions import CanManageSystem


def frontend_project_data(request):
    data = {'userAuthenticated': False, 'dashboardPath': reverse('presentation:workspace:dashboard:dashboard')}

    user = request.user
    if user.is_authenticated:
        can_manage_system = CanManageSystem.checker(user)
        data = {
            **data,
            'userAuthenticated': True,
            'userId': user.pk,
            'userFio': str(user),
            'canManageSystem': can_manage_system,
            'userIsImpersonated': bool(request and getattr(request, "impersonator", None)),
            'logoutUrl': reverse('presentation:public:logout'),
        }
    return {'project_data': json.dumps(data, ensure_ascii=False)}
