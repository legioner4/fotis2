import threading
from collections import OrderedDict

from kombu.utils.functional import LRUCache


def __getstate__(self):
    with self.mutex:
        d = dict(vars(self))
        d.pop('mutex')
        for k, v in d.items():
            if k == 'data' and type(v) == OrderedDict:
                d[k] = [(in_k, in_v) for in_k, in_v in v.items()]
        return d


def __setstate__(self, state):
    for k, v in state.items():
        if k == 'data' and type(v) == list:
            state[k] = OrderedDict(v)
    self.__dict__ = state
    self.mutex = threading.RLock()


def apply_patches():
    LRUCache.__getstate__ = __getstate__
    LRUCache.__setstate__ = __setstate__
