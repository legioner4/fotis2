import decimal

from django.db import models
from django.utils.translation import ugettext as _
from base.models import BaseModel, SortableMixin
from organization.logic.getters import value_convert_organization_currency


class DeliveryType(BaseModel, SortableMixin):
    POST_TYPE = 'post'
    COURIER_TYPE = 'courier'
    PICKUP_TYPE = 'pickup'
    TRANSPORT_COMPANY_TYPE = 'transport_company'
    RANDOM_DELIVERY_TYPE = 'random_delivery'
    TYPES = (
        (POST_TYPE, _('Почта')),
        (COURIER_TYPE, _('Курьер')),
        (PICKUP_TYPE, _('Самовывоз из фотоцентра')),
        (RANDOM_DELIVERY_TYPE, _('Произвольная доставка')),
    )
    UNIQUE_TYPES = [PICKUP_TYPE, COURIER_TYPE]
    INPUT_NAME_TYPES = [POST_TYPE, TRANSPORT_COMPANY_TYPE, RANDOM_DELIVERY_TYPE]

    organization = models.ForeignKey('organization.Organization', models.CASCADE, related_name='delivery_types', verbose_name='Организация')
    name = models.CharField(verbose_name='Наименование', max_length=512, null=True, blank=True)
    type = models.CharField(verbose_name='Тип доставки', max_length=32, choices=TYPES)
    description = models.CharField(verbose_name='Описание', max_length=512, null=True, blank=True)
    cost = models.DecimalField(verbose_name='Стоимость доставки', max_digits=6, decimal_places=2)
    payment_types = models.ManyToManyField('organization.PaymentType', verbose_name='Типы оплаты')
    enabled = models.BooleanField(verbose_name='Включен', default=False)

    class Meta:
        verbose_name = 'тип доставки'
        verbose_name_plural = 'типы доставки'
        ordering = ('order', 'type')

    def __str__(self):
        return self.name

    @property
    def cost_str(self):
        return value_convert_organization_currency(self.organization, decimal.Decimal(self.cost))

    @property
    def delivery_subdivisions(self):
        return self.organization.subdivision.all()

    @property
    def enabled_delivery_subdivisions(self):
        return self.organization.subdivision.all().filter(enabled=True)

    @property
    def enabled_delivery_regions(self):
        return self.delivery_regions.all().filter(enabled=True)


class DeliveryRegion(BaseModel, SortableMixin):
    delivery_type = models.ForeignKey(DeliveryType, models.CASCADE, verbose_name='Тип доставки', related_name='delivery_regions')
    name = models.CharField(verbose_name='Наименование', max_length=256)
    payment_types = models.ManyToManyField('organization.PaymentType', verbose_name='Типы оплаты')
    cost = models.DecimalField(verbose_name='Стоимость', max_digits=6, decimal_places=2)
    enabled = models.BooleanField(verbose_name='Включен', default=False)

    class Meta:
        verbose_name = 'регион доставки'
        verbose_name_plural = 'регионы доставки'
        ordering = ('order', 'name')

    def __str__(self):
        return self.name

    @property
    def cost_str(self):
        return value_convert_organization_currency(self.delivery_type.organization, decimal.Decimal(self.cost))


class TransportCompanyPoint(BaseModel, SortableMixin):
    delivery_type = models.ForeignKey(DeliveryType, models.CASCADE, verbose_name='Тип доставки', related_name='transport_company_points')
    address = models.CharField(verbose_name='Адрес', max_length=256)
    enabled = models.BooleanField(verbose_name='Включен', default=False)

    class Meta:
        verbose_name = 'пункт выдачи транспортной компании'
        verbose_name_plural = 'пункты выдачи транспортной компании'
        ordering = ('order', 'address')

    def __str__(self):
        return self.address
