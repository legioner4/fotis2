from django.db import models

from base.models import BaseModel, SortableMixin


class Language(BaseModel, SortableMixin):
    name = models.CharField(verbose_name='Наименование', max_length=256)
    code = models.CharField(verbose_name='Код', max_length=8)

    class Meta:
        verbose_name = 'язык'
        verbose_name_plural = 'язык'
        ordering = ('order', 'pk')

    def __str__(self):
        return self.name
