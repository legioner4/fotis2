import decimal

from django.db import models
from uuslug import slugify

from base.models import BaseModel, SortableMixin
from organization.logic.getters import value_convert_organization_currency


class PaperType(BaseModel, SortableMixin):
    slug = models.SlugField(null=False)
    name = models.CharField(verbose_name='Наименование', max_length=256)
    enabled = models.BooleanField(verbose_name='Включен', default=False)

    class Meta:
        verbose_name = 'тип бумаги'
        verbose_name_plural = 'типы бумаги'
        ordering = ('order', 'name')

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name, separator="_")
        return super().save(*args, **kwargs)

    def __str__(self):
        return self.name


class PhotoFormat(BaseModel, SortableMixin):
    organization = models.ForeignKey('organization.Organization', models.CASCADE, related_name='photo_formats', verbose_name='Организация')
    name = models.CharField(verbose_name='Наименование', max_length=256)
    slug = models.SlugField(null=False)

    width = models.PositiveIntegerField(verbose_name='Ширина (мм)', null=True, blank=True)
    height = models.PositiveIntegerField(verbose_name='Высота (мм)', null=True, blank=True)

    polaroid_frame_width = models.PositiveIntegerField(verbose_name='Ширина поля (мм)')
    polaroid_frame_bottom_width = models.PositiveIntegerField(verbose_name='Ширина нижнего поля (мм)')

    instagram_frame_width = models.PositiveIntegerField(verbose_name='Ширина поля по периметру (мм)')

    enabled = models.BooleanField(verbose_name='Включен', default=False)

    class Meta:
        verbose_name = 'формат'
        verbose_name_plural = 'форматы'
        ordering = ('order', 'name')

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name, separator="_")
        return super().save(*args, **kwargs)

    def __str__(self):
        return self.name

    @property
    def enabled_prices(self):
        return self.prices.filter(enabled=True)


class FormatPrice(BaseModel, SortableMixin):
    photo_format = models.ForeignKey(PhotoFormat, models.CASCADE, related_name='prices', verbose_name='Формат')
    paper_type = models.ForeignKey(PaperType, models.CASCADE, related_name='format_prices', verbose_name='Тип бумаги')
    price = models.DecimalField(verbose_name='Цена', max_digits=6, decimal_places=2)
    enabled = models.BooleanField(verbose_name='Включен', default=False)

    class Meta:
        verbose_name = 'цена'
        verbose_name_plural = 'цены'
        ordering = ('order', 'paper_type')

    def __str__(self):
        return f'{self.price}'

    @property
    def enabled_discounts(self):
        return self.discounts.filter(enabled=True)

    @property
    def price_str(self):
        return value_convert_organization_currency(self.photo_format.organization, decimal.Decimal(self.price))


class Discount(BaseModel, SortableMixin):
    format_price = models.ForeignKey(FormatPrice, models.CASCADE, related_name='discounts', verbose_name='цена')
    max_count = models.PositiveIntegerField(verbose_name='Максимальное количество', default=0)
    min_count = models.PositiveIntegerField(verbose_name='Минимальное количество', default=0)
    discount_value = models.DecimalField(verbose_name='Цена для указанного диапазона количества', max_digits=6, decimal_places=2, default=0)
    enabled = models.BooleanField(verbose_name='Включен', default=False)

    class Meta:
        verbose_name = 'скидка'
        verbose_name_plural = 'скидки'
        ordering = ('order', 'discount_value')

    def __str__(self):
        return f'{self.discount_value}'
