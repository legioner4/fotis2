from django.apps import AppConfig
from django.db.models.signals import post_migrate, pre_migrate


class CatalogConfig(AppConfig):
    name = 'catalog'
    verbose_name = 'Справочники'

    def ready(self):
        post_migrate.connect(syn_languages, sender=self)


def syn_languages(app_config, **kwargs):
    from catalog.logic.actions.language import update_languages

    update_languages()
