import django_filters

from base.restapi.permission import READ
from base.restapi.translation.views import TranslationFieldBaseModelViewSet, TranslationFieldNestedViewSet
from catalog import models
from catalog.api.workspace import serializers
from organization import permissions
from system.permissions import CanManageSystem


class PaperTypeFilter(django_filters.FilterSet):
    name = django_filters.CharFilter(method='filter_name')

    class Meta:
        model = models.PaperType
        fields = ('name', 'enabled')

    def filter_name(self, queryset, name, value):
        if value:
            return queryset.filter(name__icontains=value)
        return queryset


class PaperTypeView(TranslationFieldBaseModelViewSet):
    permission_classes = CanManageSystem
    queryset = models.PaperType.objects
    serializer_class = serializers.PaperTypeSerializer

    ordering_fields = ('name', 'order')
    filter_class = PaperTypeFilter


class PermissionMixin():
    permission_classes = permissions.CanManageOrganizations
    permission_classes_map = {READ: permissions.CanManageOrganizations}


class PhotoFormatFilter(django_filters.FilterSet):
    name = django_filters.CharFilter(method='filter_name')
    width = django_filters.CharFilter(method='filter_width')
    height = django_filters.CharFilter(method='filter_height')

    class Meta:
        model = models.PhotoFormat
        fields = ('name', 'width', 'height', 'enabled')

    def filter_name(self, queryset, name, value):
        if value:
            return queryset.filter(name__icontains=value)
        return queryset

    def filter_width(self, queryset, name, value):
        if value:
            return queryset.filter(width__icontains=value)
        return queryset

    def filter_height(self, queryset, name, value):
        if value:
            return queryset.filter(height__icontains=value)
        return queryset


class PhotoFormatView(PermissionMixin, TranslationFieldBaseModelViewSet):
    queryset = models.PhotoFormat.objects
    serializer_class = serializers.PhotoFormatSerializer

    ordering_fields = ('name', 'width', 'height', 'order')
    filter_class = PhotoFormatFilter

    def get_queryset(self):
        return super().get_queryset().filter(organization=self.request.user.organization)

    def perform_create(self, serializer):
        serializer.save(organization=self.request.user.organization)


class FormatPriceFilter(django_filters.FilterSet):
    class Meta:
        model = models.FormatPrice
        fields = ('paper_type', 'enabled')


class FormatPriceView(PermissionMixin, TranslationFieldNestedViewSet):
    parent_lookup_kwargs = {'photo_format_id': 'photo_format_pk'}
    serializer_class = serializers.FormatPriceSerializer
    queryset = models.FormatPrice.objects

    ordering_fields = ('price', 'paper_type', 'order')
    filter_class = FormatPriceFilter

    def get_queryset(self):
        return super().get_queryset().filter(photo_format__organization=self.request.user.organization)


class DiscountFilter(django_filters.FilterSet):
    max_count = django_filters.CharFilter(method='filter_max_count')
    min_count = django_filters.CharFilter(method='filter_min_count')
    discount_value = django_filters.CharFilter(method='filter_discount_value')

    class Meta:
        model = models.Discount
        fields = ('max_count', 'min_count', 'discount_value', 'enabled')

    def filter_max_count(self, queryset, name, value):
        if value:
            return queryset.filter(max_count__icontains=value)
        return queryset

    def filter_min_count(self, queryset, name, value):
        if value:
            return queryset.filter(min_count__icontains=value)
        return queryset

    def filter_discount_value(self, queryset, name, value):
        if value:
            return queryset.filter(discount_value_icontains=value)
        return queryset


class DiscountView(PermissionMixin, TranslationFieldNestedViewSet):
    parent_lookup_kwargs = {'format_price_id': 'format_price_pk'}
    serializer_class = serializers.DiscountSerializer
    queryset = models.Discount.objects

    ordering_fields = ('max_count', 'min_count', 'discount_value', 'order')
    filter_class = DiscountFilter

    def get_queryset(self):
        return super().get_queryset().filter(format_price__photo_format__organization=self.request.user.organization)
