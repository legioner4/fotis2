import django_filters

from base.restapi.permission import READ
from base.restapi.translation.views import TranslationFieldBaseModelViewSet, TranslationFieldNestedViewSet
from base.restapi.viewsets import BaseModelViewSet, NestedViewSet
from catalog import models
from catalog.api.workspace import serializers
from organization import permissions


class PermissionMixin():
    permission_classes = permissions.CanManageOrganizations
    permission_classes_map = {READ: permissions.CanManageOrganizations}


class DeliveryTypeFilter(django_filters.FilterSet):
    name = django_filters.CharFilter(method='filter_name')
    description = django_filters.DateFilter(method='filter_description')

    class Meta:
        model = models.DeliveryType
        fields = ('name', 'description', 'enabled', 'type',)

    def filter_name(self, queryset, name, value):
        if value:
            return queryset.filter(name__icontains=value)
        return queryset

    def filter_description(self, queryset, name, value):
        if value:
            return queryset.filter(description__icontains=value)
        return queryset


class DeliveryTypeView(PermissionMixin, TranslationFieldBaseModelViewSet):
    queryset = models.DeliveryType.objects
    serializer_class = serializers.DeliveryTypeSerializer

    ordering_fields = ('name', 'description', 'order')
    filter_class = DeliveryTypeFilter

    def get_queryset(self):
        return super().get_queryset().filter(organization=self.request.user.organization)

    def perform_create(self, serializer):
        serializer.save(organization=self.request.user.organization)


class DeliveryRegionFilter(django_filters.FilterSet):
    name = django_filters.CharFilter(method='filter_name')

    class Meta:
        model = models.DeliveryRegion
        fields = ('name', 'enabled')

    def filter_name(self, queryset, name, value):
        if value:
            return queryset.filter(name__icontains=value)
        return queryset


class DeliveryRegionView(PermissionMixin, TranslationFieldNestedViewSet):
    parent_lookup_kwargs = {'delivery_type_id': 'delivery_type_pk'}
    serializer_class = serializers.DeliveryRegionSerializer
    queryset = models.DeliveryRegion.objects

    ordering_fields = ('name', 'cost', 'order')
    filter_class = DeliveryRegionFilter

    def get_queryset(self):
        return super().get_queryset().filter(delivery_type__organization=self.request.user.organization)


class TransportCompanyPointFilter(django_filters.FilterSet):
    address = django_filters.CharFilter(method='filter_address')

    class Meta:
        model = models.TransportCompanyPoint
        fields = ('address', 'enabled')

    def filter_address(self, queryset, name, value):
        if value:
            return queryset.filter(address__icontains=value)
        return queryset


class TransportCompanyPointView(PermissionMixin, TranslationFieldNestedViewSet):
    parent_lookup_kwargs = {'delivery_type_id': 'delivery_type_pk'}
    serializer_class = serializers.TransportCompanyPointSerializer
    queryset = models.TransportCompanyPoint.objects

    ordering_fields = ('address', 'order')
    filter_class = TransportCompanyPointFilter

    def get_queryset(self):
        return super().get_queryset().filter(delivery_type__organization=self.request.user.organization)
