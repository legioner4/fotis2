from rest_framework import routers
from rest_framework_nested.routers import NestedSimpleRouter

from . import views

app_name = 'catalog'

router = routers.DefaultRouter()

router.register(r'delivery-type', views.DeliveryTypeView, 'delivery_type')
delivery_type_router = NestedSimpleRouter(router, r'delivery-type', lookup='delivery_type')
delivery_type_router.register(r'delivery-region', views.DeliveryRegionView, 'delivery_region')
delivery_type_router.register(r'transport-company-point', views.TransportCompanyPointView, 'transport_company_point')

router.register(r'paper-type', views.PaperTypeView, 'paper_type')

router.register(r'photo-format', views.PhotoFormatView, 'photo_format')
photo_format_router = NestedSimpleRouter(router, r'photo-format', lookup='photo_format')
photo_format_router.register(r'format-price', views.FormatPriceView, 'format_price')

photo_format_discount_router = NestedSimpleRouter(photo_format_router, r'format-price', lookup='format_price')
photo_format_discount_router.register(r'discount', views.DiscountView, 'discount')


urlpatterns = []
urlpatterns += router.urls
urlpatterns += delivery_type_router.urls
urlpatterns += photo_format_router.urls
urlpatterns += photo_format_discount_router.urls
