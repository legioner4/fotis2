import logging

from rest_framework import serializers

from base.restapi.serializer import BaseSerializer
from base.restapi.translation.serializer import TranslationSerializer
from catalog import models
from catalog.logic.actions.delivery import delivery_change_type
from organization.logic.getters import get_organization_payment_types

logger = logging.getLogger(__name__)


class DeliveryTypeSerializer(TranslationSerializer):

    class Meta:
        model = models.DeliveryType
        fields = "__all__"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        organization = self.request.user.organization
        if 'payment_types' in self.fields:
            self.fields['payment_types'].child_relation.queryset = get_organization_payment_types(organization)

    def validate_type(self, value):
        organization = self.request.user.organization
        delivery_types = organization.delivery_types.filter(type=value)
        if self.instance and self.instance.pk:
            delivery_types = delivery_types.exclude(pk=self.instance.pk)

        if value in models.DeliveryType.UNIQUE_TYPES and delivery_types.exists():
            raise serializers.ValidationError("Доставка с данным типом уже существует")

        return value

    def update(self, instance, validated_data):
        previous_type = instance.type
        instance = super().update(instance, validated_data)
        current_type = instance.type
        if previous_type != current_type:
            delivery_change_type(instance)
        return instance


class DeliveryRegionSerializer(TranslationSerializer):

    class Meta:
        model = models.DeliveryRegion
        fields = '__all__'

    def __init__(self, *args, delivery_type_id=None, **kwargs):
        self.delivery_type_id = delivery_type_id
        super().__init__(*args, **kwargs)
        organization = self.request.user.organization
        if 'payment_types' in self.fields:
            self.fields['payment_types'].child_relation.queryset = get_organization_payment_types(organization)

    def validate_name(self, value):
        delivery_type = models.DeliveryType.objects.get(pk=self.delivery_type_id)
        delivery_regions = delivery_type.delivery_regions.filter(name=value)
        if self.instance and self.instance.pk:
            delivery_regions = delivery_regions.exclude(pk=self.instance.pk)
        if delivery_regions.exists():
            raise serializers.ValidationError("Регион с данным наименованием уже существует")
        return value


class TransportCompanyPointSerializer(TranslationSerializer):

    class Meta:
        model = models.TransportCompanyPoint
        fields = '__all__'

    def __init__(self, *args, delivery_type_id=None, **kwargs):
        self.delivery_type_id = delivery_type_id
        super().__init__(*args, **kwargs)

    def validate_address(self, value):
        delivery_type = models.DeliveryType.objects.get(pk=self.delivery_type_id)
        transport_company_points = delivery_type.transport_company_points.filter(address=value)
        if self.instance and self.instance.pk:
            transport_company_points = transport_company_points.exclude(pk=self.instance.pk)
        if transport_company_points.exists():
            raise serializers.ValidationError("Пункт выдачи с данным адресом уже существует")
        return value
