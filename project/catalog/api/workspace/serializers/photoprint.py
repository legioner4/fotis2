import logging

from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from base.restapi.translation.serializer import TranslationSerializer
from catalog import models
from catalog.models import PaperType

logger = logging.getLogger(__name__)


class PaperTypeSerializer(TranslationSerializer):
    translation_fields = ['name']

    class Meta:
        model = models.PaperType
        fields = "__all__"


class PhotoFormatSerializer(TranslationSerializer):

    class Meta:
        model = models.PhotoFormat
        fields = "__all__"


class FormatPriceSerializer(TranslationSerializer):
    paper_type_name = serializers.SerializerMethodField(label='Тип бумаги', read_only=True)

    class Meta:
        model = models.FormatPrice
        fields = '__all__'

    def __init__(self, *args, photo_format_id=None, **kwargs):
        self.photo_format_id = photo_format_id
        super().__init__(*args, **kwargs)

    def __object_init__(self, *args, **kwargs):
        if 'paper_type' in self.fields:
            self.fields['paper_type'].queryset = PaperType.objects.filter(enabled=True)

    def validate_paper_type(self, value):
        format_prices = models.FormatPrice.objects.filter(paper_type=value, photo_format__pk=self.photo_format_id)
        if self.instance and self.instance.pk:
            format_prices = format_prices.exclude(pk=self.instance.pk)
        if format_prices.exists():
            raise ValidationError("Цена с таким типом бумаги уже существует")
        return value

    def get_paper_type_name(self, price):
        return price.paper_type.name


class DiscountSerializer(TranslationSerializer):

    class Meta:
        model = models.Discount
        fields = '__all__'

    def __init__(self, *args, format_price_id=None, **kwargs):
        self.format_price_id = format_price_id
        super().__init__(*args, **kwargs)
        if 'max_count' in self.fields:
            self.fields['max_count'].help_text = 'Для строки с максимальной скидкой, указывайте произвольное значение от 10000, например.'

    def validate_max_count(self, value):
        if value == 0:
            raise ValidationError("Для строки с максимальной скидкой невозможно использовать значение 0. Введите произвольное значение: 10000, например.")
        return value
