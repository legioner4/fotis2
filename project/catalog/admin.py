from django.contrib import admin

from .models import *

admin.site.register(PaperType)
admin.site.register(PhotoFormat)
admin.site.register(FormatPrice)
admin.site.register(Discount)
admin.site.register(DeliveryType)
admin.site.register(DeliveryRegion)
admin.site.register(TransportCompanyPoint)
admin.site.register(Language)
