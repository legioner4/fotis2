# Generated by Django 2.2.6 on 2021-01-15 19:54

from django.db import migrations
from django.conf import settings


def set_language_name_ru(apps, schema_editor):
    PaperType = apps.get_model('catalog', 'PaperType')
    PhotoFormat = apps.get_model('catalog', 'PhotoFormat')
    DeliveryType = apps.get_model('catalog', 'DeliveryType')
    DeliveryRegion = apps.get_model('catalog', 'DeliveryRegion')
    TransportCompanyPoint = apps.get_model('catalog', 'TransportCompanyPoint')

    def set_all_object_language(object, value, name):
        for code, language_name in settings.LANGUAGES:
            field_name = f'{name}_{code}'
            setattr(object, field_name, value)
        object.save()


    for object in PaperType.objects.all():
        set_all_object_language(object, object.name, 'name')

    for object in PhotoFormat.objects.all():
        set_all_object_language(object, object.name, 'name')

    for object in DeliveryType.objects.all():
        set_all_object_language(object, object.name, 'name')
        set_all_object_language(object, object.description, 'description')

    for object in DeliveryRegion.objects.all():
        set_all_object_language(object, object.name, 'name')

    for object in TransportCompanyPoint.objects.all():
        set_all_object_language(object, object.name, 'name')


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0041_auto_20210115_2237'),
    ]

    operations = [
        migrations.RunPython(set_language_name_ru, migrations.RunPython.noop),
    ]
