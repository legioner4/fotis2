# Generated by Django 2.2.6 on 2021-01-20 18:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0043_auto_20210117_0048'),
    ]

    operations = [
        migrations.AddField(
            model_name='language',
            name='name_en',
            field=models.CharField(max_length=256, null=True, verbose_name='Наименование'),
        ),
        migrations.AddField(
            model_name='language',
            name='name_et',
            field=models.CharField(max_length=256, null=True, verbose_name='Наименование'),
        ),
        migrations.AddField(
            model_name='language',
            name='name_kk',
            field=models.CharField(max_length=256, null=True, verbose_name='Наименование'),
        ),
        migrations.AddField(
            model_name='language',
            name='name_lt',
            field=models.CharField(max_length=256, null=True, verbose_name='Наименование'),
        ),
        migrations.AddField(
            model_name='language',
            name='name_lv',
            field=models.CharField(max_length=256, null=True, verbose_name='Наименование'),
        ),
        migrations.AddField(
            model_name='language',
            name='name_ro',
            field=models.CharField(max_length=256, null=True, verbose_name='Наименование'),
        ),
        migrations.AddField(
            model_name='language',
            name='name_ru',
            field=models.CharField(max_length=256, null=True, verbose_name='Наименование'),
        ),
        migrations.AddField(
            model_name='language',
            name='name_tt',
            field=models.CharField(max_length=256, null=True, verbose_name='Наименование'),
        ),
    ]
