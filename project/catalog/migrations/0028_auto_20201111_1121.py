# Generated by Django 2.2.6 on 2020-11-11 08:21

from django.db import migrations


def set_payment_type(apps, schema_editor):
    DeliveryType = apps.get_model('catalog', 'DeliveryType')
    DeliveryRegion = apps.get_model('catalog', 'DeliveryRegion')
    Organization = apps.get_model('organization', 'Organization')
    PaymentType = apps.get_model('organization', 'PaymentType')

    for organization in Organization.objects.all():
        payment_type = PaymentType.objects.filter(type='upon_receipt').last()

        for delivery_type in DeliveryType.objects.filter(organization=organization):
            delivery_type.payment_types.add(payment_type)
            delivery_type.save()

        for delivery_region in DeliveryRegion.objects.filter(delivery_type__organization=organization):
            delivery_region.payment_types.add(payment_type)
            delivery_region.save()


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0027_deliveryregion_payment_types'),
        ('organization', '0020_auto_20201111_1129'),
    ]

    operations = [
        migrations.RunPython(set_payment_type),
    ]
