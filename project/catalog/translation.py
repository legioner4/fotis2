from modeltranslation.translator import translator

from base.restapi.translation.logic.options import BaseTranslationOptions
from .models import *


class PaperTypeTranslationOptions(BaseTranslationOptions):
    fields = ('name',)
    sorted_fields = [{'field': 'name', 'order': 1}]
    required_languages = ('ru',)
    empty_values = None


translator.register(PaperType, PaperTypeTranslationOptions)


class PhotoFormatTranslationOptions(BaseTranslationOptions):
    fields = ('name',)
    sorted_fields = [{'field': 'name', 'order': 1}]
    required_languages = ('ru',)
    empty_values = None


translator.register(PhotoFormat, PhotoFormatTranslationOptions)


class DeliveryTypeTranslationOptions(BaseTranslationOptions):
    fields = ('name', 'description')
    sorted_fields = [{'field': 'description', 'order': 2}, {'field': 'name', 'order': 1}]
    required_languages = ('ru',)
    empty_values = None


translator.register(DeliveryType, DeliveryTypeTranslationOptions)


class DeliveryRegionTranslationOptions(BaseTranslationOptions):
    fields = ('name',)
    sorted_fields = [{'field': 'name', 'order': 1}]
    required_languages = ('ru',)
    empty_values = None


translator.register(DeliveryRegion, DeliveryRegionTranslationOptions)


class TransportCompanyPointTranslationOptions(BaseTranslationOptions):
    fields = ('address',)
    sorted_fields = [{'field': 'address', 'order': 1}]
    required_languages = ('ru',)
    empty_values = None


translator.register(TransportCompanyPoint, TransportCompanyPointTranslationOptions)


class LanguageTranslationOptions(BaseTranslationOptions):
    fields = ('name',)
    sorted_fields = [{'field': 'name', 'order': 1}]
    required_languages = ('ru',)
    empty_values = None


translator.register(Language, LanguageTranslationOptions)
