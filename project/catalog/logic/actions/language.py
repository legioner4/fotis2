from django.conf import settings
from catalog.models import Language


def update_languages():
    available_languages = []
    for code, language_name in settings.LANGUAGES:
        language, _ = Language.objects.get_or_create(name=language_name, code=code)
        available_languages.append(language.pk)
    Language.objects.exclude(pk__in=available_languages).delete()
