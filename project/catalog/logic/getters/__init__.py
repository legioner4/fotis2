import decimal

from pydantic import BaseModel
from rest_framework.generics import get_object_or_404

from catalog.models import FormatPrice, Discount, DeliveryType, DeliveryRegion
from organization.logic.getters import value_convert_organization_currency
from organization.models import PaymentType


class FormatPriceData(BaseModel):
    price_without_discount: decimal.Decimal = decimal.Decimal(0)
    total_price: decimal.Decimal = decimal.Decimal(0)
    discount_sum: decimal.Decimal = decimal.Decimal(0)

    def info(self, organization):
        return {
            'price_without_discount_str': value_convert_organization_currency(organization, self.price_without_discount),
            'price_without_discount': self.price_without_discount,

            'total_price_str': value_convert_organization_currency(organization, self.total_price),
            'total_price': self.total_price,

            'discount_sum_str': value_convert_organization_currency(organization, self.discount_sum),
            'discount_sum': self.discount_sum,
        }


class DeliveryPriceData(BaseModel):
    delivery_price: decimal.Decimal = decimal.Decimal(0)
    total_price: decimal.Decimal = decimal.Decimal(0)
    discount_sum: decimal.Decimal = decimal.Decimal(0)

    def info(self, organization):
        return {
            'delivery_price_str': value_convert_organization_currency(organization, self.delivery_price),
            'delivery_price': self.delivery_price,

            'total_price_str': value_convert_organization_currency(organization, self.total_price),
            'total_price': self.total_price,

            'discount_sum_str': value_convert_organization_currency(organization, self.discount_sum),
            'discount_sum': self.discount_sum,
        }


def format_price_calculate(qty, format_price_id):
    format_price = get_object_or_404(FormatPrice.objects.all(), id=format_price_id)
    discount = Discount.objects.filter(format_price=format_price).filter(min_count__lte=qty, max_count__gte=qty).first()

    total_price = qty * decimal.Decimal(format_price.price)
    price_without_discount = total_price

    discount_sum = decimal.Decimal(0)
    if discount:
        discount_price = qty * decimal.Decimal(discount.discount_value)
        discount_sum = price_without_discount - discount_price
        total_price = discount_price

    return FormatPriceData(
        price_without_discount = round(price_without_discount, 2),
        total_price=round(total_price, 2),
        discount_sum=round(discount_sum, 2),
    )


def get_delivery_price(delivery_type_id, delivery_region_id):
    delivery_type = DeliveryType.objects.filter(pk=delivery_type_id).first()

    delivery_price = decimal.Decimal(0)
    if delivery_type:
        delivery_price = delivery_type.cost
        if delivery_region_id:
            delivery_region = DeliveryRegion.objects.filter(pk=delivery_region_id).first()
            if delivery_region:
                delivery_price = delivery_region.cost

    return delivery_price


def delivery_format_price_calculate(formats, delivery_type_id, delivery_region_id):
    total_price = decimal.Decimal(0)
    discount_sum = decimal.Decimal(0)

    for format in formats:

        qty = format.get('qty', None)
        format_price_id = format.get('format_price_id', None)
        if qty and format_price_id:
            price = format_price_calculate(qty, format_price_id)
            total_price += price.total_price
            discount_sum += price.discount_sum

    delivery_price = get_delivery_price(delivery_type_id, delivery_region_id)
    if delivery_price:
        total_price = total_price + delivery_price

    return DeliveryPriceData(
        total_price=total_price,
        discount_sum=discount_sum,
        delivery_price=delivery_price
    )


def get_delivery_type_all_enabled_payment_types(delivery_type):
    payment_type_pk = list(delivery_type.payment_types.all().values_list('pk', flat=True))

    if delivery_type.type == DeliveryType.COURIER_TYPE:

        delivery_regions = DeliveryRegion.objects.filter(delivery_type=delivery_type)
        for delivery_region in delivery_regions:
            payment_type_pk += list(delivery_region.payment_types.all().values_list('pk', flat=True))

    elif delivery_type.type == DeliveryType.PICKUP_TYPE:

        for delivery_subdivision in delivery_type.enabled_delivery_subdivisions:
            payment_type_pk += list(delivery_subdivision.payment_types.all().values_list('pk', flat=True))

    return PaymentType.objects.filter(pk__in=payment_type_pk)
