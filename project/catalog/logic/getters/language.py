from django.conf import settings
from catalog.models import Language


def get_default_language():
    code = settings.MODELTRANSLATION_DEFAULT_LANGUAGE
    language_name = settings.LANGUAGES_DICT[code]
    language, _ = Language.objects.get_or_create(name=language_name, code=code)
    return language
