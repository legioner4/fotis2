import decimal

from rest_framework import serializers
from django.utils.translation import ugettext as _
from base.restapi.serializer import BaseSerializer, SelectRelatedSerializer
from catalog import models
from organization.models import PaymentType, Subdivision


class PaymentTypesSerializer(BaseSerializer):

    class Meta:
        model = PaymentType
        fields = [
            "name",
            "type",
            "description",
        ]


class DeliverySubdivisionSerializer(BaseSerializer):
    payment_types = PaymentTypesSerializer(many=True)

    class Meta:
        model = Subdivision
        fields = [
            "name",
            "address",
            "phone_list",
            "email",
            "working_hours",
            "payment_types",
        ]



class DeliveryRegionSerializer(BaseSerializer):
    cost_str = serializers.CharField(label='Стоимость', read_only=True)
    cost = serializers.SerializerMethodField(read_only=True)
    payment_types = PaymentTypesSerializer(many=True)

    class Meta:
        model = models.DeliveryRegion
        fields = [
            "name",
            "cost",
            "cost_str",
            "payment_types",
            "enabled",
            "order",
        ]

    def get_cost(self, delivery_region):
        return decimal.Decimal(delivery_region.cost)


class DeliveryTypeSerializer(BaseSerializer):
    cost_str = serializers.CharField(label='Стоимость', read_only=True)
    cost = serializers.SerializerMethodField(read_only=True)
    delivery_fields = serializers.SerializerMethodField(read_only=True)
    delivery_regions = DeliveryRegionSerializer(many=True, source='enabled_delivery_regions')
    delivery_subdivisions = serializers.SerializerMethodField(read_only=True)
    payment_types = PaymentTypesSerializer(many=True)

    class Meta:
        model = models.DeliveryType
        fields = [
            "name",
            "type",
            "description",
            "cost",
            "cost_str",
            "enabled",
            "order",
            "delivery_fields",
            "delivery_subdivisions",
            "delivery_regions",
            "payment_types"
        ]

    def get_cost(self, delivery_type):
        return decimal.Decimal(delivery_type.cost)

    def get_delivery_fields(self, delivery_type):
        fields = []
        if delivery_type.type == models.DeliveryType.POST_TYPE:
            fields = [
                {
                    "name": "city",
                    "type": "text",
                    "label": _("Город"),
                    "required": True,
                },
                {
                    "name": "post_index",
                    "type": "text",
                    "label": _("Почтовый индекс"),
                    "required": True,
                },
                {
                    "name": "street",
                    "type": "text",
                    "label": _("Улица"),
                    "required": True,
                },
                {
                    "name": "house_number",
                    "type": "text",
                    "label": _("Номер дома"),
                    "required": True,
                },
                {
                    "name": "corps",
                    "type": "text",
                    "label": _("Корпус"),
                    "required": False,
                },
                {
                    "name": "apartment_number",
                    "type": "text",
                    "label": _("Номер квартиры или офиса"),
                    "required": False,
                },
            ]
        elif delivery_type.type == models.DeliveryType.COURIER_TYPE:
            delivery_region_choices = [
                {
                    'pk': delivery_region.pk,
                    'name': delivery_region.name,
                    'address': "",
                    'phone_list': [],
                    'email': "",
                    'working_hours': "",
                } for delivery_region in delivery_type.enabled_delivery_regions.all()]

            fields = [
                {
                    "name": "delivery_region",
                    "type": "select",
                    "label": _("Регион доставки"),
                    "required": False,
                    "choices": delivery_region_choices
                },
                {
                    "name": "city",
                    "type": "text",
                    "label": _("Город"),
                    "required": True,
                },

                {
                    "name": "street",
                    "type": "text",
                    "label": _("Улица"),
                    "required": True,
                },
                {
                    "name": "house_number",
                    "type": "text",
                    "label": _("Номер дома"),
                    "required": True,
                },
                {
                    "name": "corps",
                    "type": "text",
                    "label": _("Корпус"),
                    "required": False,
                },
                {
                    "name": "apartment_number",
                    "type": "text",
                    "label": _("Номер квартиры или офиса"),
                    "required": False,
                },
            ]
        elif delivery_type.type == models.DeliveryType.PICKUP_TYPE:
            subdivision_choices = [
                {
                    'pk': subdivision.pk,
                    'name': subdivision.name,
                    'address': subdivision.address,
                    'phone_list': subdivision.phone_list,
                    'email': subdivision.email,
                    'working_hours': subdivision.working_hours,
                } for subdivision in delivery_type.enabled_delivery_subdivisions]
            fields = [
                {
                    "name": "subdivision",
                    "type": "select",
                    "label": _("Фотоцентр"),
                    "required": True,
                    "choices": subdivision_choices
                },
            ]
        elif delivery_type.type == models.DeliveryType.TRANSPORT_COMPANY_TYPE:
            point_choices = [
                {
                    'pk': point.pk,
                    'address': point.address
                } for point in delivery_type.transport_company_points.all()]
            fields = [
                {
                    "name": "transport_company_point",
                    "type": "select",
                    "label": _("Пункт выдачи транспортной компании"),
                    "required": True,
                    "choices": point_choices
                },
            ]
        elif delivery_type.type == models.DeliveryType.RANDOM_DELIVERY_TYPE:
            fields = [

            ]
        return fields

    def get_delivery_subdivisions(self, delivery_type):
        if delivery_type.type == models.DeliveryType.PICKUP_TYPE:
            serializer = DeliverySubdivisionSerializer(instance=delivery_type.enabled_delivery_subdivisions, many=True)
            return serializer.data
        return []


class DiscountRelatedSerializer(SelectRelatedSerializer):

    class Meta:
        model = models.Discount
        fields = (
            'max_count',
            'min_count',
            'discount_value',
            'enabled',
            'order',
        )


class FormatPriceRelatedSerializer(SelectRelatedSerializer):
    paper_type_name = serializers.CharField(label='Тип бумаги', read_only=True, source='paper_type.name')
    paper_type_slug = serializers.CharField(label='Тип бумаги', read_only=True, source='paper_type.slug')
    discounts = DiscountRelatedSerializer(label='Скидки', many=True, source='enabled_discounts')
    price_str = serializers.CharField(read_only=True)
    price = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = models.FormatPrice
        fields = (
            'paper_type_name',
            'paper_type_slug',
            'price',
            'price_str',
            'discounts',
            'enabled',
            'order',
        )

    def get_price(self, format_price):
        return decimal.Decimal(format_price.price)


class PhotoFormatSerializer(BaseSerializer):
    prices = FormatPriceRelatedSerializer(label='Цены', many=True, source='enabled_prices')

    class Meta:
        model = models.PhotoFormat
        fields = [
            "name",
            "width",
            "height",
            "enabled",
            "order",
            "instagram_frame_width",
            "polaroid_frame_bottom_width",
            "polaroid_frame_width",
            "prices",
        ]
