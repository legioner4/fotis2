from base.restapi.serializer import BaseSerializer
from payment import models
from rest_framework import serializers


class PaymentSerializer(BaseSerializer):
    OS_TYPE_CHOICES = (
        ('android', 'android'),
        ('ios', 'ios'),
    )
    os_type = serializers.ChoiceField(choices=OS_TYPE_CHOICES, allow_null=True, write_only=True)

    class Meta:
        model = models.Payment
        fields = ["order", "os_type"]
