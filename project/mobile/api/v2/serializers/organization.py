from base.restapi.serializer import BaseSerializer
from organization import models
from catalog import models as catalog_models


class LanguageSerializer(BaseSerializer):
    class Meta:
        model = catalog_models.Language
        fields = ["code", "name"]


class OrganizationSerializer(BaseSerializer):
    languages = LanguageSerializer(many=True, read_only=True)

    class Meta:
        model = models.Organization
        fields = [
            "short_name", "full_name", "address", "logo", "languages",
            "about_link", "personal_data_policy_link", "privacy_policy_link", "refund_policy_link"
        ]
