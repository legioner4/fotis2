from rest_framework import serializers

from base.restapi.serializer import BaseSerializer
from catalog import models as catalog_models
from organization.models import Client


class LanguageSerializer(BaseSerializer):
    language = serializers.PrimaryKeyRelatedField(allow_null=True, queryset=catalog_models.Language.objects.all())

    class Meta:
        model = Client
        fields = ['language',]

    def __init__(self, *args, organization=None, client=None, **kwargs):
        self.client = client
        self.organization = organization
        super().__init__(*args, **kwargs)
        if 'language' in self.fields:
            self.fields['language'].queryset = self.organization.languages.all()
