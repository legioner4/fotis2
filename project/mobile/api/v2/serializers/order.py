from django.utils.translation import ugettext as _
from rest_framework import serializers

from base.restapi.serializer import BaseSerializer, SelectRelatedSerializer
from catalog.logic.getters import get_delivery_type_all_enabled_payment_types
from catalog.models import DeliveryType, DeliveryRegion, FormatPrice
from order import models
from order.logic.actions import modify_photo
from organization.models import Subdivision


class DeliverySubdivisionSerializer(BaseSerializer):

    class Meta:
        model = Subdivision
        fields = ['name', 'address', 'phone_list', 'email', 'working_hours']


class DeliveryRegionSerializer(BaseSerializer):
    class Meta:
        model = DeliveryRegion
        fields = ['name',]


class OrderDeliveryRelatedSerializer(BaseSerializer):
    delivery_type_name = serializers.CharField(label='Тип доставки', read_only=True, source='delivery_type.name')
    subdivision = DeliverySubdivisionSerializer()
    delivery_region = DeliveryRegionSerializer()

    class Meta:
        model = models.OrderDelivery
        fields = '__all__'
        read_only_fields = ('order',)


class OrderPaymentRelatedSerializer(BaseSerializer):
    payment_type_name = serializers.CharField(label='Тип оплаты', read_only=True, source='payment_type.name')

    class Meta:
        model = models.OrderPayment
        fields = ('payment_type', 'payment_state', 'payment_type_name')


class OrderCreateSerializer(BaseSerializer):
    delivery = OrderDeliveryRelatedSerializer()

    class Meta:
        model = models.Order
        fields = ['delivery', 'fio', 'email', 'note', 'phone']

    def __init__(self, *args, organization=None, client=None, **kwargs):
        self.client = client
        self.organization = organization
        super().__init__(*args, **kwargs)
        self.fields['delivery_type'] = serializers.PrimaryKeyRelatedField(label='Тип доставки', queryset=self.organization.delivery_types.all(), write_only=True)
        self.fields['payment_type'] = serializers.PrimaryKeyRelatedField(label='Тип оплаты', queryset=self.organization.payment_types.all(), write_only=True)

        self.fields['city'] = serializers.CharField(write_only=True, allow_blank=True, allow_null=True, label=_('Город'), max_length=512, required=False)
        self.fields['street'] = serializers.CharField(write_only=True, allow_blank=True, allow_null=True, label=_('Улица'), max_length=512, required=False)
        self.fields['house_number'] = serializers.CharField(write_only=True, allow_blank=True, allow_null=True, label=_('Номер дома'), max_length=512, required=False)
        self.fields['corps'] = serializers.CharField(write_only=True, allow_blank=True, allow_null=True, label=_('Корпус'), max_length=512, required=False)
        self.fields['apartment_number'] = serializers.CharField(write_only=True, allow_blank=True, allow_null=True, label=_('Номер квартиры или офиса'), max_length=512,
                                                                required=False)
        self.fields['post_index'] = serializers.CharField(write_only=True, allow_blank=True, allow_null=True, label=_('Почтовый индекс'), max_length=512, required=False)

        self.fields['delivery_region'] = serializers.PrimaryKeyRelatedField(write_only=True, allow_null=True, label=_('Регион доставки'),
                                                                            queryset=DeliveryRegion.objects.all(), required=False)
        self.fields['subdivision'] = serializers.PrimaryKeyRelatedField(write_only=True, allow_null=True, label=_('Фотоцентр'), queryset=self.organization.subdivision.all(),
                                                                        required=False)

    def validate(self, data):
        delivery_type = data['delivery_type']
        required_fields = []
        if delivery_type.type == DeliveryType.POST_TYPE:
            required_fields = ['post_index', 'city', 'street', 'house_number']

        elif delivery_type.type == DeliveryType.COURIER_TYPE:
            required_fields = ['city', 'street', 'house_number']

        elif delivery_type.type == DeliveryType.PICKUP_TYPE:
            required_fields = ['subdivision']

        errors = {}
        for field_name in required_fields:
            if not data.get(field_name, None):
                errors[field_name] = 'Обязательное поле'

        payment_type = data['payment_type']
        delivery_type_payment_types = get_delivery_type_all_enabled_payment_types(delivery_type)
        if payment_type not in delivery_type_payment_types:
            errors['payment_type'] = 'Неправильный идентификатор'

        if errors:
            raise serializers.ValidationError(errors)
        return data

    def create(self, validated_data):
        delivery_type = validated_data.pop('delivery_type')
        payment_type = validated_data.pop('payment_type')
        subdivision = validated_data.pop('subdivision', None)
        delivery_region = validated_data.pop('delivery_region', None)

        delivery_data = {
            'delivery_type': delivery_type,

            'organization': delivery_type.organization,
            'delivery_name': delivery_type.name,
            'delivery_type_code': delivery_type.type,
            'delivery_description': delivery_type.description,
            'delivery_cost': delivery_type.cost,

            'city': validated_data.pop('city', ''),
            'street': validated_data.pop('street', ''),
            'house_number': validated_data.pop('house_number', ''),
            'corps': validated_data.pop('corps', ''),
            'apartment_number': validated_data.pop('apartment_number', ''),
            'post_index': validated_data.pop('post_index', ''),

            'delivery_region': delivery_region,
            'delivery_region_name': delivery_region.name if delivery_region else '',

            'subdivision': subdivision,
            'subdivision_name': subdivision.name if subdivision else '',
            'subdivision_address': subdivision.address if subdivision else '',
        }
        delivery = models.OrderDelivery.objects.create(**delivery_data)
        payment = models.OrderPayment.objects.create(
            payment_type=payment_type,
            name=payment_type.name,
            type=payment_type.type,
            description=payment_type.description,
        )

        validated_data["client"] = self.client
        validated_data["organization"] = self.organization
        order = super().create(validated_data)
        order.delivery = delivery
        order.payment = payment
        order.save()
        return order


class PhotoRelatedSerializer(SelectRelatedSerializer):
    class Meta:
        model = models.Photo
        fields = (
            'image',
            'qty',
            'border',
            'frame',
            'created_at',
            'crop_x',
            'crop_y',
            'crop_w',
            'crop_h'
        )


class OrderSerializer(BaseSerializer):
    order_number = serializers.CharField(label='Номер заказа', read_only=True, source='get_order_number')
    delivery = OrderDeliveryRelatedSerializer()
    payment = OrderPaymentRelatedSerializer()
    organization_name = serializers.CharField(label='Организация', source='organization.short_name', read_only=True)
    photos = PhotoRelatedSerializer(label='Фотографии', many=True)
    price_info = serializers.SerializerMethodField(label='Цена', read_only=True)

    class Meta:
        model = models.Order
        fields = (
            'order_number',
            'organization_name', 'client', 'created_at', 'photos', 'delivery', 'payment',
            'fio', 'email', 'note', 'phone',
            'price_info'
        )

    def __init__(self, *args, organization=None, client=None, **kwargs):
        # print(args, kwargs)
        super().__init__(*args, **kwargs)
        # print(self.fields)

    def get_price_info(self, order):
        return order.price_info


class PhotoSerializer(BaseSerializer):
    order_id = serializers.CharField(label='Заказ', write_only=True)
    format_price = serializers.CharField(label='формат', write_only=True)

    class Meta:
        model = models.Photo
        fields = ('image', 'qty', 'border', 'frame', 'format_price', 'order_id', 'crop_x', 'crop_y', 'crop_w', 'crop_h')

    def validate(self, data):
        required_fields = ['image', 'qty', 'frame', 'format_price', 'order_id']

        errors = {}
        for field_name in required_fields:
            if not data.get(field_name, None):
                errors[field_name] = 'Обязательное поле'

        crop_x = data.get('crop_x', None)
        crop_y = data.get('crop_y', None)
        crop_w = data.get('crop_w', None)
        crop_h = data.get('crop_h', None)
        if any([crop_h, crop_w, crop_x, crop_y]):
            if not crop_x:
                errors['crop_x'] = 'Обязательное поле'
            if not crop_y:
                errors['crop_y'] = 'Обязательное поле'
            if not crop_w:
                errors['crop_w'] = 'Обязательное поле'
            if not crop_h:
                errors['crop_h'] = 'Обязательное поле'

        if errors:
            raise serializers.ValidationError(errors)

        order_id = data.get('order_id', None)
        format_price_id = data.get('format_price', None)

        if format_price_id and not FormatPrice.objects.filter(pk=format_price_id).exists():
            raise serializers.ValidationError({'format_price': 'Неверный идентификатор формата'})

        if order_id:
            order = models.Order.objects.filter(pk=order_id).first()
            if not order:
                raise serializers.ValidationError({'order_id': 'Неверный идентификатор заказа'})
            if not order.creation_state:
                raise serializers.ValidationError({'order_id': 'Данный заказ не на стадии создания'})
        return data

    def create(self, validated_data):
        format_price_id = validated_data.pop('format_price')
        format_price = FormatPrice.objects.filter(pk=format_price_id).first()

        price = models.PhotoPrice.objects.create(
            format_price=format_price,
            format_price_pk=format_price.pk
        )
        format = models.PhotoFormat.objects.create(
            format=format_price.photo_format,
            paper_type_name=format_price.paper_type.name,
            format_name=format_price.photo_format.name,
            width=format_price.photo_format.width,
            height=format_price.photo_format.height,
            polaroid_frame_width=format_price.photo_format.polaroid_frame_width,
            polaroid_frame_bottom_width=format_price.photo_format.polaroid_frame_bottom_width,
            instagram_frame_width=format_price.photo_format.instagram_frame_width
        )
        validated_data['price'] = price
        validated_data['format'] = format
        instance = super().create(validated_data)

        modify_photo(instance)
        return instance
