from base.restapi.serializer import BaseSerializer
from organization import models
from catalog import models as catalog_models


class AndroidUpdateSerializer(BaseSerializer):

    class Meta:
        model = models.Organization
        fields = [
            "android_show_blocking_modal", "android_blocking_modal_text", "android_blocking_modal_button_text", "android_blocking_modal_target_version"
        ]


class IosUpdateSerializer(BaseSerializer):

    class Meta:
        model = models.Organization
        fields = [
            "ios_show_blocking_modal", "ios_blocking_modal_text", "ios_blocking_modal_button_text", "ios_blocking_modal_target_version"
        ]

