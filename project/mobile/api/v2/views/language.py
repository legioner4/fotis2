import logging

from rest_framework import status
from rest_framework.decorators import action
from rest_framework.response import Response

from base.restapi.permission import DenyAny, AllowAny
from mobile.api.base import BaseMobileModelViewSet
from mobile.api.v2 import serializers
from organization.logic.actions.language import set_client_language

logger = logging.getLogger(__name__)


class LanguageView(BaseMobileModelViewSet):
    permission_classes = DenyAny
    permission_classes_map = {'set': AllowAny}

    def get_serializer_kwargs(self):
        return {'organization': self.organization, 'client': self.client}

    @action(methods=['post'], detail=False)
    def set(self, request, *args, **kwargs):
        self.serializer_class = serializers.LanguageSerializer
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        language_pk = serializer.data.get('language', None)
        language = set_client_language(self.client, language_pk)
        data = {'code': language.code, 'name': language.name} if language else {}
        return Response(data, status=status.HTTP_200_OK)
