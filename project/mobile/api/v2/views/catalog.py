from rest_framework import status
from rest_framework.decorators import action
from rest_framework.response import Response

from catalog import models
from mobile.api.base import BaseMobileModelViewSet
from mobile.api.v2 import serializers


class DeliveryView(BaseMobileModelViewSet):
    serializer_class = serializers.DeliveryTypeSerializer
    queryset = models.DeliveryType.objects

    def get_queryset(self):
        return models.DeliveryType.objects.filter(organization=self.organization.pk).filter(enabled=True)


class PhotoFormatView(BaseMobileModelViewSet):
    serializer_class = serializers.PhotoFormatSerializer
    queryset = models.PhotoFormat.objects

    def get_queryset(self):
        return models.PhotoFormat.objects.filter(organization=self.organization.pk).filter(enabled=True)
