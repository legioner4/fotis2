from rest_framework import status
from rest_framework.decorators import action
from rest_framework.response import Response

from base.restapi.permission import DenyAny, AllowAny
from mobile.api.base import BaseMobileModelViewSet
from mobile.api.v2 import serializers
from organization import models


class AppUpdateInfoView(BaseMobileModelViewSet):
    permission_classes = DenyAny
    permission_classes_map = {
        'android': AllowAny,
        'ios': AllowAny
    }
    queryset = models.Organization.objects.none()

    @action(detail=False, methods=['get'])
    def android(self, request, *args, **kwargs):
        self.serializer_class = serializers.AndroidUpdateSerializer
        serializer = self.get_serializer(self.organization)
        return Response(serializer.data, status=status.HTTP_200_OK)

    @action(detail=False, methods=['get'])
    def ios(self, request, *args, **kwargs):
        self.serializer_class = serializers.IosUpdateSerializer
        serializer = self.get_serializer(self.organization)
        return Response(serializer.data, status=status.HTTP_200_OK)
