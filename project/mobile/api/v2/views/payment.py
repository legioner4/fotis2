from rest_framework import status
from rest_framework.response import Response

from base.restapi.permission import DenyAny, CREATE, AllowAny, RETRIEVE
from mobile.api.base import BaseMobileModelViewSet
from mobile.api.v2 import serializers
from payment import models
from order.models import Order
from payment.logic import create_sber_payment, get_return_and_fail_urls, check_payment_order, check_state_payment, \
    PaymentException, get_order_last_payment, UNPAID, \
    NOT_ORDER, SUCCESS, PROGRESS, FAIL, ERROR, get_or_create_order_payment


class PaymentView(BaseMobileModelViewSet):
    permission_classes = DenyAny
    permission_classes_map = {CREATE: AllowAny}
    queryset = models.Payment.objects.none()
    serializer_class = serializers.PaymentSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        try:
            serializer.is_valid(raise_exception=True)
            order = serializer.validated_data.get('order')
            os_type = serializer.validated_data.pop('os_type', 'android')

            payment, url = get_or_create_order_payment(order, os_type)

            return_url, fail_url = get_return_and_fail_urls(payment)
            return Response({
                'url': url,
                'return_url': return_url,
                'fail_url': fail_url,
                'order_id': payment.order.id
            }, status=status.HTTP_200_OK)
        except Exception as e:
            return Response({'error': f'{e}'}, status=status.HTTP_400_BAD_REQUEST)


class PaymentStatusView(BaseMobileModelViewSet):
    permission_classes = DenyAny
    permission_classes_map = {RETRIEVE: AllowAny}
    serializer_class = serializers.PaymentSerializer

    def get_queryset(self):
        return models.Payment.objects.filter(order__organization=self.organization)

    def retrieve(self, request, *args, **kwargs):
        order_pk = self.kwargs.get('pk', None)

        try:
            if not order_pk:
                return Response({
                    'state': NOT_ORDER,
                }, status=status.HTTP_200_OK)

            order = Order.objects.filter(pk=order_pk).first()
            last_payment = get_order_last_payment(order)

            state = None
            if not last_payment:
                state = UNPAID

            if not state:
                state = check_state_payment(last_payment)
                if state == FAIL:
                    state = PROGRESS

            return Response({
                'state': state,
                'order_id': order.id
            }, status=status.HTTP_200_OK)
        except Exception as e:
            return Response({'state': ERROR}, status=status.HTTP_200_OK)
