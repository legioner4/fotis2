from django.http import Http404
from rest_framework import status
from rest_framework import viewsets
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from mobile.api.v2.serializers.client import ClientSerializer
from organization.models import Organization, Client


class AccessTokenView(viewsets.ViewSet):
    authentication_classes = []
    permission_classes = [AllowAny]

    def list(self, request):
        organization_key = request.GET.get('companyId', None)

        if not organization_key:
            raise Http404('Учетные данные не предоставлены.')

        organization = Organization.objects.filter(key=organization_key).first()
        if not organization:
            raise Http404('Учетные данные не предоставлены.')

        client_key = request.GET.get('userId', None)
        if client_key:
            client = Client.objects.filter(organization=organization, key=client_key).first()
            if not client:
                raise Http404('Учетные данные не предоставлены.')
        else:
            client = Client.objects.create(organization=organization)

        serializer = ClientSerializer(instance=client)
        return Response(serializer.data, status=status.HTTP_200_OK)
