from rest_framework import status
from rest_framework.decorators import action
from rest_framework.response import Response

from base.restapi.permission import DenyAny, AllowAny
from catalog.logic.getters import format_price_calculate, delivery_format_price_calculate
from mobile.api.base import BaseMobileModelViewSet
from mobile.api.v2 import serializers
from order import models


class FormatPriceView(BaseMobileModelViewSet):
    permission_classes = DenyAny
    permission_classes_map = {
        'calculate': AllowAny
    }
    queryset = models.Order.objects.none()

    @action(detail=False, methods=['post'])
    def calculate(self, request, *args, **kwargs):
        self.serializer_class = serializers.FormatPriceCalculateSerializer
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        qty = serializer.data.get('qty', None)
        format_price_id = serializer.data.get('format_price_id', None)
        data = format_price_calculate(qty, format_price_id)
        return Response(data.info(self.organization), status=status.HTTP_200_OK)


class DeliveryFormatPriceView(BaseMobileModelViewSet):
    permission_classes = DenyAny
    permission_classes_map = {
        'calculate': AllowAny
    }
    queryset = models.Order.objects.none()

    @action(detail=False, methods=['post'])
    def calculate(self, request, *args, **kwargs):
        self.serializer_class = serializers.DeliveryFormatPriceCalculateSerializer
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        formats = serializer.data.get('formats', [])
        delivery_type_id = serializer.data.get('delivery_type_id', None)
        delivery_region_id = serializer.data.get('delivery_region_id', None)
        data = delivery_format_price_calculate(formats, delivery_type_id, delivery_region_id)
        return Response(data.info(self.organization), status=status.HTTP_200_OK)
