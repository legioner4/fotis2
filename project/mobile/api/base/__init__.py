from logging import getLogger

from django.conf import settings
from django.http import Http404
from django.utils import translation
from rest_framework import viewsets

from base.restapi.viewsets import BaseModelViewSet, NestedViewSet
from catalog.logic.getters.language import get_default_language
from mobile.authentication import TokenAuthentication, SessionAuthentication

logger = getLogger(__name__)


class MobileMixin():
    authentication_classes = [TokenAuthentication, SessionAuthentication] if settings.DEBUG else [TokenAuthentication]

    def get_language(self):
        if self.client and self.client.language and self.organization:
            language = self.client.language
            if language in self.organization.languages.all():
                return language
        return get_default_language()

    def get_serializer(self, *args, **kwargs):
        language = self.get_language()
        translation.activate(language.code)
        serializer = super().get_serializer(*args, **kwargs)
        return serializer

    @property
    def organization(self):
        if not self.request.user.organization:
            raise Http404("У организации должен быть администратор")
        return self.request.user.organization

    @property
    def client(self):
        if not self.request.auth:
            raise Http404()
        return self.request.auth


class BaseMobileViewSet(MobileMixin, viewsets.ViewSet):
    pass


class BaseMobileModelViewSet(MobileMixin, BaseModelViewSet):

    def get_meta(self, serializer=None):
        return {}

    def get_serializer_meta(self, serializer=None):
        return super().get_meta(serializer)


class BaseMobileNestedViewSet(MobileMixin, NestedViewSet):

    def get_meta(self, serializer=None):
        return {}

    def get_serializer_meta(self, serializer=None):
        return super().get_meta(serializer)
