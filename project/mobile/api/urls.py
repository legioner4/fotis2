from django.conf.urls import include, url

app_name = 'mobile'

urlpatterns = [
    url(r'v1/', include('mobile.api.v1.urls', namespace='v1')),
    url(r'v2/', include('mobile.api.v2.urls', namespace='v2'))
]
