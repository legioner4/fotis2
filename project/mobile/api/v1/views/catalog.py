from django.utils.translation import ugettext as _
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.response import Response

from catalog import models
from mobile.api.base import BaseMobileModelViewSet
from mobile.api.v1 import serializers


class DeliveryView(BaseMobileModelViewSet):
    serializer_class = serializers.DeliveryTypeSerializer
    queryset = models.DeliveryType.objects

    def get_queryset(self):
        return models.DeliveryType.objects.filter(organization=self.organization.pk).filter(enabled=True).filter(
            type__in=[models.DeliveryType.POST_TYPE, models.DeliveryType.COURIER_TYPE, models.DeliveryType.PICKUP_TYPE,]
        )

    @action(detail=True)
    def fields(self, *args, **kwargs):
        instance = self.get_object()
        fields = []
        if instance.type == models.DeliveryType.POST_TYPE:
            fields = [
                {
                    "name": "city",
                    "type": "text",
                    "label": _("Город"),
                    "required": True,
                },
                {
                    "name": "post_index",
                    "type": "text",
                    "label": _("Почтовый индекс"),
                    "required": True,
                },
                {
                    "name": "street",
                    "type": "text",
                    "label": _("Улица"),
                    "required": True,
                },
                {
                    "name": "house_number",
                    "type": "text",
                    "label": _("Номер дома"),
                    "required": True,
                },
                {
                    "name": "corps",
                    "type": "text",
                    "label": _("Корпус"),
                    "required": False,
                },
                {
                    "name": "apartment_number",
                    "type": "text",
                    "label": _("Номер квартиры или офиса"),
                    "required": False,
                },
            ]
        elif instance.type == models.DeliveryType.COURIER_TYPE:
            delivery_region_choices = [
                {
                    'pk': delivery_region.pk,
                    'name': delivery_region.name,
                    'address': "",
                    'phone_list': [],
                    'email': "",
                    'working_hours': "",
                } for delivery_region in instance.delivery_regions.all()]

            fields = [
                {
                    "name": "delivery_region",
                    "type": "select",
                    "label": _("Регион доставки"),
                    "required": False,
                    "choices": delivery_region_choices
                },
                {
                    "name": "city",
                    "type": "text",
                    "label": _("Город"),
                    "required": True,
                },

                {
                    "name": "street",
                    "type": "text",
                    "label": _("Улица"),
                    "required": True,
                },
                {
                    "name": "house_number",
                    "type": "text",
                    "label": _("Номер дома"),
                    "required": True,
                },
                {
                    "name": "corps",
                    "type": "text",
                    "label": _("Корпус"),
                    "required": False,
                },
                {
                    "name": "apartment_number",
                    "type": "text",
                    "label": _("Номер квартиры или офиса"),
                    "required": False,
                },
            ]
        elif instance.type == models.DeliveryType.PICKUP_TYPE:
            subdivision_choices = [
                {
                    'pk': subdivision.pk,
                    'name': subdivision.name,
                    'address': subdivision.address,
                    'phone_list': subdivision.phone_list,
                    'email': subdivision.email,
                    'working_hours': subdivision.working_hours,
                } for subdivision in instance.enabled_delivery_subdivisions]
            fields = [
                {
                    "name": "subdivision",
                    "type": "select",
                    "label": _("Фотоцентр"),
                    "required": True,
                    "choices": subdivision_choices
                },
            ]
        return Response(fields, status=status.HTTP_200_OK)


class PhotoFormatView(BaseMobileModelViewSet):
    serializer_class = serializers.PhotoFormatSerializer
    queryset = models.PhotoFormat.objects

    def get_queryset(self):
        return models.PhotoFormat.objects.filter(organization=self.organization.pk).filter(enabled=True)
