from django.http import HttpResponseRedirect
from django.urls import reverse
from rest_framework.decorators import action
from rest_framework.exceptions import ValidationError
from rest_framework.response import Response
from rest_framework import status
from base.restapi.permission import DenyAny, CREATE, AllowAny, RETRIEVE, DESTROY, LIST
from mobile.api.base import BaseMobileModelViewSet
from mobile.api.v1 import serializers
from order import models
from order.logic.actions import activate_order


class CreateOrderView(BaseMobileModelViewSet):
    permission_classes = DenyAny
    permission_classes_map = {CREATE: AllowAny}
    queryset = models.Order.objects.none()

    def get_serializer_kwargs(self):
        return {'organization': self.organization, 'client': self.client}

    def create(self, request, *args, **kwargs):
        self.serializer_class = serializers.OrderCreateSerializer
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        return HttpResponseRedirect(reverse('api:mobile:v1:order-detail', args=[serializer.instance.pk,]))


class OrderView(BaseMobileModelViewSet):
    permission_classes = DenyAny
    permission_classes_map = {LIST: AllowAny, RETRIEVE: AllowAny, DESTROY: AllowAny, 'complete_creation': AllowAny}
    serializer_class = serializers.OrderSerializer

    def get_queryset(self):
        return models.Order.objects.filter(organization=self.organization.pk, client=self.client.pk)

    @action(detail=True)
    def complete_creation(self, request, *args, **kwargs):
        instance = self.get_object()
        if not instance.photos.all().exists():
            raise ValidationError('Необходимо загрузить фотографии')
        activate_order(request, instance)
        return self.retrieve(request, *args, **kwargs)


class PhotoView(BaseMobileModelViewSet):
    permission_classes = DenyAny
    permission_classes_map = {CREATE: AllowAny, RETRIEVE: AllowAny, DESTROY: AllowAny}
    serializer_class = serializers.PhotoSerializer

    def get_queryset(self):
        return models.Photo.objects.filter(order__organization=self.organization.pk, order__client=self.client.pk)
