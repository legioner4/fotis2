from mobile.api.base import BaseMobileModelViewSet
from mobile.api.v1 import serializers
from organization import models


class OrganizationView(BaseMobileModelViewSet):
    serializer_class = serializers.OrganizationSerializer
    queryset = models.Organization.objects

    def get_object(self, queryset=None):
        return self.organization

    def get_queryset(self):
        return models.Organization.objects.filter(pk=self.organization.pk)

    def list(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)
