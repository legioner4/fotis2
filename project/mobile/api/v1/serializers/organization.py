from base.restapi.serializer import BaseSerializer
from organization import models


class OrganizationSerializer(BaseSerializer):

    class Meta:
        model = models.Organization
        fields = ["short_name", "full_name", "address", "logo"]
