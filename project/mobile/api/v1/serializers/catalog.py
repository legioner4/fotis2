import decimal

from rest_framework import serializers
from base.restapi.serializer import BaseSerializer, SelectRelatedSerializer, remove_fields
from catalog import models
from order.models import OrderDelivery


class DeliveryRegionSerializer(BaseSerializer):
    cost_str = serializers.CharField(label='Стоимость', read_only=True)
    cost = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = models.DeliveryRegion
        fields = [
            "name",
            "cost",
            "cost_str",
            "enabled",
            "order",
        ]

    def get_cost(self, delivery_region):
        return decimal.Decimal(delivery_region.cost)


class DeliveryTypeSerializer(BaseSerializer):
    name = serializers.CharField(read_only=True, label='Наименование', source='get_type_display')
    delivery_regions = DeliveryRegionSerializer(many=True)
    cost_str = serializers.CharField(label='Стоимость', read_only=True)
    cost = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = models.DeliveryType
        fields = [
            "name",
            "type",
            "description",
            "cost",
            "cost_str",
            "enabled",
            "order",
            'delivery_regions'
        ]

    def get_cost(self, delivery_type):
        return decimal.Decimal(delivery_type.cost)


class DiscountRelatedSerializer(SelectRelatedSerializer):

    class Meta:
        model = models.Discount
        fields = (
            'max_count',
            'min_count',
            'discount_value',
            'enabled',
            'order',
        )


class FormatPriceRelatedSerializer(SelectRelatedSerializer):
    paper_type_name = serializers.CharField(label='Тип бумаги', read_only=True, source='paper_type.name')
    discounts = DiscountRelatedSerializer(label='Скидки', many=True, source='enabled_discounts')
    price_str = serializers.CharField(read_only=True)
    price = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = models.FormatPrice
        fields = (
            'paper_type_name',
            'price',
            'price_str',
            'discounts',
            'enabled',
            'order',
        )

    def get_price(self, format_price):
        return decimal.Decimal(format_price.price)


class PhotoFormatSerializer(BaseSerializer):
    prices = FormatPriceRelatedSerializer(label='Цены', many=True, source='enabled_prices')

    class Meta:
        model = models.PhotoFormat
        fields = [
            "name",
            "width",
            "height",
            "enabled",
            "order",
            "instagram_frame_width",
            "polaroid_frame_bottom_width",
            "polaroid_frame_width",
            "prices",
        ]
