from rest_framework import serializers
from base.restapi.serializer import BaseSerializer
from organization.models import Client


class ClientSerializer(BaseSerializer):
    token = serializers.CharField(label='Токен', read_only=True, source='key')
    
    class Meta:
        model = Client
        fields = ['token',]
