from rest_framework import serializers

from base.restapi.serializer import BaseSerializer
from catalog.models import DeliveryType, DeliveryRegion
from order.models import Order


class FormatPriceCalculateSerializer(BaseSerializer):
    qty = serializers.IntegerField(required=True)
    format_price_id = serializers.IntegerField(required=True)

    class Meta:
        model = Order
        fields = [
            "qty",
            "format_price_id",
        ]

    def validate(self, data):
        errors = {}
        if not data.get('qty'):
            errors['qty'] = "Обязательное поле"

        if not data.get('format_price_id'):
            errors['format_price_id'] = "Обязательное поле"

        if errors:
            raise serializers.ValidationError(errors)
        return data


class DeliveryFormatPriceCalculateSerializer(BaseSerializer):
    formats = FormatPriceCalculateSerializer(many=True)
    delivery_type_id = serializers.IntegerField(required=True)
    delivery_region_id = serializers.IntegerField(required=False)

    class Meta:
        model = Order
        fields = [
            "formats",
            "delivery_type_id",
            "delivery_region_id"
        ]

    def validate(self, data):
        errors = {}

        if not data.get('formats'):
            errors['formats'] = "Обязательное поле"

        if not data.get('delivery_type_id'):
            errors['delivery_type_id'] = "Обязательное поле"

        if errors:
            raise serializers.ValidationError(errors)
        return data
