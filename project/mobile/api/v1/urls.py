from rest_framework import routers
from rest_framework_nested.routers import NestedSimpleRouter

app_name = 'mobile'

from . import views

router = routers.DefaultRouter()

# auth
router.register(r'access-user-token', views.AccessTokenView, 'access_token')

# organization
router.register(r'organization-info', views.OrganizationView, 'organization')

# delivery
router.register(r'delivery', views.DeliveryView, 'delivery')

# format
router.register(r'photo-format', views.PhotoFormatView, 'photo_format')

# price
router.register(r'format-price', views.FormatPriceView, 'format_price')
router.register(r'delivery-format-price', views.DeliveryFormatPriceView, 'delivery_format_price')

# order
router.register(r'create-order', views.CreateOrderView, 'create-order')
router.register(r'order', views.OrderView, 'order')
router.register(r'photo', views.PhotoView, 'photo')

urlpatterns = router.urls
