import logging

from rest_framework import exceptions
from rest_framework.authentication import BaseAuthentication, get_authorization_header

from organization import models

logger = logging.getLogger(__name__)


class TokenAuthentication(BaseAuthentication):
    keyword = 'Token'
    model = None

    def authenticate(self, request):
        auth = get_authorization_header(request).split()

        if not auth or auth[0].lower() != self.keyword.lower().encode():
            return None

        if len(auth) == 1:
            msg = 'Неверный заголовок токена. Учетные данные не предоставлены.'
            logger.error(msg)
            raise exceptions.AuthenticationFailed(msg)
        elif len(auth) > 2:
            msg = 'Неверный заголовок токена. Строка токена не должна содержать пробелов.'
            logger.error(msg)
            raise exceptions.AuthenticationFailed(msg)

        try:
            token = auth[1].decode()
        except UnicodeError:
            msg = 'Неверный заголовок токена. Строка токена не должна содержать недопустимых символов.'
            logger.error(msg)
            raise exceptions.AuthenticationFailed(msg)

        return self.authenticate_credentials(token)

    def authenticate_credentials(self, key):
        client = models.Client.objects.filter(key=key).first()
        if not client:
            logger.error('Неверный токен')
            raise exceptions.AuthenticationFailed('Неверный токен')

        if not client.organization.organization_admin:
            logger.error('Для организации не указан администратор')
            raise exceptions.AuthenticationFailed('Для организации не указан администратор')

        return (client.organization.organization_admin, client)

    def authenticate_header(self, request):
        return self.keyword


class SessionAuthentication(BaseAuthentication):

    def authenticate(self, request):
        user = getattr(request._request, 'user', None)
        if not user or not user.is_active:
            return None

        if not user.organization:
            msg = 'Учетные данные не предоставлены.'
            raise exceptions.AuthenticationFailed(msg)

        if not user.organization.organization_admin:
            msg = 'Учетные данные не предоставлены.'
            raise exceptions.AuthenticationFailed(msg)

        client = user.organization.clients.all().first()
        if not client:
            msg = 'Учетные данные не предоставлены.'
            raise exceptions.AuthenticationFailed(msg)

        return (client.organization.organization_admin, client)
