from django.apps import AppConfig


class MobileConfig(AppConfig):
    name = 'mobile'
    verbose_name = 'Мобильные устройства'
