import binascii
import os

from django.contrib.postgres.fields import ArrayField
from django.db import models
from django.db.models import ImageField
from django.utils.translation import ugettext as _

from base.models import BaseModel
from organization.logic.actions import get_or_create_sber_payment
from organization.logic.actions.email import decrypt_email_settings_password
from organization.permissions import CanManageOrganizations


class Organization(BaseModel):
    short_name = models.CharField(
        verbose_name='Краткое наименование организации', max_length=256, blank=True, null=True
    )
    full_name = models.CharField(
        verbose_name='Полное наименование организации', max_length=256, blank=True, null=True
    )
    address = models.CharField(verbose_name="Адрес", max_length=256, blank=True, null=True)
    logo = ImageField(
        verbose_name='Изображение',
        upload_to='organization/logo/',
        null=True, blank=True
    )
    key = models.CharField('Ключ', max_length=40, null=True, blank=True)

    RUB = 'rub'
    EURO = 'euro'
    DOLLAR = 'dollar'
    TENGE = 'tenge'
    GRIVNA = 'grivna'
    MDL = 'mdl'
    CURRENCY = (
        (RUB, 'руб'),
        (EURO, '€'),
        (DOLLAR, '$'),
        (TENGE, 'тг'),
        (GRIVNA, 'грн'),
        (MDL, 'mdl'),
    )
    currency = models.CharField('Валюта', max_length=32, choices=CURRENCY, default=RUB)

    NO_PAYMENT = 'no_payment'
    SBER_PAYMENT = 'sber_payment'
    PAYMENT_METHODS = (
        (NO_PAYMENT, _('Не выбрано')),
        (SBER_PAYMENT, _('СберБанк')),
    )
    payment_method = models.CharField('Платежная система', max_length=32, choices=PAYMENT_METHODS, default=NO_PAYMENT)

    lead_time = models.TextField('Срок выполнения заказов', default='', blank=True)

    # email settings
    smtp_host = models.CharField('SMTP адрес', max_length=256, null=True, blank=True)
    smtp_port = models.IntegerField('SMTP порт', null=True, blank=True)
    smtp_user = models.EmailField('SMTP логин',  max_length=256, null=True, blank=True)
    smtp_passwd = models.CharField('SMTP пароль', max_length=256, null=True, blank=True)

    #language
    languages = models.ManyToManyField(to='catalog.Language', verbose_name='Языки', blank=True)

    #app_config
    about_link = models.URLField('Ссылка на веб страницу "О компании"', null=True, blank=True)
    personal_data_policy_link = models.URLField('Ссылка на веб страницу "Согласие на обработку информации"', null=True, blank=True)
    privacy_policy_link = models.URLField('Ссылка на веб страницу "Пользовательское соглашение"', null=True, blank=True)
    refund_policy_link = models.URLField('Ссылка на веб страницу "Возврат денежных средств"', null=True, blank=True)

    ios_show_blocking_modal = models.BooleanField(default=False, verbose_name='Включить принудительное обновление приложения')
    ios_blocking_modal_text = models.CharField('Текст сообщения', max_length=512, null=True, blank=True)
    ios_blocking_modal_button_text = models.CharField('Текст кнопки', max_length=64, null=True, blank=True)
    ios_blocking_modal_target_version = models.CharField('Версия приложения (до которого обновиться приложение)', max_length=256, null=True, blank=True)

    android_show_blocking_modal = models.BooleanField(default=False, verbose_name='Включить принудительное обновление приложения')
    android_blocking_modal_text = models.CharField('Текст сообщения', max_length=512, null=True, blank=True)
    android_blocking_modal_button_text = models.CharField('Текст кнопки', max_length=64, null=True, blank=True)
    android_blocking_modal_target_version = models.CharField('Версия приложения (до которого обновиться приложение)',
                                                         max_length=256, null=True, blank=True)

    export_key = models.CharField('Ключ экспорта (xml)', max_length=16, null=True, blank=True)

    class Meta:
        ordering = ('short_name', 'full_name',)
        indexes = [
            models.Index(fields=['short_name', 'full_name', ]),
        ]
        unique_together = ('short_name',)
        verbose_name = "организация"
        verbose_name_plural = "организации"

    def __str__(self):
        if self.short_name:
            return self.short_name
        if self.full_name:
            return self.full_name
        return 'Организация %s' % self.uuid

    def save(self, *args, **kwargs):
        if not self.key:
            self.key = self.generate_key()
        return super().save(*args, **kwargs)

    def generate_key(self):
        return binascii.hexlify(os.urandom(20)).decode()

    @property
    def organization_admin(self):
        return CanManageOrganizations.users().filter(organization=self).first()

    def get_sber_payment(self):
        sber_payment = get_or_create_sber_payment(self)
        return sber_payment

    def has_payment_method_sber(self):
        return self.payment_method == self.SBER_PAYMENT

    def has_no_payment(self):
        return self.payment_method == self.NO_PAYMENT

    @property
    def has_email_config(self):
        return self.smtp_host and self.smtp_passwd and self.smtp_port and self.smtp_user

    @property
    def smtp_passwd_source(self):
        return decrypt_email_settings_password(self.smtp_passwd) if self.smtp_passwd else None


class Subdivision(BaseModel):
    organization = models.ForeignKey(Organization, models.CASCADE, related_name='subdivision', verbose_name='Организация')
    name = models.CharField(verbose_name="Название", max_length=256, blank=True, null=True)
    address = models.CharField(verbose_name="Адрес", max_length=256, blank=True, null=True)
    phone = models.CharField(verbose_name='Телефон', blank=True, null=True, max_length=64)
    phone_list = ArrayField(
        models.CharField(max_length=64),
        verbose_name='Телефоны', blank=True, null=True
    )
    email = models.EmailField(verbose_name='Адрес электронной почты', max_length=256, blank=True, null=True)
    working_hours = models.CharField(verbose_name="Время работы", max_length=256, blank=True, null=True)
    payment_types = models.ManyToManyField('organization.PaymentType', verbose_name='Типы оплаты')
    enabled = models.BooleanField(verbose_name='Включен', default=True)

    class Meta:
        verbose_name = 'структурное подразделение'
        verbose_name_plural = 'структурные подразделения'
        ordering = ('created_at',)

    def __str__(self):
        return self.address


class Client(BaseModel):
    organization = models.ForeignKey(Organization, models.CASCADE, related_name='clients', verbose_name='Организация')
    key = models.CharField('Ключ', max_length=40, primary_key=True)
    language = models.ForeignKey('catalog.Language', verbose_name='Язык', null=True, blank=True, on_delete=models.SET_NULL)

    class Meta:
        verbose_name = 'клиент'
        verbose_name_plural = 'клиенты'
        ordering = ('created_at',)

    def save(self, *args, **kwargs):
        if not self.key:
            self.key = self.generate_key()
        return super().save(*args, **kwargs)

    def generate_key(self):
        return binascii.hexlify(os.urandom(20)).decode()

    def __str__(self):
        return self.key
