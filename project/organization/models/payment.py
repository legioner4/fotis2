from django.db import models
from django.utils.translation import ugettext as _

from base.models import BaseModel
from organization.models import Organization


class PaymentType(BaseModel):
    ONLINE_TYPE = 'online'
    UPON_RECEIPT_TYPE = 'upon_receipt'
    PAYMENT_TYPES = (
        (ONLINE_TYPE, _('Онлайн платеж')),
        (UPON_RECEIPT_TYPE, _('Оплата при получении')),
    )
    UNIQUE_TYPES = [ONLINE_TYPE,]
    INPUT_NAME_TYPES = [UPON_RECEIPT_TYPE,]

    organization = models.ForeignKey(Organization, models.CASCADE, related_name='payment_types', verbose_name='Организация')
    name = models.CharField(verbose_name='Название', max_length=512, null=True, blank=True)
    type = models.CharField(verbose_name='Тип оплаты', choices=PAYMENT_TYPES, max_length=64)
    description = models.CharField(verbose_name='Описание', max_length=512, null=True, blank=True)

    class Meta:
        verbose_name = 'Тип оплаты'
        verbose_name_plural = 'Тип оплаты'
        ordering = ('created_at',)

    def __str__(self):
        return f'{self.name}'



class SberPayment(BaseModel):
    GENERAL_TAXATION_SYSTEM = 'general_taxation_system'
    SIMPLIFIED_TAXATION_SYSTEM_INCOME = 'simplified_taxation_system_income'
    SIMPLIFIED_TAXATION_SYSTEM_INCOME_MINUS_EXPENSES = 'simplified_taxation_system_income_minus_expenses'
    SINGLE_TAX_ON_IMPUTED_INCOME = 'single_tax_on_imputed_income'
    UNIFIED_AGRICULTURAL_TAX = 'unified_agricultural_tax'
    PATENT_TAXATION_SYSTEM = 'patent_taxation_system'
    TAXATIONS = (
        (GENERAL_TAXATION_SYSTEM, 'Общая система налогообложения'),
        (SIMPLIFIED_TAXATION_SYSTEM_INCOME, 'Упрощенная система налогообложения (Доход)'),
        (SIMPLIFIED_TAXATION_SYSTEM_INCOME_MINUS_EXPENSES, 'Упрощенная система налогообложения (Доход минус Расход)'),
        (SINGLE_TAX_ON_IMPUTED_INCOME, 'Единый налог на вменённый доход'),
        (UNIFIED_AGRICULTURAL_TAX, 'Единый сельскохозяйственный налог'),
        (PATENT_TAXATION_SYSTEM, 'Патентная система налогообложения'),
    )

    WITHOUT_VAT = 'without_vat'
    WITH_VAT = 'with_vat'
    VAT = (
        (WITH_VAT, 'ставка НДС'),
        (WITHOUT_VAT, 'без НДС')
    )

    organization = models.OneToOneField(Organization, models.CASCADE, related_name='sber_payment', verbose_name='Организация')
    login = models.CharField(verbose_name="Логин", max_length=256, blank=True, null=True)
    password = models.CharField(verbose_name="Пароль", max_length=256, blank=True, null=True)
    demo = models.BooleanField(verbose_name="Демо режим", default=False)
    fiscalization = models.BooleanField(verbose_name="Включить фискализацию", default=False)
    taxation = models.CharField(verbose_name='Система налогообложения', choices=TAXATIONS, max_length=64, null=True, blank=True)
    vat = models.CharField(verbose_name='Ставка НДС', choices=VAT, max_length=64, null=True, blank=True)
    app_deeplink = models.CharField(verbose_name='Диплинк', max_length=256, null=True, blank=True)

    class Meta:
        verbose_name = 'Платёжная система СберБанк'
        verbose_name_plural = 'Платёжная система СберБанк'
        ordering = ('created_at',)

    def __str__(self):
        return f'{self.organization}'
