from django.conf import settings
from django.db import models

from base.models import BaseModel
from organization.models import Organization


class FtpUser(BaseModel):
    organization = models.ForeignKey(Organization, models.CASCADE, related_name='ftp_users', verbose_name='Организация')
    user = models.ForeignKey(settings.AUTH_USER_MODEL, models.CASCADE, verbose_name='Пользователь')
    ftp_user = models.ForeignKey('django_ftpserver.FTPUserAccount', models.CASCADE, verbose_name='Пользователь FTP')

    class Meta:
        verbose_name = 'Пользователь FTP'
        verbose_name_plural = 'Пользователь FTP'
        ordering = ('created_at',)

    def __str__(self):
        return f'{self.user}'
