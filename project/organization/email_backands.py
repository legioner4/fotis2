from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.core.mail.backends.smtp import EmailBackend

from organization.logic.actions.email import decrypt_email_settings_password


class OrganizationEmailMultiAlternatives(EmailMultiAlternatives):

    def __init__(self, organization, *args, **kwargs):
        if organization and organization.has_email_config:
            host = organization.smtp_host
            port = organization.smtp_port
            username = organization.smtp_user
            password = decrypt_email_settings_password(organization.smtp_passwd)
            from_email = f'{organization} <{organization.smtp_user}>'
        else:
            host = settings.EMAIL_HOST
            port = settings.EMAIL_PORT
            username = settings.EMAIL_HOST_USER
            password = settings.EMAIL_HOST_PASSWORD
            from_email = f'{organization} <{settings.DEFAULT_FROM_EMAIL}>'

        connection = EmailBackend(host=host, port=port, username=username, password=password, use_tls=True, fail_silently=False)
        super().__init__(connection=connection, from_email=from_email, *args, **kwargs)
