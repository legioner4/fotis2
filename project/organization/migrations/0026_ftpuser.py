# Generated by Django 2.2.6 on 2021-01-05 15:25

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('django_ftpserver', '0001_initial'),
        ('organization', '0025_sberpayment_app_deeplink'),
    ]

    operations = [
        migrations.CreateModel(
            name='FtpUser',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uuid', models.UUIDField(db_index=True, default=uuid.uuid4, editable=False, unique=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='Дата изменения')),
                ('ftp_user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='django_ftpserver.FTPUserAccount', verbose_name='Пользователь FTP')),
                ('organization', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='ftp_users', to='organization.Organization', verbose_name='Организация')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='Пользователь')),
            ],
            options={
                'verbose_name': 'Пользователь FTP',
                'verbose_name_plural': 'Пользователь FTP',
                'ordering': ('created_at',),
            },
        ),
    ]
