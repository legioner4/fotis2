# Generated by Django 2.2.6 on 2020-10-29 18:27

from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('organization', '0010_auto_20201020_2146'),
    ]

    operations = [
        migrations.CreateModel(
            name='SberPayment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uuid', models.UUIDField(db_index=True, default=uuid.uuid4, editable=False, unique=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='Дата изменения')),
                ('login', models.CharField(blank=True, max_length=256, null=True, verbose_name='Логин')),
                ('password', models.CharField(blank=True, max_length=256, null=True, verbose_name='Пароль')),
                ('demo', models.BooleanField(default=False, verbose_name='Демо режим')),
                ('fiscalization', models.BooleanField(default=False, verbose_name='Включить фискализацию')),
                ('taxation', models.CharField(choices=[('general_taxation_system', 'Общая система налогообложения'), ('simplified_taxation_system_income', 'Упрощенная система налогообложения (Доход)'), ('simplified_taxation_system_income_minus_expenses', 'Упрощенная система налогообложения (Доход минус Расход)'), ('single_tax_on_imputed_income', 'Единый налог на вменённый доход'), ('unified_agricultural_tax', 'Единый сельскохозяйственный налог'), ('patent_taxation_system', 'Патентная система налогообложения')], max_length=64, verbose_name='Система налогообложения')),
                ('vat', models.CharField(choices=[('with_vat', 'ставка НДС'), ('without_vat', 'без НДС')], max_length=64, verbose_name='Ставка НДС')),
                ('organization', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='sber_payment', to='organization.Organization', verbose_name='Организация')),
            ],
            options={
                'verbose_name': 'Платёжная система СберБанк',
                'verbose_name_plural': 'Платёжная система СберБанк',
                'ordering': ('created_at',),
            },
        ),
    ]
