# Generated by Django 2.2.6 on 2020-07-28 16:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('organization', '0006_remove_organization_admin'),
    ]

    operations = [
        migrations.AddField(
            model_name='client',
            name='email',
            field=models.EmailField(blank=True, max_length=256, null=True, verbose_name='Адрес электронной почты'),
        ),
        migrations.AddField(
            model_name='client',
            name='fio',
            field=models.CharField(blank=True, max_length=256, null=True, verbose_name='ФИО'),
        ),
    ]
