# Generated by Django 2.2.6 on 2020-10-20 18:46

import django.contrib.postgres.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('organization', '0009_auto_20201020_2138'),
    ]

    operations = [
        migrations.AlterField(
            model_name='subdivision',
            name='phone_list',
            field=django.contrib.postgres.fields.ArrayField(base_field=models.CharField(max_length=64), blank=True, null=True, size=None, verbose_name='Телефоны'),
        ),
    ]
