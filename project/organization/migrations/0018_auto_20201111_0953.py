# Generated by Django 2.2.6 on 2020-11-11 06:53

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('organization', '0017_paymenttype'),
    ]

    operations = [
        migrations.AlterField(
            model_name='paymenttype',
            name='organization',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='payment_types', to='organization.Organization', verbose_name='Организация'),
        ),
        migrations.AlterField(
            model_name='paymenttype',
            name='type',
            field=models.CharField(choices=[('online', 'Онлайн платеж'), ('upon_receipt', 'Оплата при получении')], max_length=64, verbose_name='Тип оплаты'),
        ),
    ]
