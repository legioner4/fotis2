from modeltranslation.translator import translator

from base.restapi.translation.logic.options import BaseTranslationOptions
from .models import *


class OrganizationTranslationOptions(BaseTranslationOptions):
    fields = (
        'lead_time', 'address', 'full_name', 'short_name',
        'ios_blocking_modal_text', 'ios_blocking_modal_button_text',
        'android_blocking_modal_text', 'android_blocking_modal_button_text'
    )
    sorted_fields = [
        {'field': 'ios_blocking_modal_text', 'order': 7}, {'field': 'ios_blocking_modal_button_text', 'order': 8},
        {'field': 'android_blocking_modal_text', 'order': 5},{'field': 'android_blocking_modal_button_text', 'order': 6},
        {'field': 'lead_time', 'order': 4}, {'field': 'address', 'order': 3}, {'field': 'full_name', 'order': 2}, {'field': 'short_name', 'order': 1}
    ]
    required_languages = ('ru',)
    empty_values = None


translator.register(Organization, OrganizationTranslationOptions)


class SubdivisionTranslationOptions(BaseTranslationOptions):
    fields = ('working_hours', 'address', 'name')
    sorted_fields = [{'field': 'working_hours', 'order': 3}, {'field': 'address', 'order': 2}, {'field': 'name', 'order': 1}]
    required_languages = ('ru',)
    empty_values = None


translator.register(Subdivision, SubdivisionTranslationOptions)


class PaymentTypeTranslationOptions(BaseTranslationOptions):
    fields = ('description', 'name')
    sorted_fields = [{'field': 'description', 'order': 2}, {'field': 'name', 'order': 1}]
    required_languages = ('ru',)
    empty_values = None


translator.register(PaymentType, PaymentTypeTranslationOptions)
