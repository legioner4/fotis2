from django.contrib import admin

from organization.models import *

admin.site.register(Organization)
admin.site.register(Subdivision)
admin.site.register(Client)
admin.site.register(SberPayment)
admin.site.register(PaymentType)
