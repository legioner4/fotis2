from base.decorators import register_permission
from base.permissions import Permission
from system.permissions import CanManageSystem

ORGANIZATION_GROUP = 'Организация'


class RequireOrganizationMixin:
    def check_organization(self, user):
        return bool(user.organization)

    def has_permission(self, request, view):
        if super().has_permission(request, view):
            return self.check_organization(request.user)


@register_permission
class CanManageOrganizations(RequireOrganizationMixin, Permission):
    code = 'can_manage_organization'
    name = 'Управление'
    group = ORGANIZATION_GROUP
    depends_perms = [CanManageSystem]
    control_perms = [CanManageSystem]
