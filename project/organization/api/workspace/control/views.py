from rest_framework.decorators import action
from rest_framework.response import Response

from base.restapi.permission import DenyAny, UPDATE
from base.restapi.translation.views import TranslationFieldBaseModelViewSet, TranslationFieldNestedViewSet
from base.restapi.viewsets import NestedViewSet
from organization.api.workspace.control import serializers
from organization.logic.actions.ftp import remove_ftp_account
from system.permissions import CanManageSystem
from .filters import *


class PermissionMixin(object):
    permission_classes = CanManageSystem


class OrganizationView(PermissionMixin, TranslationFieldBaseModelViewSet):
    queryset = models.Organization.objects
    serializer_class = serializers.OrganizationSerializer
    ordering_fields = (
        "short_name",
        "full_name",
        "address",
    )
    filter_class = OrganizationFilter

    def get_translation_organization(self):
        try:
            return self.get_object()
        except Exception:
            return None


class OrganizationSmtpView(OrganizationView):
    serializer_class = serializers.OrganizationSmtpSerializer


class OrganizationMobileView(OrganizationView):
    serializer_class = serializers.OrganizationMobileSerializer


class OrganizationTranslationFieldNestedViewSet(TranslationFieldNestedViewSet):

    def get_translation_organization(self):
        return models.Organization.objects.get(pk=self.kwargs['organization_pk'])


class SubdivisionView(PermissionMixin, OrganizationTranslationFieldNestedViewSet):
    parent_lookup_kwargs = {'organization_id': 'organization_pk'}
    serializer_class = serializers.SubdivisionSerializer
    queryset = models.Subdivision.objects

    ordering_fields = ("address", "phone", "email")
    filter_class = SubdivisionFilter


class AccountView(PermissionMixin, NestedViewSet):
    parent_lookup_kwargs = {'organization_id': 'organization_pk'}
    serializer_class = serializers.AccountSerializer
    queryset = UserAccount.objects

    ordering_fields = (
        "first_name",
        "last_name",
        "patronymic",
        "username",
        "email",
    )
    filter_class = AccountFilter

    @action(methods=['put', 'get'], detail=True)
    def change_passwd(self, request, *args, **kwargs):
        if request.method.lower() == 'put':
            user = self.get_object()
            context = {'request': request}
            serializer = serializers.ChangePasswdSerializer(instance=user, data=request.data, context=context)
            serializer.is_valid(raise_exception=True)
            serializer.save()
            return Response(serializer.data)
        meta = self.get_meta(serializer=serializers.ChangePasswdSerializer())
        return Response({'meta': meta})


class SberPaymentView(PermissionMixin, NestedViewSet):
    parent_lookup_kwargs = {'organization_id': 'organization_pk'}
    serializer_class = serializers.SberPaymentSerializer
    queryset = models.SberPayment.objects

    ordering_fields = ("address", "phone", "email")
    filter_class = SberPaymentFilter


class PaymentTypeView(PermissionMixin, OrganizationTranslationFieldNestedViewSet):
    parent_lookup_kwargs = {'organization_id': 'organization_pk'}
    serializer_class = serializers.PaymentTypeSerializer
    queryset = models.PaymentType.objects

    ordering_fields = ("name",)
    filter_class = PaymentTypeFilter


class FtpUserView(PermissionMixin, NestedViewSet):
    permission_classes_map = {UPDATE: [DenyAny]}
    parent_lookup_kwargs = {'organization_id': 'organization_pk'}
    serializer_class = serializers.FtpUserSerializer
    queryset = models.FtpUser.objects

    ordering_fields = ("name",)

    def perform_destroy(self, instance):
        remove_ftp_account(instance.user)
        super().perform_destroy(instance)

