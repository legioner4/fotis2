import logging

from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.password_validation import validate_password
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from account.logic.getters.role import get_default_order_manager_role, get_default_organization_manager_role
from account.models import UserAccount
from base.restapi.translation.serializer import TranslationSerializer
from base.restapi.serializer import remove_fields, BaseSerializer
from catalog.logic.getters.language import get_default_language
from chunked_upload_app.api.fields import ChunkUploadField
from organization import models
from organization.logic.actions.email import encrypt_email_settings_password
from organization.logic.actions.ftp import get_or_create_ftp_group, create_ftp_account

logger = logging.getLogger(__name__)


class OrganizationSerializer(TranslationSerializer):
    logo = ChunkUploadField(label='Изображение', required=True)
    organization_admin = serializers.SerializerMethodField(label='Администратор организации', read_only=True)
    sber_payment = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = models.Organization
        fields = "__all__"
        extra_kwargs = {"languages": {"required": True}}

    def get_translation_organization(self):
        return self.instance if self.instance and not isinstance(self.instance, list) else None

    def validate_short_name_ru(self, value):
        organizations = models.Organization.objects.filter(short_name=value)
        if self.instance and self.instance.pk:
            organizations = organizations.exclude(pk=self.instance.pk)
        if organizations.exists():
            raise ValidationError("Организация с таким кратким наименованием уже существует")
        return value

    def validate_languages(self, value):
        default_language = get_default_language()
        if not value:
            raise ValidationError("Обязательное поле")
        if default_language not in value:
            raise ValidationError(f"Язык по умолчанию ({default_language}) не выбран")
        return value

    def get_organization_admin(self, organization):
        return organization.organization_admin.pk if organization.organization_admin else None

    def get_sber_payment(self, organization):
        sber_payment = organization.get_sber_payment()
        return sber_payment.pk

    def create(self, validated_data):
        from organization.logic.actions import after_create_organization

        instance = super().create(validated_data)
        after_create_organization(instance)
        return instance


class OrganizationSmtpSerializer(TranslationSerializer):
    smtp_passwd_source = serializers.CharField(label='SMTP пароль', required=False, allow_null=True)

    class Meta:
        model = models.Organization
        fields = ['smtp_host', 'smtp_port', 'smtp_user', 'smtp_passwd_source']

    def validate(self, data):
        smtp_host = data.get('smtp_host', None)
        smtp_port = data.get('smtp_port', None)
        smtp_user = data.get('smtp_user', None)
        smtp_passwd = data.get('smtp_passwd_source', None)

        if any([smtp_host, smtp_port, smtp_user, smtp_passwd]):
            if not smtp_host or not smtp_port or not smtp_user or not smtp_passwd:
                raise serializers.ValidationError("Все поля обязательны для заполнения")
        return data

    def update(self, instance, validated_data):
        smtp_passwd = validated_data.pop('smtp_passwd_source', None)
        validated_data['smtp_passwd'] = encrypt_email_settings_password(smtp_passwd) if smtp_passwd else None
        return super().update(instance, validated_data)


class OrganizationMobileSerializer(TranslationSerializer):

    class Meta:
        model = models.Organization
        fields = [
            'ios_show_blocking_modal', 'ios_blocking_modal_text', 'ios_blocking_modal_button_text',
            'ios_blocking_modal_target_version',
            'android_show_blocking_modal', 'android_blocking_modal_text', 'android_blocking_modal_button_text',
            'android_blocking_modal_target_version'
        ]

    def get_translation_organization(self):
        return self.instance if self.instance and not isinstance(self.instance, list) else None

    def validate(self, data):
        ios_show_blocking_modal = data.get('ios_show_blocking_modal', None)
        if ios_show_blocking_modal:
            ios_blocking_modal_target_version = data.get('ios_blocking_modal_target_version', None)
            ios_blocking_modal_text_ru = data.get('ios_blocking_modal_text_ru', None)
            ios_blocking_modal_button_text_ru = data.get('ios_blocking_modal_button_text_ru', None)
            if not ios_blocking_modal_target_version or not ios_blocking_modal_text_ru or not ios_blocking_modal_button_text_ru:
                raise serializers.ValidationError(
                    {'ios_show_blocking_modal': "Необходимо заполнить все поля."}
                )

        android_show_blocking_modal = data.get('android_show_blocking_modal', None)
        if android_show_blocking_modal:
            android_blocking_modal_target_version = data.get('android_blocking_modal_target_version', None)
            android_blocking_modal_text_ru = data.get('android_blocking_modal_text_ru', None)
            android_blocking_modal_button_text_ru = data.get('android_blocking_modal_button_text_ru', None)
            if not android_blocking_modal_target_version or not android_blocking_modal_text_ru or not android_blocking_modal_button_text_ru:
                raise serializers.ValidationError(
                    {'android_show_blocking_modal': "Необходимо заполнить все поля."}
                )

        return data


class OrganizationNestedTranslationSerializer(TranslationSerializer):

    def get_translation_organization(self):
        return models.Organization.objects.get(pk=self.organization_id)


class SubdivisionSerializer(OrganizationNestedTranslationSerializer):
    class Meta:
        model = models.Subdivision
        fields = '__all__'

    def __init__(self, *args, organization_id=None, **kwargs):
        self.organization_id = organization_id
        super().__init__(*args, **kwargs)


class AccountSerializer(BaseSerializer):
    password = serializers.CharField(label="Пароль", max_length=20, required=True, write_only=True)
    password_confirm = serializers.CharField(label="Пароль", max_length=20, required=True, write_only=True)
    organization_permission = serializers.BooleanField(label="Администратор организации")
    order_permission = serializers.BooleanField(label="Администратор заказов")

    class Meta:
        model = UserAccount
        fields = "__all__"

    def __init__(self, *args, organization_id=None, **kwargs):
        self.organization_id = organization_id
        super().__init__(*args, **kwargs)

    def __list_init__(self, *args, **kwargs):
        super().__list_init__(*args, **kwargs)
        if 'groups' in self.fields:
            remove_fields(self, 'groups')

    def __object_init__(self, *args, **kwargs):
        super().__object_init__(*args, **kwargs)
        if 'groups' in self.fields:
            remove_fields(self, 'groups')

    def validate(self, data):
        if data.get('password') != data.get('password_confirm'):
            raise serializers.ValidationError(
                {'password': "Пароли не совпадают.", 'password_confirm': "Пароли не совпадают."}
            )
        return data

    def validate_password(self, value):
        validate_password(value)
        return value

    def validate_email(self, value):
        users = UserAccount.objects.filter(email__iexact=value.lower())
        if self.instance and self.instance.pk:
            users = users.exclude(pk=self.instance.pk)
        if users.exists():
            raise serializers.ValidationError("Пользователь с таким адресом электронной почты уже существует")
        return value.lower()

    def create(self, validated_data):
        order_permission = validated_data.pop('order_permission', False)
        organization_permission = validated_data.pop('organization_permission', False)
        password = validated_data.pop('password')
        validated_data.pop('password_confirm')
        instance = super().create(validated_data)
        instance.set_password(password)
        instance.email_is_confirmed = True
        instance.save()

        if organization_permission:
            role = get_default_organization_manager_role()
            instance.groups.add(role)
            instance.save()
        else:
            if order_permission:
                role = get_default_order_manager_role()
                instance.groups.add(role)
                instance.save()
        return instance

    def update(self, instance, validated_data):
        order_permission = validated_data.pop('order_permission', False)
        organization_permission = validated_data.pop('organization_permission', False)
        instance = super().update(instance, validated_data)
        order_role = get_default_order_manager_role()
        organization_role = get_default_organization_manager_role()

        if organization_permission:
            instance.groups.remove(order_role)
            instance.groups.add(organization_role)
        else:
            if order_permission:
                instance.groups.add(order_role)
                instance.groups.remove(organization_role)
            else:
                instance.groups.remove(order_role)
                instance.groups.remove(organization_role)
        instance.save()
        return instance


class SberPaymentSerializer(BaseSerializer):
    class Meta:
        model = models.SberPayment
        fields = '__all__'

    def __init__(self, *args, organization_id=None, **kwargs):
        self.organization_id = organization_id
        super().__init__(*args, **kwargs)


class ChangePasswdSerializer(serializers.ModelSerializer):
    new_password = serializers.CharField(label="Новый пароль", max_length=20, write_only=True, required=True)
    new_password_confirm = serializers.CharField(
        label="Подтверждение нового пароля", max_length=20, write_only=True, required=True
    )

    class Meta:
        model = UserAccount
        fields = ['new_password', 'new_password_confirm']

    def validate_new_password(self, value):
        validate_password(value)
        return value

    def validate(self, data):
        if data['new_password'] != data['new_password_confirm']:
            raise serializers.ValidationError(
                {'password': "Пароли не совпадают.", 'password_confirm': "Пароли не совпадают."}
            )
        return data

    def update(self, instance, validated_data):
        password = validated_data.get('new_password')
        instance.set_password(password)
        instance.save()

        request = self._context['request']
        if request.user == instance:
            update_session_auth_hash(request, instance)
        return instance


class PaymentTypeSerializer(OrganizationNestedTranslationSerializer):
    class Meta:
        model = models.PaymentType
        fields = '__all__'

    def __init__(self, *args, organization_id=None, **kwargs):
        self.organization_id = organization_id
        self.organization = models.Organization.objects.get(pk=self.organization_id)
        super().__init__(*args, **kwargs)

    def validate_type(self, value):
        payment_types = self.organization.payment_types.filter(type=value)
        if self.instance and self.instance.pk:
            payment_types = payment_types.exclude(pk=self.instance.pk)

        if value in models.PaymentType.UNIQUE_TYPES and payment_types.exists():
            raise serializers.ValidationError("Тип данных с данным типом уже существует")

        return value


class FtpUserSerializer(TranslationSerializer):
    class Meta:
        model = models.FtpUser
        fields = '__all__'

    def __init__(self, *args, organization_id=None, **kwargs):
        self.organization_id = organization_id
        self.organization = models.Organization.objects.get(pk=self.organization_id)
        super().__init__(*args, **kwargs)

        if 'user' in self.fields:
            self.fields['user'].queryset = self.organization.users.all()

    def validate(self, attrs):
        user = attrs.get('user')

        if self.organization.ftp_users.filter(user=user).exists():
            raise serializers.ValidationError({'user': 'данный пользователь уже добавлен'})
        return attrs

    def create(self, validated_data):
        user = validated_data.get('user')
        ftp_group = get_or_create_ftp_group()
        ftp_user = create_ftp_account(self.organization, user, ftp_group)
        validated_data['ftp_user'] = ftp_user
        return super().create(validated_data)
