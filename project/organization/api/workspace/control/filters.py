import django_filters

from account.models import UserAccount
from organization import models


class OrganizationFilter(django_filters.FilterSet):
    short_name = django_filters.CharFilter(method='filter_short_name')
    full_name = django_filters.CharFilter(method='filter_full_name')
    address = django_filters.CharFilter(method='filter_address')

    class Meta:
        model = models.Organization
        fields = ('short_name', 'full_name', 'address')

    def filter_short_name(self, queryset, name, value):
        if value:
            return queryset.filter(short_name__icontains=value)
        return queryset

    def filter_full_name(self, queryset, name, value):
        if value:
            return queryset.filter(full_name__icontains=value)
        return queryset

    def filter_address(self, queryset, name, value):
        if value:
            return queryset.filter(address__icontains=value)
        return queryset


class SubdivisionFilter(django_filters.FilterSet):
    address = django_filters.CharFilter(method='filter_address')
    phone = django_filters.CharFilter(method='filter_phone')
    email = django_filters.CharFilter(method='filter_email')

    class Meta:
        model = models.Subdivision
        fields = ('address', 'phone', 'email')

    def filter_address(self, queryset, name, value):
        if value:
            return queryset.filter(address__icontains=value)
        return queryset

    def filter_phone(self, queryset, name, value):
        if value:
            return queryset.filter(phone__icontains=value)
        return queryset

    def filter_email(self, queryset, name, value):
        if value:
            return queryset.filter(email__icontains=value)
        return queryset


class AccountFilter(django_filters.FilterSet):
    first_name = django_filters.CharFilter(method='filter_first_name')
    last_name = django_filters.CharFilter(method='filter_last_name')
    patronymic = django_filters.CharFilter(method='filter_patronymic')
    email = django_filters.CharFilter(method='filter_email')

    class Meta:
        model = UserAccount
        fields = ('first_name', 'last_name', 'patronymic', 'email')

    def filter_first_name(self, queryset, name, value):
        if value:
            return queryset.filter(first_name__icontains=value)
        return queryset

    def filter_last_name(self, queryset, name, value):
        if value:
            return queryset.filter(last_name__icontains=value)
        return queryset

    def filter_patronymic(self, queryset, name, value):
        if value:
            return queryset.filter(patronymic__icontains=value)
        return queryset

    def filter_email(self, queryset, name, value):
        if value:
            return queryset.filter(email__icontains=value)
        return queryset


class SberPaymentFilter(django_filters.FilterSet):
    login = django_filters.CharFilter(method='filter_login')

    class Meta:
        model = models.SberPayment
        fields = ['login',]

    def filter_login(self, queryset, name, value):
        if value:
            return queryset.filter(login__icontains=value)
        return queryset


class PaymentTypeFilter(django_filters.FilterSet):
    name = django_filters.CharFilter(method='filter_name')

    class Meta:
        model = models.PaymentType
        fields = ['name',]

    def filter_name(self, queryset, name, value):
        if value:
            return queryset.filter(name__icontains=value)
        return queryset
