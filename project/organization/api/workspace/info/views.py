import django_filters

from base.restapi.permission import CREATE, DenyAny, UPDATE, WRITE
from base.restapi.permission import DESTROY, LIST
from base.restapi.translation.views import TranslationFieldBaseModelViewSet, TranslationFieldNestedViewSet
from base.restapi.viewsets import NestedViewSet
from organization import models as organization_models
from organization import permissions
from organization.api.workspace.info import serializers
from organization.logic.actions.ftp import remove_ftp_account


class PermissionMixin():
    permission_classes = permissions.CanManageOrganizations


class OrganizationFilter(django_filters.FilterSet):
    short_name = django_filters.CharFilter(method='filter_short_name')
    full_name = django_filters.CharFilter(method='filter_full_name')
    address = django_filters.CharFilter(method='filter_address')

    class Meta:
        model = organization_models.Organization
        fields = ('short_name', 'full_name', 'address')

    def filter_address(self, queryset, name, value):
        if value:
            return queryset.filter(address__icontains=value)
        return queryset

    def filter_short_name(self, queryset, name, value):
        if value:
            return queryset.filter(short_name__icontains=value)
        return queryset

    def filter_full_name(self, queryset, name, value):
        if value:
            return queryset.filter(full_name__icontains=value)
        return queryset


class OrganizationInfoView(TranslationFieldBaseModelViewSet):
    permission_classes_map = {CREATE: [DenyAny], DESTROY: [DenyAny], LIST: [DenyAny], UPDATE: [permissions.CanManageOrganizations]}
    queryset = organization_models.Organization.objects
    serializer_class = serializers.OrganizationSerializer
    ordering_fields = (
        "short_name",
        "full_name",
        "address",
    )
    filter_class = OrganizationFilter

    def get_queryset(self):
        return super().get_queryset().filter(pk=self.request.user.organization.pk)


class OrganizationSmtpView(OrganizationInfoView):
    serializer_class = serializers.OrganizationSmtpSerializer


class OrganizationMobileView(OrganizationInfoView):
    permission_classes = permissions.CanManageSystem
    serializer_class = serializers.OrganizationMobileSerializer


class SubdivisionFilter(django_filters.FilterSet):
    address = django_filters.CharFilter(method='filter_address')
    phone = django_filters.CharFilter(method='filter_phone')
    email = django_filters.CharFilter(method='filter_email')

    class Meta:
        model = organization_models.Subdivision
        fields = ['email', 'phone', 'address']

    def filter_address(self, queryset, name, value):
        if value:
            return queryset.filter(address__icontains=value)
        return queryset

    def filter_phone(self, queryset, name, value):
        if value:
            return queryset.filter(phone__icontains=value)
        return queryset

    def filter_email(self, queryset, name, value):
        if value:
            return queryset.filter(email__icontains=value)
        return queryset


class SubdivisionView(PermissionMixin, TranslationFieldNestedViewSet):
    permission_classes_map = {WRITE: permissions.CanManageOrganizations}
    parent_lookup_kwargs = {'organization_id': 'organization_pk'}
    serializer_class = serializers.SubdivisionSerializer
    queryset = organization_models.Subdivision.objects

    ordering_fields = ("address", "phone", "email")
    filter_class = SubdivisionFilter


class SberPaymentFilter(django_filters.FilterSet):
    login = django_filters.CharFilter(method='filter_login')

    class Meta:
        model = organization_models.SberPayment
        fields = ['login',]

    def filter_login(self, queryset, name, value):
        if value:
            return queryset.filter(login__icontains=value)
        return queryset


class SberPaymentView(PermissionMixin, NestedViewSet):
    permission_classes_map = {WRITE: permissions.CanManageOrganizations}
    parent_lookup_kwargs = {'organization_id': 'organization_pk'}
    serializer_class = serializers.SberPaymentSerializer
    queryset = organization_models.SberPayment.objects

    ordering_fields = ("address", "phone", "email")
    filter_class = SberPaymentFilter



class PaymentTypeFilter(django_filters.FilterSet):
    name = django_filters.CharFilter(method='filter_name')

    class Meta:
        model = organization_models.PaymentType
        fields = ['name',]

    def filter_name(self, queryset, name, value):
        if value:
            return queryset.filter(name__icontains=value)
        return queryset


class PaymentTypeView(PermissionMixin, TranslationFieldNestedViewSet):
    permission_classes_map = {WRITE: permissions.CanManageOrganizations}
    parent_lookup_kwargs = {'organization_id': 'organization_pk'}
    serializer_class = serializers.PaymentTypeSerializer
    queryset = organization_models.PaymentType.objects

    ordering_fields = ("name",)
    filter_class = PaymentTypeFilter


class FtpUserView(PermissionMixin, NestedViewSet):
    permission_classes_map = {UPDATE: [DenyAny]}
    parent_lookup_kwargs = {'organization_id': 'organization_pk'}
    serializer_class = serializers.FtpUserSerializer
    queryset = organization_models.FtpUser.objects

    ordering_fields = ("name",)

    def perform_destroy(self, instance):
        remove_ftp_account(instance.user)
        super().perform_destroy(instance)
