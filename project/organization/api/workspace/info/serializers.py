import logging

from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from base.restapi.serializer import BaseSerializer
from base.restapi.translation.serializer import TranslationSerializer
from catalog.logic.getters.language import get_default_language
from chunked_upload_app.api.fields import ChunkUploadField
from organization import models
from organization.logic.actions.email import encrypt_email_settings_password
from organization.logic.actions.ftp import get_or_create_ftp_group, create_ftp_account
from organization.logic.getters import get_organization_payment_types

logger = logging.getLogger(__name__)


class OrganizationSerializer(TranslationSerializer):
    logo = ChunkUploadField(label='Изображение', required=True)
    sber_payment = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = models.Organization
        fields = "__all__"
        extra_kwargs = {"languages": {"required": True}}

    def get_sber_payment(self, organization):
        sber_payment = organization.get_sber_payment()
        return sber_payment.pk

    def validate_languages(self, value):
        default_language = get_default_language()
        if not value:
            raise ValidationError("Обязательное поле")
        if default_language not in value:
            raise ValidationError(f"Язык по умолчанию ({default_language}) не выбран")
        return value

    def create(self, validated_data):
        from organization.logic.actions import after_create_organization

        instance = super().create(validated_data)
        after_create_organization(instance)
        return instance


class OrganizationSmtpSerializer(TranslationSerializer):
    smtp_passwd_source = serializers.CharField(label='SMTP пароль', required=False, allow_null=True)

    class Meta:
        model = models.Organization
        fields = ['smtp_host', 'smtp_port', 'smtp_user', 'smtp_passwd_source']

    def validate(self, data):
        smtp_host = data.get('smtp_host', None)
        smtp_port = data.get('smtp_port', None)
        smtp_user = data.get('smtp_user', None)
        smtp_passwd = data.get('smtp_passwd_source', None)
        print([smtp_host, smtp_port, smtp_user, smtp_passwd])
        if any([smtp_host, smtp_port, smtp_user, smtp_passwd]):
            if not smtp_host or not smtp_port or not smtp_user or not smtp_passwd:
                raise serializers.ValidationError("Все поля обязательны для заполнения")
        return data

    def update(self, instance, validated_data):
        smtp_passwd = validated_data.pop('smtp_passwd_source', None)
        validated_data['smtp_passwd'] = encrypt_email_settings_password(smtp_passwd) if smtp_passwd else None
        return super().update(instance, validated_data)


class OrganizationMobileSerializer(TranslationSerializer):

    class Meta:
        model = models.Organization
        fields = [
            'ios_show_blocking_modal', 'ios_blocking_modal_text', 'ios_blocking_modal_button_text',
            'ios_blocking_modal_target_version',
            'android_show_blocking_modal', 'android_blocking_modal_text', 'android_blocking_modal_button_text',
            'android_blocking_modal_target_version'
        ]


class SubdivisionSerializer(TranslationSerializer):
    class Meta:
        model = models.Subdivision
        fields = '__all__'

    def __init__(self, *args, organization_id=None, **kwargs):
        self.organization_id = organization_id
        super().__init__(*args, **kwargs)
        organization = models.Organization.objects.get(pk=self.organization_id)
        if 'payment_types' in self.fields:
            self.fields['payment_types'].child_relation.queryset = get_organization_payment_types(organization)


class SberPaymentSerializer(BaseSerializer):
    class Meta:
        model = models.SberPayment
        fields = '__all__'

    def __init__(self, *args, organization_id=None, **kwargs):
        self.organization_id = organization_id
        super().__init__(*args, **kwargs)


class PaymentTypeSerializer(TranslationSerializer):
    class Meta:
        model = models.PaymentType
        fields = '__all__'

    def __init__(self, *args, organization_id=None, **kwargs):
        self.organization_id = organization_id
        self.organization = models.Organization.objects.get(pk=self.organization_id)
        super().__init__(*args, **kwargs)

    def validate_type(self, value):
        payment_types = self.organization.payment_types.filter(type=value)
        if self.instance and self.instance.pk:
            payment_types = payment_types.exclude(pk=self.instance.pk)

        if value in models.PaymentType.UNIQUE_TYPES and payment_types.exists():
            raise serializers.ValidationError("Тип данных с данным типом уже существует")

        return value


class FtpUserSerializer(BaseSerializer):
    class Meta:
        model = models.FtpUser
        fields = '__all__'

    def __init__(self, *args, organization_id=None, **kwargs):
        self.organization_id = organization_id
        self.organization = models.Organization.objects.get(pk=self.organization_id)
        super().__init__(*args, **kwargs)

        if 'user' in self.fields:
            self.fields['user'].queryset = self.organization.users.all()

    def validate(self, attrs):
        user = attrs.get('user')

        if self.organization.ftp_users.filter(user=user).exists():
            raise serializers.ValidationError({'user': 'данный пользователь уже добавлен'})
        return attrs

    def create(self, validated_data):
        user = validated_data.get('user')
        ftp_group = get_or_create_ftp_group()
        ftp_user = create_ftp_account(self.organization, user, ftp_group)
        validated_data['ftp_user'] = ftp_user
        return super().create(validated_data)
