from rest_framework import routers
from rest_framework_nested.routers import NestedSimpleRouter

from . import views

app_name = 'organization_info'

router = routers.DefaultRouter()
router.register(r'organizations', views.OrganizationInfoView, 'organization')
router.register(r'organizations/mobile', views.OrganizationMobileView, 'organization_mobile')
router.register(r'organizations/smtp', views.OrganizationSmtpView, 'organization_smtp')
organization_router = NestedSimpleRouter(router, r'organizations', lookup='organization')
organization_router.register(r'subdivisions', views.SubdivisionView, 'subdivision')
organization_router.register(r'sber-payment', views.SberPaymentView, 'sber_payment')
organization_router.register(r'payment-type', views.PaymentTypeView, 'payment_type')
organization_router.register(r'ftp-users', views.FtpUserView, 'ftp_user')


urlpatterns = []
urlpatterns += router.urls
urlpatterns += organization_router.urls
