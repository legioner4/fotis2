from django.conf.urls import include
from django.urls import path

app_name = 'organization'

urlpatterns = [
    path('', include('organization.api.workspace.info.urls', namespace='info')),
    path('control/', include('organization.api.workspace.control.urls', namespace='control')),
]
