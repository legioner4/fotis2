

def get_or_create_sber_payment(organization):
    from organization.models import SberPayment

    sber_payment, _ = SberPayment.objects.get_or_create(organization=organization)
    return sber_payment


def after_create_organization(organization):
    from catalog.logic.getters.language import get_default_language

    if not organization.languages.exists():
        default_language = get_default_language()
        organization.languages.add(default_language)
        organization.save()
