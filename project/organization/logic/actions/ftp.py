import os
from django.conf import settings


DEFAULT_FTP_GROUP = 'default'


def organization_ftp_home_dir(organization):
    dir_path = os.path.abspath(os.path.join(settings.MEDIA_ROOT, f'organization/{organization.uuid}'))
    if not os.path.exists(dir_path):
        os.makedirs(dir_path)
    return dir_path


def organization_ftp_dir(organization):
    return f'organization/{organization.uuid}'


def get_or_create_ftp_group(name=DEFAULT_FTP_GROUP):
    from django_ftpserver.models import FTPUserGroup

    group, created = FTPUserGroup.objects.get_or_create(name=name)
    if created:
        group.permission = 'elr'
        group.save()
    return group


def create_ftp_account(organization, user, ftp_group):
    from django_ftpserver.models import FTPUserAccount

    home_dir = organization_ftp_home_dir(organization)
    ftp_account = FTPUserAccount.objects.create(user=user, group=ftp_group, home_dir=home_dir)
    return ftp_account


def remove_ftp_account(user):
    from django_ftpserver.models import FTPUserAccount

    FTPUserAccount.objects.filter(user=user).delete()
