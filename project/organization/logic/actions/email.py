from cryptography.fernet import Fernet
from django.conf import settings


def encrypt_email_settings_password(raw_password):
    """шифруем пароль"""
    cipher_suite = Fernet(settings.EMAIL_PASSWD_KEY)
    raw_password = bytes(raw_password, encoding='utf8')
    password = cipher_suite.encrypt(raw_password)
    return password.decode("utf-8")


def decrypt_email_settings_password(encoded_password):
    """дешифруем пароль"""
    cipher_suite = Fernet(settings.EMAIL_PASSWD_KEY)
    encoded_password = bytes(encoded_password, encoding='utf8')
    password = cipher_suite.decrypt(encoded_password)
    return password.decode("utf-8")
