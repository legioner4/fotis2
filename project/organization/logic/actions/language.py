from catalog.models import Language
from organization.models import Client


def set_client_language(client: Client, language_pk: int) -> Language:
    language = Language.objects.filter(pk=language_pk).first()
    if language:
        client.language = language
        client.save()
    return language
