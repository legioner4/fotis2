from organization.models import PaymentType


def value_convert_organization_currency(organization, value):
    return f'{value} {organization.get_currency_display()}'


def get_organization_payment_types(organization):
    payment_types = organization.payment_types.all()
    if organization.has_no_payment():
        payment_types = payment_types.exclude(type=PaymentType.ONLINE_TYPE)
    return payment_types

