@echo off

pushd %~dp0

if not exist venv (
  ECHO "INIT..."
  CALL python -m venv venv || GOTO :exit
)
CALL venv\Scripts\activate || GOTO :exit

CALL python -m pip install --upgrade pip==20.0.2 pip-tools==4.5.1 || GOTO :exit

CALL python -m pip install --upgrade -r requirements\main.txt || GOTO :exit

pushd frontend
CALL yarn install --no-progress --non-interactive
popd

CALL python project\manage.py migrate --noinput || GOTO :exit

:exit
popd
if "%errorlevel%" NEQ "0" exit /b %errorlevel%
